//
//  SignUpVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 9/30/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import Toaster
import PinterestSDK

class SignUpVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,GIDSignInUIDelegate,GIDSignInDelegate {
    
    var countryPicker = UIPickerView()
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var logoTopConstraints: NSLayoutConstraint!
    //SOCIAL
    
    @IBOutlet var viewSocialBG : UIView!
    @IBOutlet var socialBGWidth : NSLayoutConstraint!
    
    @IBOutlet var viewFbWidth : NSLayoutConstraint!
    @IBOutlet var viewInstaWidth : NSLayoutConstraint!
    @IBOutlet var viewGoogleWidth : NSLayoutConstraint!
    @IBOutlet var viewlinkedInWidth : NSLayoutConstraint!
    @IBOutlet var viewPintrestWidth : NSLayoutConstraint!
    
    //FACEBOOK
    @IBOutlet var viewFbLogin : UIView!
    @IBOutlet var buttonFbLogin : UIButton!
    @IBOutlet weak var imgFacebook : UIImageView!
    
    //GMAIL
    @IBOutlet var viewGmailLogin : UIView!
    @IBOutlet var buttonGmailLogin : UIButton!
    @IBOutlet weak var imgGmail : UIImageView!
    
    //INSTAGRAM
    @IBOutlet var viewInstaLogin : UIView!
    @IBOutlet var buttonInstaLogin : UIButton!
    @IBOutlet weak var imgInstagram : UIImageView!
    
    //LINKEDIN
    @IBOutlet var viewLinkedInLogin : UIView!
    @IBOutlet var buttonLinkedInLogin : UIButton!
    @IBOutlet weak var imgLinkedIn : UIImageView!
    
    @IBOutlet weak var imageLinkedInWidth: NSLayoutConstraint!
    
    //PINTREST
    @IBOutlet var viewPintrestLogin : UIView!
    @IBOutlet var buttonPintrestLogin : UIButton!
    @IBOutlet weak var imgPintrest : UIImageView!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet var scrlView : UIScrollView!
    
    // Social
    var dictfacebook:[String : AnyObject]  = [:]
    var dictGoogle:[String : AnyObject]  = [:]
    var dictInstagram:[String : AnyObject] = [:]
    var dictLinkedIn:[String : AnyObject] = [:]
    var dictPintrest : [String : AnyObject] = [:]
    var dictResponse:[String:AnyObject] = [:]
    
    var socialType: String = ""
    var socialId: String = ""
    var socialName: String = ""
    var socialEmail : String = ""
    
    var pintrestDetail : [String : AnyObject] = [:]
    
    // Validation
    var strFname:String = ""
    var strLname:String = ""
    var strEmail:String = ""
    var strCountryCode:String = ""
    var strMobile:String = ""
    var strPassword:String = ""
    var strConfirmPassword:String = ""
    var arrCountryCode:[AnyObject] = []
    
    var socialParameter : Parameters = [:]
    var strMobileNo : String = ""
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DisplayDetailOfSocialData()
        callCountryCodeService()
        removeInstagramCookies()
        
        txtMobile.keyboardType = .numberPad
        addToolBar()
        
        // Set Left Padding on textfields
        txtFirstName.setLeftPaddingPoints(10.0)
        txtLastName.setLeftPaddingPoints(10.0)
        txtEmail.setLeftPaddingPoints(10.0)
        txtCountryCode.setLeftPaddingPoints(10.0)
        txtMobile.setLeftPaddingPoints(10.0)
        txtPassword.setLeftPaddingPoints(10.0)
        txtConfirmPassword.setLeftPaddingPoints(10.0)
        
        if IS_IPHONE_5_OR_5S() {
            logoTopConstraints.constant = 10.0
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if IS_IPAD_DEVICE() {
            let viewSocialWidth : CGFloat = 75.0
            viewFbWidth.constant = viewSocialWidth
            viewInstaWidth.constant = viewSocialWidth
            viewGoogleWidth.constant = viewSocialWidth
            viewPintrestWidth.constant = viewSocialWidth
            viewlinkedInWidth.constant = viewSocialWidth

            print(APPDELEGATE.arySocialLoginButton)

            socialBGWidth.constant = 75.0 * CGFloat(APPDELEGATE.arySocialLoginButton.count)
            //print(socialBGWidth.constant)
            viewSocialBG.layoutIfNeeded()
        }else {
            print(APPDELEGATE.arySocialLoginButton)
            socialBGWidth.constant = 55.0 * CGFloat(APPDELEGATE.arySocialLoginButton.count)
            //print(socialBGWidth.constant)
            viewSocialBG.layoutIfNeeded()
        }

        if APPDELEGATE.arySocialLoginButton.contains("FB") {
            viewFbLogin.isHidden = false
        }else {
            viewFbWidth.constant = 0.0
            viewFbLogin.isHidden = true
        }

        if APPDELEGATE.arySocialLoginButton.contains("Instagram") {
            viewInstaLogin.isHidden = false
        }else {
            viewInstaWidth.constant = 0.0
            viewInstaLogin.isHidden = true
        }

        if APPDELEGATE.arySocialLoginButton.contains("Google") {
            viewGmailLogin.isHidden = false
        }else {
            viewGoogleWidth.constant = 0.0
            viewGmailLogin.isHidden = true
        }

        if APPDELEGATE.arySocialLoginButton.contains("Pinterest") {
            viewPintrestLogin.isHidden = false
        }else {
            viewPintrestWidth.constant = 0.0
            viewPintrestLogin.isHidden = true
        }

        if APPDELEGATE.arySocialLoginButton.contains("Linkedin") {
            viewLinkedInLogin.isHidden = false
        }else {
            viewlinkedInWidth.constant = 0.0
            viewLinkedInLogin.isHidden = true
        }
        
        // Set dynamic border Color & Radius
        txtFirstName.setTextFieldDynamicColor()
        txtLastName.setTextFieldDynamicColor()
        txtEmail.setTextFieldDynamicColor()
        txtMobile.setTextFieldDynamicColor()
        txtPassword.setTextFieldDynamicColor()
        txtConfirmPassword.setTextFieldDynamicColor()
        txtCountryCode.setTextFieldDynamicColor()
        
        txtFirstName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        txtLastName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        txtMobile.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        txtConfirmPassword.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        txtCountryCode.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        
        // Set Shadow and corner radius
        btnSignUp.Shadow()
        
        // Set button text color
        btnSignUp.setButtonDynamicColor()
        
        // Set dynamic Button text Color
        btnLogin.setButtonTextThemeColor()
        
        // Main Logo Image set
        var LogoImage:String = APPDELEGATE.logoImage
        LogoImage = LogoImage.replacingOccurrences(of: " ", with: "%20")
        let url1 = URL(string: LogoImage)
        imgLogo.kf.setImage(with: url1)
        
        // Gmail Logo Image set
        var GmailImage:String = APPDELEGATE.googleImage
        GmailImage = GmailImage.replacingOccurrences(of: " ", with: "%20")
        let url2 = URL(string: GmailImage)
        imgGmail.kf.setImage(with: url2)
        
        // facebook Logo Image set
        var FacebookImage:String = APPDELEGATE.facebookImage
        FacebookImage = FacebookImage.replacingOccurrences(of: " ", with: "%20")
        let url3 = URL(string: FacebookImage)
        imgFacebook.kf.setImage(with: url3)
        
        // Insta Logo Image set
        var InstaImage:String = APPDELEGATE.instagramImage
        InstaImage = InstaImage.replacingOccurrences(of: " ", with: "%20")
        let url4 = URL(string: InstaImage)
        imgInstagram.kf.setImage(with: url4)
        
        // LinkedIn Logo Image set
        var linkedInImage:String = APPDELEGATE.linkedInImage
         linkedInImage = linkedInImage.replacingOccurrences(of: " ", with: "%20")
         let url5 = URL(string: linkedInImage)
         imgLinkedIn.kf.setImage(with: url5)
         
         // Pintrest Logo Image set
         var pintrestImage:String = APPDELEGATE.pintrestImage
         pintrestImage = pintrestImage.replacingOccurrences(of: " ", with: "%20")
         let url6 = URL(string: pintrestImage)
         imgPintrest.kf.setImage(with: url6)
    }
    
    // Paste Disable
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    // MARK: - Textfireld Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.scrlView.contentOffset = CGPoint(x: 0.0, y: 0.0)
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true
        }
        
        if txtFirstName == textField {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }else {
                return true
            }
        }
        
        
        if txtLastName == textField {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }else {
                return true
            }
        }
        
        if txtEmail == textField {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }else {
                return true
            }
        }
        
        if textField == txtCountryCode {
            return false
        }
        
        if textField == txtMobile {
            if (textField.text?.characters.count)! >= 15 {
                return false
            }
            if !(string.isNumber()) {
                return false
            }
        }
        
        if textField == txtPassword {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
        }
        
        if textField == txtConfirmPassword {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
        }
        return true
    }
    
    
    // MARK: - PickerView Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountryCode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict : [String:AnyObject] = arrCountryCode[row] as! [String:AnyObject]
        
        let strCodeData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "code")
        return strCodeData
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dict : [String:AnyObject] = arrCountryCode[row] as! [String:AnyObject]
        let strCodeData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "code")
        txtCountryCode.text = "+" + strCodeData
    }
    
    // MARK: - ActionMethod
    @IBAction func btnSignUp(_ sender: Any) {
        strMobileNo = "\(txtCountryCode.text!) \(txtMobile.text!)"
        print(strMobileNo)
        
        if checkValidation() {
            callRegisterService()
        }
    }
    
    @IBAction func btnLogIn(_ sender: Any) {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            myDelegate.openLoginPage()
        }
    }
    
    // MARK: - Service Method
    
    func callCountryCodeService() {
        
        let strurl: String = BazarBit.countryCodeService()
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        let parameter : Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true) { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dict:[String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "status")
            
            if status == "ok" {
                
                if (dict["payload"] as? [AnyObject]) != nil  {
                    self.arrCountryCode = dict["payload"] as! [AnyObject]
                    self.txtCountryCode.inputView = self.countryPicker
                    self.countryPicker.dataSource = self
                    self.countryPicker.delegate = self
                    self.countryPicker.reloadAllComponents()
                }
                
            } else {
                let message: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "message")
                PIAlertUtils.displayAlertWithMessage(message)
            }
        }
    }
    
    func callRegisterService() {
        
        let strurl:String = BazarBit.SignUpService()
        let url:URL = URL(string: strurl)!
        
        var headers : HTTPHeaders = [:]
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["Content-Type"] = BazarBit.applicationJson
        
        var parameter : Parameters = [:]
        parameter["firstname"] = txtFirstName.text
        parameter["lastname"] = txtLastName.text
        parameter["email"] = txtEmail.text
        parameter["password"] = txtPassword.text
        parameter["confirmpassword"] = txtConfirmPassword.text
        parameter["device_id"] = UserDefaults.standard.object(forKey: BazarBit.DEVICEID)
        parameter["phoneno"] = txtMobile.text
        parameter["countrycodes"] = txtCountryCode.text
        
        if socialType != "" {
            parameter["socialmediaid"] = self.socialId
            parameter["social_type"] = self.socialType
        }
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true) { (swiftyJson, responseDict) in
            
            let strStoredEmail : String = self.txtEmail.text!
            print(strStoredEmail)
            
            print(responseDict)
            
            let messege = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status:String = responseDict["status"] as! String
            
            print(messege)
            
            if status == "ok" {
                
                let payload : [String:AnyObject] = responseDict["payload"] as! [String:AnyObject]
                
                UserDefaults.standard.set(self.txtEmail.text, forKey: BazarBit.USEREMAILID)
                
                let userTokenForRegistration : String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "user_token")
                
                UserDefaults.standard.set(userTokenForRegistration, forKey: "user_token_signup")
                
                print(APPDELEGATE.allow_otp_verification)
                
                if APPDELEGATE.allow_otp_verification == "1" {
                    
                        let objOtpVC : OtpViewController = self.storyboard?.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
                        objOtpVC.strMobileNo = self.strMobileNo
                        objOtpVC.strEmail = strStoredEmail
                        objOtpVC.strRegUserToken = userTokenForRegistration
                        self.navigationController?.pushViewController(objOtpVC, animated: true)
                }else {
                    
                    self.User_Login()
                }
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage(messege)
            }
        }
    }
    
    
    // MARK:-  Social Action Method
    @IBAction func btnFacebook(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    @IBAction func btnGmail(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().signOut()
    }
    
    @IBAction func btnInstagram(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "instagramVC")
        self.navigationController?.pushViewController(nav!, animated: true)
    }
    
    @IBAction func btnLinkedIn(_ sender: Any) {
        let objLinkedIn = self.storyboard?.instantiateViewController(withIdentifier: "LinkedInViewController")
        self.navigationController?.pushViewController(objLinkedIn!, animated: true)
    }
    
    @IBAction func btnPintrest(_ sender: Any) {
        
        PDKClient.sharedInstance().authenticate(withPermissions: [PDKClientReadPublicPermissions,PDKClientWritePublicPermissions,PDKClientReadRelationshipsPermissions,PDKClientWriteRelationshipsPermissions], withSuccess: { (PDKResponseObject) in            
            
            self.pintrestDetail["id"] = PDKResponseObject?.user().identifier as AnyObject
            self.pintrestDetail["firstName"] = PDKResponseObject?.user().firstName as AnyObject
            self.pintrestDetail["lastName"] = PDKResponseObject?.user().lastName as AnyObject
            
            self.dictPintrest = self.pintrestDetail
            
            self.socialId = (PDKResponseObject?.user().identifier)!
            self.socialType = "pintrest"
            
            let fName : String = (PDKResponseObject?.user().firstName)!
            let lName : String = (PDKResponseObject?.user().lastName)!
            
            self.socialName = "\(fName) \(lName)"
            self.socialParameter["social_id"] = self.socialId
            self.socialParameter["social_type"] = self.socialType
            self.socialLoginService()
            
        }) { (Error) in
            print(Error ?? "nil")
        }
    }
    
    // MARK: - SOCIAL HELPER METHOD
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dictfacebook = result as! [String : AnyObject]
                    
                    // Get user Social Details
                    self.socialId = self.dictfacebook["id"] as! String
                    self.socialType = "facebook"
                    self.socialName = self.dictfacebook["name"] as! String
                    
                    self.socialParameter["social_id"] = self.socialId
                    self.socialParameter["social_type"] = self.socialType
                    self.socialParameter["email_id"] =  self.socialEmail
                    self.socialLoginService()
                }
            })
        }
    }
    
    // Get user Gmail Social Details Delegate methods
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let firstName = user.profile.givenName
            let lastName = user.profile.familyName
            let email = user.profile.email
            
            // Custom Key For use in SignUp Page
            self.dictGoogle["fullName"] = fullName as AnyObject
            self.dictGoogle["givenName"] = firstName as AnyObject
            self.dictGoogle["familyName"] = lastName as AnyObject
            self.dictGoogle["email"] = email as AnyObject
            
            self.socialId = userId!
            self.socialType = "google"
            self.socialName = fullName!
            self.socialEmail = email!
            
            socialParameter["social_id"] = socialId
            socialParameter["social_type"] = socialType
            socialParameter["email_id"] = socialEmail
            socialLoginService()
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("error")
    }
    
    //MARK: SOCIAL SERVICE METHOD
    func socialLoginService() {
        
        let strurl: String = BazarBit.SocialMediaService()
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        PIService.serviceCall(url: url, method: .post, parameter: socialParameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true) { (swiftyJson, responseDict) in
            
            let dict:[String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "status")
            
            if status == "ok" {
                
                if (dict["payload"] != nil) {
                    
                    let payload: [String: AnyObject] = dict["payload"] as! [String : AnyObject]
                    
                    UserDefaultFunction.setCustomDictionary(dict: payload, key: BazarBit.USERACCOUNT)
                    
                    let strUserId: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "user_id")
                    
                    UserDefaults.standard.set(strUserId, forKey: BazarBit.USERID)
                    
                    let authoUser: [String: AnyObject] = payload["authUser"] as! [String : AnyObject]
                    UserDefaults.standard.set(authoUser["user_token"], forKey: BazarBit.AUTHTOKEN)
                    
                    let strFName:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "firstname")
                    
                    UserDefaults.standard.set(strFName, forKey: BazarBit.USERNAME)
                    
                    let strEmail:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "username")
                    
                    UserDefaults.standard.set(strEmail, forKey: BazarBit.USEREMAILADDRESS)
                    
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        myDelegate.openHomePage()
                    }
                }
            } else {
                self.DisplayDetailOfSocialData()
                PIAlertUtils.displayAlertWithMessage(message)
            }
        }
    }
    
    // MARK: - Helper Method For AutoFill Data
    func DisplayDetailOfSocialData()  {
        
        if self.socialType == "facebook" {
            if dictfacebook.count > 0 {
                self.txtFirstName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictfacebook, key: "first_name")
                self.txtLastName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictfacebook, key: "last_name")
                self.txtEmail.text = PIService.object_forKeyWithValidationForClass_String(dict: dictfacebook, key: "email")
            }
            
        } else if self.socialType == "google" {
            if dictGoogle.count > 0 {
                self.txtFirstName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictGoogle, key: "givenName")
                self.txtLastName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictGoogle, key: "familyName")
                self.txtEmail.text = PIService.object_forKeyWithValidationForClass_String(dict: dictGoogle, key: "email")
            }
        }
        else if self.socialType == "instagram" {
            if dictInstagram.count > 0 {
                self.txtFirstName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictInstagram, key: "full_name")
            }
        }
        else if self.socialType == "linkedin" {
            if dictLinkedIn.count > 0 {
                print(dictLinkedIn)
                self.txtFirstName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictLinkedIn, key: "firstName")
                self.txtLastName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictLinkedIn, key: "lastName")
                self.txtEmail.text = PIService.object_forKeyWithValidationForClass_String(dict: dictLinkedIn, key: "emailAddress")
            }
        }
        else if self.socialType == "pintrest" {
            if dictPintrest.count > 0 {
                print(dictPintrest)
                self.txtFirstName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictPintrest, key: "firstName")
                self.txtLastName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictPintrest, key: "lastName")
            }
        }
    }
    
    // Once Login After Logout
    func removeInstagramCookies()  {
        let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! as [HTTPCookie]{
            NSLog("cookie.domain = %@", cookie.domain)
            
            if cookie.domain == "www.instagram.com" ||
                cookie.domain == "api.instagram.com"{
                cookieJar.deleteCookie(cookie)
            }
        }
    }
    
    // MARK: - Validation Method
    func checkValidation() -> Bool {
        
        var isValid: Bool = true
        
        strFname = txtFirstName.text!
        strLname = txtLastName.text!
        strEmail = txtEmail.text!
        strCountryCode = txtCountryCode.text!
        strPassword = txtPassword.text!
        strConfirmPassword = txtConfirmPassword.text!
        strMobile = txtMobile.text!
        
        strFname = strFname.trimmed()
        strLname = strLname.trimmed()
        strEmail = strEmail.trimmed()
        strCountryCode = strCountryCode.trimmed()
        strPassword = strPassword.trimmed()
        strConfirmPassword = strConfirmPassword.trimmed()
        strMobile = strMobile.trimmed()
        
        if PIValidation.isBlankString(str: strFname) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter First Name...!")
            return isValid
        }
        
        if PIValidation.isBlankString(str: strLname) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Last Name...!")
            return isValid
        }
        
        if PIValidation.isBlankString(str: strEmail) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Email...!")
            return isValid
            
        } else if (PIValidation.isEmailString(str: strEmail)) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Email Not Valid...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strCountryCode) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please select country code...!")
            return isValid
            
        }  else if PIValidation.isBlankString(str: strMobile) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Mobile Number...!")
            return isValid
            
        }  else if strMobile.characters.count < 10 || strMobile.characters.count > 15 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Enter Valid Mobile No...!")
            return isValid
            
        }  else if PIValidation.isBlankString(str: strPassword) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Password...!")
            return isValid
        }
            
        else if strPassword.characters.count < 6 || strPassword.characters.count > 30 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Password Between 6 to 30 Character...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strConfirmPassword) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Confirm Password...!")
            return isValid
        }
            
        else if strConfirmPassword.characters.count < 6 || strConfirmPassword.characters.count > 30 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Confirm Password Between 6 to 30 Character...!")
            return isValid
        }
            
        else if PIValidation.isCompareTwoStringEqual(firstStr: strPassword, secondStr: strConfirmPassword) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Confirm Password Not Match...!")
            return isValid
        }
        return isValid
    }
    
    // MARK: - Picker toolbar Button Method
    func addToolBar(){
        
        let toolBar = UIToolbar()
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        txtCountryCode.inputView = countryPicker
        self.countryPicker.dataSource = self
        self.countryPicker.delegate = self
        self.countryPicker.reloadAllComponents()
        txtCountryCode.inputAccessoryView = toolBar
    }
    
    func donePressed(sender:UIButton)  {
        txtCountryCode.resignFirstResponder()
    }
    
    // MARK: - Login without OTP
    
    func User_Login() {
        
        if checkValidation() {
            let strurl: String = BazarBit.loginService()
            let url: URL = URL(string: strurl)!
            
            var headers: HTTPHeaders = [:]
            headers[BazarBit.ContentType] = BazarBit.applicationJson
            headers["app-id"] = BazarBit.appId
            headers["app-secret"] = BazarBit.appSecret
            
            var parameter: Parameters = [:]
            parameter["email"] = txtEmail.text
            parameter["password"] = txtPassword.text
            parameter["device_id"] = UserDefaults.standard.object(forKey: BazarBit.DEVICEID)
            
            print(parameter)
            
            PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
                
                //print(responseDict)
                self.dictResponse = responseDict
                //print(url)
                
                let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
                
                let status: String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictResponse, key: "status")
                
                if status == "ok" {
                    if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil) {
                        UserDefaults.standard.removeObject(forKey: BazarBit.SHIPPINGADDRESS)
                    }
                    
                    if (UserDefaults.standard.object(forKey: BazarBit.BILLINGADDRESS) != nil) {
                        UserDefaults.standard.removeObject(forKey: BazarBit.BILLINGADDRESS)
                    }
                    
                    if (self.dictResponse["payload"] != nil) {
                        
                        let payload: [String: AnyObject] = self.dictResponse["payload"] as! [String : AnyObject]
                        
                        UserDefaultFunction.setCustomDictionary(dict: payload as [String : AnyObject], key: BazarBit.USERACCOUNT)
                        
                        // USER ID
                        let strUserId: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "user_id")
                        
                        UserDefaults.standard.set(strUserId, forKey: BazarBit.USERID)
                        
                        // USER NAME
                        let strUserName : String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "firstname")
                        
                        UserDefaults.standard.set(strUserName, forKey: BazarBit.USERNAME)
                        print(strUserName)
                        
                        // USER EMAIL ADDRESS
                        let strUserEmail : String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "email")
                        
                        UserDefaults.standard.set(strUserEmail, forKey: BazarBit.USEREMAILADDRESS)
                        print(strUserEmail)
                        
                        // USER PROFILE IMAGE
                        if (payload["profilepicture"] as? [AnyObject]) != nil
                        {
                            let arrUserProfile : [AnyObject] = payload["profilepicture"] as! [AnyObject]
                            print(arrUserProfile)
                            
                            if arrUserProfile.count > 0
                            {
                                let profileImage : String = arrUserProfile[0] as! String
                                print(profileImage)
                                
                                UserDefaults.standard.set(profileImage, forKey: BazarBit.USERPROFILEIMAGE)
                            }
                        }
                        
                        let authoUser: [String: Any] = payload["authUser"] as! [String : Any]
                        UserDefaults.standard.set(authoUser["user_token"], forKey: BazarBit.AUTHTOKEN)
                        
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            
                            if APPDELEGATE.GlobalVC != nil {
                                
                                self.navigationController?.popToViewController(APPDELEGATE.GlobalVC, animated: true)
                                APPDELEGATE.GlobalVC = nil
                                
                            } else {
                                myDelegate.openHomePage()
                            }
                        }
                    }
                }
                else
                {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            })
        }
    }
    
}
