//
//  AddAddressViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 10/25/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import Toaster

class AddAddressViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
  
  var dimView : UIView!
  
  @IBOutlet var viewNavigation : UIView!
  @IBOutlet var lableTitleBillingAddress : UILabel!
  @IBOutlet var lableTitleSameAddress : UILabel!
  
  @IBOutlet weak var lableHeaderTitle: UILabel!
  @IBOutlet weak var lableBillingTitle: UILabel!
    
  @IBOutlet weak var scrollViewAddAddress: UIScrollView!
  
  var isFromMenu : Bool = false
  @IBOutlet var buttonBack : UIButton!
  @IBOutlet var buttonMenu : UIButton!
  
  @IBOutlet var btnAddAddress: UIButton!
  @IBOutlet var btnBilling: UIButton!
  
  @IBOutlet var btnUpdate: UIButton!
  @IBOutlet var labelTitleShippingAddress: UIButton!
  
  //Shipping Address
  @IBOutlet var txtFirstName: UITextField!
  @IBOutlet var txtLastName: UITextField!
  @IBOutlet var txtAddress: UITextField!
  @IBOutlet var txtLandmark: UITextField!
  @IBOutlet var txtPincode: UITextField!
  @IBOutlet var txtMobileNo: UITextField!
  @IBOutlet var txtCountry: UITextField!
  @IBOutlet var txtState: UITextField!
  @IBOutlet var txtCity: UITextField!
  
  //Billing Address
  @IBOutlet var txtBillingFirstName: UITextField!
  @IBOutlet var txtBillingLastName: UITextField!
  @IBOutlet var txtBillingAddress: UITextField!
  @IBOutlet var txtBillingLandmark: UITextField!
  @IBOutlet var txtBillingPincode: UITextField!
  @IBOutlet var txtBillingMobileNo: UITextField!
  @IBOutlet var txtBillingCountry: UITextField!
  @IBOutlet var txtBillingState: UITextField!
  @IBOutlet var txtBillingCity: UITextField!
  var addAddressListObj: AddressListViewController!
  
  @IBOutlet var viewCountry: UIView!
  @IBOutlet var pickerCountry: UIPickerView!
  var arrCountryCode: [AnyObject] = []
  
  @IBOutlet weak var pickerCoutrybilling: UIPickerView!
  
  var isShipping: String = "1"
  
  var isBilling: Bool = false
  
  @IBOutlet var viewCountryBilling: UIView!
  
  @IBOutlet var viewStateBilling: UIView!
  @IBOutlet var viewState: UIView!
  
  @IBOutlet var viewBillingAddressHeight: NSLayoutConstraint!
  @IBOutlet var viewBillingAddress: UIView!
  
  @IBOutlet var viewAddress: UIView!
  @IBOutlet var viewShipping: UIView!
  
  @IBOutlet var viewAddressHeight: NSLayoutConstraint!
  @IBOutlet var viewShippingHeight: NSLayoutConstraint!
  
  @IBOutlet weak var viewBilling: UIView!
  
  @IBOutlet weak var viewBillingHeightConstraints: NSLayoutConstraint!
  
  @IBOutlet var pickerState: UIPickerView!
  
  @IBOutlet weak var pickerStateBilling: UIPickerView!
  var arrState: [AnyObject] = []
  
  //Shipping Address
  var strFirstName: String = ""
  var strLastName: String = ""
  var strAddress: String = ""
  var strLandmark: String = ""
  var strPincode: String = ""
  var strMobileNo: String = ""
  var strCountry: String = ""
  var strState: String = ""
  var strCity: String = ""
  
  //Billing Address
  var strBillingFirstName: String = ""
  var strBillingLastName: String = ""
  var strBillingAddress: String = ""
  var strBillingLandmark: String = ""
  var strBillingPincode: String = ""
  var strBillingMobileNo: String = ""
  var strBillingCountry: String = ""
  var strBillingState: String = ""
  var strBillingCity: String = ""
  
  //var dictEdit: [String: AnyObject] = [:]
  var strPerform: String = "Add"
  
  var strCheckCountry: String = ""
  var strCheckState: String = ""
  
  var fromVC: String = ""
  
  //AJAY : 24-04-2018
  var isFromBillingAdd : Bool = false
  @IBOutlet var viewShippingAddress : UIView!
  @IBOutlet var viewShippingAddressHeight : NSLayoutConstraint!
  
  @IBOutlet var viewExistingShippingAddress : UIView!
  @IBOutlet var viewExistingShippingAddressHeight : NSLayoutConstraint!
    @IBOutlet var viewBottomSize : NSLayoutConstraint!

  @IBOutlet var lblShippingName: UILabel!
  @IBOutlet var lblShippingAddress: UILabel!
  @IBOutlet var lblShippingCity: UILabel!
  @IBOutlet var lblShippingState: UILabel!
  @IBOutlet var lblShippingPhoneNo: UILabel!
  
  // MARK: - UIView Method
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    isBilling = true
    self.viewBilling.isHidden = true
    btnBilling.setImage(#imageLiteral(resourceName: "rightSquare"), for: .normal)
    viewBillingHeightConstraints.constant = 0
    isShipping = "0"
    APPDELEGATE.chkSameAsShipping = "SameAsShipping"
    
    self.navigationController?.navigationBar.isHidden = true
    viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    
    viewAddress.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    
    if APPDELEGATE.chkBottomBar == true {
        viewBottomSize.constant = 60
        APPDELEGATE.chkBottomBar = false
    } else {
        viewBottomSize.constant = 12
    }
    
    setBorderPropertiesForTextField()
    setBorderPropertiesForTextFieldForBilling()
    addPropertyCountryStatePopUp()
    
    CallGetCountryList()
    
    btnAddAddress.setButtonDynamicColor()
    btnAddAddress.Shadow()
    
    lableTitleBillingAddress.setTextLabelDynamicTextColor()
    lableTitleSameAddress.setTextLabelDynamicTextColor()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    if isFromMenu == true {
      buttonMenu.isHidden = false
      buttonBack.isHidden = true
    }else {
      buttonMenu.isHidden = true
      buttonBack.isHidden = false
    }
    
    if strPerform == "Edit" {
      //            viewBillingAddressHeight.constant = 0.0
      //            viewBillingAddress.isHidden = true
      //
      //            DisplayDefaultRecord()
      //            btnAddAddress.setTitle("UPDATE", for: .normal)
    } else {
      //Shipping
      txtFirstName.text = ""
      txtLastName.text = ""
      txtAddress.text = ""
      txtCountry.text = ""
      txtState.text = ""
      txtCity.text = ""
      txtPincode.text = ""
      txtMobileNo.text = ""
      txtLandmark.text = ""
      
      //Billing
      txtBillingFirstName.text = ""
      txtBillingLastName.text = ""
      txtBillingAddress.text = ""
      txtBillingCountry.text = ""
      txtBillingState.text = ""
      txtBillingCity.text = ""
      txtBillingPincode.text = ""
      txtBillingMobileNo.text = ""
      txtBillingLandmark.text = ""
      
      btnAddAddress.setTitle("Add Address", for: .normal)
    }
    
    if fromVC == "CheckOut" {
      self.viewAddress.isHidden = false
      self.viewAddressHeight.constant = 64.0
    } else {
      self.viewAddress.isHidden = true
      self.viewAddressHeight.constant = 0.0
    }
    
    //AJAY : 24-04-2018
    if isFromBillingAdd == true {
      viewShippingAddress.isHidden = true
      viewShippingAddressHeight.constant = 0.0
      
      viewBillingAddress.isHidden = true
      viewBillingAddressHeight.constant = 0.0
      
      self.viewBilling.isHidden = false
      if IS_IPAD_DEVICE() {
        viewBillingHeightConstraints.constant = 300.0
      }else {
        viewBillingHeightConstraints.constant = 250.0
      }
      
      viewExistingShippingAddress.isHidden = false
      viewExistingShippingAddress.applyShadowDefault()
      viewExistingShippingAddress.applyCornerRadius(10.0)
      if IS_IPAD_DEVICE() {
        viewExistingShippingAddressHeight.constant = 150.0
      }else {
        viewExistingShippingAddressHeight.constant = 130.0
      }
      
      //SHIPPINGADDRESS
      if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil) {
        
        let dictShippingAddress : [String:AnyObject] = UserDefaultFunction.getDictionary(forKey: BazarBit.SHIPPINGADDRESS)! as [String : AnyObject]
        
        print(dictShippingAddress)
        
        let strFName : String = PIService.object_forKeyWithValidationForClass_String(dict: dictShippingAddress, key: "first_name")
        
        let strLName : String = PIService.object_forKeyWithValidationForClass_String(dict: dictShippingAddress, key: "last_name")
        
        let strAddress : String = PIService.object_forKeyWithValidationForClass_String(dict: dictShippingAddress, key: "address")
        
        let strCity : String = PIService.object_forKeyWithValidationForClass_String(dict: dictShippingAddress, key: "city")
        
        let strPincode : String = PIService.object_forKeyWithValidationForClass_String(dict: dictShippingAddress, key: "pincode")
        
        let strState : String = PIService.object_forKeyWithValidationForClass_String(dict: dictShippingAddress, key: "state")
        
        let strPhone : String = PIService.object_forKeyWithValidationForClass_String(dict: dictShippingAddress, key: "phone_number")
        
        self.lblShippingName.text = "\(strFName) \(strLName)"
        self.lblShippingAddress.text = strAddress
        self.lblShippingCity.text = strCity + " - " + strPincode
        self.lblShippingState.text = strState
        self.lblShippingPhoneNo.text = strPhone
      }
    }else {
      viewShippingAddress.isHidden = false
      viewBillingAddress.isHidden = false
      if IS_IPAD_DEVICE() {
        viewShippingAddressHeight.constant = 300.0
        viewBillingAddressHeight.constant = 45.0
      }else {
        viewShippingAddressHeight.constant = 250.0
        viewBillingAddressHeight.constant = 50.0
      }
      
      viewExistingShippingAddress.isHidden = true
      viewExistingShippingAddressHeight.constant = 0.0
    }
  }
  
  //MARK: buttonOpenDrawer
  @IBAction func btnOpenDrawer(_ sender: Any) {
    self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - PickerView Methods
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if pickerView == pickerCountry || pickerView == pickerCoutrybilling {
      return arrCountryCode.count
    } else {
      return arrState.count
    }
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
    if pickerView == pickerCountry || pickerView == pickerCoutrybilling{
      let dict : [String: AnyObject] = arrCountryCode[row] as! [String:AnyObject]
      let strCountryData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country_name")
      return strCountryData
    }else
    {
      let dict : [String: AnyObject] = arrState[row] as! [String:AnyObject]
      let strStateData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state_name")
      return strStateData
    }
  }
  
  // MARK: - Textfield Methods
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
    if viewCountry.frame.origin.y != UIScreen.main.bounds.size.height {
      self.hidePopUp(viewPicker: viewCountry)
    }
    if viewState.frame.origin.y != UIScreen.main.bounds.size.height {
      self.hidePopUp(viewPicker: viewState)
    }
    if viewCountryBilling.frame.origin.y != UIScreen.main.bounds.size.height {
      self.hidePopUp(viewPicker: viewCountryBilling)
    }
    if viewStateBilling.frame.origin.y != UIScreen.main.bounds.size.height {
      self.hidePopUp(viewPicker: viewStateBilling)
    }
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if string == ""
    {
      return true
      
    } else if txtFirstName == textField {
      if (textField.text?.characters.count)! >= 20 {
        return false
      }
      if (string.isNumber()) {
        return false
      }
      
    } else if txtLastName == textField {
      if (textField.text?.characters.count)! >= 20 {
        return false
      }
      if (string.isNumber()) {
        return false
      }
      
    } else if txtAddress == textField {
      if (textField.text?.characters.count)! >= 50 {
        return false
      } else {
        return true
      }
      
    } else if textField == txtCountry {
      return false
      
    } else if textField == txtState {
      return false
      
    } else if textField == txtMobileNo {
      if (textField.text?.characters.count)! >= 15 {
        return false
      }
      if !(string.isNumber()) {
        return false
      }
      
    } else if txtLandmark == textField {
      if (textField.text?.characters.count)! >= 20 {
        return false
      }
      
    } else if textField == txtPincode {
      if (textField.text?.characters.count)! >= 6 {
        return false
      }
      if !(string.isNumber()) {
        return false
      }
      
    } else if textField == txtCity {
      if (textField.text?.characters.count)! >= 30 {
        return false
      }
    }
      
      //Billing
    else if txtBillingFirstName == textField {
      if (textField.text?.characters.count)! >= 20 {
        return false
      }
      if (string.isNumber()) {
        return false
      }
      
    } else if txtBillingLastName == textField {
      if (textField.text?.characters.count)! >= 20 {
        return false
      }
      if (string.isNumber()) {
        return false
      }
      
    } else if txtBillingAddress == textField {
      if (textField.text?.characters.count)! >= 50 {
        return false
      } else {
        return true
      }
      
    } else if textField == txtBillingCountry {
      return false
      
    } else if textField == txtBillingState {
      return false
      
    } else if textField == txtBillingMobileNo {
      if (textField.text?.characters.count)! >= 15 {
        return false
      }
      if !(string.isNumber()) {
        return false
      }
      
    } else if txtBillingLandmark == textField {
      if (textField.text?.characters.count)! >= 20 {
        return false
      }
      
    } else if textField == txtBillingPincode {
      if (textField.text?.characters.count)! >= 6 {
        return false
      }
      if !(string.isNumber()) {
        return false
      }
      
    } else if textField == txtBillingCity {
      if (textField.text?.characters.count)! >= 30 {
        return false
      }
    }
    
    if textField == txtFirstName || textField == txtLastName || textField == txtLandmark || textField == txtCity {
      let aSet = NSCharacterSet(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz").inverted
      let compSepByCharInSet = string.components(separatedBy: aSet)
      let numberFiltered = compSepByCharInSet.joined(separator: "")
      return string == numberFiltered
    }
    
    //Billing
    if textField == txtBillingFirstName || textField == txtBillingLastName || textField == txtBillingLandmark || textField == txtBillingCity {
      let aSet = NSCharacterSet(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz").inverted
      let compSepByCharInSet = string.components(separatedBy: aSet)
      let numberFiltered = compSepByCharInSet.joined(separator: "")
      return string == numberFiltered
    }
    return true
  }
  
  // MARK: - Handle Country Action For Billing And Shipping Method
  @IBAction func btnBillingCountry(_ sender: Any)  {
    strCheckCountry = "Billing"
    showPickerPopUp(viewPicker: viewCountry)
  }
  
  //Shipping
  @IBAction func btnCountry(_ sender: Any)  {
    if arrCountryCode.count > 0 {
      strCheckCountry = "Shipping"
      self.view.endEditing(true)
      showPickerPopUp(viewPicker: viewCountry)
    }else {
      PIAlertUtils.displayAlertWithMessage("Country not found...!")
    }
  }
  
  //Billing
  @IBAction func btnCountryForBilling(_ sender: Any) {
    if arrCountryCode.count > 0 {
      strCheckCountry = "Billing"
      self.view.endEditing(true)
      showPickerPopUp(viewPicker: viewCountryBilling)
    }else {
      PIAlertUtils.displayAlertWithMessage("Country not found...!")
    }
  }
  
  //Shipping
  @IBAction func btnCountryDone(_ sender: Any)  {
    hidePopUp(viewPicker: viewCountry)
    
    let row: Int = pickerCountry.selectedRow(inComponent: 0)
    let dict : [String: AnyObject] = arrCountryCode[row] as! [String:AnyObject]
    strCountry = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country_name")
    txtCountry.text = strCountry
  }
  
  //Shipping
  @IBAction func btnCountryCancel(_ sender: Any)  {
    hidePopUp(viewPicker: viewCountry)
  }
  
  //Billing
  @IBAction func btnCountryBillingDone(_ sender: Any) {
    hidePopUp(viewPicker: viewCountryBilling)
    
    let row: Int = pickerCoutrybilling.selectedRow(inComponent: 0)
    let dict : [String: AnyObject] = arrCountryCode[row] as! [String:AnyObject]
    strBillingCountry = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country_name")
    txtBillingCountry.text = strBillingCountry
  }
  //Billing
  @IBAction func btnCountryBillingCancel(_ sender: Any) {
    hidePopUp(viewPicker: viewCountryBilling)
  }
  
  // MARK: - Handle State Action For Billing
  @IBAction func btnState(_ sender: Any)  {
    
    if txtCountry.text != "" {
      strCountry = txtCountry.text!
      CallGetStateList(strCountryName: strCountry)
    } else {
      PIAlertUtils.displayAlertWithMessage("Select Country!")
    }
  }
  
  //Handle State Action For Shipping
  @IBAction func btnStateForBilling(_ sender: Any) {
    
    if txtBillingCountry.text != "" {
      strBillingCountry = txtBillingCountry.text!
      CallGetStateList(strCountryName: strBillingCountry)
    } else {
      PIAlertUtils.displayAlertWithMessage("Select Country!")
    }
  }
  
  //Shipping
  @IBAction func btnStateDone(_ sender: Any)  {
    
    hidePopUp(viewPicker: viewState)
    
    let row: Int = pickerState.selectedRow(inComponent: 0)
    let dict : [String: AnyObject] = arrState[row] as! [String:AnyObject]
    strState = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state_name")
    txtState.text = strState
  }
  
  //Shipping
  @IBAction func btnStateCancel(_ sender: Any)  {
    hidePopUp(viewPicker: viewState)
  }
  
  //Billing
  @IBAction func btnStateBillingDone(_ sender: Any) {
    hidePopUp(viewPicker: viewStateBilling)
    
    let row: Int = pickerStateBilling.selectedRow(inComponent: 0)
    let dict : [String: AnyObject] = arrState[row] as! [String:AnyObject]
    strBillingState = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state_name")
    txtBillingState.text = strBillingState
  }
  
  //Billing
  @IBAction func btnStateBillingCancel(_ sender: Any) {
    hidePopUp(viewPicker: viewStateBilling)
  }
  
  // MARK: - Action Method
  @IBAction func btnBack(_ sender: Any)  {
    APPDELEGATE.chkListingBack = true
    APPDELEGATE.isBilling = true
    APPDELEGATE.strvalidate = ""
    let viewVC : [UIViewController] = (self.navigationController?.viewControllers)!
    print(viewVC)
    
    if APPDELEGATE.checkDisplayBillingRecord == false {
        APPDELEGATE.isBilling = false
    } else {
        APPDELEGATE.isBilling = true
    }
    
    if viewVC[viewVC.count-2] == APPDELEGATE.shoppingCartVC{
      let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
      
      
      self.navigationController?.pushViewController(checkOutVC, animated: true)
    } else {
      self.navigationController?.popViewController(animated: true)
    }
  }
  
  @IBAction func btnBilling(_ sender: Any)  {
    
    //Shipping
    if isBilling == false {
      self.viewBilling.isHidden = true
      isShipping = "0"
      APPDELEGATE.chkSameAsShipping = "SameAsShipping"
      btnBilling.setImage(#imageLiteral(resourceName: "rightSquare"), for: .normal)
      viewBillingHeightConstraints.constant = 0
      
      txtBillingFirstName.text = ""
      txtBillingLastName.text = ""
      txtBillingAddress.text = ""
      txtBillingCountry.text = ""
      txtBillingState.text = ""
      txtBillingCity.text = ""
      txtBillingPincode.text = ""
      txtBillingMobileNo.text = ""
      txtBillingLandmark.text = ""
      isBilling = true
      
      //Billing
    } else {
      self.viewBilling.isHidden = false
      isShipping = "1"
      APPDELEGATE.chkSameAsShipping = "Individual"
      btnBilling.setImage(#imageLiteral(resourceName: "square"), for: .normal)
      viewBillingHeightConstraints.constant = 250
      isBilling = false
    }
  }
  
  @IBAction func btnAddAdress(_ sender: Any)  {
    
    if isFromBillingAdd == true {
      if checkValidation2() {
        isShipping = "1"
        self.CallAddAddressForBiling()
      }
    }else {
      //Select Same as Shipping check mark
      if APPDELEGATE.chkSameAsShipping == "SameAsShipping"{
        //Same as Billing
        if checkValidation() {
          isShipping = "0"
          APPDELEGATE.strvalidate = "Shipping"
          APPDELEGATE.chkSameAsShipping = "SameAsShipping"
          CallAddAddressForShipping()
        }
      } else if
        //un Select Same as Shipping check mark
        APPDELEGATE.chkSameAsShipping == "Individual" {
        if checkValidation() {
          if checkValidation2() {
            isShipping = "0"
            CallAddAddressForShipping()
          }
        }
      }
    }
  }
  
  // MARK: - Service Method For Shipping
  func CallAddAddressForShipping() {
    
    let strurl:String = BazarBit.GetAddAddressService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    
    var parameter:Parameters = [:]
    parameter["first_name"] = strFirstName
    parameter["last_name"] = strLastName
    parameter["address"] = strAddress
    parameter["landmark"] = strLandmark
    parameter["pincode"] = strPincode
    parameter["country"] = strCountry
    parameter["state"] = strState
    parameter["city"] = strCity
    parameter["phone_number"] = strMobileNo
    parameter["is_shipping"] = isShipping
    
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      print(responseDict)
      
      var response : [String : AnyObject] = responseDict
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      if status == "ok"
      {
        if self.fromVC == "CheckOut" {
          self.setShippingBillingRecord(strMessage: message)
        } else {
          if self.addAddressListObj != nil {
            if APPDELEGATE.chkSameAsShipping == "SameAsShipping" {
              self.addAddressListObj.strCheckService = ""
              
              APPDELEGATE.chkAddAddress = false
              APPDELEGATE.chkFirstAddress = false
              APPDELEGATE.isBilling = true
              self.isShipping = "1"
              
              self.strBillingFirstName = self.strFirstName
              self.strBillingLastName = self.strLastName
              self.strBillingAddress = self.strAddress
              self.strBillingCity = self.strCity
              self.strBillingPincode = self.strPincode
              self.strBillingState = self.strState
              self.strBillingMobileNo = self.strMobileNo
              self.strBillingLandmark = self.strLandmark
              self.strBillingCountry = self.strCountry
              self.strBillingState = self.strState
              
              self.CallAddAddressForBiling()
              
              self.navigationController?.popViewController(animated: true)
            } else if APPDELEGATE.chkSameAsShipping == "Individual" {
              
              self.isShipping = "1"
              APPDELEGATE.chkAddAddress = false
              APPDELEGATE.chkFirstAddress = false
              APPDELEGATE.isBilling = true
              self.CallAddAddressForBiling()
            }
            
          }
          else {
            
            //When Click on Add button from Checkout screen only For shipping (Click on shipping add)
            if APPDELEGATE.strvalidate == "Shipping" {
              if APPDELEGATE.chkSameAsShipping == "SameAsShipping" {
                if self.isShipping == "0" {
                  //Open Check out Screen And set to both
                  
                  APPDELEGATE.chkAddAddress = true
                  APPDELEGATE.chkFirstAddress = false
                  APPDELEGATE.isBilling = true
                  self.isShipping = "1"
                  
                  self.strBillingFirstName = self.strFirstName
                  self.strBillingLastName = self.strLastName
                  self.strBillingAddress = self.strAddress
                  self.strBillingCity = self.strCity
                  self.strBillingPincode = self.strPincode
                  self.strBillingState = self.strState
                  self.strBillingMobileNo = self.strMobileNo
                  self.strBillingLandmark = self.strLandmark
                  self.strBillingCountry = self.strCountry
                  self.strBillingState = self.strState
                  
                  self.CallAddAddressForBiling()
                }
              }
            }
              
              //When Click on Add button from Checkout screen only For Billing (Click on billing add)
            else if APPDELEGATE.strvalidate == "Billing" {
              if APPDELEGATE.chkSameAsShipping == "SameAsShipping" {
                if self.isShipping == "0" {
                  //Open Check out Screen And set to both
                  
                  APPDELEGATE.chkAddAddress = true
                  APPDELEGATE.chkFirstAddress = false
                  APPDELEGATE.isBilling = true
                  self.isShipping = "1"
                  
                  self.strBillingFirstName = self.strFirstName
                  self.strBillingLastName = self.strLastName
                  self.strBillingAddress = self.strAddress
                  self.strBillingCity = self.strCity
                  self.strBillingPincode = self.strPincode
                  self.strBillingState = self.strState
                  self.strBillingMobileNo = self.strMobileNo
                  self.strBillingLandmark = self.strLandmark
                  self.strBillingCountry = self.strCountry
                  self.strBillingState = self.strState
                  
                  self.CallAddAddressForBiling()
                }
              }
              //No any address First time Call (Un select same as shipping)
              
            }
            //Individual address Add on click of Shipping Addclick from checkout
            if APPDELEGATE.strvalidate == "Shipping" {
              if APPDELEGATE.chkSameAsShipping == "Individual" {
                
                self.isShipping = "1"
                APPDELEGATE.chkAddAddress = false
                APPDELEGATE.chkFirstAddress = false
                APPDELEGATE.isBilling = true
                self.CallAddAddressForBiling()
              }
            }
            
            //Individual address Add on click of Billing Addclick from checkout
            if APPDELEGATE.strvalidate == "Billing" {
              if APPDELEGATE.chkSameAsShipping == "Individual" {
                
                self.isShipping = "1"
                APPDELEGATE.chkAddAddress = false
                APPDELEGATE.chkFirstAddress = false
                APPDELEGATE.isBilling = true
                self.CallAddAddressForBiling()
              }
            }
            
            //Individual address Add on click of Billing Addclick from checkout
            if APPDELEGATE.strvalidate == "" {
              if APPDELEGATE.chkSameAsShipping == "Individual" {
                
                self.isShipping = "1"
                APPDELEGATE.chkAddAddress = false
                APPDELEGATE.chkFirstAddress = false
                APPDELEGATE.isBilling = true
                self.CallAddAddressForBiling()
              }
            }
            
            //From checkout when Click on billing And Set individual address (Same as Shipping selected)
            if APPDELEGATE.strvalidate == "NewBilling" {
              if APPDELEGATE.chkSameAsShipping == "Individual" {
                if self.isShipping == "0" {
                  self.isShipping = "1"
                  APPDELEGATE.chkAddAddress = false
                  APPDELEGATE.chkFirstAddress = false
                  APPDELEGATE.chkNewUser = true
                  APPDELEGATE.isBilling = true
                  self.CallAddAddressForBiling()
                  
                }
              }
            }
          }
          //PIAlertUtils.displayAlertWithMessage(message)
        }
        
      } else  {
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  // MARK: - Service Method For Billing

    func CallAddAddressForBiling() {
        
        let strurl:String = BazarBit.GetAddAddressService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["first_name"] = strBillingFirstName
        parameter["last_name"] = strBillingLastName
        parameter["address"] = strBillingAddress
        parameter["landmark"] = strBillingLandmark
        parameter["pincode"] = strBillingPincode
        parameter["country"] = strBillingCountry
        parameter["state"] = strBillingState
        parameter["city"] = strBillingCity
        parameter["phone_number"] = strBillingMobileNo
        parameter["is_shipping"] = isShipping
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            print(responseDict)
            
            let response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if self.fromVC == "CheckOut" {
                    
                    if APPDELEGATE.strvalidate == "Billing" {
                        APPDELEGATE.isBilling = false
                        APPDELEGATE.checkDisplayBillingRecord = false
                    }
                    
                    self.setShippingBillingRecord(strMessage: message)
                } else {
                    if self.addAddressListObj != nil {
                        self.addAddressListObj.strCheckService = ""
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        
                        if APPDELEGATE.chkAddAddress == true {
                            
                            // First Time Added address when no any address in list
                            if APPDELEGATE.chkFirstAddress == true {
                                
                                if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) == nil) {
                                    
                                    let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                                    
                                    checkOutVC.strFirstName = self.strFirstName
                                    checkOutVC.strLastName = self.strLastName
                                    checkOutVC.strAddress = self.strAddress
                                    checkOutVC.strcity = self.strCity
                                    checkOutVC.strPincode = self.strPincode
                                    checkOutVC.strState = self.strState
                                    checkOutVC.strPhone = self.strMobileNo
                                    self.navigationController?.pushViewController(checkOutVC, animated: true)
                                    
                                } else {
                                    let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                                    
                                    checkOutVC.strFirstName = self.strFirstName
                                    checkOutVC.strLastName = self.strLastName
                                    checkOutVC.strAddress = self.strAddress
                                    checkOutVC.strcity = self.strCity
                                    checkOutVC.strPincode = self.strPincode
                                    checkOutVC.strState = self.strState
                                    checkOutVC.strPhone = self.strMobileNo
                                    self.navigationController?.pushViewController(checkOutVC, animated: true)
                                }
                                
                            } else {
                                let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                                
                                checkOutVC.strFirstName = self.strFirstName
                                checkOutVC.strLastName = self.strLastName
                                checkOutVC.strAddress = self.strAddress
                                checkOutVC.strcity = self.strCity
                                checkOutVC.strPincode = self.strPincode
                                checkOutVC.strState = self.strState
                                checkOutVC.strPhone = self.strMobileNo
                                checkOutVC.strShippingLandMark = self.strLandmark
                                checkOutVC.strShippingCountry = self.strCountry
                                
                                checkOutVC.strBillingFirstName = self.strBillingFirstName
                                checkOutVC.strBillingLastName = self.strBillingLastName
                                checkOutVC.strBillingAddress = self.strBillingAddress
                                checkOutVC.strBillingCity = self.strBillingCity
                                checkOutVC.strBillingPinCode = self.strBillingPincode
                                checkOutVC.strBillingState = self.strBillingState
                                checkOutVC.strBillingPhoneNo = self.strBillingMobileNo
                                checkOutVC.strBillingLandMark = self.strBillingLandmark
                                checkOutVC.strBillingCountry = self.strBillingCountry
                                
                                APPDELEGATE.chkNewUser = true
                                self.navigationController?.pushViewController(checkOutVC, animated: true)
                            }
                        }
                        
                        //Call when Select individual when Add Shipping Click And Un select Same a shipping from Add address screen
                        if APPDELEGATE.strvalidate == "Shipping" {
                            
                            if APPDELEGATE.chkSameAsShipping == "Individual" {
                                let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                                
                                checkOutVC.strFirstName = self.strFirstName
                                checkOutVC.strLastName = self.strLastName
                                checkOutVC.strAddress = self.strAddress
                                checkOutVC.strcity = self.strCity
                                checkOutVC.strPincode = self.strPincode
                                checkOutVC.strState = self.strState
                                checkOutVC.strPhone = self.strMobileNo
                                checkOutVC.strShippingLandMark = self.strLandmark
                                checkOutVC.strShippingCountry = self.strCountry
                                
                                checkOutVC.strBillingFirstName = self.strBillingFirstName
                                checkOutVC.strBillingLastName = self.strBillingLastName
                                checkOutVC.strBillingAddress = self.strBillingAddress
                                checkOutVC.strBillingCity = self.strBillingCity
                                checkOutVC.strBillingPinCode = self.strBillingPincode
                                checkOutVC.strBillingState = self.strBillingState
                                checkOutVC.strBillingPhoneNo = self.strBillingMobileNo
                                checkOutVC.strBillingLandMark = self.strBillingLandmark
                                checkOutVC.strBillingCountry = self.strBillingCountry
                                
                                self.navigationController?.pushViewController(checkOutVC, animated: true)
                            }
                        }
                            //Call when Select individual when Add Shipping Click And Un select Same a shipping from Add address screen
                        else if APPDELEGATE.strvalidate == "Billing" {
                            APPDELEGATE.isBilling = false
                            APPDELEGATE.checkDisplayBillingRecord = false
                            
                            if APPDELEGATE.chkSameAsShipping == "Individual" {
                                
                                let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                                
                                checkOutVC.strBillingFirstName = self.strBillingFirstName
                                checkOutVC.strBillingLastName = self.strBillingLastName
                                checkOutVC.strBillingAddress = self.strBillingAddress
                                checkOutVC.strBillingCity = self.strBillingCity
                                checkOutVC.strBillingPinCode = self.strBillingPincode
                                checkOutVC.strBillingState = self.strBillingState
                                checkOutVC.strBillingPhoneNo = self.strBillingMobileNo
                                
                                self.navigationController?.pushViewController(checkOutVC, animated: true)
                                
                            }
                        }
                        
                        //When First Time No any address so call here only in Individual select
                        if APPDELEGATE.chkNewUser == true {
                            if APPDELEGATE.chkSameAsShipping == "Individual" {
                                let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                                
                                //APPDELEGATE.chkNewUser = false
                                //Shipping
                                checkOutVC.strFirstName = self.strFirstName
                                checkOutVC.strLastName = self.strLastName
                                checkOutVC.strAddress = self.strAddress
                                checkOutVC.strcity = self.strCity
                                checkOutVC.strPincode = self.strPincode
                                checkOutVC.strState = self.strState
                                checkOutVC.strPhone = self.strMobileNo
                                
                                //Billing
                                checkOutVC.strBillingFirstName = self.strBillingFirstName
                                checkOutVC.strBillingLastName = self.strBillingLastName
                                checkOutVC.strBillingAddress = self.strBillingAddress
                                checkOutVC.strBillingCity = self.strBillingCity
                                checkOutVC.strBillingPinCode = self.strBillingPincode
                                checkOutVC.strBillingState = self.strBillingState
                                checkOutVC.strBillingPhoneNo = self.strBillingMobileNo
                                
                                self.navigationController?.pushViewController(checkOutVC, animated: true)
                            }
                        }
                        
                        if APPDELEGATE.strvalidate == "" {
                            if APPDELEGATE.chkSameAsShipping == "Individual" {
                                
                                let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                                
                                checkOutVC.strFirstName = self.strFirstName
                                checkOutVC.strLastName = self.strLastName
                                checkOutVC.strAddress = self.strAddress
                                checkOutVC.strcity = self.strCity
                                checkOutVC.strPincode = self.strPincode
                                checkOutVC.strState = self.strState
                                checkOutVC.strPhone = self.strMobileNo
                                checkOutVC.strShippingLandMark = self.strLandmark
                                checkOutVC.strShippingCountry = self.strCountry
                                
                                checkOutVC.strBillingFirstName = self.strBillingFirstName
                                checkOutVC.strBillingLastName = self.strBillingLastName
                                checkOutVC.strBillingAddress = self.strBillingAddress
                                checkOutVC.strBillingCity = self.strBillingCity
                                checkOutVC.strBillingPinCode = self.strBillingPincode
                                checkOutVC.strBillingState = self.strBillingState
                                checkOutVC.strBillingPhoneNo = self.strBillingMobileNo
                                checkOutVC.strBillingLandMark = self.strBillingLandmark
                                checkOutVC.strBillingCountry = self.strBillingCountry
                                
                                APPDELEGATE.chkNewUser = true
                                self.navigationController?.pushViewController(checkOutVC, animated: true)
                                
                            }
                        }
                    }
                    
                    //AJAY : 24-04-2018
                    if self.isFromBillingAdd == true {
                        self.setBillingData()
                        self.navigationController?.popViewController(animated: true)
                    }
                    //PIAlertUtils.displayAlertWithMessage(message)
                }
                
            } else  {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }

    
  func CallGetCountryList() {
    
    let strurl:String = BazarBit.GetCountryListService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    
    let parameter:Parameters = [:]
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      print(responseDict)
      
      var response : [String : AnyObject] = responseDict
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      if status == "ok"
      {
        if (response["payload"] as? [AnyObject]) != nil {
          
          let payload:[AnyObject] = response["payload"] as! [AnyObject]
          
          self.arrCountryCode = payload
          
          //Shipping
          self.pickerCountry.dataSource = self
          self.pickerCountry.delegate = self
          self.pickerCountry.reloadAllComponents()
          
          //Billing
          self.pickerCoutrybilling.dataSource = self
          self.pickerCoutrybilling.delegate = self
          self.pickerCoutrybilling.reloadAllComponents()
        }
        else
        {
          PIAlertUtils.displayAlertWithMessage(message)
        }
      }
    })
  }
  
  func CallGetStateList(strCountryName : String) {
    
    let strurl:String = BazarBit.GetStateListService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    
    var parameter:Parameters = [:]
    parameter["country_name"] = strCountryName
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      var response : [String : AnyObject] = responseDict
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      if status == "ok"
      {
        if (response["payload"] != nil)
        {
          let payload:[AnyObject] = response["payload"] as! [AnyObject]
          print(payload)
          self.arrState = payload
          
          if self.arrState.count > 0 {
            //self.strCheckState = "Shipping"
            
            if self.strCheckCountry == "Shipping" {
              self.pickerState.dataSource = self
              self.pickerState.delegate = self
              self.pickerState.reloadAllComponents()
              self.showPickerPopUp(viewPicker: self.viewState)
            }
            
            //Billing
            if self.strCheckCountry == "Billing" {
              self.pickerStateBilling.dataSource = self
              self.pickerStateBilling.delegate = self
              self.pickerStateBilling.reloadAllComponents()
              self.showPickerPopUp(viewPicker: self.viewStateBilling)
            }
            
          }else {
            PIAlertUtils.displayAlertWithMessage("State not found...!")
          }
        }
        else
        {
          PIAlertUtils.displayAlertWithMessage(message)
        }
      }
    })
  }
  
  // MARK: - Helper Method
  func setShippingBillingRecord(strMessage: String)  {
    
    if isShipping == "0" {
      
      var localBillingDict: [String: AnyObject] = [:]      
      localBillingDict["first_name"] = self.strFirstName as AnyObject
      localBillingDict["last_name"] = self.strLastName as AnyObject
      localBillingDict["address"] = self.strAddress as AnyObject
      localBillingDict["country"] = self.strCountry as AnyObject
      localBillingDict["pincode"] = self.strPincode as AnyObject
      localBillingDict["landmark"] = self.strLandmark as AnyObject
      localBillingDict["city"] = self.strCity as AnyObject
      localBillingDict["state"] = self.strState as AnyObject
      localBillingDict["phone_number"] = self.strMobileNo as AnyObject
      
      print(localBillingDict)
      
      UserDefaultFunction.setCustomDictionary(dict: localBillingDict, key: BazarBit.BILLINGADDRESS)
      
    } else {
      
      var localShippingDict: [String: AnyObject] = [:]
      
      localShippingDict["first_name"] = self.strFirstName as AnyObject
      localShippingDict["last_name"] = self.strLastName as AnyObject
      localShippingDict["address"] = self.strAddress as AnyObject
      localShippingDict["country"] = self.strCountry as AnyObject
      localShippingDict["pincode"] = self.strPincode as AnyObject
      localShippingDict["landmark"] = self.strLandmark as AnyObject
      localShippingDict["city"] = self.strCity as AnyObject
      localShippingDict["state"] = self.strState as AnyObject
      localShippingDict["phone_number"] = self.strMobileNo as AnyObject
      
      UserDefaultFunction.setCustomDictionary(dict: localShippingDict, key: BazarBit.SHIPPINGADDRESS)
    }
    
    if isFromBillingAdd == false {
      var viewVC: [UIViewController] = (self.navigationController?.viewControllers)!
      var total: Int = viewVC.count - 1
      
      while total  >= 0  {
        let localView: UIViewController = viewVC[total]
        
        if localView is CheckOutViewController {
          self.navigationController?.popToViewController(localView, animated: true)
          PIAlertUtils.displayAlertWithMessage(strMessage)
          return
        }
        total = total - 1
      }
    }
  }
  
  //MARK: AJAY : 25-04-2018
  //MARK: SetBillingData
  func setBillingData() {
    
    APPDELEGATE.strvalidate = ""
    APPDELEGATE.chkSameAsShipping = ""
    
    var localBillingDict: [String: AnyObject] = [:]
    
    localBillingDict["first_name"] = self.strBillingFirstName as AnyObject
    localBillingDict["last_name"] = self.strBillingLastName as AnyObject
    localBillingDict["address"] = self.strBillingAddress as AnyObject
    localBillingDict["country"] = self.strBillingCountry as AnyObject
    localBillingDict["pincode"] = self.strBillingPincode as AnyObject
    localBillingDict["landmark"] = self.strBillingLandmark as AnyObject
    localBillingDict["city"] = self.strBillingCity as AnyObject
    localBillingDict["state"] = self.strBillingState as AnyObject
    localBillingDict["phone_number"] = self.strBillingMobileNo as AnyObject
    
    UserDefaultFunction.setCustomDictionary(dict: localBillingDict, key: BazarBit.BILLINGADDRESS)
  }
  
  func addPropertyCountryStatePopUp()  {
    
    //Shippping
    viewCountry.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewCountry.frame.size.height)
    self.view.addSubview(viewCountry)
    viewCountry.layoutIfNeeded()
    
    viewState.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewState.frame.size.height)
    self.view.addSubview(viewState)
    viewState.layoutIfNeeded()
    
    //Billing
    viewCountryBilling.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewCountryBilling.frame.size.height)
    self.view.addSubview(viewCountryBilling)
    viewCountryBilling.layoutIfNeeded()
    
    viewStateBilling.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewStateBilling.frame.size.height)
    self.view.addSubview(viewStateBilling)
    viewStateBilling.layoutIfNeeded()
  }
  
  // MARK:- Show-Hide Picker Method
  func showPickerPopUp(viewPicker:UIView) {
    if dimView == nil   {
      dimView = UIView()
      dimView?.frame = self.view.frame
      dimView?.backgroundColor = UIColor.black
      dimView?.alpha = 0.5
      self.view.addSubview(dimView!)
    }
    
    self.view.bringSubview(toFront: viewPicker)
    viewPicker.alpha = 1.0
    
    viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: viewPicker.frame.size.height)
    
    UIView.animate(withDuration: 0.5, animations:{
      viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - viewPicker.frame.size.height , width: UIScreen.main.bounds.width, height: viewPicker.frame.size.height)
    })
  }
  
  func hidePopUp(viewPicker:UIView) {
    UIView.animate(withDuration: 0.5, animations: {
      viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: viewPicker.frame.size.height)
    }) { (sucess) in
      viewPicker.alpha = 0.0
      self.dimView?.removeFromSuperview()
      self.dimView = nil
    }
  }
  
  //Shipping Address
  func setBorderPropertiesForTextField()  {
    
    txtFirstName.setLeftPaddingPoints(10.0)
    txtFirstName.DynamictextBorder()
    txtFirstName.setTextFieldDynamicColor()
    
    txtLastName.setLeftPaddingPoints(10.0)
    txtLastName.DynamictextBorder()
    txtLastName.setTextFieldDynamicColor()
    
    txtAddress.setLeftPaddingPoints(10.0)
    txtAddress.DynamictextBorder()
    txtAddress.setTextFieldDynamicColor()
    
    txtLandmark.setLeftPaddingPoints(10.0)
    txtLandmark.DynamictextBorder()
    txtLandmark.setTextFieldDynamicColor()
    
    txtPincode.setLeftPaddingPoints(10.0)
    txtPincode.DynamictextBorder()
    txtPincode.setTextFieldDynamicColor()
    
    txtMobileNo.setLeftPaddingPoints(10.0)
    txtMobileNo.DynamictextBorder()
    txtMobileNo.setTextFieldDynamicColor()
    
    txtCountry.setLeftPaddingPoints(10.0)
    txtCountry.DynamictextBorder()
    txtCountry.setTextFieldDynamicColor()
    
    txtState.setLeftPaddingPoints(10.0)
    txtState.DynamictextBorder()
    txtState.setTextFieldDynamicColor()
    
    txtCity.setLeftPaddingPoints(10.0)
    txtCity.DynamictextBorder()
    txtCity.setTextFieldDynamicColor()
    
    btnAddAddress.layer.cornerRadius = 20.0
    btnAddAddress.DynamicBtnTitleColor()
  }
  
  //Billing Address
  func setBorderPropertiesForTextFieldForBilling()  {
    
    txtBillingFirstName.setLeftPaddingPoints(10.0)
    txtBillingFirstName.DynamictextBorder()
    txtBillingFirstName.setTextFieldDynamicColor()
    
    txtBillingLastName.setLeftPaddingPoints(10.0)
    txtBillingLastName.DynamictextBorder()
    txtBillingLastName.setTextFieldDynamicColor()
    
    txtBillingAddress.setLeftPaddingPoints(10.0)
    txtBillingAddress.DynamictextBorder()
    txtBillingAddress.setTextFieldDynamicColor()
    
    txtBillingLandmark.setLeftPaddingPoints(10.0)
    txtBillingLandmark.DynamictextBorder()
    txtBillingLandmark.setTextFieldDynamicColor()
    
    txtBillingPincode.setLeftPaddingPoints(10.0)
    txtBillingPincode.DynamictextBorder()
    txtBillingPincode.setTextFieldDynamicColor()
    
    txtBillingMobileNo.setLeftPaddingPoints(10.0)
    txtBillingMobileNo.DynamictextBorder()
    txtBillingMobileNo.setTextFieldDynamicColor()
    
    txtBillingCountry.setLeftPaddingPoints(10.0)
    txtBillingCountry.DynamictextBorder()
    txtBillingCountry.setTextFieldDynamicColor()
    
    txtBillingState.setLeftPaddingPoints(10.0)
    txtBillingState.DynamictextBorder()
    txtBillingState.setTextFieldDynamicColor()
    
    txtBillingCity.setLeftPaddingPoints(10.0)
    txtBillingCity.DynamictextBorder()
    txtBillingCity.setTextFieldDynamicColor()
  }
  
  func setRecordForShippingAddress()  {
    
    strFirstName = txtFirstName.text!
    strLastName = txtLastName.text!
    strAddress = txtAddress.text!
    strCountry = txtCountry.text!
    strState = txtState.text!
    strCity = txtCity.text!
    strPincode = txtPincode.text!
    strMobileNo = txtMobileNo.text!
    strLandmark = txtLandmark.text!
    
    strFirstName = strFirstName.trimmed()
    strLastName = strLastName.trimmed()
    strAddress = strAddress.trimmed()
    strCountry = strCountry.trimmed()
    strCity = strCity.trimmed()
    strState = strState.trimmed()
    strPincode = strPincode.trimmed()
    strMobileNo = strMobileNo.trimmed()
    strLandmark = strLandmark.trimmed()
  }
  
  func setRecordForBillingAddress()  {
    
    strBillingFirstName = txtBillingFirstName.text!
    strBillingLastName = txtBillingLastName.text!
    strBillingAddress = txtBillingAddress.text!
    strBillingCountry = txtBillingCountry.text!
    strBillingState = txtBillingState.text!
    strBillingCity = txtBillingCity.text!
    strBillingPincode = txtBillingPincode.text!
    strBillingMobileNo = txtBillingMobileNo.text!
    strBillingLandmark = txtBillingLandmark.text!
    
    strBillingFirstName = strBillingFirstName.trimmed()
    strBillingLastName = strBillingLastName.trimmed()
    strBillingAddress = strBillingAddress.trimmed()
    strBillingCountry = strBillingCountry.trimmed()
    strBillingState = strBillingState.trimmed()
    strBillingCity = strBillingCity.trimmed()
    strBillingPincode = strBillingPincode.trimmed()
    strBillingMobileNo = strBillingMobileNo.trimmed()
    strBillingLandmark = strBillingLandmark.trimmed()
  }
  
  // MARK: - Validation Method
  func checkValidation() -> Bool {
    
    var isValid: Bool = true
    
    setRecordForShippingAddress()
    
    if PIValidation.isBlankString(str: strFirstName) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter First Name...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strLastName) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Last Name...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strAddress) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Address...!")
      return isValid
      
    } else if (PIValidation.isBlankString(str: strLandmark)) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Landmark!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strPincode) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Pincode...!")
      return isValid
      
    }  else if strPincode.characters.count != 6  {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Enter Valid PinCode...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strCountry)  {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Select Country...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strState) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Select State...!")
      return isValid
      
    }  else if PIValidation.isBlankString(str: strCity) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter City...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strMobileNo) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter MobileNo...!")
      return isValid
      
    } else if strMobileNo.characters.count < 10 || strMobileNo.characters.count > 15 {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Enter Valid Mobile No...!")
      return isValid
    }
    return isValid
  }
  
  func checkValidation2() -> Bool {
    
    var isValid: Bool = true
    
    setRecordForBillingAddress()
    
    if PIValidation.isBlankString(str: strBillingFirstName) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter First Name...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strBillingLastName) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Last Name...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strBillingAddress) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Address...!")
      return isValid
      
    } else if (PIValidation.isBlankString(str: strBillingLandmark)) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Landmark!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strBillingPincode) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Pincode...!")
      return isValid
      
    }  else if strBillingPincode.characters.count != 6  {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Enter Valid PinCode...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strBillingCountry)  {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Select Country...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strBillingState) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Select State...!")
      return isValid
      
    }  else if PIValidation.isBlankString(str: strBillingCity) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter City...!")
      return isValid
      
    } else if PIValidation.isBlankString(str: strBillingMobileNo) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Mobile No...!")
      return isValid
      
    } else if strBillingMobileNo.characters.count < 10 || strBillingMobileNo.characters.count > 15 {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Enter Valid Mobile No...!")
      return isValid
    }
    return isValid
  }
  
  //First Time User Have no any address
  func callbillingService() {
    APPDELEGATE.chkAddAddress = true
    APPDELEGATE.chkFirstAddress = true
    APPDELEGATE.isBilling = true
    self.isShipping = "1"
    
    self.strBillingFirstName = self.strFirstName
    self.strBillingLastName = self.strLastName
    self.strBillingAddress = self.strAddress
    self.strBillingCity = self.strCity
    self.strBillingPincode = self.strPincode
    self.strBillingState = self.strState
    self.strBillingMobileNo = self.strMobileNo
    self.strBillingLandmark = self.strLandmark
    self.strBillingCountry = self.strCountry
    self.strBillingState = self.strState
    
    self.CallAddAddressForBiling()
  }
}
