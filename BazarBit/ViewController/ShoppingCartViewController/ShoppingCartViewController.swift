//
//  ShoppingCartViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 11/1/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Toaster
import Alamofire

class ShoppingCartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var prodDetailVC:ProductDetailsVC!
    
    @IBOutlet var viewNavigation : UIView!    
    @IBOutlet var tblShoppingCart: UITableView!
    
    @IBOutlet var ScrollShoppingCart: UIScrollView!
    @IBOutlet var viewCheckOut: UIView!
    @IBOutlet var viewEmptyCart: UIView!
    
    @IBOutlet weak var viewBottomSize: NSLayoutConstraint!
    @IBOutlet var btnTotalItem: UIButton!
    @IBOutlet var btnCheckOutItem: UIButton!
    
    @IBOutlet var btnContinueShopping: UIButton!
    @IBOutlet var lableEmptyMsg: UILabel!
    @IBOutlet var lableStartMsg: UILabel!
    
    var arrShoppingCart: [AnyObject] = []
    var tabbar: TabbarVC!
    
    var strType: String = ""
    var strOrderCartId: String = ""
    var strUpdateQuanity: String = ""
    
    var isClick: Bool = false
    
    // Cart Count Variables
    var dicResponceCartTotalList:[String:AnyObject] = [:]
    
    // MARK: - UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Every time push to shopping cart
        APPDELEGATE.shoppingCartVC = self
       // tabbar.createTabbar()
        
        self.tabBarController?.tabBar.isHidden = false
       // self.hidesBottomBarWhenPushed = false
        
        if APPDELEGATE.chkBottomBar == true {
            viewBottomSize.constant = 50
           // APPDELEGATE.chkBottomBar = false
        }
       
        callService_ShoppingCartList()
        // Do any additional setup after loading the view.
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        btnTotalItem.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
        
        btnCheckOutItem.setTitleColor(UIColor(hexString: Constants.BUTTON_TEXT_COLOR), for: .normal)
        btnCheckOutItem.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
        
        btnContinueShopping.setTitleColor(UIColor(hexString: Constants.BUTTON_TEXT_COLOR), for: .normal)
        btnContinueShopping.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
        
        lableEmptyMsg.textColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
        lableStartMsg.textColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APPDELEGATE.chkAddAddress = false
        
        if APPDELEGATE.chkListingBack == true {
            viewBottomSize.constant = 0
            APPDELEGATE.chkListingBack = false
        }
         //self.hidesBottomBarWhenPushed = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblShoppingCart {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblShoppingCart {
            return arrShoppingCart.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblShoppingCart {
            
            let identifier : String = "ShoppingCartCell"
            
            let shoppingCartCell : ShoppingCartCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ShoppingCartCell
            
            let dict: [String: AnyObject] = arrShoppingCart[indexPath.row] as! [String: AnyObject]
            
            if (dict["product_data"] as? [String:AnyObject]) != nil {
                let productData: [String: AnyObject] = dict["product_data"] as! [String: AnyObject]
                if (productData["main_image"] as? [String :AnyObject]) != nil {
                    
                    let mainImage: [String: AnyObject] = productData["main_image"] as! [String: AnyObject]
                    
                    var strImage: String = PIService.object_forKeyWithValidationForClass_String(dict: mainImage, key: "main_image")
                    strImage = GetImageUrl(strurl: strImage)
                    let strurl: URL = URL(string: strImage)!
                    shoppingCartCell.imgShoppingView.kf.setImage(with: strurl)
                }
                
                let strName: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "name")
                let strDescribtion: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "short_description")
                
                var strPrize: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "price")
                strPrize = BazarBitCurrency.setPriceSign(strPrice: strPrize)
                
                var attributedPrizeString = NSAttributedString(string: strPrize)
                attributedPrizeString = attributedPrizeString.strikethrough()
                
                var strSalePrice: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "sale_price")
                strSalePrice = String(format: "%.2f", strSalePrice.toFloat()!)
                let strSalePrize = BazarBitCurrency.setPriceSign(strPrice: strSalePrice)
                
                print(strSalePrice)
                
                if strSalePrice == "0" || strSalePrice == "0.00" {
                    shoppingCartCell.lblPrize.text = strPrize
                    shoppingCartCell.lblPrize.setTextLabelDynamicTextColor()
                    shoppingCartCell.lblSalePrize.isHidden = true
                    shoppingCartCell.lblSalePriceWidth.constant = 0.0
                }else {
                    shoppingCartCell.lblPrize.attributedText = attributedPrizeString
                    shoppingCartCell.lblSalePrize.text = strSalePrize
                    shoppingCartCell.lblSalePrize.isHidden = false
                    shoppingCartCell.lblSalePriceWidth.constant = 60.0
                }
                
                let strQuantity: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_quantity")
                
                shoppingCartCell.lblName.text = strName
                shoppingCartCell.lblDescribition.text = strDescribtion
                shoppingCartCell.lblQuantity.text = strQuantity
                
                shoppingCartCell.viewShoppingCart.applyShadowWithColor(.lightGray)
                
                shoppingCartCell.buttonPlus.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
                
                shoppingCartCell.buttonMinus.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
                
                return shoppingCartCell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if Constants.DeviceType.IS_IPAD {
            
            let dict: [String: AnyObject] = arrShoppingCart[indexPath.row] as! [String: AnyObject]
            
            if (dict["product_data"] as? [String:AnyObject]) != nil {
                let productData: [String: AnyObject] = dict["product_data"] as! [String: AnyObject]
                let strDescribtion: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "short_description")
                
                var width: CGFloat = 0
                width = UIScreen.main.bounds.width - 350
                let size: CGSize = CGSize(width: width, height: 1000.0)
                
                var strDiscriptionHeight:CGFloat = Constants.HeightforLabel(text: strDescribtion, font: UIFont.systemFont(ofSize: 18.0), maxSize: size)
                
                if strDiscriptionHeight <=  60 {
                    strDiscriptionHeight = 60
                }
                return 130 - 60 + strDiscriptionHeight
            }
            
        } else {
            
            let dict: [String: AnyObject] = arrShoppingCart[indexPath.row] as! [String: AnyObject]
            
            if (dict["product_data"] as? [String:AnyObject]) != nil {
                let productData: [String: AnyObject] = dict["product_data"] as! [String: AnyObject]
                let strDescribtion: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "short_description")
                
                var width: CGFloat = 0
                
                width = UIScreen.main.bounds.width - 100
                
                let size: CGSize = CGSize(width: width, height: 1000.0)
                
                var strDiscriptionHeight:CGFloat = Constants.HeightforLabel(text: strDescribtion, font: UIFont.systemFont(ofSize: 12.0), maxSize: size)
                
                if strDiscriptionHeight <=  30 {
                    strDiscriptionHeight = 30
                }
                return 100 - 30 + strDiscriptionHeight
            }
        }
        return 0.0
    }
    
    // MARK: - Action Method
    @IBAction func btnBack(_ sender: Any)  {
        APPDELEGATE.variationDisable = true
        self.navigationController?.popViewController(animated: true)
        GetCartCountData()
    }
    
    @IBAction func btnCountinueShopping(_ sender: Any)  {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCheckOut(_ sender: Any)  {
        UserDefaults.standard.removeObject(forKey: BazarBit.BILLINGADDRESS)
        UserDefaults.standard.removeObject(forKey: BazarBit.SHIPPINGADDRESS)
        APPDELEGATE.chklastOrder = false
        APPDELEGATE.isBilling = true
        APPDELEGATE.chkLablecheck = "Shipping"
        //APPDELEGATE.strvalidate = "Shipping"
        APPDELEGATE.checkDisplayBillingRecord = true

        APPDELEGATE.chkProfile = false
        APPDELEGATE.strChkProfile = false
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
            self.callService_LastOrderService()
            
        } else {
            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                myDelegate.openLoginPage()
                //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
            }
        }
    }
    
    @IBAction func btnMinusQuantity(_ sender: AnyObject)  {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblShoppingCart)
        let indexPath = self.tblShoppingCart.indexPathForRow(at: buttonPosition)
        
        let dict: [String: AnyObject] = arrShoppingCart[indexPath!.row] as! [String: AnyObject]
        
        strOrderCartId = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "ordercart_id")
        strUpdateQuanity = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_quantity")
        strType = "0"
        
        var updateQuantity: Int = 0
        
        if (strUpdateQuanity.toInt() != nil) {
            updateQuantity = strUpdateQuanity.toInt()!
        }
        
        if updateQuantity > 1 {
            if isClick == false {
                isClick = true
                callService_UpdateCartItemQty()
            }
        } else {
            PIAlertUtils.displayAlertWithMessage("Minimum quantity reached!")
        }
    }
    
    @IBAction func btnPlusQuanity(_ sender: AnyObject)  {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblShoppingCart)
        let indexPath = self.tblShoppingCart.indexPathForRow(at: buttonPosition)
        
        let dict: [String: AnyObject] = arrShoppingCart[indexPath!.row] as! [String: AnyObject]
        
        strOrderCartId = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "ordercart_id")
        strUpdateQuanity = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_quantity")
        strType = "1"
        
        var updateQuantity: Int = 0
        
        if (strUpdateQuanity.toInt() != nil) {
            updateQuantity = strUpdateQuanity.toInt()!
        }
        
        if updateQuantity < 5 {
            if isClick == false {
                isClick = true
                callService_UpdateCartItemQty()
            }
        } else {
            PIAlertUtils.displayAlertWithMessage("Maximum quantity reached!")
        }
    }
    
    @IBAction func btnRemoveCart(_ sender: AnyObject)  {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblShoppingCart)
        let indexPath = self.tblShoppingCart.indexPathForRow(at: buttonPosition)
        
        let dict: [String: AnyObject] = arrShoppingCart[indexPath!.row] as! [String: AnyObject]
        
        strOrderCartId = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "ordercart_id")
        callService_RemoveCartItem()
        GetCartCountData()
    }
    
    // MARK: - Service Method
    func callService_LastOrderService() {
        
        let strurl: String = BazarBit.GetLastOrderService()
        let url: URL = URL(string: strurl)!
        
        let strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        let parameter: Parameters = [:]
        
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                
                if (dictResponse["payload"] as? [String :AnyObject]) != nil {
                    
                    let dict: [String: AnyObject] = dictResponse["payload"] as! [String : AnyObject]
                    print(dict)
                    
                    if dict.count == 0 {
                        self.callService_AddressList()
                    }else {
                        let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                        
                        //checkOutVC.shoppingCartVC = self
                        self.navigationController?.pushViewController(checkOutVC, animated: true)
                    }
                } else {
                    self.callService_AddressList()
                }
                
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func callService_AddressList()  {
        
        let strurl: String = BazarBit.GetaddressListService()
        
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter: Parameters = [:]
        parameter["address_type"] = "0"
        print(parameter)
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                
                if (dictResponse["payload"] as? [AnyObject]) == nil {
                    
                    let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
                    
                    self.navigationController?.pushViewController(addAddressVC, animated: true)
                }else {
                    
                    APPDELEGATE.chklastOrder = true
                    let arrPayLoadList: [AnyObject] = dictResponse["payload"] as! [AnyObject]
                    
                    if arrPayLoadList.count > 0 {
                        
                        let checkOutViewControllerObj: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                        
                        checkOutViewControllerObj.strAddressType = "1"
                        checkOutViewControllerObj.strPushFromShopping = "false"
                        
                        let addressListViewControllerObj: AddressListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
                        
                        //                        addressListViewControllerObj.checkOutVC = checkOutViewControllerObj
                        
                        addressListViewControllerObj.fromVC = "CheckOut"
                        addressListViewControllerObj.strAddressType = "0"
                        self.navigationController?.pushViewController(addressListViewControllerObj, animated: true)
                        
                    } else {
                        
                        let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
                        
                        self.navigationController?.pushViewController(addAddressVC, animated: true)
                    }
                }
            }else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    // GetUpdateCartItemQtyService
    func callService_ShoppingCartList()  {
        
        let strurl: String = BazarBit.GetCartitemsService()
        var strAuthToken: String = ""
        let url: URL = URL(string: strurl)!
        
        if (UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) != nil) {
            strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        } else {
            strAuthToken  = ""
        }
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
            let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
            let strSessionId: String = ""
            
            parameter["session_id"] = strSessionId
            parameter["user_id"] = strUserId
            
        } else {
            let strUserId: String = ""
            let strSessionId: String = BazarBit.getSessionId()
            
            parameter["session_id"] = strSessionId
            parameter["user_id"] = strUserId
        }
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dictResponse: [String: AnyObject] = responseDict
            
            print(url)
            BazarBitLoader.stopLoader()
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                
                if (dictResponse["payload"] as? [String :AnyObject]) != nil {
                    
                    let dict: [String: AnyObject] = dictResponse["payload"] as! [String : AnyObject]
                    
                    if (dict["cart_items"] as? [AnyObject]) != nil {
                        
                        self.arrShoppingCart = dict["cart_items"] as! [AnyObject]
                        
                        if self.arrShoppingCart.count > 0 {
                            var totalPrize: Int = 0
                            for i in 0...self.arrShoppingCart.count - 1 {
                                
                                let dict:[String: AnyObject] = self.arrShoppingCart[i] as! [String: AnyObject]
                                
                                if (dict["product_data"] as? [String:AnyObject]) != nil {
                                    let productData:[String: AnyObject] = dict["product_data"] as! [String: AnyObject]
                                    let strProductQuantity: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_quantity")
                                    
                                    var localproductQuantity: Int = 0
                                    if (strProductQuantity.toInt() != nil) {
                                        localproductQuantity = strProductQuantity.toInt()!
                                    }
                                    
                                    var strlocalSalePrize:String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "sale_price")
                                    
                                    if strlocalSalePrize.toInt() == 0 {
                                        strlocalSalePrize = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "price")
                                    }
                                    
                                    var localSalePrize: Int = 0
                                    
                                    if (strlocalSalePrize.toInt() != nil) {
                                        localSalePrize = strlocalSalePrize.toInt()!
                                    }
                                    totalPrize = totalPrize + (localproductQuantity * localSalePrize )
                                    
                                    self.viewCheckOut.isHidden = false
                                    
                                    var strLocalTotalPrize: String = "\(totalPrize)"
                                    strLocalTotalPrize = BazarBitCurrency.setPriceSign(strPrice: strLocalTotalPrize)
                                    let strDisplayTotalPrize: String = "\(strLocalTotalPrize)(\(self.arrShoppingCart.count) items)"
                                    
                                    self.btnTotalItem.setTitle(strDisplayTotalPrize, for: .normal)
                                    
                                    self.ShowHideView()
                                    self.tblShoppingCart.dataSource = self
                                    self.tblShoppingCart.delegate = self
                                    self.tblShoppingCart.reloadData()
                                }
                            }
                            
                        } else {
                            self.ShowHideView()
                        }
                        
                    } else {
                        self.ShowHideView()
                    }
                    
                } else {
                    self.ShowHideView()
                }
                
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func callService_UpdateCartItemQty()  {
        
        let strurl: String = BazarBit.GetUpdateCartItemQtyService()
        var strAuthToken: String = ""
        let url: URL = URL(string: strurl)!
        
        if (UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) != nil) {
            strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        } else {
            strAuthToken  = ""
        }
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
            let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
            let strSessionId: String = ""
            
            parameter["session_id"] = strSessionId
            parameter["user_id"] = strUserId
            
        } else {
            let strUserId: String = ""
            let strSessionId: String = BazarBit.getSessionId()
            
            parameter["session_id"] = strSessionId
            parameter["user_id"] = strUserId
        }
        
        parameter["ordercart_id"] = strOrderCartId
        parameter["quantity"] = strUpdateQuanity
        parameter["type"] = strType
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            self.isClick = false
            let dictResponse: [String: AnyObject] = responseDict
            BazarBitLoader.stopLoader()
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                //PIAlertUtils.displayAlertWithMessage(message)
                self.callService_ShoppingCartList()
                
            } else {
                
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func callService_RemoveCartItem()  {
        
        let strurl: String = BazarBit.GetRemoveCartItemService()
        var strAuthToken: String = ""
        let url: URL = URL(string: strurl)!
        
        if (UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) != nil) {
            strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        } else {
            strAuthToken  = ""
        }
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
            let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
            let strSessionId: String = ""
            
            parameter["session_id"] = strSessionId
            parameter["user_id"] = strUserId
            
        } else {
            
            let strUserId: String = ""
            let strSessionId: String = BazarBit.getSessionId()
            
            parameter["session_id"] = strSessionId
            parameter["user_id"] = strUserId
        }
        
        parameter["cartitem_id"] = strOrderCartId
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            let dictResponse: [String: AnyObject] = responseDict
            
            BazarBitLoader.stopLoader()
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                PIAlertUtils.displayAlertWithMessage(message)
                self.callService_ShoppingCartList()
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func GetCartCountData() {
        
        let strurl:String = BazarBit.CountCartTotalService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }
        else
        {
            let strSessionId:String = BazarBit.getSessionId()
            parameter["session_id"] = strSessionId
            parameter["user_id"] = ""
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceCartTotalList = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCartTotalList, key: "status")
            
            if status == "ok"
            {
                if self.dicResponceCartTotalList["payload"] != nil
                {
                    let payload:[String:AnyObject] = self.dicResponceCartTotalList["payload"] as! [String:AnyObject]
                    print(payload)
                    
                    let strCartTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "cart_items")
                    UserDefaults.standard.set(strCartTotal, forKey: BazarBit.CARTTOTALCOUNT)
                }
            }
        })
    }
    
    // MARK: - Helper Method
    
    func ShowHideView()  {
        
        if arrShoppingCart.count > 0 {
            ScrollShoppingCart.isHidden  = false
            viewCheckOut.isHidden = false
            viewEmptyCart.isHidden = true
        } else {
            ScrollShoppingCart.isHidden  = true
            viewCheckOut.isHidden = true
            viewEmptyCart.isHidden = false
        }
    }
    
    func GetImageUrl(strurl: String) -> String {
        
        var strUrl = strurl
        
        strUrl = strUrl.replacingOccurrences(of: "[", with: "")
        strUrl = strUrl.replacingOccurrences(of: "\"", with: "")
        strUrl = strUrl.replacingOccurrences(of: "]", with: "")
        strUrl = strUrl.replacingOccurrences(of: " ", with: "%20")
        
        return strUrl
    }
}

class ShoppingCartCell: UITableViewCell {
    
    @IBOutlet var imgShoppingView: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDescribition: UILabel!
    @IBOutlet var lblPrize: UILabel!
    @IBOutlet var lblSalePrize: UILabel!
    @IBOutlet var lblQuantity: UILabel!
    
    @IBOutlet var lblSalePriceWidth: NSLayoutConstraint!
    @IBOutlet var lblBasePriceWidth: NSLayoutConstraint!
    
    @IBOutlet var buttonMinus : UIButton!
    @IBOutlet var buttonPlus : UIButton!
    
    @IBOutlet var viewShoppingCart: UIView!
    
    override func awakeFromNib() {
        
        self.contentView.applyShadowDefault()
        lblName.setTextLabelDynamicTextColor()
        lblDescribition.setTextLabelLightColor()
        lblPrize.setTextLabelLightColor()
        lblSalePrize.setTextLabelDynamicTextColor()
        lblQuantity.setTextLabelDynamicTextColor()
    }
    
}
