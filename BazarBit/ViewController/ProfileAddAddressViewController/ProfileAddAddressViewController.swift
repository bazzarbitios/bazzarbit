//
//  ProfileAddAddressViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 11/04/18.
//  Copyright © 2018 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire

class ProfileAddAddressViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {

    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtAddress: UITextField!
    @IBOutlet var txtLandmark: UITextField!
    @IBOutlet var txtPincode: UITextField!
    @IBOutlet var txtMobileNo: UITextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet weak var btnBilling: UIButton!
    
    @IBOutlet var viewCountry: UIView!
    @IBOutlet var viewState: UIView!
    
    @IBOutlet weak var pickerCountry: UIPickerView!
    @IBOutlet weak var pickerState: UIPickerView!
    @IBOutlet weak var btnAddAddress: UIButton!
    
    var strFirstName: String = ""
    var strLastName: String = ""
    var strAddress: String = ""
    var strLandmark: String = ""
    var strPincode: String = ""
    var strMobileNo: String = ""
    var strCountry: String = ""
    var strState: String = ""
    var strCity: String = ""
    var isBilling: Bool = false
    var isShipping: String = "1"
    
    var dictEdit: [String: AnyObject] = [:]
    //var strAddressType : String = ""
    var arrCountryCode: [AnyObject] = []
    var arrState: [AnyObject] = []
    
    @IBOutlet var viewNavigation: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addPropertyCountryStatePopUp()
        self.setBorderPropertiesForTextField()
        self.CallGetCountryList()
        APPDELEGATE.chkProfile = false
        APPDELEGATE.strChkProfile = false
        btnBilling.setImage(#imageLiteral(resourceName: "square"), for: .normal)
        
        self.viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        self.btnAddAddress.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
        
    }
    
    //MARK:- UITextfield Delegate Method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == ""
        {
            return true
            
        } else if txtFirstName == textField {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }
            if (string.isNumber()) {
                return false
            }
            
        } else if txtLastName == textField {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }
            if (string.isNumber()) {
                return false
            }
            
        } else if txtAddress == textField {
            if (textField.text?.characters.count)! >= 50 {
                return false
            } else {
                return true
            }
            
        } else if textField == txtCountry {
            return false
            
        } else if textField == txtState {
            return false
            
        } else if textField == txtMobileNo {
            if (textField.text?.characters.count)! >= 15 {
                return false
            }
            if !(string.isNumber()) {
                return false
            }
            
        } else if txtLandmark == textField {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }
            
        } else if textField == txtPincode {
            if (textField.text?.characters.count)! >= 6 {
                return false
            }
            if !(string.isNumber()) {
                return false
            }
            
        } else if textField == txtCity {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
        }
        
        if textField == txtFirstName || textField == txtLastName || textField == txtLandmark || textField == txtCity {
            let aSet = NSCharacterSet(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    
    //MARK:- Button back clicked
    @IBAction func btnBack(_ sender: Any) {
        APPDELEGATE.chkProfile = true
        APPDELEGATE.strChkProfile = true
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Button Add Address
    @IBAction func btnAddAddress(_ sender: Any) {
        if checkValidation() {
            self.CallAddAddressForShipping()
        }
    }
    
    //MARK:- Button Country Clicked
    @IBAction func btnCountry(_ sender: Any) {
        
        if arrCountryCode.count > 0 {
            self.view.endEditing(true)
            showPickerPopUp(viewPicker: viewCountry)
        }else {
            PIAlertUtils.displayAlertWithMessage("Country not found...!")
        }
    }
    
    //MARK:- Button Country Done Clicked
    @IBAction func btnContryDone(_ sender: Any) {
        
        hidePopUp(viewPicker: viewCountry)
        
        let row: Int = pickerCountry.selectedRow(inComponent: 0)
        let dict : [String: AnyObject] = arrCountryCode[row] as! [String:AnyObject]
        strCountry = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country_name")
        txtCountry.text = strCountry
        txtState.text = ""
    }
    
    //MARK:- Button Country Cancel Clicked
    @IBAction func btnCountryCancel(_ sender: Any) {
        hidePopUp(viewPicker: viewCountry)
    }
    
    
    //MARK:- Button State Clicked
    @IBAction func btnState(_ sender: Any) {
        
        if txtCountry.text != "" {
            strCountry = txtCountry.text!
            CallGetStateList(strCountryName: strCountry)
        } else {
            PIAlertUtils.displayAlertWithMessage("Select Country!")
        }
    }
    
    //MARK:- Button State Done Clicked
    @IBAction func btnStateDone(_ sender: Any) {
        
        hidePopUp(viewPicker: viewState)
        
        let row: Int = pickerState.selectedRow(inComponent: 0)
        let dict : [String: AnyObject] = arrState[row] as! [String:AnyObject]
        strState = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state_name")
        txtState.text = strState
    }
    
    //MARK:- Button State Cancel Clicked
    @IBAction func btnStateCancel(_ sender: Any) {
        hidePopUp(viewPicker: viewState)
    }
    
    //MARK:- Button Billing Click
    @IBAction func btnBilling(_ sender: Any) {
        
        //Shipping
        if isBilling == false {
            isShipping = "0"
            btnBilling.setImage(#imageLiteral(resourceName: "rightSquare"), for: .normal)
            isBilling = true
            
            //Billing
        } else {
            isShipping = "1"
            btnBilling.setImage(#imageLiteral(resourceName: "square"), for: .normal)
            isBilling = false
        }
    }

    //MARk:- Ser Textfield properties
    func setBorderPropertiesForTextField()  {
        
        txtFirstName.setLeftPaddingPoints(10.0)
        txtFirstName.DynamictextBorder()
        txtFirstName.setTextFieldDynamicColor()
        
        txtLastName.setLeftPaddingPoints(10.0)
        txtLastName.DynamictextBorder()
        txtLastName.setTextFieldDynamicColor()
        
        txtAddress.setLeftPaddingPoints(10.0)
        txtAddress.DynamictextBorder()
        txtAddress.setTextFieldDynamicColor()
        
        txtLandmark.setLeftPaddingPoints(10.0)
        txtLandmark.DynamictextBorder()
        txtLandmark.setTextFieldDynamicColor()
        
        txtPincode.setLeftPaddingPoints(10.0)
        txtPincode.DynamictextBorder()
        txtPincode.setTextFieldDynamicColor()
        
        txtMobileNo.setLeftPaddingPoints(10.0)
        txtMobileNo.DynamictextBorder()
        txtMobileNo.setTextFieldDynamicColor()
        
        txtCountry.setLeftPaddingPoints(10.0)
        txtCountry.DynamictextBorder()
        txtCountry.setTextFieldDynamicColor()
        
        txtState.setLeftPaddingPoints(10.0)
        txtState.DynamictextBorder()
        txtState.setTextFieldDynamicColor()
        
        txtCity.setLeftPaddingPoints(10.0)
        txtCity.DynamictextBorder()
        txtCity.setTextFieldDynamicColor()
        
        btnAddAddress.layer.cornerRadius = 20.0
        btnAddAddress.DynamicBtnTitleColor()
    }
    
    //MARK:- Country State Picker helper Method
    func addPropertyCountryStatePopUp()  {
        
        viewCountry.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewCountry.frame.size.height)
        self.view.addSubview(viewCountry)
        viewCountry.layoutIfNeeded()
        
        viewState.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewState.frame.size.height)
        self.view.addSubview(viewState)
        viewState.layoutIfNeeded()
    }
    
    //MARK:- Show Picker
    func showPickerPopUp(viewPicker: UIView) {
        
        viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewPicker.frame.size.height)
        
        UIView.animate(withDuration: 0.5, animations:{
            viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - viewPicker.frame.size.height , w: UIScreen.main.bounds.width, h: viewPicker.frame.size.height)
        })
    }
    
    //MARK:- Hide Picker
    func hidePopUp(viewPicker: UIView)  {
        
        viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - viewPicker.frame.size.height , w: UIScreen.main.bounds.width, h: viewPicker.frame.size.height)
        
        UIView.animate(withDuration: 0.5, animations:{
            viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewPicker.frame.size.height)
        })
    }
    
    // MARK: - Validation Method
    func checkValidation() -> Bool {
        
        var isValid: Bool = true
        
        setRecordForShippingAddress()
        
        if PIValidation.isBlankString(str: strFirstName) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter First Name...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strLastName) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Last Name...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strAddress) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Address...!")
            return isValid
            
        } else if (PIValidation.isBlankString(str: strLandmark)) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Landmark!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strPincode) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Pincode...!")
            return isValid
            
        }  else if strPincode.characters.count != 6  {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Enter Valid PinCode...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strCountry)  {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Select Country...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strState) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Select State...!")
            return isValid
            
        }  else if PIValidation.isBlankString(str: strCity) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter City...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strMobileNo) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter MobileNo...!")
            return isValid
            
        } else if strMobileNo.characters.count < 10 || strMobileNo.characters.count > 15 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Enter Valid Mobile No...!")
            return isValid
        }
        return isValid
    }
    
    //MARK:- Helper MEthod
    func setRecordForShippingAddress()  {
        
        strFirstName = txtFirstName.text!
        strLastName = txtLastName.text!
        strAddress = txtAddress.text!
        strCountry = txtCountry.text!
        strState = txtState.text!
        strCity = txtCity.text!
        strPincode = txtPincode.text!
        strMobileNo = txtMobileNo.text!
        strLandmark = txtLandmark.text!
        
        strFirstName = strFirstName.trimmed()
        strLastName = strLastName.trimmed()
        strAddress = strAddress.trimmed()
        strCountry = strCountry.trimmed()
        strCity = strCity.trimmed()
        strState = strState.trimmed()
        strPincode = strPincode.trimmed()
        strMobileNo = strMobileNo.trimmed()
        strLandmark = strLandmark.trimmed()
        
    }
    
    // MARK: - PickerView Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerCountry
        {
            return arrCountryCode.count
        }
        else
        {
            return arrState.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerCountry {
            let dict : [String: AnyObject] = arrCountryCode[row] as! [String:AnyObject]
            let strCountryData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country_name")
            return strCountryData
        }
        else
        {
            let dict : [String: AnyObject] = arrState[row] as! [String:AnyObject]
            let strStateData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state_name")
            return strStateData
        }
    }
    
    //MARK:- Service call for Coutry list
    func CallGetCountryList() {
        
        let strurl:String = BazarBit.GetCountryListService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            print(responseDict)
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (response["payload"] as? [AnyObject]) != nil {
                    
                    let payload:[AnyObject] = response["payload"] as! [AnyObject]
                    
                    self.arrCountryCode = payload
                    
                    //Shipping
                    self.pickerCountry.dataSource = self
                    self.pickerCountry.delegate = self
                    self.pickerCountry.reloadAllComponents()
                    
                    //Billing
                    self.pickerCountry.dataSource = self
                    self.pickerCountry.delegate = self
                    self.pickerCountry.reloadAllComponents()
                }
                else
                {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            }
        })
    }
    
    //MARK:- Service call for State list
    func CallGetStateList(strCountryName : String) {
        
        let strurl:String = BazarBit.GetStateListService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["country_name"] = strCountryName
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (response["payload"] != nil)
                {
                    let payload:[AnyObject] = response["payload"] as! [AnyObject]
                    print(payload)
                    self.arrState = payload
                    
                    if self.arrState.count > 0 {
                        
                        self.pickerState.dataSource = self
                        self.pickerState.delegate = self
                        self.pickerState.reloadAllComponents()
                        self.showPickerPopUp(viewPicker: self.viewState)
                        
                    }else {
                        PIAlertUtils.displayAlertWithMessage("State not found...!")
                    }
                }
                else
                {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            }
        })
    }
    
    // MARK: - Service Method For Shipping
    func CallAddAddressForShipping() {
        
        let strurl:String = BazarBit.GetAddAddressService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["first_name"] = strFirstName
        parameter["last_name"] = strLastName
        parameter["address"] = strAddress
        parameter["landmark"] = strLandmark
        parameter["pincode"] = strPincode
        parameter["country"] = strCountry
        parameter["state"] = strState
        parameter["city"] = strCity
        parameter["phone_number"] = strMobileNo
        parameter["is_shipping"] = isShipping
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            print(responseDict)
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                APPDELEGATE.strChkProfile = true
                if APPDELEGATE.chkLablecheck == "" {
                    let checkOutVC: AddressListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
                    
                    self.navigationController?.pushViewController(checkOutVC, animated: true)
                } else {
                    self.navigationController?.popViewController(animated: true)
                    APPDELEGATE.chkLablecheck = ""
                }
                
                //PIAlertUtils.displayAlertWithMessage(message)
            }
            else  {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
}
