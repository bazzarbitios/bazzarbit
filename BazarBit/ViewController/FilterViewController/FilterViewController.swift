//
//  FilterViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/26/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var viewNavigation: UIView!
    @IBOutlet var labelTitle: PICustomTitleLableBlack!
    @IBOutlet var buttonClose: UIButton!
    @IBOutlet var buttonClear: PIClearButton!
    var objMegaMenuVC : MegaMenuIteListViewController!
    
    @IBOutlet var viewButtonBack : UIView!
    @IBOutlet var buttonApply: PIApplyButton!
    @IBOutlet var tableViewFilterList: UITableView!
    
    var tblCellIdentifier : String = "filterTableViewCell"
    var tblButtonCellIdentifier : String = "buttonTableViewCell"
    var arySortingData : [AnyObject] = []
    var arrSelectedAttributed : [AnyObject] = [AnyObject]()
    
    var isTemp : Bool = false
    var isClear : Bool = false
    var arrISExpand : [AnyObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewFilterList.backgroundColor = UIColor(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        tableViewFilterList.layoutIfNeeded()
        self.ServiceCallForGetAttributesSorting()
    }
    
    //MARK: buttonApply_Clicked
    @IBAction func buttonApplyClicked(_ sender: Any) {
        
//        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableViewFilterList)
//        let indexpath = self.tableViewFilterList.indexPathForRow(at:buttonPosition)
//        print(indexpath)
//        let cell = self.tableViewFilterList.cellForRow(at: indexpath!) as! FilterTypeCollectionViewCell
//        cell.isSelect = false
        
//        let filterCell : FilterTypeCollectionViewCell!
//        filterCell.isSelect = false
        objMegaMenuVC.objSelectedData = APPDELEGATE.arrSelect
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: buttonClose_Clicked
    @IBAction func buttonCloseClicked(_ sender: Any) {
        
        let actionSheetController = UIAlertController (title: "Are you sure you want to exit?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        //Add Cancel-Action
        actionSheetController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
        
        //Add Save-Action
        actionSheetController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            self.navigationController?.popViewController(animated: true)
        }))
        
        //present actionSheetController
        self.presentVC(actionSheetController)
        
        
    }
    
    //MARK: buttonClear_Clicked
    @IBAction func buttonClearClicked(_ sender: Any) {
        
        
        let actionSheetController = UIAlertController (title: "Are you sure you want to clear variation?", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        //Add Cancel-Action
        actionSheetController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
        
        //Add Save-Action
        actionSheetController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (actionSheetController) -> Void in
            self.isClear = true
            APPDELEGATE.arrSelect = []
            self.tableViewFilterList.reloadData()
        }))
        
        //present actionSheetController
        self.presentVC(actionSheetController)
    }
    
    //MARK: UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return arySortingData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var dict : [String : AnyObject] = arySortingData[indexPath.section] as! [String : AnyObject]
        
        if indexPath.row == 0 {
            
            if arrISExpand[indexPath.section] as! String == "false" {
                let arr : [AnyObject] = dict["attributesterms"] as! [AnyObject]
                
                if arr.count <= 8 {
                    
                    if arr.count % 4 == 0 {
                        
                        if IS_IPAD_DEVICE() {
                            return CGFloat((arr.count / 4 * 80) + 80)
                        } else if IS_IPHONE_5_OR_5S() {
                            return CGFloat((arr.count / 4 * 50) + 50)
                        } else {
                            return CGFloat((arr.count / 4 * 60) + 60)
                        }
                        
                    } else {
                        let height: Float = Float((arr.count / 4) + 1)
                        if IS_IPAD_DEVICE() {
                            return CGFloat((height * 80) + 80)
                        } else if IS_IPHONE_5_OR_5S() {
                            return CGFloat((height * 50) + 50)
                        } else {
                            return CGFloat((height * 60) + 60)
                        }
                    }
                    
                    
                }
                else
                {
                    if IS_IPAD_DEVICE() {
                        return 200.0
                    }
                    if IS_IPHONE_5_OR_5S() {
                        return 160
                    }
                    return 180.0
                }
            }
            else
            {
                let arr : [AnyObject] = dict["attributesterms"] as! [AnyObject]
                
                if IS_IPAD_DEVICE() {
                    return CGFloat((arr.count / 4 * 120) + 70)
                }
                if IS_IPHONE_5_OR_5S() {
                    return CGFloat((arr.count / 4 * 80) + 40)
                }
                return CGFloat((arr.count / 4 * 100) + 50)
            }
        }
        else
        {
            if IS_IPAD_DEVICE() {
                return 60.0
            }
            return 50.0
        }
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let dict = arySortingData[section] as! [String : AnyObject]
        print(dict)
        
        if (dict["attributesterms"] as? [AnyObject]) != nil {
            let arr : [AnyObject] = dict["attributesterms"] as! [AnyObject]
            if arr.count <= 8 {
                return 1
            }else {
                return 2
            }
        }else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let filterCell : FilterTableViewCell = tableView.dequeueReusableCell(withIdentifier: tblCellIdentifier) as! FilterTableViewCell
            
            let dictData : [String : AnyObject] = arySortingData[indexPath.section] as! [String : AnyObject]
            
            var arrTempData : [AnyObject] = []
            
            if let arr = dictData["attributesterms"] {
                arrTempData = arr as! [AnyObject]
            }
            
            if arrTempData.count <= 8 {
                filterCell.setUpData(arrData: arrTempData, strName: dictData["name"] as! String)
            }
            
            if filterCell.isShowRecord == false
            {
                self.isTemp = false
                arrTempData = Array(arrTempData.prefix(8))
                filterCell.setUpData(arrData: arrTempData, strName: dictData["name"] as! String)
            } else
            {
                self.isTemp = true
                filterCell.setUpData(arrData: arrTempData, strName: dictData["name"] as! String)
            }
            
            filterCell.objFilterVC = self
            return filterCell
        }
        else
        {
            let buttonCell : ButtonTableViewCell = tableView.dequeueReusableCell(withIdentifier: tblButtonCellIdentifier) as! ButtonTableViewCell
            
            buttonCell.buttonDropDown.addTarget(self, action: #selector(buttonDropDown), for: UIControlEvents.touchUpInside)
            buttonCell.buttonDropDown.tag = indexPath.row
            
            return buttonCell
        }
    }
    
    //MARK: Button
    func buttonDropDown(sender : UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewFilterList)
        let indexPath = self.tableViewFilterList.indexPathForRow(at: buttonPosition)
        let index : IndexPath = IndexPath(row: 0, section: (indexPath?.section)!)
        print(indexPath ?? "nil")
        
        let cell : FilterTableViewCell = tableViewFilterList.cellForRow(at: index) as! FilterTableViewCell
        
        cell.isShowRecord = !cell.isShowRecord
        
        if cell.isShowRecord == true {
            sender.setImage(UIImage(named: "dropu"), for: UIControlState.normal)
        }else {
            sender.setImage(UIImage(named: "dropd"), for: UIControlState.normal)
        }
        
        if arrISExpand[(indexPath?.section)!] as! String == "false" {
            arrISExpand[(indexPath?.section)!] = "true" as AnyObject
        }else{
            arrISExpand[(indexPath?.section)!] = "false" as AnyObject
        }
        tableViewFilterList.reloadData()
    }
    
    //MARK: ServiceCallForGetAttributesSorting
    func ServiceCallForGetAttributesSorting() {
        
        let strurl:String = BazarBit.GetAttributesSortingService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        print(headers)
        
        let parameter:Parameters = [:]
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
            let messege = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                self.arySortingData = []
                if (responseDict["payload"] != nil)
                {
                    self.arySortingData = responseDict["payload"] as! [AnyObject]
                    print(self.arySortingData)
                    
                    self.arrISExpand = []
                    
                    for _ in 0...self.arySortingData.count - 1 {
                        self.arrISExpand.append("false" as AnyObject)
                    }
                    
                    self.tableViewFilterList.delegate = self
                    self.tableViewFilterList.dataSource = self
                    self.tableViewFilterList.reloadData()
                }
            }else {
                PIAlertUtils.displayAlertWithMessage(messege)
            }
        })
    }
}
