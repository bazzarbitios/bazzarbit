//
//  ProductDetailsVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 23/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import ImageSlideshow
import Cosmos
import Alamofire
import FBSDKShareKit
import TwitterKit
import TwitterCore
import ReadMoreTextView

class ProductDetailsVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,KIImagePagerDataSource,KIImagePagerDelegate, FBSDKSharingDelegate {
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print(error)
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
    
  
  @IBOutlet weak var slideshow: KIImagePager!
  @IBOutlet var viewNavigation : UIView!
  @IBOutlet var labelTitleRelatedProduct: UILabel!
  @IBOutlet var labelTitleProductDetail: UILabel!
  
  @IBOutlet weak var viewcart: UIView!
  @IBOutlet weak var lblCartTotal: UILabel!
  @IBOutlet weak var lblCartTotalWidthConstraints: NSLayoutConstraint!
  @IBOutlet weak var lblToplableName: UILabel!
  @IBOutlet weak var lblProductName: UILabel!
  @IBOutlet weak var lblShortDescription: UILabel!
  @IBOutlet weak var lblLongDescription: UILabel!
  @IBOutlet weak var lblsaleprice: UILabel!
  @IBOutlet var lblSalePriceWidth: NSLayoutConstraint!
  @IBOutlet var viewPriceWidth: NSLayoutConstraint!

  @IBOutlet weak var lblbasePrice: UILabel!
  @IBOutlet var lblBasePriceWidth: NSLayoutConstraint!
  
  @IBOutlet weak var bottomView: UIView!
  
  @IBOutlet weak var viewBottomSize: NSLayoutConstraint!
  @IBOutlet weak var bottomViewHeightConstraints: NSLayoutConstraint!
  @IBOutlet weak var btnWishList: UIButton!
  @IBOutlet weak var btnWishListHeightConstraints: NSLayoutConstraint!
  
  @IBOutlet weak var btnWishlistwidthConstraints: NSLayoutConstraint!
  @IBOutlet weak var btnAddToCart: UIButton!
  @IBOutlet weak var btnAddToCartHeightConstraints: NSLayoutConstraint!
  @IBOutlet weak var btnAddTocartWidthconstraints: NSLayoutConstraint!
  @IBOutlet var lableTitleCheckDelivery: UILabel!
  
  @IBOutlet weak var btnCheck: UIButton!
  @IBOutlet weak var btnBack: UIButton!
  @IBOutlet weak var txtPincode: UITextField!
  @IBOutlet var lableProductStatus: UILabel!
  @IBOutlet var viewHeightPIncode: NSLayoutConstraint!
  
  @IBOutlet weak var cltRelatedProducts: UICollectionView!
  @IBOutlet weak var mainRatingView: CosmosView!
  @IBOutlet weak var LongDescriptionHeightConstraints: NSLayoutConstraint!
  @IBOutlet weak var tblUserReviewHeightConstraints: NSLayoutConstraint!
  
    
    @IBOutlet weak var btn_ShareSocial: UIButton!
    
  
  var isSelect : Bool = true
  var selectedName : String = ""
  var count:String = ""
  var dictFirst:[String:AnyObject] = [:]
  var dictSecond:[String:AnyObject] = [:]
  var defaultvariationCount: Int = 0
  var arrMergeImages:[String] = []
  var chkVariationId:Bool = false
  var chkChangeGoToCart:Bool = false
  var chkCheckPincode : Bool = false
  
  //VARIATION
  var arrTermsAttributeID: [String] = []
  var arrAttributeID: [String] = []
  var Variation_Prod_ID : String = ""
  var chkSalevalue:Bool = false
  // Cart Count Variables
  var dicResponceCartTotalList:[String:AnyObject] = [:]
  
  // For Popular Review Tab
  @IBOutlet weak var tblReview: UITableView!
  var arrUserReview:[AnyObject] = []
  
  @IBOutlet weak var lblPopularReview: UILabel!  
  @IBOutlet weak var lblPopularReviewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var readMoreTextView: ReadMoreTextView!
    
  // For Check Delivery option Tab
  @IBOutlet weak var lblExpectedDelivery: UILabel!
  @IBOutlet weak var lblCashOnDelivery: UILabel!
  @IBOutlet weak var lblReturnAvailable: UILabel!
  
  var ExpectedDelivery:String = ""
  var ReturnAvailabe:String = ""
  
  @IBOutlet weak var btnCashOnDelivery: UIButton!
  
  // For Variation
  @IBOutlet weak var tblVariation: UITableView!
  @IBOutlet weak var tblVariationHeightConstraints: NSLayoutConstraint!
  
  var payloadProductSlug:[String:AnyObject] = [:]
  var arrDisableProductID:[String] = []
  var arrDisableAttributeName:[String] = []
  var productType:String = ""
  var arrVariation:[AnyObject] = []
  var aryAllImagesvariation : [String] = [String]()
  
  // Service Variables
  var dicResponceproductDetails:[String:AnyObject] = [:]
  var arrSlideShow:[String] = [String]()
  var arrSlideShowVariation:[String] = [String]()
  var arrSlideShowSetVariation:[String] = [String]()
  var chkvariation:Bool = false
  
  var dicResponceRelatedProductDetails:[String:AnyObject] = [:]
  var arrRelatedProducts:[AnyObject] = []
  var arrRelatedProductImages:[String] = [String]()
  
  var dictCheckItemWishlist:[String:AnyObject] = [:]
  var dictWishlist:[String:AnyObject] = [:]
  
  var WishList:Bool = false
  var dicResponceUserReviews:[String:AnyObject] = [:]
  var dicResponcePinCode:[String:AnyObject] = [:]
  var strPinCode:String = ""
  
  //Variation Code : Ajay
  var arrUsedVariation:[AnyObject] = []
  var aryVariationItem : [AnyObject] = [AnyObject]()
  var aryDafaultVariationItem : [AnyObject] = [AnyObject]()
  
  var AvaialbleDelivery:String = ""
  var NotAvailableDelivery:String = ""
  var Checksts:String = ""
  
  var product_ID : String = ""
  var Product_Name : String = ""
  var Product_Sale_Price : String = ""
  var Product_Base_Price : String = ""
  
  var globalRating:Double = 0.0
  var tabbar: TabbarVC!
  
   
    var str_URL_ShareToSocialMedia: String = "https://www.pureites.com/contactus"
    
  // MARK:- UIVIEW METHODS
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationController?.isNavigationBarHidden = true
    viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    
    labelTitleRelatedProduct.setTextLabelDynamicTextColor()
    labelTitleProductDetail.setTextLabelDynamicTextColor()
    lableTitleCheckDelivery.setTextLabelDynamicTextColor()
    lblShortDescription.setTextLabelLightColor()
    lblLongDescription.setTextLabelLightColor()
    lblbasePrice.setTextLabelLightColor()
    lblsaleprice.setTextLabelDynamicTextColor()
    
    btnWishList.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
    btnAddToCart.setButtonDynamicColor()
    btnAddToCart.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
    
    //txtPincode.DynamicPlaceholdertextColor()
    txtPincode.setTextFieldDynamicColor()
    txtPincode.keyboardType = .numberPad
    
    // For Check Delivery option Tab
    lblExpectedDelivery.setTextLabelDynamicTextColor()
    lblCashOnDelivery.setTextLabelDynamicTextColor()
    lblReturnAvailable.setTextLabelDynamicTextColor()
    
    txtPincode.setLeftPaddingPoints(10.0)
    //txtPincode.DynamictextBorder()
    
    btnCheck.DynamicViewBorder()
    btnCheck.setButtonDynamicColor()
    btnCheck.Shadow()
    
    //Review
    lblPopularReview.setTextLabelDynamicTextColor()
    lblProductName.setTextLabelDynamicTextColor()
    
    // Set Equal width Constraints
    btnWishlistwidthConstraints.constant = self.view.frame.size.width / 2
    btnAddTocartWidthconstraints.constant = self.view.frame.size.width / 2
    
    // Set 0/1 Hide Butoon Or Not
    if APPDELEGATE.is_wishlist_active == "1" && APPDELEGATE.is_cart_active == "0" {
      if Constants.DeviceType.IS_IPAD {
        btnAddTocartWidthconstraints.constant = 0
      }else {
        btnAddTocartWidthconstraints.constant = 0
      }
    }
    
    if APPDELEGATE.is_wishlist_active == "0" && APPDELEGATE.is_cart_active == "1" {
      if Constants.DeviceType.IS_IPAD {
        btnWishlistwidthConstraints.constant = 0
      }else {
        btnWishlistwidthConstraints.constant = 0
      }
    }
    
    btn_ShareSocial.layer.cornerRadius = btn_ShareSocial.width / 2
    btn_ShareSocial.clipsToBounds = true
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    if UserDefaults.standard.object(forKey: BazarBit.AUTOPINCODE) != nil {
      let strAutoPinCode = UserDefaults.standard.object(forKey: BazarBit.AUTOPINCODE)
      txtPincode.text = strAutoPinCode as? String
    }
    
    APPDELEGATE.variationDisable = false
    self.tabBarController?.tabBar.isHidden = true
    cltRelatedProducts.applyShadowWithColor(.clear, opacity: 0.5, radius: 1)
    self.tblReview.estimatedRowHeight = 90
    self.tblReview.rowHeight = UITableViewAutomaticDimension
    
    if (UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) != nil) {
      
      let strCartCount: String = UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) as! String
      
      if strCartCount != "0" {
        lblCartTotal.text = strCartCount
        lblCartTotal.isHidden = false
      }else {
        lblCartTotal.isHidden = true
      }
      
      if Constants.DeviceType.IS_IPAD {
        lblCartTotal.layer.masksToBounds = true
        lblCartTotal.layer.cornerRadius = 10
      }else {
        lblCartTotal.layer.masksToBounds = true
        lblCartTotal.layer.cornerRadius = 10
      }
      
      if APPDELEGATE.is_cart_active == "0"
      {
        if Constants.DeviceType.IS_IPAD {
          viewcart.isHidden = true
        }else {
          viewcart.isHidden = true
        }
      } else {
        if Constants.DeviceType.IS_IPAD {
          viewcart.isHidden = false
        }else {
          viewcart.isHidden = false
        }
      }
    }
    
    //MARK: SERVICE CALL
    GetProductDetailsBySlug()
    GetCartCountData()
    
    // Active/Disactive WishList And Add To Cart
    if APPDELEGATE.is_wishlist_active == "0" && APPDELEGATE.is_cart_active == "0"
    {
      if Constants.DeviceType.IS_IPAD {
        btnWishList.isHidden = true
        btnAddToCart.isHidden = true
        btnWishListHeightConstraints.constant = 0
        btnAddToCartHeightConstraints.constant = 0
        bottomViewHeightConstraints.constant = 0
      }else {
        btnWishList.isHidden = true
        btnAddToCart.isHidden = true
        btnWishListHeightConstraints.constant = 0
        btnAddToCartHeightConstraints.constant = 0
        bottomViewHeightConstraints.constant = 0
      }
    }
    else
    {
      if Constants.DeviceType.IS_IPAD {
        btnWishList.isHidden = false
        btnAddToCart.isHidden = false
        bottomViewHeightConstraints.constant = 40
      }
      else
      {
        btnWishList.isHidden = false
        btnAddToCart.isHidden = false
        bottomViewHeightConstraints.constant = 40
      }
    }
    
    //MARK: SERVICE CALL
    GetRelatedProductList()
    GetUserReviewsDataList()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
    self.slideshow.pageControl.isEnabled = false
    self.slideshow.slideshowTimeInterval = UInt(2.0)
    self.slideshow.slideshowShouldCallScrollToDelegate = true
    self.slideshow.imageCounterDisabled = true
    self.slideshow.backgroundColor = UIColor.white
    self.slideshow.pageControl.pageIndicatorTintColor = UIColor.gray
    self.slideshow.pageControl.currentPageIndicatorTintColor = UIColor.black
    self.slideshow.contentMode = .scaleAspectFit
    
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    APPDELEGATE.TermsProductId = ""
    APPDELEGATE.TermsAtributedID = ""
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK:- KIImagePaper DataSource and Delegate Method
  func array(withImages pager: KIImagePager!) -> [Any]! {
    if chkvariation == false {
      if productType == "variable" {
        return self.arrSlideShowVariation
      } else {
        return self.arrSlideShow
      }
    } else {
      return self.arrSlideShowSetVariation
    }
    //return nil
  }
  
  func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIViewContentMode {
    return UIViewContentMode.scaleAspectFit
  }
  
  func captionForImage(at index: Int, in pager: KIImagePager!) -> String! {
    return ""
  }
  
  func imagePager(_ imagePager: KIImagePager!, didScrollTo index: UInt) {
    
  }
  
  func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
    print("Index \(index)")
    
    var arr_tmp = [String]()
    
    if chkvariation == false {
        if productType == "variable" {
            arr_tmp = self.arrSlideShowVariation
        } else {
            arr_tmp = self.arrSlideShow
        }
    } else {
        arr_tmp = self.arrSlideShowSetVariation
    }

    
    let obj_ProductViewController = self.storyboard?.instantiateViewController(withIdentifier: "FullViewProduct_ViewController") as! FullViewProduct_ViewController
    
    obj_ProductViewController.images = arr_tmp
    
    self.navigationController?.pushViewController(obj_ProductViewController, animated: false)
    
    
  
  }
  
  
  // MARK:- UITEXTFIELD METHOD
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if string == ""
    {
      return true
    }
    
    if textField == txtPincode {
      if (textField.text?.characters.count)! >= 6 {
        return false
      }
      if !(string.isNumber()) {
        return false
      }
    }
    return true
  }
  
  // MARK:- UICOLLECTION VIEW METHODS
  func numberOfSections(in collectionView: UICollectionView) -> Int
  {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return arrRelatedProducts.count
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    
    return UIEdgeInsets(top: 0, left: 1.0, bottom: 0, right: 0)
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let RelatedproductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cltnRelatedProductCell", for: indexPath) as! cltnRelatedProductCell
    
    let dict = arrRelatedProducts[indexPath.row] as! [String:AnyObject]
    print(dict)
    
    RelatedproductCell.contentView.layer.cornerRadius = 2
    RelatedproductCell.contentView.layer.borderWidth = 1.0
    RelatedproductCell.contentView.layer.borderColor = UIColor.clear.cgColor
    RelatedproductCell.contentView.layer.masksToBounds = true
    
    RelatedproductCell.layer.shadowColor = UIColor.lightGray.cgColor
    RelatedproductCell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
    RelatedproductCell.layer.shadowRadius = 2.0
    RelatedproductCell.layer.shadowOpacity = 1.0
    RelatedproductCell.layer.masksToBounds = false
    
    RelatedproductCell.lblProductName.text = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
    let sale_Price : String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "sale_price")
    let strSalePrice = String(format: "%.2f", sale_Price.toFloat()!)
    
    var strPrize = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "price")
    let strBasePrice = String(format: "%.2f", (strPrize.toFloat()!))
    strPrize = BazarBitCurrency.setPriceSign(strPrice: strBasePrice)
    
    var attributedPrizeString = NSAttributedString(string: strPrize)
    attributedPrizeString = attributedPrizeString.strikethrough()
    
    print(strSalePrice)
    print(strBasePrice)
    
    if strSalePrice == "0" {
      RelatedproductCell.lblBasePrice.text = strPrize
      RelatedproductCell.lblBasePrice.setTextLabelDynamicTextColor()
      RelatedproductCell.lblSalePrice.isHidden = true
   //   RelatedproductCell.lblSalePriceWidth.constant = 0.0
      
    }else {
      RelatedproductCell.lblBasePrice.attributedText = attributedPrizeString
      RelatedproductCell.lblSalePrice.text = BazarBitCurrency.setPriceSign(strPrice: strSalePrice)
      RelatedproductCell.lblSalePrice.isHidden = false
      RelatedproductCell.lblSalePriceWidth.constant = 85.0
    }
    
    // Rating Value Set
    let rate = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "avg_rating")
    
    let value = Double(rate)
    
    if value != nil {
      RelatedproductCell.ratingView.rating = value!
    }else {
      RelatedproductCell.ratingView.rating = 0.0
    }
    
    if let key = dict["main_image"] {
      if (key as? [String : AnyObject]) != nil
      {
        var imgUrl:String = PIService.object_forKeyWithValidationForClass_String(dict:key as! [String : AnyObject], key: "main_image")
        imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
        let url1 = URL(string: imgUrl)
        RelatedproductCell.lblProductImage.kf.setImage(with: url1)
      }
    }
    
    RelatedproductCell.btnWishList.tag = indexPath.row
    RelatedproductCell.btnWishList.addTarget(self, action: #selector(self.BtnAddToWishList), for: .touchUpInside)
    
    if APPDELEGATE.is_wishlist_active == "1"{
      RelatedproductCell.btnWishList.isHidden = false
    } else{
      RelatedproductCell.btnWishList.isHidden = true
    }
    
    let ExistInWishList:String = PIService.object_forKeyWithValidationForClass_String(dict:dict, key: "exists_in_wishlist")
    
    if ExistInWishList == "1" {
      RelatedproductCell.btnWishList.setImage((UIImage(named: "like1.png"), for: UIControlState.normal))
      WishList = true
    } else {
      RelatedproductCell.btnWishList.setImage((UIImage(named: "like2.png"), for: UIControlState.normal))
      WishList = false
    }
    
    RelatedproductCell.lblProductName.setTextLabelDynamicTextColor()
    RelatedproductCell.lblBasePrice.setTextLabelDynamicTextColor()
    RelatedproductCell.lblSalePrice.setTextLabelDynamicTextColor()
    
    return RelatedproductCell
  }
  
  //  Collection View Cell Height
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if collectionView == cltRelatedProducts {
        
        if Constants.DeviceType.IS_IPAD {
            let width = collectionView.frame.size.width
            return CGSize(width: (width/3) - 10, height:350)
        }else if IS_IPHONE_5_OR_5S() {
            return CGSize(width: 160, height : 210.0)
        }else {
            return CGSize(width: 180, height : 210.0)
        }
        
    }
    return CGSize.zero
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let dict = arrRelatedProducts[indexPath.row] as! [String:AnyObject]
    print(dict)
    
    APPDELEGATE.ProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
    APPDELEGATE.slugName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "slug")
    
    //        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
    //            myDelegate.OpenProductDetailPage()
    //        }
    let productDetailVC = storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
    self.navigationController?.pushViewController(productDetailVC, animated: true)
  }
  
  // MARK:- UITABLE VIEW METHODS
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == tblVariation {
      print(arrUsedVariation.count)
      return arrUsedVariation.count
    }
    
    if tableView == tblReview {
      return arrUserReview.count
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if tableView == tblVariation //VARIATION
    {
      let VariationCell = tableView.dequeueReusableCell(withIdentifier: "tblvariationCell", for: indexPath) as! variationListTableViewCell
      
      let dict = arrUsedVariation[indexPath.row] as! [String:AnyObject]
      
      VariationCell.productdetailvc = self
      VariationCell.tblPosition = indexPath.row
      
      let Title = "SELECT "
      let TitleName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
      let FinalResult:String = Title + TitleName.uppercased()
      print(FinalResult)
      
      VariationCell.lblVariationName.text = FinalResult
      VariationCell.lblVariationName.setTextLabelDynamicTextColor()
      
      VariationCell.setvariationData(dictModelDataVariation: dict, arrDisableData: self.aryVariationItem)
      
      VariationCell.setDefaultvariationData(aryModelDataDefaultVariation: aryDafaultVariationItem)
      
      VariationCell.BlockCollectionViewDidSelectHandler = {(isServiceCall : Bool, product_ID : String) in
        print(product_ID)
        self.SetVariationService(product_id: product_ID)
      }
      return VariationCell
    }
    
    if tableView == tblReview
    {
      let UserReviewCell = tableView.dequeueReusableCell(withIdentifier: "tblUserReviewCell", for: indexPath) as! UserRatingDetailTableviewCell
      
      if arrUserReview.count > 0 {
        let dict = arrUserReview[indexPath.row] as! [String:AnyObject]
        
        UserReviewCell.lbluserratingname.text = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
        
        UserReviewCell.lbluserreviews.text = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "description")
        
        let ReviewDate:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "added_on")
        let Rating:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "rating")
        
        let value = Double(Rating)
        
        if value != nil {
          UserReviewCell.ratingview.rating = value!
        }else {
          UserReviewCell.ratingview.rating = 0.0
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFromString = dateFormatter.date(from: ReviewDate)
        print(dateFromString ?? "")
        dateFormatter.dateFormat = "dd-MMM-yy"
        let Dateresult = dateFormatter.string(from: dateFromString!)
        print(Dateresult)
        
        UserReviewCell.lbluserratingdate.text = Dateresult
        //var ReviewDescription = UserReviewCell.lbluserreviews.text
        return UserReviewCell
      }
    }
    return UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    if tableView == tblVariation {
      if Constants.DeviceType.IS_IPAD {
        return 100
      } else {
        return 75
      }
    }
    
    if tableView == tblReview {
      if Constants.DeviceType.IS_IPAD {
        return 120
      }else {
        return UITableViewAutomaticDimension
      }
    }
    return 0.0
  }
  
  // MARK:- ACTION METHOD
  
  @IBAction func btnBack(_ sender: Any) {
    if (UIApplication.shared.delegate as? AppDelegate) != nil {
      //myDelegate.openHomePage()
      self.navigationController?.popViewController(animated: true)
      GetCartCountData()
    }
  }
  
  @IBAction func btnWishList(_ sender: Any) {
    
    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
      AddToWishListForProduct()
    }
    else
    {
      if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
        myDelegate.openLoginPage()
        //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
      }
    }
  }
  
    @IBAction func btnAddToCart(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "NOTIFY ME" {
            self.ServiceCallForCheckItemInNotify(strProduct_ID: product_ID)
            
        }  else if sender.titleLabel?.text == "GO TO CART" {
            APPDELEGATE.chkBottomBar = true
            let shoppingCartViewController : ShoppingCartViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingCartViewController") as! ShoppingCartViewController
            
            self.navigationController?.pushViewController(shoppingCartViewController, animated: true)
            
        } else {
            
           if APPDELEGATE.check_pincode_availibility == "1" {
            
            if self.chkCheckPincode == true {
                self.Checksts = "1"
                CheckPincodeService()
                
            }  else {
                
                if txtPincode.text == "" {
                    PIAlertUtils.displayAlertWithMessage("Please enter pincode and check delivery available or not!")
                } else {
                    
                    if UserDefaults.standard.object(forKey: BazarBit.AUTOPINCODE) != nil {
                        
                        let strAutoPinCode: String = UserDefaults.standard.object(forKey: BazarBit.AUTOPINCODE) as! String
                        
                        if strAutoPinCode == txtPincode.text {
                            
                            if self.chkVariationId == true {
                                self.ServiceCallForAddToCart(strProduct_ID: self.Variation_Prod_ID, strProductName: self.Product_Name, strPrice: self.Product_Sale_Price)
                            } else {
                                self.ServiceCallForAddToCart(strProduct_ID: self.product_ID, strProductName: self.Product_Name, strPrice: self.Product_Sale_Price)
                            }
                            
                        } else if NotAvailableDelivery == "Delivery is Not Available" {
                            PIAlertUtils.displayAlertWithMessage("Please enter another pincode because delivery not available!")
                            
                        } else {
                            PIAlertUtils.displayAlertWithMessage("check delivery available or not!")
                        }
                        
                    } else if NotAvailableDelivery == "Delivery is Not Available" {
                        PIAlertUtils.displayAlertWithMessage("Please enter another pincode because delivery not available!")
                        
                    } else {
                        PIAlertUtils.displayAlertWithMessage("check delivery available or not!")
                        
                    }
                }
            }
           }else {
           
           self.ServiceCallForAddToCart(strProduct_ID: self.product_ID, strProductName: self.Product_Name, strPrice: self.Product_Sale_Price)
            
            
           }
         }
        
    }

  @IBAction func btnCheck(_ sender: Any) {
    
    if checkValidation() {
      self.Checksts = ""
      CheckPincodeService()
    }
  }
  
  func BtnAddToWishList(sender:UIButton) {
    print(sender.tag)
    
    let dict = arrRelatedProducts[sender.tag] as! [String:AnyObject]
    APPDELEGATE.RelatedProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
    
    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
      AddToWishListForRelatedProducts()
    }
    else
    {
      if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
        myDelegate.openLoginPage()
        //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
      }
    }
    sender.setImage(UIImage(named: "like1.png"), for: UIControlState.normal)
  }
  
  @IBAction func btnShoppingCart(_ sender: Any) {
    
    APPDELEGATE.chkBottomBar = true
    let shoppingCartViewController : ShoppingCartViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingCartViewController") as! ShoppingCartViewController
    
    self.navigationController?.pushViewController(shoppingCartViewController, animated: true)
  }
  
    @IBAction func btn_ShareSocial(_ sender: Any) {
        
        let alert = UIAlertController(title: "Select Share Type", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Facebook", style: .default, handler: { (_) in
            print("User click Approve button")


            self.OpenFB()


        }))

        alert.addAction(UIAlertAction(title: "Twitter", style: .default, handler: { (_) in
            print("User click Edit button")
            self.Twitter()
        }))

        alert.addAction(UIAlertAction(title: "Skype", style: .default, handler: { (_) in
            print("User click Delete button")
            
            var installed: Bool? = nil
            if let url = URL(string: "skype:") {
                installed = UIApplication.shared.canOpenURL(url)
            }
            if installed ?? false {
                if let url = URL(string: "skype:+\(self.str_URL_ShareToSocialMedia)+?sms") {
                    UIApplication.shared.openURL(url)
                }
            } else {
                if let url = URL(string: "http://itunes.com/apps/skype/skype") {
                    UIApplication.shared.openURL(url)
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "Linkdin", style: .default, handler: { (_) in
            print("User click Dismiss button")
            self.shareOnLinkedIn()
        }))

        alert.addAction(UIAlertAction(title: "Whatsapp", style: .default, handler: { (_) in
            print("User click Dismiss button")
            
            let msg = self.str_URL_ShareToSocialMedia
            let urlWhats = "whatsapp://send?text=\(msg)"
            
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                        UIApplication.shared.openURL(whatsappURL as URL)
                    } else {
                        print("please install watsapp")
                        //https://itunes.apple.com/in/app/whatsapp-messenger/id310633997?mt=8
                        
                        let urlStr = "https://itunes.apple.com/us/app/whatsapp-messenger/id310633997"
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                            
                        } else {
                            UIApplication.shared.openURL(URL(string: urlStr)!)
                        }

                    }
                }
            }

            
            
        }))

        alert.addAction(UIAlertAction(title: "Pinterest", style: .default, handler: { (_) in
            print("User click Dismiss button")
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            print("User click Dismiss button")
        }))


        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func OpenFB() {
        
        let content = FBSDKShareLinkContent()
        content.contentURL = URL(string: str_URL_ShareToSocialMedia)
        
        FBSDKShareDialog.show(from: self, with: content, delegate: self)
        
    }
    
    func Twitter() {
        
       
        let tweetText = " "
        let tweetUrl = str_URL_ShareToSocialMedia
        
        let shareString = "https://twitter.com/intent/tweet?text=\(tweetText)&url=\(tweetUrl)"
        
        // encode a space to %20 for example
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        // open in safari
        UIApplication.shared.openURL(url!)
        
    }
    
    func shareOnLinkedIn(){
        
        LISDKSessionManager.createSession(withAuth:
            [LISDK_BASIC_PROFILE_PERMISSION,LISDK_W_SHARE_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: {(sucess) in
                let session = LISDKSessionManager.sharedInstance().session
                print("Session ",session!)
                //let url = "https://api.linkedin.com/v1/people/~"
                if LISDKSessionManager.hasValidSession(){
                    let url: String = "https://api.linkedin.com/v1/people/~/shares"
                    
                    let payloadStr: String = "{\"comment\":\"+\(self.str_URL_ShareToSocialMedia)\",\"visibility\":{\"code\":\"anyone\"}}"
                    
                    let payloadData = payloadStr.data(using: String.Encoding.utf8)
                    
                    LISDKAPIHelper.sharedInstance().postRequest(url, body: payloadData, success: { (response) in
                        print(response!.data)
                    }, error: { (error) in
                        print(error!)
                        
                        let alert = UIAlertController(title: "Alert!", message: "something went to wrong", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    })
                }
        }) {(error) in
            print("Error \(String(describing: error))")
        }
    }

   
  // MARK:- Service Methods
  func GetProductDetailsBySlug() {
    
    let strurl:String = BazarBit.GetProductDetailBySlugService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter:Parameters = [:]
    let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
    
    if UserId != nil {
      parameter["slug"] = APPDELEGATE.slugName
      parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
    }else {
      parameter["slug"] = APPDELEGATE.slugName
      parameter["user_id"] = ""
    }
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      self.dicResponceproductDetails = responseDict
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceproductDetails, key: "status")
      
      if status == "ok"
      {
        if (self.dicResponceproductDetails["payload"] != nil)
        {
          let payload:[String:AnyObject] = self.dicResponceproductDetails["payload"] as! [String:AnyObject]
          print(payload)
          
          self.arrUsedVariation = []
          if payload["products"] != nil
          {
            self.payloadProductSlug = payload["products"] as! [String:AnyObject]
            print(self.payloadProductSlug)
            
            self.productType = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "product_type")
            
            APPDELEGATE.slugProdID = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "product_id")
            
            // Sale price
            var salePrice:String = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "sale_price")
            
            //Base price
            var strPrize:String = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "price")
            
            print(salePrice)
            
            self.product_ID = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "product_id")
            
            self.Product_Name = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "name")
            
            self.Product_Sale_Price = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "sale_price")
            
            // Normal Merge Images (Default Set)
            var arrMergeImages:[String] = []
            var imgMainArray:[String] = []
            var imgGallaryArray:[String] = []
            
            if let key = self.payloadProductSlug["main_image"] {
              if (key as? [String : AnyObject]) != nil
              {
                print(key)
                var imgUrl:String = PIService.object_forKeyWithValidationForClass_String(dict: key as! [String : AnyObject], key: "main_image")
                imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                
                imgMainArray.append(imgUrl)
                print(imgMainArray)
              }
            }
            
            print(self.payloadProductSlug)
            
            if (self.payloadProductSlug["gallery_images"] as? [AnyObject] != nil) {
              
              let arrGallary = self.payloadProductSlug["gallery_images"] as! [AnyObject]
              print(arrGallary)
              
              if arrGallary.count > 0 {
                for i in 0...arrGallary.count - 1
                {
                  let dictProduct:[String:AnyObject] = arrGallary[i] as! [String:AnyObject]
                  print(dictProduct)
                  
                  let mainImage = PIService.object_forKeyWithValidationForClass_String(dict: dictProduct, key: "main_image")
                  
                  imgGallaryArray.append(mainImage)
                  print(imgGallaryArray)
                }
              }
            }
            
            arrMergeImages.append(contentsOf: imgMainArray)
            arrMergeImages.append(contentsOf: imgGallaryArray)
            print(arrMergeImages)
            
            self.chkvariation = false
            if self.productType == "variable"
            {
              if APPDELEGATE.variationDisable == false {
                self.GetDefaultVariationService()
              }
              
              let variation:[AnyObject] = self.payloadProductSlug["variations"] as! [AnyObject]
              //print(variation)
              var arrMergeImagesVariation:[String] = []
              var imgMainArrayVariation:[String] = []
              var imgGallaryArrayVariation:[String] = []
              
              for arrVariationData in variation
              {
                self.arrVariation = []
                self.arrSlideShow = []
                arrMergeImagesVariation = []
                self.arrVariation.append(arrVariationData)
                
                for i in 0...self.arrVariation.count - 1
                {
                  let dictVariationData:[String:AnyObject] = self.arrVariation[i] as! [String:AnyObject]
                  print(dictVariationData)
                  
                  let dictmainImage = dictVariationData["main_image"] as! [String:AnyObject]
                  //print(dictmainImage)
                  
                  var salePricevariation = PIService.object_forKeyWithValidationForClass_String(dict: dictVariationData, key: "sale_price")
                  print(salePricevariation)
                  
                  var basePriceVariation = PIService.object_forKeyWithValidationForClass_String(dict: dictVariationData, key: "price")
                  print(basePriceVariation)
                  
                  var imgUrl:String = PIService.object_forKeyWithValidationForClass_String(dict: dictmainImage, key: "main_image")
                  
                  imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                  
                  imgMainArrayVariation.append(imgUrl)
                  
                  if (dictVariationData["gallery_images"] as? [AnyObject] != nil) {
                    let dictGallaryImage = dictVariationData["gallery_images"] as! [AnyObject]
                    print(dictGallaryImage)
                    
                    if dictGallaryImage.count > 0 {
                      for i in 0...dictGallaryImage.count - 1
                      {
                        let dictProduct:[String:AnyObject] = dictGallaryImage[i] as! [String:AnyObject]
                        print(dictProduct)
                        
                        let mainImage = PIService.object_forKeyWithValidationForClass_String(dict: dictProduct, key: "main_image")
                        
                        imgGallaryArrayVariation.append(mainImage)
                      }
                    }
                  }
                    
                  arrMergeImagesVariation.append(contentsOf: imgMainArrayVariation)
                  arrMergeImagesVariation.append(contentsOf: imgGallaryArrayVariation)
                  
                  print(arrMergeImagesVariation)
                  //print(arrMergeImagesVariation)
                  
                  //Default Variation
                  let defaultVariation:String = PIService.object_forKeyWithValidationForClass_String(dict: dictVariationData, key: "defaultvariation")
                  print(defaultVariation)
                  
                  if defaultVariation == "1"
                  {
                    self.arrSlideShowVariation = []
                    // Slider Code
                    
                    for strImages in arrMergeImagesVariation
                    {
                      let strImg = strImages.replacingOccurrences(of: " ", with: "%20")
                      self.arrSlideShowVariation.append(strImg)
                    }
                    
                    print(self.arrSlideShowVariation)
                    self.slideshow.dataSource = self
                    self.slideshow.delegate = self
                    self.slideshow.reloadData()
                    
                    let Variation_Product_Stock_Status = PIService.object_forKeyWithValidationForClass_String(dict: dictVariationData, key: "stock_status")
                    print(Variation_Product_Stock_Status)
                    
                    let Variation_Product_Qty = PIService.object_forKeyWithValidationForClass_String(dict: dictVariationData, key: "quantity")
                    print(Variation_Product_Qty)
                    
                    if APPDELEGATE.variationDisable == false  {
                      
                      if salePricevariation == "0.00" || salePricevariation == "0" {
                        self.lblsaleprice.isHidden = false
                        self.lblSalePriceWidth.constant = 85.0
                        self.lblBasePriceWidth.constant = 0.0
                        
                        // Base Price
                        let strBasePriceVariation = String(format: "%.2f", (basePriceVariation.toFloat()!))
                        basePriceVariation = BazarBitCurrency.setPriceSign(strPrice: strBasePriceVariation)
                        
                        self.lblsaleprice.text = basePriceVariation
                        self.Product_Sale_Price = strBasePriceVariation
                      }else {
                        self.lblsaleprice.isHidden = false
                        self.lblSalePriceWidth.constant = 85.0
                        self.lblBasePriceWidth.constant = 85.0
                        
                        // Sale Price
                        let strSalePriceVariation = String(format: "%.2f", (salePricevariation.toFloat()!))
                        salePricevariation = BazarBitCurrency.setPriceSign(strPrice: strSalePriceVariation)
                        
                        self.lblsaleprice.text = salePricevariation
                        self.Product_Sale_Price = strSalePriceVariation
                        
                        // Base Price
                        let strBasePriceVariation = String(format: "%.2f", (basePriceVariation.toFloat()!))
                        basePriceVariation = BazarBitCurrency.setPriceSign(strPrice: strBasePriceVariation)
                        
                        var attributedPrizeString = NSAttributedString(string: basePriceVariation)
                        attributedPrizeString = attributedPrizeString.strikethrough()
                        self.lblbasePrice.attributedText = attributedPrizeString
                      }
                      
                      self.viewPriceWidth.constant = self.lblSalePriceWidth.constant + self.lblBasePriceWidth.constant
                      
                      //Check Item in Notify Me or Not
                      self.Variation_Prod_ID = PIService.object_forKeyWithValidationForClass_String(dict: dictVariationData, key: "product_id")
                      print(self.Variation_Prod_ID)
                      
                      let Variation_Product_Stock_Status = PIService.object_forKeyWithValidationForClass_String(dict: dictVariationData, key: "stock_status")
                      print(Variation_Product_Stock_Status)
                      
                      let Variation_Product_Qty = PIService.object_forKeyWithValidationForClass_String(dict: dictVariationData, key: "quantity")
                      print(Variation_Product_Qty)
                      
                      if (Variation_Product_Stock_Status.toInt()! == 0 || Variation_Product_Qty.toInt()! <= 0)
                      {
                        //print("NOTIFY ME")
                        self.btnAddToCart.setTitle("NOTIFY ME", for: .normal)
                      }else {
                        //Check Item in Cart)
                        
                        self.chkVariationId = true
                        self.ServiceCallForCheckItemExistInCart(strProduct_ID: self.Variation_Prod_ID,buttton: self.btnAddToCart)
                      }
                      
                    }
                    
                    if self.chkChangeGoToCart == true {
                      if (Variation_Product_Stock_Status.toInt()! == 0 || Variation_Product_Qty.toInt()! <= 0)
                      {
                        //print("NOTIFY ME")
                        self.btnAddToCart.setTitle("NOTIFY ME", for: .normal)
                      }else {
                        //print("Check Item in Cart")
                        
                        self.chkVariationId = true
                        self.ServiceCallForCheckItemExistInCart(strProduct_ID: self.Variation_Prod_ID,buttton: self.btnAddToCart)
                      }
                    }
                  }
                }
              }
            }
            else
            {
              self.arrSlideShow = []
              for strImages in arrMergeImages
              {
                let strImg = strImages.replacingOccurrences(of: " ", with: "%20")
                
                self.arrSlideShow.append(strImg)
                
                //                                self.slideshow.dataSource = self
                //                                self.slideshow.delegate = self
                //self.slideshow.reloadData()
                
              }
              print(self.arrSlideShow)
              self.slideshow.dataSource = self
              self.slideshow.delegate = self
              self.slideshow.reloadData()
              
              // Slider Code
              //self.slideshow.setImageInputs(self.arrLocalSource)
              
              //Check Item in Notify Me or Not
              let Prod_ID = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "product_id")
              print(Prod_ID)
              
              let Product_Stock_Status = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "stock_status")
              print(Product_Stock_Status)
              
              let Product_Qty = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "quantity")
              print(Product_Qty)
              
              if (Product_Stock_Status.toInt()! == 0 || Product_Qty.toInt()! <= 0)
              {
                //print("NOTIFY ME")
                self.btnAddToCart.setTitle("NOTIFY ME", for: .normal)
              }else {
                //Check Item in Cart
                self.chkVariationId = false
                self.ServiceCallForCheckItemExistInCart(strProduct_ID: Prod_ID,buttton: self.btnAddToCart)
              }
              
              // Sale Price
              let strSalePrice = String(format: "%.2f", (salePrice.toFloat()!))
              salePrice = BazarBitCurrency.setPriceSign(strPrice: strSalePrice)
              
              // Base Price
              // Remove Decimal & set price Symbol & strike out
              let strBasePrice = String(format: "%.2f", (strPrize.toFloat()!))
              strPrize = BazarBitCurrency.setPriceSign(strPrice: strBasePrice)
              
              var attributedPrizeString = NSAttributedString(string: strPrize)
              attributedPrizeString = attributedPrizeString.strikethrough()
              
              if strSalePrice == "0" || strSalePrice == "0.00" {
                self.lblbasePrice.text = strPrize
                self.lblbasePrice.setTextLabelDynamicTextColor()
                self.lblSalePriceWidth.constant = 0.0
              }else {
                self.lblsaleprice.text = salePrice
                self.lblbasePrice.attributedText = attributedPrizeString
                self.lblSalePriceWidth.constant = 85.0
              }
            }
            
            self.viewPriceWidth.constant = self.lblSalePriceWidth.constant + self.lblBasePriceWidth.constant
            
            if (self.payloadProductSlug["usedVariation"] as? [AnyObject]) != nil
            {
              self.arrUsedVariation = self.payloadProductSlug["usedVariation"] as! [AnyObject]
              print(self.arrUsedVariation)
              
              // For Variation Height Managed
              if self.arrUsedVariation.count == 0 {
                self.tblVariationHeightConstraints.constant = 0
              }else {
                if Constants.DeviceType.IS_IPAD {
                  // For set Variation Height Dynamically
                  self.tblVariationHeightConstraints.constant = CGFloat(self.arrUsedVariation.count * 120)
                }else {
                  self.tblVariationHeightConstraints.constant = CGFloat(self.arrUsedVariation.count * 75)
                }
              }
            }else {
              self.tblVariationHeightConstraints.constant = 0
            }
            
            let Productname:String = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "name")
            let shortDescription:String = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "short_description")
            
            let Description:String = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "description")
            self.ExpectedDelivery = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "expected_delivery_time")
            
            self.ReturnAvailabe = PIService.object_forKeyWithValidationForClass_String(dict: self.payloadProductSlug, key: "return_days")
            
            self.lblToplableName.text = Productname
            self.lblProductName.text = Productname
            self.lblShortDescription.text = shortDescription
            self.lblLongDescription.text = Description + "............................"
            self.readMoreTextView.text = Description
            
//            let readMoreTextAttributes: [NSAttributedStringKey: Any] = [
//                NSAttributedStringKey.foregroundColor: view.tintColor,
//                NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16)
//
//                NSForegroundColorAttributeName:UIColor.lightGray,
//                NSFontAttributeName:UIFont.systemFont(ofSize: 14)
//            ]
            
               let readMoreTextAttributes = [
                    NSForegroundColorAttributeName:UIColor.blue,                    NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16)
                    ]
            
            
//            let readLessTextAttributes = [
//                NSAttributedStringKey.foregroundColor: UIColor.red,
//                NSAttributedStringKey.font: UIFont.italicSystemFont(ofSize: 16)
//            ]
            
            let readLessTextAttributes = [
                NSForegroundColorAttributeName:UIColor.red,                    NSFontAttributeName:UIFont.italicSystemFont(ofSize: 16)
            ]
            
            
            self.readMoreTextView.attributedReadMoreText = NSAttributedString(string: "... Read more", attributes: readMoreTextAttributes)
            //self.readMoreTextView.attributedReadLessText = NSAttributedString(string: " Read less", attributes: readLessTextAttributes)
            self.readMoreTextView.maximumNumberOfLines = 3
            self.readMoreTextView.shouldTrim = true

            // Expected Delivery
            let Expected = "Expected Delivery"
            let Days = " Days"
            let resultExpectedDelivery = Expected + ", " + self.ExpectedDelivery + Days
            self.lblExpectedDelivery.text = resultExpectedDelivery
            
            // Return Available
            let Return = "Days Returns Available"
            let resultreturnAvailable = self.ReturnAvailabe + " " + Return
            self.lblReturnAvailable.text = resultreturnAvailable
            self.DynamicHeight()
          }
          self.tblVariation.reloadData()
        }
      }
    })
  }
  
  func GetRelatedProductList() {
    
    let strurl:String = BazarBit.GetRelatedProductDataService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter:Parameters = [:]
    parameter["slug"] = APPDELEGATE.slugName
    
    let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
    if UserId != nil {
      headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
      parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
    }else {
      parameter["user_id"] = ""
    }
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      print(responseDict)
      self.dicResponceRelatedProductDetails = responseDict
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceRelatedProductDetails, key: "status")
      
      if status == "ok"
      {
        if (self.dicResponceRelatedProductDetails["payload"] != nil)
        {
          let payload:[String:AnyObject] = self.dicResponceRelatedProductDetails["payload"] as! [String:AnyObject]
          print(payload)
          
          if (payload["relatedproducts"] as? [AnyObject]) != nil
          {
            let arrProducts = payload["relatedproducts"] as! [AnyObject]
            //print(arrProducts)
            
            self.arrRelatedProducts = []
            self.arrRelatedProductImages = []
            
            for dict in arrProducts {
              self.arrRelatedProducts.append(dict)
            }
            self.cltRelatedProducts.reloadData()
          }
        }
      }
    })
  }
  
  func AddToWishListForRelatedProducts() {
    
    let strurl:String = BazarBit.AddItemToWishListService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    
    var parameter:Parameters = [:]
    parameter["product_id"] = APPDELEGATE.RelatedProductID
    
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      self.dictCheckItemWishlist = responseDict
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictCheckItemWishlist, key: "status")
      let messege = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      if status == "ok" {
        self.GetRelatedProductList()
      }else {
        PIAlertUtils.displayAlertWithMessage(messege)
      }
    })
  }
  
  
  func AddToWishListForProduct() {
    
    let strurl:String = BazarBit.AddItemToWishListService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    
    var parameter:Parameters = [:]
    parameter["product_id"] = self.product_ID
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      self.dictWishlist = responseDict
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictWishlist, key: "status")
      let messege = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      if status == "ok" {
        PIAlertUtils.displayAlertWithMessage(messege)
      }else {
        PIAlertUtils.displayAlertWithMessage(messege)
      }
    })
  }
  
  func GetUserReviewsDataList() {
    
    let strurl:String = BazarBit.GetProductReviewsDataService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter:Parameters = [:]
    parameter["slug"] = APPDELEGATE.slugName
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      self.dicResponceUserReviews = responseDict
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceUserReviews, key: "status")
      
      if status == "ok"
      {
        if (self.dicResponceUserReviews["payload"] as? [AnyObject]) != nil
        {
          self.arrUserReview = self.dicResponceUserReviews["payload"] as! [AnyObject]
          print(self.arrUserReview)
          
          var sumation:Float = 0.0
          
          if self.arrUserReview.count > 0
          {
            for i in 0...self.arrUserReview.count-1
            {
              let dictUserReviews:[String:AnyObject] = self.arrUserReview[i] as! [String : AnyObject]
              //print(self.arrUserReview.count)
              
              let Rating:String = PIService.object_forKeyWithValidationForClass_String(dict: dictUserReviews, key: "rating")
              print(Rating)
              sumation = sumation + Rating.toFloat()!
            }
            //print(sumation)
            
            self.globalRating = Double(sumation) / Double(self.arrUserReview.count)
            //print(self.globalRating)
            
            if self.globalRating != nil
            {
              self.mainRatingView.rating = self.globalRating
            }else {
              self.mainRatingView.rating = 0.0
            }
          }
          
          // For UserReview Tbl Height Managed (Array Count * My TBL Height)
          if self.arrUserReview.count == 0
          {
            self.mainRatingView.rating = 0.0
            self.lblPopularReviewHeightConstraints.constant = 0
            self.lblPopularReview.isHidden = true
            self.tblUserReviewHeightConstraints.constant = 0
          }
          else
          {
            if Constants.DeviceType.IS_IPAD
            {
              // For set userReview Tbl Height Dynamically
              self.tblUserReviewHeightConstraints.constant = CGFloat(self.arrUserReview.count * 120)
            }
            else
            {
              self.tblUserReviewHeightConstraints.constant = CGFloat(self.arrUserReview.count * 90)
            }
          }
        }
        else
        {
          self.lblPopularReviewHeightConstraints.constant = 0
          self.lblPopularReview.isHidden = true
          self.tblUserReviewHeightConstraints.constant = 0
        }
        
        self.tblReview.reloadData()
      }
    })
  }
  
  func CheckPincodeService() {
    
    let strurl:String = BazarBit.CheckPincodeExistService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter:Parameters = [:]
    parameter["pincode"] = txtPincode.text
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      self.dicResponcePinCode = responseDict
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponcePinCode, key: "status")
      
      if status == "ok"
      {
        let strAutoPinCode = self.txtPincode.text!
        print(strAutoPinCode)
        
        
        if (self.dicResponcePinCode["payload"] as? [String:AnyObject]) != nil
        {
          let payload:[String:AnyObject] = self.dicResponcePinCode["payload"] as! [String:AnyObject]
          print(payload)
          
          self.count = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "count")
          //print(self.count)
          self.txtPincode.resignFirstResponder()
          
          if self.count == "1"
          {
            UserDefaults.standard.set(strAutoPinCode, forKey: BazarBit.AUTOPINCODE)

            if self.Checksts == "1"
            {
              if self.chkVariationId == true {
                self.ServiceCallForAddToCart(strProduct_ID: self.Variation_Prod_ID, strProductName: self.Product_Name, strPrice: self.Product_Sale_Price)
              } else {
                self.ServiceCallForAddToCart(strProduct_ID: self.product_ID, strProductName: self.Product_Name, strPrice: self.Product_Sale_Price)
              }
              
              self.Checksts = ""
            } else {
              self.chkCheckPincode = true
              PIAlertUtils.displayAlertWithMessage("Delivery is Available")
              self.lableProductStatus.text = "Delivery Available"
              self.lableProductStatus.textColor = UIColor(red: 0.0/255.0, green: 95.0/255.0, blue: 0.0/255.0, alpha: 1.0)
              self.txtPincode.resignFirstResponder()
              self.AvaialbleDelivery = "Delivery is Available"
              self.btnCashOnDelivery.setImage(UIImage(named:"checked"), for: .normal)
            }
          }
          else
          {
            PIAlertUtils.displayAlertWithMessage("Please enter another pincode because delivery not available!")
            self.NotAvailableDelivery = "Delivery is Not Available"
            self.lableProductStatus.text = "Delivery is Not Available"
            self.lableProductStatus.textColor = UIColor.red
            self.btnCashOnDelivery.setImage(UIImage(named:"cancel"), for: .normal)
          }
        }
      }
    })
  }
  
  func CheckPincodeServiceForAddToCart() {
    
    let strurl:String = BazarBit.CheckPincodeExistService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter:Parameters = [:]
    parameter["pincode"] = txtPincode.text
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      self.dicResponcePinCode = responseDict
      
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponcePinCode, key: "status")
      
      if status == "ok"
      {
        if (self.dicResponcePinCode["payload"] as? [String:AnyObject]) != nil
        {
          let payload:[String:AnyObject] = self.dicResponcePinCode["payload"] as! [String:AnyObject]
          print(payload)
          
          self.count = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "count")
          //print(self.count)
          
          if self.count == "1"
          {
            self.txtPincode.resignFirstResponder()
            self.AvaialbleDelivery = "Delivery is Available"
            self.btnCashOnDelivery.setImage(UIImage(named:"checked"), for: .normal)
            
            if self.Checksts == "1"
            {
              if self.chkVariationId == true {
                if self.chkSalevalue == true {
                  self.Product_Sale_Price = self.Product_Base_Price
                  print(self.Product_Sale_Price)
                }
                //Variatin ID
                self.ServiceCallForAddToCart(strProduct_ID: self.Variation_Prod_ID, strProductName: self.Product_Name, strPrice: self.Product_Sale_Price)
              } else {
                //Normal Flow ID
                self.ServiceCallForAddToCart(strProduct_ID: self.product_ID, strProductName: self.Product_Name, strPrice: self.Product_Sale_Price)
              }
              
              self.GetCartCountData()
              self.Checksts = ""
            }
          }
          else
          {
            self.NotAvailableDelivery = "Delivery is Not Available"
            self.btnCashOnDelivery.setImage(UIImage(named:"cancel"), for: .normal)
          }
        }
      }
    })
  }
  
  //MARK: DisableVariationOptionService
  func DisableVariationOptionService(aryAttributeId : [String], aryAttributeTermsId : [String]) {
    
    let strurl:String = BazarBit.DisableVariationOptionService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter:Parameters = [:]
    parameter["count"] = aryAttributeId.count
    parameter["product_id"] = APPDELEGATE.slugProdID
    parameter["attrArr"] = aryAttributeId
    parameter["attrtermArr"] = aryAttributeTermsId
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      BazarBitLoader.stopLoader()
      
      print(responseDict)
      
      let status : String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
      
      if status == "ok"
      {
        if (responseDict["payload"] != nil) {
          
          let dictData : [String : AnyObject] = responseDict["payload"] as! [String : AnyObject]
          
          let strProductID : String = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "product_id")
          
          self.SetVariationService(product_id: strProductID)
          self.tblVariation.reloadData()
          self.btnWishlistwidthConstraints.constant = self.view.frame.size.width / 2
          //self.btnAddToCart.setTitle("ADD TO CART", for: .normal)
        }
        
      }else {
        self.btnWishlistwidthConstraints.constant = 0
        self.btnAddToCart.setTitle("OUT OF STOCK", for: .normal)
        self.tblVariation.reloadData()
      }
    })
  }
  
  //MARK:VariationClickService
  func SetVariationService(product_id : String) {
    
    let strurl:String = BazarBit.GetProductVariationService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    print(headers)
    
    var parameter:Parameters = [:]
    parameter["z_productid_pk"] = product_id
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
      print(status)
      
      if status == "ok"
      {
        if (responseDict["payload"] as? [String : AnyObject]) != nil
        {
          let payload:[String : AnyObject] = responseDict["payload"] as! [String : AnyObject]
          print(payload)
          
          self.product_ID = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "product_id")
          self.Variation_Prod_ID = self.product_ID
            
          APPDELEGATE.slugName = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "slug")
          
          self.Product_Name = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "name")
          self.Product_Sale_Price = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "sale_price")
          
          self.Product_Base_Price = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "price")
          let sales = self.Product_Sale_Price
          // Sale price
          let strSalePrice = String(format: "%.2f", (self.Product_Base_Price.toFloat()!))
          
          //Base price
          var strPrize:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "price")
          
          //Check Product In Notify Me Or Not
          //Check Item in Notify Me or Not
          let Variation_Product_Stock_Status = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "stock_status")
          print(Variation_Product_Stock_Status)
          
          let Variation_Product_Qty = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "quantity")
          print(Variation_Product_Qty)
          
          if (Variation_Product_Stock_Status.toInt()! == 0 || Variation_Product_Qty.toInt()! <= 0)
          {
            //print("NOTIFY ME")
            self.btnAddToCart.setTitle("NOTIFY ME", for: .normal)
          }else {
            //print("Check Item in Cart")
            
            self.chkVariationId = true
            self.ServiceCallForCheckItemExistInCart(strProduct_ID: self.Variation_Prod_ID,buttton: self.btnAddToCart)
          }
          
          if sales == "0.00" || sales == "0" {
            self.lblBasePriceWidth.constant = 85.0
            self.lblSalePriceWidth.constant = 0.0
            self.chkSalevalue = true
            self.lblsaleprice.text = ""
            self.lblbasePrice.setTextLabelDynamicTextColor()
            let strBasePrice = String(format: "%.2f", (strPrize.toFloat()!))
            strPrize = BazarBitCurrency.setPriceSign(strPrice: strBasePrice)
            self.lblbasePrice.text = strPrize
            self.Product_Sale_Price = self.Product_Base_Price
            
          } else {
            self.chkSalevalue = false
            
            //Sale price
            var strSalePrice = String(format: "%.2f", (self.Product_Sale_Price.toFloat()!))
            strSalePrice = BazarBitCurrency.setPriceSign(strPrice: strSalePrice)
            self.lblsaleprice.text = strSalePrice
            
            //base price
            strPrize = String(format: "%.2f", (strPrize.toFloat()!))
            strPrize = BazarBitCurrency.setPriceSign(strPrice: strPrize)
            var attributedPrizeString = NSAttributedString(string: strPrize)
            attributedPrizeString = attributedPrizeString.strikethrough()
            self.lblbasePrice.attributedText = attributedPrizeString
            
            self.lblBasePriceWidth.constant = 85.0
            self.lblSalePriceWidth.constant = 85.0
          }
          
         self.viewPriceWidth.constant = self.lblSalePriceWidth.constant + self.lblBasePriceWidth.constant
            
          var arrMergeImages:[String] = []
          var imgMainArray:[String] = []
          var imgGallaryArray:[String] = []
          
          if let key = payload["main_image"] {
            if (key as? [String : AnyObject]) != nil
            {
              var imgUrl:String = key["main_image"] as! String
              imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
              imgMainArray.append(imgUrl)
            }
          }
          
          if (payload["gallery_images"] as? [AnyObject] != nil) {
            let arrGallary = payload["gallery_images"] as! [AnyObject]
            print(arrGallary)
            
            if arrGallary.count > 0 {
              for i in 0...arrGallary.count - 1
              {
                let dictProduct:[String:AnyObject] = arrGallary[i] as! [String:AnyObject]
                print(dictProduct)
                
                let mainImage = PIService.object_forKeyWithValidationForClass_String(dict: dictProduct, key: "main_image")
                
                imgGallaryArray.append(mainImage)
              }
            }
          }
          
          arrMergeImages.append(contentsOf: imgMainArray)
          arrMergeImages.append(contentsOf: imgGallaryArray)
          
          self.chkvariation = true
          self.arrSlideShowSetVariation = []
          for strImages in arrMergeImages
          {
            let strImg = strImages.replacingOccurrences(of: " ", with: "%20")
            
            self.arrSlideShowSetVariation.append(strImg)
            
          }
          self.slideshow.dataSource = self
          self.slideshow.delegate = self
          self.slideshow.reloadData()
          print(self.arrSlideShowSetVariation)
          self.GetUserReviewsDataList()
        }
      }
    })
  }
  
  //MARK:Variation default Service
  func GetDefaultVariationService() {
    
    let strurl:String = BazarBit.GetDefaultProductVariationService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    print(headers)
    
    var parameter:Parameters = [:]
    parameter["product_id"] = APPDELEGATE.slugProdID
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      BazarBitLoader.stopLoader()
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
      
      if status == "ok"
      {
        if (responseDict["payload"] as? [String : AnyObject]) != nil
        {
          let payload:[String : AnyObject] = responseDict["payload"] as! [String : AnyObject]
          print(payload)
          
          if (payload["defaultVariationData"] as? [AnyObject]) != nil
          {
            let defaultVariation = payload["defaultVariationData"] as! [AnyObject]
            print(defaultVariation)
            
            self.aryDafaultVariationItem = []
            
            for aryVariation in defaultVariation
            {
              self.aryDafaultVariationItem.append(aryVariation)
            }
            print(self.aryDafaultVariationItem)
            
            if self.aryDafaultVariationItem.count > 0 {
              self.dictFirst = self.aryDafaultVariationItem[0] as! [String:AnyObject]
              
              self.defaultvariationCount = self.aryDafaultVariationItem.count
              
              self.arrTermsAttributeID = []
              self.arrAttributeID = []
              
              if APPDELEGATE.TermsProductId == ""  {
                APPDELEGATE.TermsProductId = PIService.object_forKeyWithValidationForClass_String(dict: self.dictFirst, key: "z_productid_fk")
                //print(APPDELEGATE.TermsProductId)
              }
              
              for i in 0...self.aryDafaultVariationItem.count - 1 {
                
                let dictAtt = self.aryDafaultVariationItem[i] as! [String:AnyObject]
                
                //print(dictAtt)
                
                let strAttributeTerms: String = PIService.object_forKeyWithValidationForClass_String(dict: dictAtt, key: "z_attributetermid_fk")
                
                self.arrTermsAttributeID.append(strAttributeTerms)
                
                let strAttribute : String = PIService.object_forKeyWithValidationForClass_String(dict: dictAtt, key: "z_attributeid_pk")
                
                self.arrAttributeID.append(strAttribute)
              }
            }
            
            self.defaultvariationCount = self.arrAttributeID.count
        
            self.DisableVariationOptionService(aryAttributeId: self.arrAttributeID, aryAttributeTermsId: self.arrTermsAttributeID)
            
            self.tblVariation.reloadData()
            
          }
        }
      }
    })
  }
  
  func ServiceCallForCheckItemInNotify(strProduct_ID : String) {
    
    let strurl:String = BazarBit.CheckItemInNotifyService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    print(headers)
    
    var parameter:Parameters = [:]
    parameter["product_id"] = strProduct_ID
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      BazarBitLoader.stopLoader()
      print(responseDict)
      let response : [String : AnyObject] = responseDict
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      if status == "ok"
      {
        //PIAlertUtils.displayAlertWithMessage(message)
        self.ServiceCallForAddProductInNotify(strProduct_ID: strProduct_ID)
      }else {
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  //MARK: ServiceCallForAddProductInNotify
  func ServiceCallForAddProductInNotify(strProduct_ID : String) {
    
    let strurl:String = BazarBit.AddItemInNotifyService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    print(headers)
    
    var parameter:Parameters = [:]
    parameter["product_id"] = strProduct_ID
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      BazarBitLoader.stopLoader()
      
      print(responseDict)
      
      let response : [String : AnyObject] = responseDict
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      if status == "ok" {
        PIAlertUtils.displayAlertWithMessage(message)
      }else {
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  func ServiceCallForCheckItemExistInCart(strProduct_ID : String, buttton : UIButton)
  {
    let strurl:String = BazarBit.CheckItemExistInCartService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    print(headers)
    
    var parameter:Parameters = [:]
    let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
    
    if UserId != nil {
      parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
      parameter["product_id"] = strProduct_ID
      parameter["session_id"] = ""
    }
    else
    {
      let strSessionId:String = BazarBit.getSessionId()
      parameter["user_id"] = ""
      parameter["session_id"] = strSessionId
      parameter["product_id"] = strProduct_ID
    }
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      BazarBitLoader.stopLoader()
      print(responseDict)
      var response : [String : AnyObject] = responseDict
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
      
      if status == "ok"
      {
        if (response["payload"] != nil)
        {
          let payload:[String:AnyObject] = response["payload"] as! [String:AnyObject]
          print(payload)
          
          let itemExistCount : String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "itemexists_count")
          
          if itemExistCount.toInt()! > 0
          {
            //print("GO TO CART")
            buttton.setTitle("GO TO CART", for: .normal)
          }
          else
          {
            //print("ADD TO CART")
            buttton.setTitle("ADD TO CART", for: .normal)
          }
        }
      }
    })
  }
  
  func ServiceCallForAddToCart(strProduct_ID : String, strProductName : String, strPrice: String) {
    
    let strurl:String = BazarBit.AddToCartService()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    print(headers)
    
    var parameter:Parameters = [:]
    let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
    
    if UserId != nil {
      parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
      parameter["product_id"] = strProduct_ID
      parameter["product_name"] = strProductName
      parameter["price"] = strPrice
      parameter["session_id"] = ""
    }
    else
    {
      parameter["user_id"] = ""
      parameter["product_id"] = strProduct_ID
      parameter["product_name"] = strProductName
      parameter["price"] = strPrice
      let strSessionId:String = BazarBit.getSessionId()
      parameter["session_id"] = strSessionId
    }
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      BazarBitLoader.stopLoader()
      print(responseDict)
      let response : [String : AnyObject] = responseDict
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      if status == "ok"
      {
        PIAlertUtils.displayAlertWithMessage(message)
        self.chkChangeGoToCart = true
        APPDELEGATE.variationDisable = true
        self.GetCartCountData()
        
        if self.chkVariationId == true {
            self.ServiceCallForCheckItemExistInCart(strProduct_ID: self.Variation_Prod_ID,buttton: self.btnAddToCart)
            
        } else {
            self.ServiceCallForCheckItemExistInCart(strProduct_ID: self.product_ID,buttton: self.btnAddToCart)
        }
        
        // self.GetProductDetailsBySlug()
      }
    })
  }
  
  func GetCartCountData() {
    
    let strurl:String = BazarBit.CountCartTotalService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter:Parameters = [:]
    
    let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
    
    if UserId != nil {
      parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
    }
    else
    {
      let strSessionId:String = BazarBit.getSessionId()
      parameter["session_id"] = strSessionId
      parameter["user_id"] = ""
    }
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      self.dicResponceCartTotalList = responseDict
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCartTotalList, key: "status")
      
      if status == "ok"
      {
        if self.dicResponceCartTotalList["payload"] != nil
        {
          let payload:[String:AnyObject] = self.dicResponceCartTotalList["payload"] as! [String:AnyObject]
          print(payload)
          
          let strCartTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "cart_items")
          UserDefaults.standard.set(strCartTotal, forKey: BazarBit.CARTTOTALCOUNT)
          
          if (UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) != nil) {
            
            let strCartCount: String = UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) as! String
            
            if strCartCount != "0" || strCartCount != "0.00" {
              print(strCartCount.characters.count)
              if strCartCount.characters.count == 4 {
                self.lblCartTotalWidthConstraints.constant = 30
              } else if strCartCount.characters.count == 3  {
                self.lblCartTotalWidthConstraints.constant = 25
              }  else {
                self.lblCartTotalWidthConstraints.constant = 18
              }
              
              self.lblCartTotal.text = strCartCount
              self.lblCartTotal.isHidden = false
            } else {
              self.lblCartTotal.isHidden = true
            }
            
            if Constants.DeviceType.IS_IPAD {
              self.lblCartTotal.layer.masksToBounds = true
              self.lblCartTotal.layer.cornerRadius = 10
            }
            else
            {
              self.lblCartTotal.layer.masksToBounds = true
              self.lblCartTotal.layer.cornerRadius = 10
            }
          }
        }
      }
    })
  }
  
  // MARK:- Helper Method (Dynamic Description height)
  func DynamicHeight() {
    var width:CGFloat = UIScreen.main.bounds.width;
    
    if Constants.DeviceType.IS_IPAD {
      width = UIScreen.main.bounds.width - 120
    } else {
      width = UIScreen.main.bounds.width - 25
    }
    
    let size: CGSize = CGSize(width: width, height: 1000.0)
    
    if Constants.DeviceType.IS_IPAD {
      let strDiscriptionHeight:CGFloat = Constants.HeightforLabel(text: lblLongDescription.text!, font: UIFont.systemFont(ofSize: 19.0), maxSize: size)
      
      if strDiscriptionHeight > 50 {
        LongDescriptionHeightConstraints.constant = strDiscriptionHeight + 40
      } else {
        LongDescriptionHeightConstraints.constant = 90
      }
    }
    else
    {
      let strDiscriptionHeight:CGFloat = Constants.HeightforLabel(text: lblLongDescription.text!, font: UIFont.systemFont(ofSize: 14.0), maxSize: size)
      
      if strDiscriptionHeight > 50 {
        LongDescriptionHeightConstraints.constant = strDiscriptionHeight + 28
      } else {
        LongDescriptionHeightConstraints.constant = 75
      }
    }
  }
  
  func checkValidation() -> Bool {
    
    var isValid: Bool = true
    strPinCode = txtPincode.text!
    strPinCode = strPinCode.trimmed()
    
    if PIValidation.isBlankString(str: strPinCode) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Pincode Required!")
      return isValid
    }
      
    else if strPinCode.characters.count != 6  {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Valid PinCode...!")
      return isValid
    }
    return true
  }
}
