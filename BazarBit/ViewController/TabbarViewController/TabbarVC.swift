//
//  TabbarVC.swift
//  PPC KART
//
//  Created by Parghi Infotech on 7/7/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class TabbarVC: UITabBarController, UITabBarControllerDelegate {
    
    var navigationHomeVC: UINavigationController!
    var navigationCategoryVC: UINavigationController!
    var navigationProfileVC: UINavigationController!
    
    var homeVC: HomeVC!
    var catagoryVC: CategoryVC!    
    var profileVC: ProfileVC!
    
    // MARK: - UIView Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        // Do any additional setup after loading the view.
        
        tabBar.tintColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createTabbar()
    }
    
    // MARK: - Helper Method
    func createTabbar()  {
        
        if APPDELEGATE.is_wishlist_active == "0"
        {
            if Constants.DeviceType.IS_IPAD {
                
                let mainStoryboard : UIStoryboard!
                mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                // Create Home Tab
                homeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                homeVC.tabbar = self
                navigationHomeVC = UINavigationController(rootViewController: homeVC)
                
                let HomeBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
                
                homeVC.tabBarItem = HomeBarItem
                
                // Create Category Tab
                catagoryVC = mainStoryboard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
                catagoryVC.tabbar = self
                navigationCategoryVC = UINavigationController(rootViewController: catagoryVC)
                
                let categoryBarItem = UITabBarItem(title: "Category", image: UIImage(named: "category"), selectedImage: UIImage(named: "category"))
                
                catagoryVC.tabBarItem = categoryBarItem
                
                // Create Profile Tab
                if profileVC == nil {
                    profileVC = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    profileVC.tabbar = self
                    navigationProfileVC = UINavigationController(rootViewController: profileVC)
                    let profileBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
                    
                    profileVC.tabBarItem = profileBarItem
                    
                    self.viewControllers = [navigationHomeVC, navigationCategoryVC, navigationProfileVC]
                }
            }
            else
            {
                let mainStoryboard : UIStoryboard!
                mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                // Create Home Tab
                homeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                homeVC.tabbar = self
                navigationHomeVC = UINavigationController(rootViewController: homeVC)
                
                let HomeBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
                
                homeVC.tabBarItem = HomeBarItem
                
                // Create Category Tab
                catagoryVC = mainStoryboard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
                catagoryVC.tabbar = self
                navigationCategoryVC = UINavigationController(rootViewController: catagoryVC)
                
                let categoryBarItem = UITabBarItem(title: "Category", image: UIImage(named: "category"), selectedImage: UIImage(named: "category"))
                
                catagoryVC.tabBarItem = categoryBarItem
                
                // Create Profile Tab
                if profileVC == nil {
                    profileVC = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    profileVC.tabbar = self
                    navigationProfileVC = UINavigationController(rootViewController: profileVC)
                    let profileBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
                    
                    profileVC.tabBarItem = profileBarItem
                    
                    self.viewControllers = [navigationHomeVC, navigationCategoryVC, navigationProfileVC]
                }
            }
        }
        else
        {
            if Constants.DeviceType.IS_IPAD {
                
                let mainStoryboard : UIStoryboard!
                mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                // Create WishList Tab
                APPDELEGATE.wishListVC = mainStoryboard.instantiateViewController(withIdentifier: "WishListVC") as! WishListVC
                APPDELEGATE.navigationWishListVC = UINavigationController(rootViewController: APPDELEGATE.wishListVC)
                
                let wishlistBarItem = UITabBarItem(title: "Wishlist", image: UIImage(named: "wishlist"), selectedImage: UIImage(named: "wishlist"))
                
                APPDELEGATE.wishListVC.tabBarItem = wishlistBarItem
                
                // Create Home Tab
                homeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                homeVC.tabbar = self
                navigationHomeVC = UINavigationController(rootViewController: homeVC)
                
                let HomeBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
                
                homeVC.tabBarItem = HomeBarItem
                
                // Create Category Tab
                catagoryVC = mainStoryboard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
                catagoryVC.tabbar = self
                navigationCategoryVC = UINavigationController(rootViewController: catagoryVC)
                
                let categoryBarItem = UITabBarItem(title: "Category", image: UIImage(named: "category"), selectedImage: UIImage(named: "category"))
                
                catagoryVC.tabBarItem = categoryBarItem
                
                // Create Profile Tab
                if profileVC == nil {
                    profileVC = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    profileVC.tabbar = self
                    navigationProfileVC = UINavigationController(rootViewController: profileVC)
                    let profileBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
                    
                    profileVC.tabBarItem = profileBarItem
                    
                    self.viewControllers = [navigationHomeVC, navigationCategoryVC, APPDELEGATE.navigationWishListVC , navigationProfileVC]
                }
            }
            else
            {
                let mainStoryboard : UIStoryboard!
                mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                // Create WishList Tab
                APPDELEGATE.wishListVC = mainStoryboard.instantiateViewController(withIdentifier: "WishListVC") as! WishListVC
                APPDELEGATE.navigationWishListVC = UINavigationController(rootViewController: APPDELEGATE.wishListVC)
                
                let wishlistBarItem = UITabBarItem(title: "Wishlist", image: UIImage(named: "wishlist"), selectedImage: UIImage(named: "wishlist"))
                
                APPDELEGATE.wishListVC.tabBarItem = wishlistBarItem
                
                // Create Home Tab
                homeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                homeVC.tabbar = self
                navigationHomeVC = UINavigationController(rootViewController: homeVC)
                
                let HomeBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
                
                homeVC.tabBarItem = HomeBarItem
                
                // Create Category Tab
                catagoryVC = mainStoryboard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
                catagoryVC.tabbar = self
                navigationCategoryVC = UINavigationController(rootViewController: catagoryVC)
                
                let categoryBarItem = UITabBarItem(title: "Category", image: UIImage(named: "category"), selectedImage: UIImage(named: "category"))
                
                catagoryVC.tabBarItem = categoryBarItem
                
                // Create Profile Tab
                if profileVC == nil {
                    profileVC = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    profileVC.tabbar = self
                    navigationProfileVC = UINavigationController(rootViewController: profileVC)
                    let profileBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
                    
                    profileVC.tabBarItem = profileBarItem
                    
                    self.viewControllers = [navigationHomeVC, navigationCategoryVC, APPDELEGATE.navigationWishListVC , navigationProfileVC]
                }
            }
        }
    }
    
    // MARK: - UITabbar Delegate Method
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        tabBarController.tabBar.tintColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        if APPDELEGATE.is_wishlist_active == "0"
        {
            if Constants.DeviceType.IS_IPAD {
                
                if tabBarController.selectedIndex == 0 {
                    navigationHomeVC.popToRootViewController(animated: true)
                    
                    
                } else if tabBarController.selectedIndex == 1 {
                    navigationCategoryVC.popToRootViewController(animated: true)
                    
                } else if tabBarController.selectedIndex == 2 {
                    
                    // Check Skip User Or Not Using USERID
                    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
                        navigationProfileVC.popToRootViewController(animated: true)
                    } else {
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            myDelegate.openLoginPage()
                            //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                        }
                    }
                }
            }
            else
            {
                if tabBarController.selectedIndex == 0 {
                    navigationHomeVC.popToRootViewController(animated: true)
                    self.homeVC.txtSearchProduct.resignFirstResponder()
                    //self.homeVC.isFromBtnHome = true
                    
                } else if tabBarController.selectedIndex == 1 {
                    navigationCategoryVC.popToRootViewController(animated: true)
                    self.view.resignFirstResponder()
                    
                } else if tabBarController.selectedIndex == 2 {
                    self.view.resignFirstResponder()
                    // Check Skip User Or Not Using USERID
                    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
                        navigationProfileVC.popToRootViewController(animated: true)
                    } else {
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            myDelegate.openLoginPage()
                            //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                        }
                    }
                }
            }
        }else {
            if Constants.DeviceType.IS_IPAD {
                
                if tabBarController.selectedIndex == 0 {
                    navigationHomeVC.popToRootViewController(animated: true)
                    
                } else if tabBarController.selectedIndex == 1 {
                    navigationCategoryVC.popToRootViewController(animated: true)
                    
                } else if tabBarController.selectedIndex == 2 {
                    
                    // Check Skip User Or Not Using USERID
                    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
                        APPDELEGATE.navigationWishListVC.popToRootViewController(animated: true)
                    } else {
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            myDelegate.openLoginPage()
                            //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                        }
                    }
                    
                }  else if tabBarController.selectedIndex == 3 {
                    
                    // Check Skip User Or Not Using USERID
                    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
                        navigationProfileVC.popToRootViewController(animated: true)
                    } else {
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            myDelegate.openLoginPage()
                            //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                        }
                    }
                }
            }
            else
            {
                if tabBarController.selectedIndex == 0 {
                    navigationHomeVC.popToRootViewController(animated: true)
                    
                    self.view.resignFirstResponder()
                    
                } else if tabBarController.selectedIndex == 1 {
                    navigationCategoryVC.popToRootViewController(animated: true)
                    self.view.resignFirstResponder()
                    
                } else if tabBarController.selectedIndex == 2 {
                    self.view.resignFirstResponder()
                    // Check Skip User Or Not Using USERID
                    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
                        APPDELEGATE.navigationWishListVC.popToRootViewController(animated: true)
                    } else {
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            myDelegate.openLoginPage()
                            //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                        }
                    }
                    
                }  else if tabBarController.selectedIndex == 3 {
                    self.view.resignFirstResponder()
                    // Check Skip User Or Not Using USERID
                    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
                        navigationProfileVC.popToRootViewController(animated: true)
                    } else {
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            myDelegate.openLoginPage()
                            //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                        }
                    }
                }
            }
        }
    }
}
