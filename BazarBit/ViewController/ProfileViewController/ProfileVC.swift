//
//  ProfileVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 09/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import EZSwiftExtensions

class ProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tabbar: TabbarVC!
    var identifier : String = "menuListCell"
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    
    @IBOutlet var viewProfile : UIView!
    @IBOutlet var imageViewProfile : UIImageView!
    @IBOutlet var lableName : PICustomTitleLable!
    @IBOutlet var lableEmail : PICustomTitleLable!
    @IBOutlet var labelLogout : PICustomMenuTitle!
    
    @IBOutlet var tableViewMenuList : UITableView!
    
    var arrMenuTitle : [String] = [String]()
    var arrMenuImages : [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        imageViewProfile.RoundImage()
        
        self.view.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        tableViewMenuList.delegate = self
        tableViewMenuList.dataSource = self
        
        self.tableViewMenuList.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        arrMenuTitle = ["My Account","Change Password","Order History","My Address","Reviews","India Rupee","Commission List"]
        arrMenuImages = ["man-user","lock","history","home_Icon","review","review","review"]
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        self.labelLogout.textColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        viewProfile.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
        lableName.text = UserDefaults.standard.object(forKey: BazarBit.USERNAME) as? String
        lableEmail.text = UserDefaults.standard.object(forKey: BazarBit.USEREMAILADDRESS) as? String
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERPROFILEIMAGE) != nil)
        {
            var imgUrl:String = UserDefaults.standard.object(forKey: BazarBit.USERPROFILEIMAGE) as! String
            imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
            let url1 = URL(string: imgUrl)
            self.imageViewProfile.kf.setImage(with: url1)
            self.imageViewProfile.kf.setImage(with: url1, placeholder: #imageLiteral(resourceName: "profile"))
        }
        else
        {
            let url1 = URL(string: "")
            self.imageViewProfile.kf.setImage(with: url1, placeholder: #imageLiteral(resourceName: "profile"))
        }
    }
    
    //MARK: buttonBack_Clicked
    @IBAction func buttonBack_Clicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if IS_IPAD_DEVICE() {
            return 70.0
        }
        return 46.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch arrMenuTitle[indexPath.row] {
        case "My Account" :
            print("My Account")
            let objMyAccountVC : MyAccountViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountViewController") as! MyAccountViewController
            
            self.navigationController?.pushViewController(objMyAccountVC, animated: true)
            break
        case "Change Password" :
            
            if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
                let changePasswordVC : ChangePasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                
                self.navigationController?.pushViewController(changePasswordVC, animated: true)
            } else {
                let loginVC : LoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            break
        case "Order History" :
            let orderViewController : OrderViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderViewController") as! OrderViewController
            
            self.navigationController?.pushViewController(orderViewController, animated: true)
            break
        case "My Address" :
            self.callService_AddressList()
            
        case "Reviews" :
            let reviewDetailViewController : ReviewDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReviewDetailViewController") as! ReviewDetailViewController
            
            self.navigationController?.pushViewController(reviewDetailViewController, animated: true)
            
            break
            
        case "India Rupee" :
            
            break
            
        case "Commission List" :
            
            let reviewDetailViewController : CommissionList_Controller = self.storyboard?.instantiateViewController(withIdentifier: "CommissionList_Controller") as! CommissionList_Controller
            self.navigationController?.pushViewController(reviewDetailViewController, animated: true)
            
            break
            
        default:
            break
        }
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let menuListCell : MenuListTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! MenuListTableViewCell
        
        menuListCell.lableTitle.text = arrMenuTitle[indexPath.row]
        menuListCell.imageViewMenuIcon.image = UIImage(named: arrMenuImages[indexPath.row])
        
        menuListCell.lableTitle.textColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        return menuListCell
    }
    
    // MARK: - Service Method
    
    func callService_AddressList()  {
        
        let strurl: String = BazarBit.GetaddressListService()
        
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        let parameter: Parameters = [:]
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            
            print(parameter)
        }
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dictResponse: [String: Any] = responseDict
            
            print(url)
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = dictResponse["status"] as! String
            
            if status == "ok" {
                
                if (dictResponse["payload"] as? [AnyObject]) == nil {
                    
                    let addAddressVC: ProfileAddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileAddAddressViewController") as! ProfileAddAddressViewController
                    
                    self.navigationController?.pushViewController(addAddressVC, animated: true)
                }else {
                    
                    let arrPayLoadList: [AnyObject] = dictResponse["payload"] as! [AnyObject]
                    
                    if arrPayLoadList.count > 0 {
                        let addressListViewControllerObj: AddressListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
                        
                        addressListViewControllerObj.isFromMyAddress = true
                        self.navigationController?.pushViewController(addressListViewControllerObj, animated: true)
                        
                        APPDELEGATE.chkProfile = true
                        
                    } else {
                        
                        let addAddressVC: ProfileAddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileAddAddressViewController") as! ProfileAddAddressViewController
                        self.navigationController?.pushViewController(addAddressVC, animated: true)
                    }
                }
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    // MARK:- btnOpenDrawer
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    //MARK: buttonLogout_Clicked
    @IBAction func buttonLogout_Clicked(_ sender: Any) {
        
        let strurl: String = BazarBit.logoutService()
        let url: URL = URL(string: strurl)!
        var headers: HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret 
        
        var parameter: Parameters = [:]
        parameter["user_token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN)
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            var response : [String : AnyObject] = responseDict
            BazarBitLoader.stopLoader()
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            
            let statusCode: Int = BazarBit.getValue(dict: responseDict, key: "code")
            
            print(status)
            
            if statusCode == 401 || statusCode == 200 {
                
                if (response["payload"] != nil) {
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERACCOUNT)
                    UserDefaults.standard.removeObject(forKey: BazarBit.AUTHTOKEN)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERID)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILID)
                    
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERNAME)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILADDRESS)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERPROFILEIMAGE)
                    
                    UserDefaults.standard.removeObject(forKey: BazarBit.SHIPPINGADDRESS)
                    UserDefaults.standard.removeObject(forKey: BazarBit.BILLINGADDRESS)
                    
                    UserDefaults.standard.removeObject(forKey: "slug")
                    UserDefaults.standard.removeObject(forKey: "title")
                    
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        UserDefaults.standard.removeObject(forKey: BazarBit.AUTOPINCODE)
                        myDelegate.openLoginPage()
                    }
                }
            }
            else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
}
