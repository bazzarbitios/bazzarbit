//
//  CheckOutViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 11/2/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import Toaster

class CheckOutViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource, VPMOTPViewDelegate, UITextFieldDelegate {
  
  @IBOutlet var viewNavigation : UIView!
  @IBOutlet var viewCustomTab : UIView!
  @IBOutlet var viewThankYou : UIView!
  @IBOutlet weak var viewFailPayUmoney: UIView!
  
  @IBOutlet weak var lableHeadertitle: PICustomTitleLable!
  @IBOutlet weak var buttonBack: UIButton!
  @IBOutlet var viewPaymentInfoHeight : NSLayoutConstraint!
  
  @IBOutlet var viewDiscountHeight : NSLayoutConstraint!
  @IBOutlet var viewShippingChargeHeight : NSLayoutConstraint!
  @IBOutlet var viewCodeHeight : NSLayoutConstraint!
  
  //OTP
  var arrCountryCode:[Any] = []
  var countryPicker = UIPickerView()
  var setPincode:String = ""
  
  @IBOutlet var lblTitleConfirmPurchase: UILabel!
  @IBOutlet var viewConfirmPurchase: UIView!
  @IBOutlet var textFieldCountryCode: PITextField!
  @IBOutlet var textFieldMobileNumber: PITextField!
  @IBOutlet var buttonSendOtp: PICustomButtonRoundedCornerBlack!
  
  //VERIFY OTP
  @IBOutlet var lblTitleConfirmPurchaseOtp: UILabel!
  @IBOutlet var viewConfirmPurchaseOtp: UIView!
  @IBOutlet var lableMobileNo: PIVarificationMsgLable!
  @IBOutlet var viewOtp: VPMOTPView!
  
  var strCountryCode:String = ""
  var strMobile:String = ""
  var strMobileNo : String = ""
  
  var strOtp : String = ""
  var strOrder_Otp_Id : String = ""
  var arrAddressList: [AnyObject] = []
  
  //WEBVIEW FOR PayuMoney
  var myWebView: UIWebView!
  @IBOutlet var loaderbackView : UIView!
  @IBOutlet var layerView : UIView!
  @IBOutlet var backView : UIView!
  @IBOutlet var loaderImage : UIImageView!
  
  var merchantKey : String = ""
  var salt : String = ""
  var PayUBaseUrl : String = ""
  var params : PUMRequestParams = PUMRequestParams.shared()
  var strGrandTotal : String = ""
  var dictPayUMoney : [String : AnyObject] = [String : AnyObject]()
  var sUrl : String = ""
  var fUrl : String = ""
  
  var strSUrl : String = ""
  var strFUrl : String = ""
  
  var chkpayumoney:Bool = false
  var dimView:UIView?
  
  @IBOutlet var viewShipping: UIView!
  @IBOutlet var viewBilling: UIView!
  
  @IBOutlet var viewMainBilling: UIView!
  
  @IBOutlet var viewPromoCodeHeight: NSLayoutConstraint!
  
  @IBOutlet var viewPromoCode: UIView!
  @IBOutlet var viewTotal: UIView!
  
  @IBOutlet var viewPromoCodeApplied: UIView!
  @IBOutlet var viewRoundPromoCode: UIView!
  
  @IBOutlet var viewPromoCodeAppliedHeight: NSLayoutConstraint!
  
  @IBOutlet var viewMainBillingHeight: NSLayoutConstraint!
  
  @IBOutlet var tblShoppingCart: UITableView!
  @IBOutlet var tblShoppingCartHeight: NSLayoutConstraint!
  
  @IBOutlet var tblPaymentGateway: UITableView!
  @IBOutlet var tblPaymentGatewayHeight: NSLayoutConstraint!
  
  @IBOutlet var imgCashOnDelivery: UIImageView!
  @IBOutlet var imgPayUMoney: UIImageView!
  
  var arrShoppingCart: [AnyObject] = []
  
  @IBOutlet var btnShippingAdd: UIButton!
  @IBOutlet var btnShippingChange: UIButton!
  
  @IBOutlet var lblTitleShippingAddress: UILabel!
  @IBOutlet var lblShippingName: UILabel!
  @IBOutlet var lblShippingAddress: UILabel!
  @IBOutlet var lblShippingCity: UILabel!
  @IBOutlet var lblShippingState: UILabel!
  @IBOutlet var lblShippingPhoneNo: UILabel!
  
  @IBOutlet var btnBillingAdd: UIButton!
  @IBOutlet var btnBillingChange: UIButton!
  
  @IBOutlet var lblTitleBillingAddress: UILabel!
  @IBOutlet var lblTitleSameAddress: UILabel!
  @IBOutlet var lblBillingName: UILabel!
  @IBOutlet var lblBillingAddress: UILabel!
  @IBOutlet var lblBillingCity: UILabel!
  @IBOutlet var lblBillingState: UILabel!
  @IBOutlet var lblBillingPhoneNo: UILabel!
  
  @IBOutlet var txtPromoCode: UITextField!
  @IBOutlet var lblPromoCode: UILabel!
  
  @IBOutlet var btnItemTotal: UIButton!
  @IBOutlet var btnApplyPromoCode: UIButton!
  
  @IBOutlet var imgBilling: UIImageView!
  
  @IBOutlet var lblTitleSubTotal: UILabel!
  @IBOutlet var lblTitleDiscount: UILabel!
  @IBOutlet var lblTitleShippingCharge: UILabel!
  @IBOutlet var lblTitleCodeCharge: UILabel!
  @IBOutlet var lblTitleTotal: UILabel!
  
  @IBOutlet var lblSubTotal: UILabel!
  @IBOutlet var lblDiscount: UILabel!
  @IBOutlet var lblShippingCharge: UILabel!
  @IBOutlet var lblCodeCharge: UILabel!
  @IBOutlet var lblTotal: UILabel!
  
  @IBOutlet var ViewPromo: UIView!
  @IBOutlet var lblTitlePayment: UILabel!
  
  var strShippingFirstName:String = ""
  var strShippingLastName:String = ""
  
  var strShippingName:String = ""
  var strShippingAddress:String = ""
  var strShippingCity:String = ""
  var strShippingOnlyCity:String = ""
  
  var strShippingState:String = ""
  var strShippingPhoneNo:String = ""
  var strShippingLandMark:String = ""
  var strShippingCountry:String = ""
  
  var paramterPinCode: String = ""
  
  var shippingPinCode: String = ""
  var strBillingPinCode: String = ""
  
  var strBillingFirstName:String = ""
  var strBillingLastName:String = ""
  
  var strBillingName:String = ""
  var strBillingAddress:String = ""
  var strBillingCity:String = ""
  var strBillingOnlyCity:String = ""
  
  var strBillingState:String = ""
  var strBillingPhoneNo:String = ""
  var strBillingLandMark:String = ""
  var strBillingCountry:String = ""
  
  var strPaymentType:String = "1"
  
  var previousIndexPath: IndexPath = IndexPath(row: 0, section: 0)
  var arrPaymentGateWay: [AnyObject] = []
  var strVC: String = ""
  
  var strBillingFlag: String = "0"
  var strAddressType: String = ""
  var strPushFromShopping: String = ""
  
  @IBOutlet var btnPlaceOrder: UIButton!
  
  @IBOutlet var btnPlaceOrder_Width: NSLayoutConstraint!
  @IBOutlet var btnTitle_Width: NSLayoutConstraint!
  
  @IBOutlet var imgAddress: UIImageView!
  @IBOutlet var imgPayment: UIImageView!
  @IBOutlet var imgSuccess: UIImageView!
  
  // For shipping
  var userName:String = ""
  var useraddress:String = ""
  var usercity:String = ""
  var userstate:String = ""
  var usermobile:String = ""
  var userPincode:String = ""
  
  //New user For Shipping And billing Both
  var strFirstName : String = ""
  var strLastName : String = ""
  var strAddress : String = ""
  var strcity : String = ""
  var strPincode : String = ""
  var strState : String = ""
  var strPhone : String = ""
  var chkshippingBilling : Bool = false
  
  var isFromBillingChange : Bool = false
  
  // MARK: - UIView Method
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.tabBarController?.tabBar.isHidden = true
    //First Time No Addresss So call here
    if APPDELEGATE.chkFirstAddress == true {
      self.lblShippingName.text = "\(strFirstName) \(strLastName)"
      self.lblBillingName.text = "\(strFirstName) \(strLastName)"
      
      self.lblShippingAddress.text = strAddress
      self.lblBillingAddress.text = strAddress
      
      self.lblShippingCity.text = strcity + " - " + strPincode
      self.lblBillingCity.text = strcity + " - " + strPincode
      
      self.lblShippingState.text = strState
      self.lblBillingState.text = strState
      
      self.lblShippingPhoneNo.text = strPhone
      self.lblBillingPhoneNo.text = strPhone
      APPDELEGATE.chkFirstAddress = false
      chkshippingBilling = true
      
      var localShippingDict: [String: AnyObject] = [:]
      
      localShippingDict["first_name"] = strFirstName as AnyObject
      localShippingDict["last_name"] = strLastName as AnyObject
      localShippingDict["address"] = strAddress as AnyObject
      localShippingDict["pincode"] = strPincode as AnyObject
      localShippingDict["city"] = strcity as AnyObject
      localShippingDict["state"] = strState as AnyObject
      localShippingDict["phone_number"] = strPhone as AnyObject
      localShippingDict["country"] = strShippingCountry as AnyObject
      localShippingDict["landmark"] = strShippingLandMark as AnyObject
      
      UserDefaultFunction.setCustomDictionary(dict: localShippingDict, key: BazarBit.SHIPPINGADDRESS)
      UserDefaultFunction.setCustomDictionary(dict: localShippingDict, key: BazarBit.BILLINGADDRESS)
    }
    
    //Call when Select individual when Add Shipping Click And Un select Same a shipping from Add address screen
    if APPDELEGATE.strvalidate == "Shipping" {
      if APPDELEGATE.chkSameAsShipping == "Individual" {
        
        self.lblShippingName.text = "\(strFirstName) \(strLastName)"
        self.lblShippingAddress.text = strAddress
        self.lblShippingCity.text = strcity + " - " + strPincode
        self.lblShippingState.text = strState
        self.lblShippingPhoneNo.text = strPhone
        
        APPDELEGATE.chkFirstAddress = false
        chkshippingBilling = true
        
        var localShippingDict: [String: AnyObject] = [:]
        
        localShippingDict["first_name"] = strFirstName as AnyObject
        localShippingDict["last_name"] = strLastName as AnyObject
        localShippingDict["address"] = strAddress as AnyObject
        localShippingDict["pincode"] = strPincode as AnyObject
        localShippingDict["city"] = strcity as AnyObject
        localShippingDict["state"] = strState as AnyObject
        localShippingDict["phone_number"] = strPhone as AnyObject
        localShippingDict["country"] = strShippingCountry as AnyObject
        localShippingDict["landmark"] = strShippingLandMark as AnyObject
        
        UserDefaultFunction.setCustomDictionary(dict: localShippingDict, key: BazarBit.SHIPPINGADDRESS)
        APPDELEGATE.chkSameAsShipping = ""
      }
    }
      
      //Call when Select individual when Add Billing Click And Un select Same a shipping from Add address screen
    else if APPDELEGATE.strvalidate == "Billing" {
      if APPDELEGATE.chkSameAsShipping == "Individual" {
        
        self.lblBillingName.text = "\(strBillingFirstName) \(strBillingLastName)"
        self.lblBillingAddress.text = strBillingAddress
        self.lblBillingCity.text = strBillingCity + " - " + strBillingPinCode
        self.lblBillingState.text = strBillingState
        self.lblBillingPhoneNo.text = strBillingPhoneNo
        
        APPDELEGATE.chkFirstAddress = false
        chkshippingBilling = true
        
        var localBillingDict: [String: AnyObject] = [:]
        
        localBillingDict["first_name"] = strBillingFirstName as AnyObject
        localBillingDict["last_name"] = strBillingLastName as AnyObject
        localBillingDict["address"] = strBillingAddress as AnyObject
        localBillingDict["pincode"] = strBillingPinCode as AnyObject
        localBillingDict["city"] = strBillingCity as AnyObject
        localBillingDict["state"] = strBillingState as AnyObject
        localBillingDict["phone_number"] = strBillingPhoneNo as AnyObject
        localBillingDict["country"] = strBillingCountry as AnyObject
        localBillingDict["landmark"] = strBillingLandMark as AnyObject
        
        UserDefaultFunction.setCustomDictionary(dict: localBillingDict, key: BazarBit.BILLINGADDRESS)
        APPDELEGATE.chkSameAsShipping = ""
      }
    }
    
    //Call when Select individual only first time when address is blank
    if APPDELEGATE.chkNewUser == true {
      self.lblShippingName.text = "\(strFirstName) \(strLastName)"
      self.lblBillingName.text = "\(strBillingFirstName) \(strBillingLastName)"
      
      self.lblShippingAddress.text = strAddress
      self.lblBillingAddress.text = strBillingAddress
      
      self.lblShippingCity.text = strcity + " - " + strPincode
      self.lblBillingCity.text = strBillingCity + " - " + strBillingPinCode
      
      self.lblShippingState.text = strState
      self.lblBillingState.text = strBillingState
      
      self.lblShippingPhoneNo.text = strPhone
      self.lblBillingPhoneNo.text = strBillingPhoneNo
      APPDELEGATE.chkFirstAddress = false
      chkshippingBilling = true
      
      var localShippingDict: [String: AnyObject] = [:]
      var localBillingDict: [String: AnyObject] = [:]
      
      localShippingDict["first_name"] = strFirstName as AnyObject
      localShippingDict["last_name"] = strLastName as AnyObject
      localShippingDict["address"] = strAddress as AnyObject
      localShippingDict["pincode"] = strPincode as AnyObject
      localShippingDict["city"] = strcity as AnyObject
      localShippingDict["state"] = strState as AnyObject
      localShippingDict["phone_number"] = strPhone as AnyObject
      localShippingDict["country"] = strShippingCountry as AnyObject
      localShippingDict["landmark"] = strShippingLandMark as AnyObject
      
      localBillingDict["first_name"] = strBillingFirstName as AnyObject
      localBillingDict["last_name"] = strBillingLastName as AnyObject
      localBillingDict["address"] = strBillingAddress as AnyObject
      localBillingDict["pincode"] = strBillingPinCode as AnyObject
      localBillingDict["city"] = strBillingCity as AnyObject
      localBillingDict["state"] = strBillingState as AnyObject
      localBillingDict["phone_number"] = strBillingPhoneNo as AnyObject
      localBillingDict["country"] = strBillingCountry as AnyObject
      localBillingDict["landmark"] = strBillingLandMark as AnyObject
        
      UserDefaultFunction.setCustomDictionary(dict: localShippingDict, key: BazarBit.SHIPPINGADDRESS)
      UserDefaultFunction.setCustomDictionary(dict: localBillingDict, key: BazarBit.BILLINGADDRESS)
      APPDELEGATE.chkSameAsShipping = ""
      APPDELEGATE.chkNewUser = false
    }
    
    //Call Only When click on CheckOut (Shopping cart To Check Out page)
    if APPDELEGATE.chklastOrder == false {
      self.callService_LastOrderService()
      APPDELEGATE.chklastOrder = true
    }
    
    buttonBack.isHidden = false
    HideShow_PromoCode()
    SetPropertiesOFView()
    
    self.viewThankYou.isHidden = true
    self.viewFailPayUmoney.isHidden = true
    
    addPropertyPopupFunction()
    addPropertyConfirmPurchasePopupFunction()
    addPropertyConfirmPurchaseOtpPopupFunction()
    
    callService_PaymentGatewayService()
    callService_ShoppingCartList()
    
    self.callCountryCodeService()
    
    self.imgAddress.isHidden = true
    self.imgPayment.isHidden = true
    self.imgSuccess.isHidden = true
    
    self.navigationController?.navigationBar.isHidden = true
    viewMainBillingHeight.constant = 0
    viewMainBilling.isHidden = true
    // Do any additional setup after loading the view.
    
    viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    viewCustomTab.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    btnItemTotal.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
    btnPlaceOrder.setTitleColor(UIColor(hexString: Constants.BUTTON_TEXT_COLOR), for: .normal)
    btnPlaceOrder.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
    
    //SHIPPING
    btnShippingAdd.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
    btnShippingChange.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
    
    lblTitleShippingAddress.setTextLabelLightColor()
    
    btnApplyPromoCode.setTitleColor(UIColor(hexString: Constants.BUTTON_TEXT_COLOR), for: .normal)
    btnApplyPromoCode.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
    btnApplyPromoCode.setCornerRadius(radius: 10.0)
    
    txtPromoCode.setLeftPaddingPoints(10)
    txtPromoCode.setTextFieldDynamicColor()
    
    txtPromoCode.setCornerRadius(radius: 15.0)
    lblShippingName.setTextLabelDynamicTextColor()
    lblShippingAddress.setTextLabelLightColor()
    lblShippingCity.setTextLabelLightColor()
    lblShippingState.setTextLabelLightColor()
    lblShippingPhoneNo.setTextLabelDynamicTextColor()
    
    //BILLING
    btnBillingAdd.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
    btnBillingChange.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
    
    lblTitleSameAddress.setTextLabelLightColor()
    lblTitleBillingAddress.setTextLabelDynamicTextColor()
    
    lblBillingName.setTextLabelDynamicTextColor()
    lblBillingAddress.setTextLabelLightColor()
    lblBillingCity.setTextLabelLightColor()
    lblBillingState.setTextLabelLightColor()
    lblBillingPhoneNo.setTextLabelDynamicTextColor()
    
    txtPromoCode.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
    
    //ORDER AMOUNT
    lblTitleSubTotal.setTextLabelLightColor()
    lblTitleDiscount.setTextLabelLightColor()
    lblTitleShippingCharge.setTextLabelLightColor()
    lblTitleCodeCharge.setTextLabelLightColor()
    lblTitleTotal.setTextLabelDynamicTextColor()
    
    lblSubTotal.setTextLabelLightColor()
    lblDiscount.setTextLabelLightColor()
    lblShippingCharge.setTextLabelLightColor()
    lblCodeCharge.setTextLabelLightColor()
    lblTotal.setTextLabelDynamicTextColor()
    
    viewPromoCode.applyShadowDefault()
    viewTotal.applyShadowDefault()
    
    lblTitleConfirmPurchase.setTextLabelDynamicTextColor()
    
    if IS_IPHONE_5_OR_5S() {
      viewOtp.otpFieldSize = 38
      
    } else if IS_IPAD_DEVICE()  {
      viewOtp.otpFieldSize = 50
      
    } else  {
      viewOtp.otpFieldSize = 45
    }
    
    viewOtp.otpFieldsCount = 6
    viewOtp.otpFieldDisplayType = .circular
    viewOtp.otpFieldBorderWidth = 0
    viewOtp.cursorColor = UIColor.white
    viewOtp.otpFieldDefaultBackgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!
    viewOtp.otpFieldEnteredBackgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!
    viewOtp.delegate = self as? VPMOTPViewDelegate
    viewOtp.initalizeUI()
  }
  
  func hasEnteredAllOTP(hasEntered: Bool) {
    print("Has entered all OTP? \(hasEntered)")
    if hasEntered {
      var strEnterOTP = viewOtp.getOTPString()
      strEnterOTP = strEnterOTP.trimmed()
      strOtp = strEnterOTP
      
      if strEnterOTP.characters.count == 6 {
        self.ServiceCallForVerifyOtp()
      }else {
        PIAlertUtils.displayAlertWithMessage("Please Enter OTP..!")
      }
    }
  }
  
  func enteredOTP(otpString: String) {
    print("OTPString: \(otpString)")
    strOtp = otpString
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    self.chkpayumoney = false
    textFieldMobileNumber.keyboardType = .numberPad
    
    self.viewPaymentInfoHeight.constant = 0
    self.viewTotal.isHidden = true
    // landmark and Country
    
    //Set last order for both Shipping And Billing
    showLastOrderServiceRecord()
    
    if APPDELEGATE.isBilling == false {
      imgBilling.image = #imageLiteral(resourceName: "square")
      
      if Constants.DeviceType.IS_IPAD {
        viewMainBillingHeight.constant = 180
      } else {
        viewMainBillingHeight.constant = 120
      }
      
      viewMainBilling.isHidden = false
      self.strBillingFlag = "1"
      
    } else {
      imgBilling.image = #imageLiteral(resourceName: "rightSquare")
      viewMainBillingHeight.constant = 0
      viewMainBilling.isHidden = true
      self.strBillingFlag = "0"
    }
    
    if APPDELEGATE.is_order_active == "1" {
      btnPlaceOrder_Width.constant = SCREEN_WIDTH/2
      btnTitle_Width.constant = SCREEN_WIDTH/2
    }else {
      btnPlaceOrder_Width.constant = 0
      btnTitle_Width.constant = SCREEN_WIDTH
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  //MARK: PAY U MONEY
  func initPayment(strOrderId : String) {
    
    let frame : CGRect = CGRect(x: 0, y: 128, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - 128)
    self.myWebView = UIWebView(frame: frame)//UIWebView(frame: UIScreen.main.bounds)
    self.myWebView.delegate = self
    self.view.addSubview(self.myWebView)
    self.startLoader()
    self.chkpayumoney = true
    let i = arc4random()
    
    print(self.dictPayUMoney)
    
    merchantKey = self.dictPayUMoney["merchantid"] as! String
    salt = self.dictPayUMoney["secretkey"] as! String
    
    PayUBaseUrl = "https://test.payu.in/_payment"
    
    params.amount = self.strGrandTotal
    params.firstname = self.strShippingName
    
    params.phone = ""
    params.email = ""
    params.productinfo = "Order"
    params.surl = self.dictPayUMoney["payumoneysuccessurl"] as! String
    
    sUrl = self.dictPayUMoney["payumoneysuccessurl"] as! String
    params.furl = self.dictPayUMoney["payumoneyfailureurl"] as! String
    fUrl = self.dictPayUMoney["payumoneyfailureurl"] as! String
    
   // var service_provider = "payu_paisa"
     var service_provider : String = "payu_paisa"
    let strHash:String = self.sha1(string: String.localizedStringWithFormat("%d%@", i, NSDate()))
    print(strHash)
    
    let myString = strHash
    let start = myString.index(myString.startIndex, offsetBy: 0)
    let end = myString.index(myString.startIndex, offsetBy: 20)
    let myRange = start..<end
    myString.substring(with: myRange)
    
    params.udf1 = strOrderId
    print(params.udf1)
    
    params.udf2 = UserDefaults.standard.value(forKey: BazarBit.USERID) as! String
    print(params.udf2)
    
    params.udf3 = UserDefaults.standard.value(forKey: BazarBit.AUTHTOKEN) as! String
    print(params.udf3)
    
    params.udf4 = ""
    params.udf5 = ""
    params.environment = PUMEnvironment.test
    
    params.delegate = self
    
    params.udf6 = ""
    params.udf7 = ""
    params.udf8 = ""
    params.udf9 = ""
    params.udf10 = ""
    
    print(myString.substring(with: myRange))
    
    let txnid1 = myString.substring(with: myRange)
    
    let hashSequence = "\(merchantKey)|\(txnid1)|\(params.amount!)|\(params.productinfo!)|\(params.firstname!)|\(params.email!)|\(params.udf1!)|\(params.udf2!)|\(params.udf3!)|\(params.udf4!)|\(params.udf5!)||||||\(salt)"
    
    let hash = self.sha1(string: hashSequence)
    
    print(hashSequence)
    print(hash)
    
    //PROPER
 
//    let postStr = "key=" + merchantKey + "&amount=" + params.amount + "&txnid=" + txnid1 + "&productinfo=" + params.productinfo + "&email=" + params.email + "&firstname=" + params.firstname + "&phone=" + params.phone + "&surl=" + sUrl + "&furl=" + fUrl + "&hash=" + hash + "&udf1=" + params.udf1 + "&udf2=" + params.udf2 + "&udf3=" + params.udf3 + "&udf4=" + params.udf4 + "&udf5=" + params.udf5 + "&service_provider=" + service_provider


    let postStr = "key=" + merchantKey + "&amount=" + params.amount + "&txnid=" + txnid1 + "&productinfo=" + params.productinfo + "&email=" + params.email + "&firstname=" + params.firstname + "&phone=" + params.phone + "&surl=" + sUrl + "&furl=" + fUrl + "&hash=" + hash + "&udf1=" + params.udf1 + "&udf2=" + params.udf2 + "&udf3=" + params.udf3 + "&udf4=" + params.udf4 + "&udf5=" + params.udf5 + "&service_provider=payu_paisa"
    
    print(postStr)
    
    let url = NSURL(string: String.localizedStringWithFormat("%@", PayUBaseUrl))
    
    print("check my url", url!, postStr)
    
    let request = NSMutableURLRequest(url: url! as URL)
    
    do {
      
      let postLength = String.localizedStringWithFormat("%lu",postStr.characters.count)
      request.httpMethod = "POST"
      request.setValue("application/x-www -form-urlencoded", forHTTPHeaderField: "Current-Type")
      request.setValue(postLength, forHTTPHeaderField: "Content-Length")
      request.httpBody = postStr.data(using: String.Encoding.utf8)
      myWebView.loadRequest(request as URLRequest)
      
    } catch {
      
    }
  }
  
  //WEBVIEW DELEGATE
  func webViewDidStartLoad(_ webView: UIWebView)
  {
    //startLoader()
  }
  
  func startLoader() {
    
    myWebView.isHidden = true
    layerView.backgroundColor = UIColor(white: 0.0, alpha: 0.7)
    backView.layer.cornerRadius = 10.0
    loaderbackView.isHidden = false
    
    let myimgArr = ["icon1","icon2","icon3","icon4"]
    var images = [UIImage]()
    
    for i in 0..<myimgArr.count
    {
      images.append(UIImage(named: myimgArr[i])!)
    }
    
    loaderImage.animationImages = images
    loaderImage.animationDuration = 1.0
    loaderImage.startAnimating()
    if APPDELEGATE.chkPayUMoneyTitle == true {
      lableHeadertitle.text = "Payment Information"
    }
  }
  
  func stopLoader() {
    loaderImage.stopAnimating()
    loaderbackView.isHidden = true
    myWebView.isHidden = false
    APPDELEGATE.chkPayUMoneyTitle = false
  }
  
  func webViewDidFinishLoad(_ webView: UIWebView)
  {
    stopLoader()
    let requestURL = self.myWebView.request?.url
    let requestString:String = (requestURL?.absoluteString)!
    print(requestString)
    if requestString.contains(sUrl) {
      print("success payment done")
      buttonBack.isHidden = true
      self.imgPayment.isHidden = false
      self.imgSuccess.isHidden = false
      self.myWebView.isHidden = true
      self.viewThankYou.isHidden = false
      self.myWebView.delegate = nil
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
        self.navigationController?.popViewController(animated: true)
        APPDELEGATE.openHomePage()
      }
      
    }else if requestString.contains(fUrl) {
      print("payment failure")
      buttonBack.isHidden = true
      self.viewFailPayUmoney.isHidden = false
      self.myWebView.isHidden = true
      self.myWebView.delegate = nil
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
        self.navigationController?.popViewController(animated: true)
        APPDELEGATE.openHomePage()
      }
    }
  }
  
  func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
  {
    let requestURL = self.myWebView.request?.url
    print("WebView failed loading with requestURL: \(String(describing: requestURL)) with error: \(String(describing: error.localizedDescription)) & error code: \(String(describing: error))")
    
    if error._code == -1009 || error._code == -1003 {
      self.myWebView.isHidden = true
      print("Please check your internet connection!")
    }else if error._code == -1001 {
      self.myWebView.isHidden = true
      print("The request timed out.")
    }
  }
  
  func sha1(string:String) -> String {
    let cstr = string.cString(using: String.Encoding.utf8)
    
    let data = NSData(bytes: cstr, length: string.characters.count)
    var digest = [UInt8](repeating: 0, count:Int(CC_SHA512_DIGEST_LENGTH))
    CC_SHA512(data.bytes, CC_LONG(data.length), &digest)
    let hexBytes = digest.map { String(format: "%02x", $0) }
    return hexBytes.joined(separator: "")
  }
  
  // MARK:- UITableView Method
  func numberOfSections(in tableView: UITableView) -> Int {
    if tableView == tblShoppingCart {
      return 1
      
    } else if tableView == tblPaymentGateway {
      return 1
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if tableView == tblShoppingCart {
      return arrShoppingCart.count
      
    } else if tableView == tblPaymentGateway {
      return arrPaymentGateWay.count
    }
    
    return 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if tableView == tblShoppingCart {
      
      let identifier : String = "ShoppingCartCell"
      
      let shoppingCartCell : ShoppingCartCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ShoppingCartCell
      
      let dict: [String: AnyObject] = arrShoppingCart[indexPath.row] as! [String: AnyObject]
      
      let productData: [String: AnyObject] = dict["product_data"] as! [String: AnyObject]
      
      if (productData["main_image"] as? [String :AnyObject]) != nil {
        
        let mainImage: [String: AnyObject] = productData["main_image"] as! [String: AnyObject]
        
        var strImage: String = PIService.object_forKeyWithValidationForClass_String(dict: mainImage, key: "main_image")
        strImage = GetImageUrl(strurl: strImage)
        let strurl: URL = URL(string: strImage)!
        shoppingCartCell.imgShoppingView.kf.setImage(with: strurl)
      }
      
      let strName: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "name")
      let strDescribtion: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "short_description")
      
      var strPrize: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "price")
      strPrize = BazarBitCurrency.setPriceSign(strPrice: strPrize)
      
      var attributedPrizeString = NSAttributedString(string: strPrize)
      attributedPrizeString = attributedPrizeString.strikethrough()
      
      var strSalePrice: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "sale_price")
      strSalePrice = String(format: "%.2f", strSalePrice.toFloat()!)
      let strSalePrize = BazarBitCurrency.setPriceSign(strPrice: strSalePrice)
      
      let strQuantity: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_quantity")
      
      if strSalePrice == "0" || strSalePrice == "0.00" {
        shoppingCartCell.lblPrize.text = strPrize
        shoppingCartCell.lblPrize.setTextLabelDynamicTextColor()
        shoppingCartCell.lblSalePrize.isHidden = true
        shoppingCartCell.lblSalePriceWidth.constant = 0.0
      }else {
        shoppingCartCell.lblPrize.attributedText = attributedPrizeString
        shoppingCartCell.lblSalePrize.text = strSalePrize
        shoppingCartCell.lblSalePrize.isHidden = false
        shoppingCartCell.lblSalePriceWidth.constant = 60.0
      }
      
      shoppingCartCell.lblName.text = strName
      shoppingCartCell.lblDescribition.text = strDescribtion
      shoppingCartCell.lblQuantity.text = "Quantity :- \(strQuantity)"
      shoppingCartCell.viewShoppingCart.applyShadowWithColor(.lightGray)
      
      return shoppingCartCell
      
    }  else if tableView == tblPaymentGateway {
      
      let identifier : String = "PaymentGatewayCell"
      
      let paymentGatewayCell : PaymentGatewayCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! PaymentGatewayCell
      
      if indexPath.row == previousIndexPath.row {
        paymentGatewayCell.imgPayment.image = #imageLiteral(resourceName: "radio")
      } else {
        paymentGatewayCell.imgPayment.image = #imageLiteral(resourceName: "radioLight")
      }
      
      let dict: [String: AnyObject] = arrPaymentGateWay[indexPath.row] as! [String: AnyObject]
      let strTitle:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "title")
      paymentGatewayCell.lblTitle.text = strTitle
      
      return paymentGatewayCell
    }
    
    return UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    if tableView == tblShoppingCart {
      
      if Constants.DeviceType.IS_IPAD {
        
        let dict: [String: AnyObject] = arrShoppingCart[indexPath.row] as! [String: AnyObject]
        let productData: [String: AnyObject] = dict["product_data"] as! [String: AnyObject]
        let strDescribtion: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "short_description")
        
        var width: CGFloat = 0
        
        width = UIScreen.main.bounds.width - 350
        
        
        let size: CGSize = CGSize(width: width, height: 1000.0)
        
        var strDiscriptionHeight:CGFloat = Constants.HeightforLabel(text: strDescribtion, font: UIFont.systemFont(ofSize: 18.0), maxSize: size)
        
        if strDiscriptionHeight <=  60 {
          strDiscriptionHeight = 60
        }
        
        return 130 - 60 + strDiscriptionHeight
        
      } else {
        
        let dict: [String: AnyObject] = arrShoppingCart[indexPath.row] as! [String: AnyObject]
        let productData: [String: AnyObject] = dict["product_data"] as! [String: AnyObject]
        let strDescribtion: String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "short_description")
        
        var width: CGFloat = 0
        
        width = UIScreen.main.bounds.width - 100
        
        let size: CGSize = CGSize(width: width, height: 1000.0)
        
        var strDiscriptionHeight:CGFloat = Constants.HeightforLabel(text: strDescribtion, font: UIFont.systemFont(ofSize: 12.0), maxSize: size)
        
        if strDiscriptionHeight <=  30 {
          strDiscriptionHeight = 30
        }
        return 100 - 30 + strDiscriptionHeight
      }
      
    } else if tableView == tblPaymentGateway {
      
      if Constants.DeviceType.IS_IPAD {
        return 44.0
      } else {
        return 30.0
      }
    } else {
      return 0
    }
  }
  
  // MARK: - Textfield Methods
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if string == "" {
      return true
    }
    
    if textField == textFieldCountryCode {
      return false
    }
    
    if textField == textFieldMobileNumber {
      if (textField.text?.characters.count)! >= 15 {
        return false
      }
      if !(string.isNumber()) {
        return false
      }
    }
    return true
  }
  
  //MARK: - Validation Method
  func checkValidation() -> Bool {
    
    var isValid: Bool = true
    
    strCountryCode = textFieldCountryCode.text!
    strMobile = textFieldMobileNumber.text!
    
    strCountryCode = strCountryCode.trimmed()
    strMobile = strMobile.trimmed()
    
    if PIValidation.isBlankString(str: strCountryCode) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please select country code...!")
      return isValid
      
    }  else if PIValidation.isBlankString(str: strMobile) {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Please Enter Mobile Number...!")
      return isValid
      
    }  else if strMobile.characters.count < 10 || strMobile.characters.count > 15 {
      isValid = false
      PIAlertUtils.displayAlertWithMessage("Enter Valid Mobile No...!")
      return isValid
    }
    
    return isValid
  }
  
  // MARK: - Action Method
  @IBAction func buttonSendOtpClicked(_ sender: Any) {
    
    strMobileNo = "(\(textFieldCountryCode.text!))\(textFieldMobileNumber.text!)"
    print(strMobileNo)
    
    strMobile = textFieldMobileNumber.text!
    
    if checkValidation() {
      self.ServiceCallForSendOtp(strOtp: strCountryCode, strMobileNo: textFieldMobileNumber.text!)
    }
  }
  
  //OTP
  @IBAction func buttonVerifyClicked(_ sender: Any) {
    var strEnterOTP = viewOtp.getOTPString()
    strEnterOTP = strEnterOTP.trimmed()
    
    if strEnterOTP.characters.count == 6 {
      self.ServiceCallForVerifyOtp()
    }else {
      PIAlertUtils.displayAlertWithMessage("Please Enter OTP..!")
    }
  }
  
  @IBAction func buttonResendClicked(_ sender: Any) {
    viewOtp.clearTextField()
    viewOtp.initalizeUI2()
    self.ServiceCallForSendOtp(strOtp: strCountryCode, strMobileNo: strMobile)
  }
  
  @IBAction func btnCloseConfirmPurchaseOtp(_ sender: AnyObject)  {
    viewOtp.clearTextField()
    viewOtp.initalizeUI2()
    viewOtp.resignFirstResponder()
    self.popUpHide(popupView: viewConfirmPurchaseOtp)
  }
  
  @IBAction func btnPlaceOrder(_ sender: Any)  {
    if APPDELEGATE.check_pincode_availibility == "1" {
        self.CheckPincodeService()
    }else {
       self.callService_PlaceOrderService()
    }
    
  }
  
  @IBAction func btnCloseConfirmPurchase(_ sender: AnyObject)  {
    self.textFieldMobileNumber.text = ""
    self.textFieldCountryCode.text = ""
    self.textFieldMobileNumber.resignFirstResponder()
    self.textFieldCountryCode.resignFirstResponder()
    self.popUpHide(popupView: viewConfirmPurchase)
  }
  
  @IBAction func btnBack(_ sender: Any)  {
    if self.chkpayumoney == false {
      //Every Time push to Shopping cart VC
      APPDELEGATE.chkListingBack = true
      self.navigationController?.popToViewController(APPDELEGATE.shoppingCartVC, animated: true)
      //            self.navigationController?.popViewController(animated: true)
    } else {
      self.myWebView.isHidden = true
      buttonBack.isHidden = true
      self.viewFailPayUmoney.isHidden = false
      imgPayment.isHidden = false
      imgSuccess.isHidden = false
      self.imgPayment.image = UIImage(named: "cancel")
      self.imgSuccess.image = UIImage(named: "cancel")
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
        APPDELEGATE.openHomePage()
      }
    }
  }
  
  @IBAction func btnPaymentGateway(_ sender: AnyObject)  {
    
    let buttonPosition = sender.convert(CGPoint.zero, to: self.tblPaymentGateway)
    previousIndexPath = self.tblPaymentGateway.indexPathForRow(at: buttonPosition)!
    
    let dict : [String : AnyObject] = arrPaymentGateWay[previousIndexPath.row] as! [String : AnyObject]
    
    let strTitle:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "title")
    
    if strTitle == "Cash On Delivery"
    {
      strPaymentType = "1"
      btnPlaceOrder.setTitle("Place Order", for: .normal)
    }
    else if strTitle == "PayU Money"
    {
      strPaymentType = "2"
      btnPlaceOrder.setTitle("Next Payment Info", for: .normal)
    }
    else if strTitle == "PayPal"
    {
      strPaymentType = "3"
      btnPlaceOrder.setTitle("Place Order", for: .normal)
    }
    else if strTitle == "CC avenue"
    {
      strPaymentType = "4"
      btnPlaceOrder.setTitle("Place Order", for: .normal)
    }
    tblPaymentGateway.reloadData()
  }
  
  @IBAction func btnShippingChange(_ sender: AnyObject)  {
    self.strAddressType = "0"
    APPDELEGATE.strChkProfile = false
    APPDELEGATE.chkProfile = false
    //self.strAddressType = "1"
    self.callService_AddressList()
    APPDELEGATE.chkLablecheck = "Shipping"
    isFromBillingChange = false
  }
  
  @IBAction func btnShippingAdd(_ sender: AnyObject) {
    let addAddressVC : AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
    
    //addAddressVC.fromVC = "CheckOut"
    
    if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil) {
      APPDELEGATE.strvalidate = "Shipping"
    } else {
      APPDELEGATE.strvalidate = ""
    }
    
    self.navigationController?.pushViewController(addAddressVC, animated: true)
  }
  
  @IBAction func btnBillingChange(_ sender: AnyObject)  {
    self.strAddressType = "1"
    APPDELEGATE.strChkProfile = false
    APPDELEGATE.chkProfile = false
    self.callService_AddressList()
    APPDELEGATE.chkLablecheck = "Billing"
    isFromBillingChange = true
  }
  
  @IBAction func btnBillingAdd(_ sender: AnyObject)  {
    
    let addAddressVC : AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
    
    APPDELEGATE.strvalidate = "Billing"
    
    if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil)
    {
      addAddressVC.isFromBillingAdd = true
    }else {
      addAddressVC.isFromBillingAdd = false
    }
    
    self.navigationController?.pushViewController(addAddressVC, animated: true)
  }
  
  @IBAction func btnRemovePromoCode(_ sender: AnyObject)  {
    callService_RemovePromoCodeService()
  }
  
  @IBAction func btnOpenPromoCode(_ sender: AnyObject)  {
    self.popUpShow(popupView: ViewPromo)
  }
  
  @IBAction func btnClosePromoCode(_ sender: AnyObject)  {
    self.txtPromoCode.resignFirstResponder()
    self.txtPromoCode.text = ""
    self.popUpHide(popupView: ViewPromo)
  }
  
  @IBAction func btnPromoCode(_ sender: AnyObject)  {
    
    self.popUpHide(popupView: ViewPromo)
    self.view.endEditing(true)
    
    if txtPromoCode.text != "" {
      callService_ValidatePromoCodeService(strPromocode: txtPromoCode.text!)
    } else {
      PIAlertUtils.displayAlertWithMessage("Please Enter PromoCode...!")
    }
  }
  
  @IBAction func btnBilling(_ sender: AnyObject)  {
    
    APPDELEGATE.isBilling = !APPDELEGATE.isBilling
    
    if APPDELEGATE.strvalidate == "Shipping" {
      APPDELEGATE.chkAddAddress = false
    }
    
    if APPDELEGATE.chkAddAddress == true {
      if chkshippingBilling == true {
        if APPDELEGATE.isBilling == false {
          imgBilling.image = #imageLiteral(resourceName: "square")
          if Constants.DeviceType.IS_IPAD {
            viewMainBillingHeight.constant = 180
          } else {
            viewMainBillingHeight.constant = 120
          }
          viewMainBilling.isHidden = false
          self.strBillingFlag = "1"
          
        } else {
          imgBilling.image = #imageLiteral(resourceName: "rightSquare")
          viewMainBillingHeight.constant = 0
          viewMainBilling.isHidden = true
          self.strBillingFlag = "0"
        }
      }
    } else {
      
      if (UserDefaults.standard.object(forKey: BazarBit.BILLINGADDRESS) != nil) {
        
        if APPDELEGATE.isBilling == false {
          imgBilling.image = #imageLiteral(resourceName: "square")
          if Constants.DeviceType.IS_IPAD {
            viewMainBillingHeight.constant = 180
          } else {
            viewMainBillingHeight.constant = 120
          }
          viewMainBilling.isHidden = false
          self.strBillingFlag = "1"
          
        } else {
          imgBilling.image = #imageLiteral(resourceName: "rightSquare")
          viewMainBillingHeight.constant = 0
          viewMainBilling.isHidden = true
          self.strBillingFlag = "0"
        }
        
      } else {
        strAddressType = "1"
        APPDELEGATE.chkLablecheck = "Billing"
        APPDELEGATE.strvalidate = "NewBilling"
        self.callService_AddressList()
      }
    }
  }
  
  //AJAY
  // MARK: - PickerView Methods
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return arrCountryCode.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
    let dict : [String:AnyObject] = arrCountryCode[row] as! [String:AnyObject]
    let strCodeData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "code")
    return strCodeData
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
    let dict : [String:AnyObject] = arrCountryCode[row] as! [String:AnyObject]
    let strCodeData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "code")
    textFieldCountryCode.text = "+" + strCodeData
    strCountryCode = strCodeData
  }
  
  // MARK: - Picker toolbar Button Method
  
  func addToolBar(){
    
    let toolBar = UIToolbar()
    toolBar.isTranslucent = true
    toolBar.tintColor = UIColor.black
    toolBar.sizeToFit()
    
    let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
    
    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    toolBar.setItems([spaceButton, doneButton], animated: false)
    toolBar.isUserInteractionEnabled = true
    toolBar.sizeToFit()
    
    textFieldCountryCode.inputView = countryPicker
    self.countryPicker.dataSource = self
    self.countryPicker.delegate = self
    self.countryPicker.reloadAllComponents()
    
    textFieldCountryCode.inputAccessoryView = toolBar
  }
  
  func donePressed(sender:UIButton)  {
    textFieldCountryCode.resignFirstResponder()
  }
  
  // MARK: - Service Method
  
  func callCountryCodeService() {
    
    let strurl: String = BazarBit.countryCodeService()
    let url: URL = URL(string: strurl)!
    
    var headers: HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    let parameter : Parameters = [:]
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true) { (swiftyJson, responseDict) in
      
      print(responseDict)
      let dict:[String: AnyObject] = responseDict
      
      let message = BazarBit.getmessageFromService(dict: dict as [String : AnyObject])
      
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "status")
      
      if status == "ok" {
        if (dict["payload"] as? [AnyObject]) != nil  {
          self.arrCountryCode = dict["payload"] as! [AnyObject]
          self.textFieldCountryCode.inputView = self.countryPicker
          self.countryPicker.dataSource = self
          self.countryPicker.delegate = self
          self.countryPicker.reloadAllComponents()
        }
        
      } else {
        let message: String = dict["message"] as! String
        PIAlertUtils.displayAlertWithMessage(message)
      }
    }
  }
  
  // MARK: - Service Method
    
    func CheckPincodeService() {
        
        let strurl:String = BazarBit.CheckPincodeExistService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        parameter["pincode"] = self.shippingPinCode
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            let dicResponcePinCode: [String: AnyObject] = responseDict
            BazarBitLoader.stopLoader()
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: dicResponcePinCode, key: "status")
            
            if status == "ok"
            {
                
                if (dicResponcePinCode["payload"] as? [String:AnyObject]) != nil
                {
                    let payload:[String:AnyObject] = dicResponcePinCode["payload"] as! [String:AnyObject]
                    print(payload)
                    
                    let count: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "count")
                    
                    if count == "1" {
                        if APPDELEGATE.allow_otp_verification == "1" {
                             self.popUpShow(popupView: self.viewConfirmPurchase)
                        }else {
                            self.callService_PlaceOrderService()
                        }
                    } else {
                        PIAlertUtils.displayAlertWithMessage("Delivery Not Available !")
                    }
                }
            }
        })
    }
    
  func callService_PaymentGatewayService()  {
    
    let strurl: String = BazarBit.GetPaymentGatewayService()
    let url: URL = URL(string: strurl)!
    
    let strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
    
    var headers: HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = strAuthToken
    
    let parameter: Parameters = [:]
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      let dictResponse: [String: AnyObject] = responseDict
      let message = BazarBit.getmessageFromService(dict: dictResponse as [String : AnyObject])
      
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        
        if (dictResponse["payload"] as? [String :AnyObject]) != nil {
          
          self.arrPaymentGateWay = []
          
          let payLoad: [String: AnyObject] = dictResponse["payload"] as! [String: AnyObject]
          
          if (payLoad["cod_detail"] as? [String :AnyObject]) != nil {
            
            var COD: [String: AnyObject] = payLoad["cod_detail"] as! [String: AnyObject]
            
            COD["title"] = "Cash On Delivery" as AnyObject
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: COD, key: "status")
            
            if status == "1" {
              self.arrPaymentGateWay.append(COD as AnyObject)
            }
          }
          
          
          if (payLoad["payumoney_detail"] as? [String :AnyObject]) != nil {
            
            self.dictPayUMoney = payLoad["payumoney_detail"] as! [String: AnyObject]
            
            print(self.dictPayUMoney)
            
            self.dictPayUMoney["title"] = "PayU Money" as AnyObject
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictPayUMoney, key: "status")
            
            if status == "1" {
              self.arrPaymentGateWay.append(self.dictPayUMoney as AnyObject)
            }
          }
          
          if (payLoad["ccavenue_detail"] as? [String :AnyObject]) != nil {
            
            var ccavenue_detail: [String: AnyObject] = payLoad["ccavenue_detail"] as! [String: AnyObject]
            ccavenue_detail["title"] = "CC avenue" as AnyObject
            
            let status: String = ccavenue_detail["status"] as! String
            
            if status == "1" {
              self.arrPaymentGateWay.append(ccavenue_detail as AnyObject)
            }
          }
          
          if (payLoad["paypal_detail"] as? [String :AnyObject]) != nil {
            
            var paypal_detail: [String: AnyObject] = payLoad["paypal_detail"] as! [String: AnyObject]
            
            paypal_detail["title"] = "PayPal" as AnyObject
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: paypal_detail, key: "status")
            
            if status == "1" {
              self.arrPaymentGateWay.append(paypal_detail as AnyObject)
            }
          }
          
          self.tblPaymentGateway.dataSource = self
          self.tblPaymentGateway.delegate = self
          self.tblPaymentGateway.reloadData()
          self.tblPaymentGatewayHeight.constant = self.tblPaymentGateway.contentSize.height
          
        }
        
        UserDefaults.standard.removeObject(forKey: BazarBit.PROMOCODE)
        self.HideShow_PromoCode()
        self.callService_ShippingChages()
        
      } else {
        
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  //MARK: ServiceCallForVerifyOtp
  func ServiceCallForVerifyOtp() {
    
    let strurl:String = BazarBit.PlaceOrderVerifyOtp()
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    print(headers)
    
    var parameter:Parameters = [:]
    parameter["verify_order_otp"] = strOtp
    parameter["otp_to_verify"] = strOrder_Otp_Id
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      print(responseDict)
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
      
      if status == "ok"
      {
        if (responseDict["payload"] != nil)
        {
          let payload:[String:AnyObject] = responseDict["payload"] as! [String:AnyObject]
          print(payload)
          
          self.viewOtp.clearTextField()
          self.viewOtp.initalizeUI2()
          self.viewOtp.resignFirstResponder()
          self.popUpHide(popupView: self.viewConfirmPurchaseOtp)
          self.callService_PlaceOrderService()
            //if APPDELEGATE.allow_otp_verification == "1" {
                
        }
      } else {
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  //MARK: ServiceCallForSendOtp
  func ServiceCallForSendOtp(strOtp : String, strMobileNo : String) {
    
    let strurl:String = BazarBit.PlaceOrderSendOtp()
    let url:URL = URL(string: strurl)!
    var headers:HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    print(headers)
    
    var parameter:Parameters = [:]
    parameter["otp_confirm_country_code"] = strOtp
    parameter["confirm_order_mobile"] = strMobileNo
    
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      print(responseDict)
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
      
      if status == "ok"
      {
        self.popUpHide(popupView: self.viewConfirmPurchase)
        self.popUpShow(popupView: self.viewConfirmPurchaseOtp)
        
        if (responseDict["payload"] != nil)
        {
          let payload:[String:AnyObject] = responseDict["payload"] as! [String:AnyObject]
          print(payload)
          
          self.strOtp = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "otp")
          self.strOrder_Otp_Id = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "order_otp_id")
          
          self.lableMobileNo.text = "Please type the verification code send to \(self.strMobileNo)"
          //PIAlertUtils.displayAlertWithMessage(message)
          self.textFieldMobileNumber.text?.removeAll()
          self.textFieldCountryCode.text = ""
        }
      }
      else
      {
        let message:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "message")
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  func callService_PlaceOrderService()  {
    
    let strurl: String = BazarBit.GetPlaceOrderService()
    let url: URL = URL(string: strurl)!
    
    let strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
    
    var headers: HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = strAuthToken
    
    var parameter: Parameters = [:]
    
    if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil) {
        
        let shipdict:[String:AnyObject] = UserDefaultFunction.getDictionary(forKey: BazarBit.SHIPPINGADDRESS)! as [String : AnyObject]
        
        print(shipdict)
    }
    
    if (UserDefaults.standard.object(forKey: BazarBit.BILLINGADDRESS) != nil) {
        
        let BillDict:[String:AnyObject] = UserDefaultFunction.getDictionary(forKey: BazarBit.BILLINGADDRESS)! as [String : AnyObject]
        
        print(BillDict)
    }
    
    parameter["firstname"] = strShippingFirstName
    parameter["lastname"] = strShippingLastName
    parameter["address"] = strShippingAddress
    parameter["landmark"] = strShippingLandMark
    parameter["pincode"] = shippingPinCode
    parameter["country"] = strShippingCountry
    parameter["state"] = strShippingState
    parameter["city"] = strShippingOnlyCity
    parameter["phonenumber"] = strShippingPhoneNo
    
    parameter["billing_firstname"] = strBillingFirstName
    parameter["billing_lastname"] = strBillingLastName
    parameter["billing_address"] = strBillingAddress
    parameter["billing_landmark"] = strBillingLandMark
    parameter["billing_pincode"] = strBillingPinCode
    parameter["billing_country"] = strBillingCountry
    parameter["billing_state"] = strBillingState
    parameter["billing_city"] = strBillingOnlyCity
    parameter["billing_phonenumber"] = strBillingPhoneNo
    parameter["payment_method"] = strPaymentType
    parameter["billingflag"] = self.strBillingFlag
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      print(responseDict)
      let dictResponse: [String: AnyObject] = responseDict
      
      print(url)
      
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        
        if (dictResponse["payload"] as? [String :AnyObject]) != nil
        {
          let dict: [String: AnyObject] = dictResponse["payload"] as! [String : AnyObject]
          
          print(dict)
          
          let order_ID : String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "orderid")
          print(order_ID)
          
          if self.strPaymentType == "1"
          {
            self.viewThankYou.isHidden = false
            
            self.imgAddress.isHidden = false
            self.imgPayment.isHidden = true
            self.imgSuccess.isHidden = false
            
            UserDefaults.standard.removeObject(forKey: BazarBit.SHIPPINGADDRESS)
            UserDefaults.standard.removeObject(forKey: BazarBit.BILLINGADDRESS)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
              self.navigationController?.popViewController(animated: true)
              APPDELEGATE.openHomePage()
            }
            
            self.GetCartCountData()
          }
          else if self.strPaymentType == "2"
          {
            //PAYU
            self.imgAddress.isHidden = false
            self.imgPayment.isHidden = true
            self.imgSuccess.isHidden = true
            //self.viewThankYou.isHidden = true
            self.viewFailPayUmoney.isHidden = true
            APPDELEGATE.chkPayUMoneyTitle = true
            
            self.initPayment(strOrderId: order_ID)
          }
          else if self.strPaymentType == "3"
          {
            //PAYPAL
          }
          else if self.strPaymentType == "4"
          {
            //CC AVENUE
          }
        }
        
      } else {
        
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  func callService_RemovePromoCodeService()  {
    
    let strurl: String = BazarBit.GetRemovePromocodeService()
    let url: URL = URL(string: strurl)!
    
    let strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
    
    var headers: HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = strAuthToken
    
    let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
    
    var parameter: Parameters = [:]
    parameter["user_id"] = strUserId
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      let dictResponse: [String: AnyObject] = responseDict
      
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        UserDefaults.standard.removeObject(forKey: BazarBit.PROMOCODE)
        self.HideShow_PromoCode()
        PIAlertUtils.displayAlertWithMessage(message)
        self.callService_ShippingChages()
        
      } else {
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  func callService_ValidatePromoCodeService(strPromocode : String)  {
    
    let strurl: String = BazarBit.GetValidatePromoCodeService()
    let url: URL = URL(string: strurl)!
    
    let strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
    
    var headers: HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = strAuthToken
    
    let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
    
    var parameter: Parameters = [:]
    parameter["user_id"] = strUserId
    parameter["promocode"] = strPromocode
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      print(responseDict)
      let dictResponse: [String: AnyObject] = responseDict
      
      print(url)
      
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        
        let strPromoCode: String = self.txtPromoCode.text!
        print(strPromoCode)
        UserDefaults.standard.set(strPromoCode, forKey: BazarBit.PROMOCODE)
        
        self.txtPromoCode.text = ""
        
        self.HideShow_PromoCode()
        PIAlertUtils.displayAlertWithMessage(message)
        self.callService_ShippingChages()
        
      } else {
        
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  func callService_AddressList()  {
    
    let strurl: String = BazarBit.GetaddressListService()
    
    let url: URL = URL(string: strurl)!
    
    var headers: HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter: Parameters = [:]
    parameter["address_type"] = strAddressType
    
    print(parameter)
    
    let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
    
    if UserId != nil {
      headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
    }
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      let dictResponse: [String: AnyObject] = responseDict
      
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        
        if (dictResponse["payload"] as? [AnyObject]) == nil {
          
          let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
          
          self.navigationController?.pushViewController(addAddressVC, animated: true)
        }else {
          
          let arrPayLoadList: [AnyObject] = dictResponse["payload"] as! [AnyObject]
          
          if arrPayLoadList.count > 0 {
            let addressListViewControllerObj: AddressListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
            
            addressListViewControllerObj.strAddressType = self.strAddressType
            
            addressListViewControllerObj.fromVC = "CheckOut"
            addressListViewControllerObj.checkOutVC = self
            self.navigationController?.pushViewController(addressListViewControllerObj, animated: true)
            
          } else {
            
            let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
            
            //AJAY : 01-05-2018
            
            if self.isFromBillingChange == true {
              if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil)
              {
                addAddressVC.isFromBillingAdd = true
              }else {
                addAddressVC.isFromBillingAdd = false
              }
            }
            self.navigationController?.pushViewController(addAddressVC, animated: true)
          }
        }
        
      } else {
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  func callService_LastOrderService()  {
    
    let strurl: String = BazarBit.GetLastOrderService()
    let url: URL = URL(string: strurl)!
    
    let strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
    
    var headers: HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = strAuthToken
    
    let parameter: Parameters = [:]
    
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      print(responseDict)
      let dictResponse: [String: AnyObject] = responseDict
      
      print(url)
      
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        
        if (dictResponse["payload"] as? [String :AnyObject]) != nil {
          
          let dict: [String: AnyObject] = dictResponse["payload"] as! [String : AnyObject]
          print(dict)
          
          if dict.count > 0 {
            
            self.setShippingBillingRecord(dict: dict)
            
          }    else    {
            
            let addressListController : AddressListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
            
            addressListController.checkOutVC = self
            //                        self.strAddressType = "1"
            self.strAddressType = "0"
            //                        addressListController.strAddressType = "1"
            addressListController.strAddressType = "0"
            addressListController.fromVC = "CheckOut"
            
            self.navigationController?.pushViewController(addressListController, animated: true)
          }
        } else {
          self.callService_AddressList()
        }
        
      } else {
        
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  func callService_ShippingChages()  {
    
    let strurl: String = BazarBit.GetShippingChargesService()
    let url: URL = URL(string: strurl)!
    
    let strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
    
    var headers: HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = strAuthToken
    
    let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
    
    var parameter: Parameters = [:]
    
    parameter["user_id"] = strUserId
    parameter["pincode"] = paramterPinCode
    
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      print(responseDict)
      let dictResponse: [String: AnyObject] = responseDict
      
      print(url)
      
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        self.callService_CartTotal()
      }
    })
  }
  
  func callService_CartTotal()  {
    
    let strurl: String = BazarBit.GetCartTotalService()
    let url: URL = URL(string: strurl)!
    
    let strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
    
    var headers: HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = strAuthToken
    
    let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
    
    var parameter: Parameters = [:]
    parameter["user_id"] = strUserId
    
    print(parameter)
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      print(responseDict)
      let dictResponse: [String: AnyObject] = responseDict
      
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        
        if (dictResponse["payload"] as? [String :AnyObject]) != nil {
          
          let payload: [String: AnyObject] = dictResponse["payload"] as! [String: AnyObject]
          
          print(payload)
          
          let strSubTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "sub_total")
          let strDiscount: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "promocode_discount")
          let strShippingCharges: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "shipping_price")
          let strCodeCharges: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "shipping_price_cod")
          let strTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "total_price")
          
          var heightValue: CGFloat = 0.0
          
          if Constants.DeviceType.IS_IPAD {
            heightValue = 30.0
            self.viewPaymentInfoHeight.constant = 175.0
          } else {
            heightValue = 21.0
            self.viewPaymentInfoHeight.constant = 125.0
          }
            
            self.viewTotal.isHidden = false
          
          self.viewPaymentInfoHeight.constant = self.viewPaymentInfoHeight.constant
          
          if strSubTotal == "" {
            self.lblSubTotal.text = "0"
          } else {
            self.lblSubTotal.text = strSubTotal
          }
          
          if strDiscount == "" || strDiscount.toInt() == 0
          {
            self.viewDiscountHeight.constant = 0.0
            self.viewPaymentInfoHeight.constant = self.viewPaymentInfoHeight.constant - heightValue + 2.0
          } else {
            self.viewDiscountHeight.constant = 21.0
            self.lblDiscount.text = strDiscount
          }
          
          if strShippingCharges != "" || strShippingCharges.toInt() == 0 {
            self.viewShippingChargeHeight.constant = 0.0
            self.viewPaymentInfoHeight.constant = self.viewPaymentInfoHeight.constant - heightValue + 2.0
          } else {
            self.lblShippingCharge.text = strShippingCharges
          }
          
          if strCodeCharges != "" || strCodeCharges.toInt() == 0 {
            self.viewCodeHeight.constant = 0.0
            self.viewPaymentInfoHeight.constant = self.viewPaymentInfoHeight.constant - heightValue + 2.0
          } else {
            self.lblCodeCharge.text = strCodeCharges
          }
          
          if strTotal == "" {
            self.lblTotal.text = "0"
          } else {
            self.lblTotal.text = strTotal
          }
        }
        
      } else {
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  func GetCartCountData() {
    
    let strurl:String = BazarBit.CountCartTotalService()
    
    let url:URL = URL(string: strurl)!
    
    var headers:HTTPHeaders = [:]
    
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    
    var parameter:Parameters = [:]
    
    let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
    
    if UserId != nil {
      parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
    }
    else
    {
      let strSessionId:String = BazarBit.getSessionId()
      parameter["session_id"] = strSessionId
      parameter["user_id"] = ""
    }
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
      
      let dict = responseDict
      
      let status:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "status")
      
      if status == "ok"
      {
        if dict["payload"] != nil
        {
          let payload:[String:AnyObject] = dict["payload"] as! [String:AnyObject]
          print(payload)
          
          let strCartTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "cart_items")
          UserDefaults.standard.set(strCartTotal, forKey: BazarBit.CARTTOTALCOUNT)
        }
      }
    })
  }
  
  //GetShippingChargesService
  func callService_ShoppingCartList()  {
    
    let strurl: String = BazarBit.GetCartitemsService()
    var strAuthToken: String = ""
    let url: URL = URL(string: strurl)!
    
    if (UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) != nil) {
      strAuthToken  = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
    } else {
      strAuthToken  = ""
    }
    
    var headers: HTTPHeaders = [:]
    headers[BazarBit.ContentType] = BazarBit.applicationJson
    headers["app-id"] = BazarBit.appId
    headers["app-secret"] = BazarBit.appSecret
    headers["auth-token"] = strAuthToken
    
    var parameter: Parameters = [:]
    
    if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
      let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
      let strSessionId: String = ""
      
      parameter["session_id"] = strSessionId
      parameter["user_id"] = strUserId
      
    } else {
      
      let strUserId: String = ""
      let strSessionId: String = BazarBit.getSessionId()
      
      parameter["session_id"] = strSessionId
      parameter["user_id"] = strUserId
    }
    
    PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
      
      print(responseDict)
      let dictResponse: [String: AnyObject] = responseDict
      
      let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
      
      let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
      
      if status == "ok" {
        
        if (dictResponse["payload"] as? [String :AnyObject]) != nil {
          
          let dict: [String: AnyObject] = dictResponse["payload"] as! [String : AnyObject]
          
          if (dict["cart_items"] as? [AnyObject]) != nil {
            
            self.arrShoppingCart = dict["cart_items"] as! [AnyObject]
            
            if self.arrShoppingCart.count > 0 {
              
              var totalPrize: Int = 0
              
              for i in 0...self.arrShoppingCart.count - 1 {
                
                let dict:[String: AnyObject] = self.arrShoppingCart[i] as! [String: AnyObject]
                
                let productData:[String: AnyObject] = dict["product_data"] as! [String: AnyObject]
                let strProductQuantity: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_quantity")
                
                var localproductQuantity: Int = 0
                
                if (strProductQuantity.toInt() != nil) {
                  localproductQuantity = strProductQuantity.toInt()!
                }
                
                var strlocalSalePrize:String = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "sale_price")
                
                if strlocalSalePrize.toInt() == 0 {
                  strlocalSalePrize = PIService.object_forKeyWithValidationForClass_String(dict: productData, key: "price")
                }
                
                var localSalePrize: Int = 0
                
                if (strlocalSalePrize.toInt() != nil) {
                  localSalePrize = strlocalSalePrize.toInt()!
                }
                totalPrize = totalPrize + (localproductQuantity * localSalePrize )
              }
              
              var strLocalTotalPrize: String = "\(totalPrize)"
              self.strGrandTotal = totalPrize.toString
              strLocalTotalPrize = BazarBitCurrency.setPriceSign(strPrice: strLocalTotalPrize)
              let strDisplayTotalPrize: String = "\(strLocalTotalPrize)(\(self.arrShoppingCart.count) items)"
              
              self.btnItemTotal.setTitle(strDisplayTotalPrize, for: .normal)
              
              self.tblShoppingCart.dataSource = self
              self.tblShoppingCart.delegate = self
              self.tblShoppingCart.reloadData()
              
              self.tblShoppingCartHeight.constant = self.tblShoppingCart.contentSize.height
            }
            else
            {
              self.btnItemTotal.setTitle("Rs 0(0 Items)", for: .normal)
              self.tblShoppingCartHeight.constant = 0
            }
          }
        }
        
      } else {
        PIAlertUtils.displayAlertWithMessage(message)
      }
    })
  }
  
  //    func callService_AddressList()  {
  //
  //        let strurl: String = BazarBit.GetaddressListService()
  //        let url: URL = URL(string: strurl)!
  //
  //        var headers: HTTPHeaders = [:]
  //        headers[BazarBit.ContentType] = BazarBit.applicationJson
  //        headers["app-id"] = BazarBit.appId
  //        headers["app-secret"] = BazarBit.appSecret
  //
  //        var parameter: Parameters = [:]
  //
  //        print(parameter)
  //
  //        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
  //
  //        if UserId != nil {
  //            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
  //            parameter["address_type"] = "1"
  //        }
  //        else
  //        {
  //            parameter["address_type"] = "1"
  //        }
  //
  //        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, completion: { (swiftyJson, responseDict) in
  //
  //            let dictResponse: [String: AnyObject] = responseDict
  //
  //            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
  //            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
  //
  //            if status == "ok" {
  //
  //                if (dictResponse["payload"] as? [AnyObject]) != nil {
  //                    self.arrAddressList = dictResponse["payload"] as! [AnyObject]
  //
  //                    if self.arrAddressList.count > 0 {
  //
  //
  //
  //                    } else {
  //
  //                        let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
  //
  //                        self.navigationController?.pushViewController(addAddressVC, animated: true)
  //
  //                    }
  //
  //                } else {
  //
  //                    let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
  //
  //                    self.navigationController?.pushViewController(addAddressVC, animated: true)
  //                }
  //
  //            } else {
  //
  //                PIAlertUtils.displayAlertWithMessage(message)
  //            }
  //        })
  //    }
  
  
  // MARK:- UIPopup method
  
  func popUpShow(popupView :UIView) {
    
    dimView = UIView()
    dimView?.frame = self.view.frame
    dimView?.backgroundColor = UIColor.black
    dimView?.alpha = 0.5
    
    var x:Int = Int(UIScreen.main.bounds.width)
    var y:Int = Int(UIScreen.main.bounds.height)
    
    x =  (x - Int(popupView.frame.width)) / 2
    y = (y - Int(popupView.frame.height)) / 2
    
    popupView.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: popupView.frame.width, h: popupView.frame.height)
    popupView.layer.cornerRadius = 5.0
    popupView.layer.masksToBounds = true
    
    self.view.addSubview(dimView!)
    self.view.bringSubview(toFront: popupView)
    popupView.alpha = 1.0
  }
  
  func popUpHide(popupView :UIView) {
    dimView?.removeFromSuperview()
    popupView.alpha = 0.0
  }
  
  // MARK: - Helper Method
  func setShippingBillingRecord(dict: [String: AnyObject])  {
    
    print(dict)
    
    var localShippingDict: [String: AnyObject] = [:]
    var localBillingDict: [String: AnyObject] = [:]
    
    localShippingDict["first_name"] = dict["first_name"]
    localShippingDict["last_name"] = dict["last_name"]
    localShippingDict["address"] = dict["address"]
    localShippingDict["country"] = dict["country"]
    localShippingDict["pincode"] = dict["pincode"]
    localShippingDict["landmark"] = dict["landmark"]
    localShippingDict["city"] = dict["city"]
    localShippingDict["state"] = dict["state"]
    localShippingDict["phone_number"] = dict["phone_number"]
    
    self.paramterPinCode = dict["pincode"] as! String
    
    UserDefaultFunction.setCustomDictionary(dict: localShippingDict, key: BazarBit.SHIPPINGADDRESS)
    
    localBillingDict["first_name"] = dict["bill_first_name"]
    localBillingDict["last_name"] = dict["bill_last_name"]
    localBillingDict["address"] = dict["bill_address"]
    localBillingDict["country"] = dict["bill_country"]
    localBillingDict["pincode"] = dict["bill_pincode"]
    localBillingDict["landmark"] = dict["bill_landmark"]
    localBillingDict["city"] = dict["bill_city"]
    localBillingDict["state"] = dict["bill_state"]
    localBillingDict["phone_number"] = dict["bill_phone_number"]
    
    UserDefaultFunction.setCustomDictionary(dict: localBillingDict, key: BazarBit.BILLINGADDRESS)
    showLastOrderServiceRecord()
  }
  
  //MARK:- Get Last order Helper Method
  func showLastOrderServiceRecord()  {
    
    //SHIPPINGADDRESS
    if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil) {
      
      let shipdict:[String:AnyObject] = UserDefaultFunction.getDictionary(forKey: BazarBit.SHIPPINGADDRESS)! as [String : AnyObject]
      
      print(shipdict)
      
      self.displayShippingAddress(dict: shipdict)
      
      if APPDELEGATE.chkSelectedList == true  {
        if self.strAddressType == "0" {
          if APPDELEGATE.strvalidate != "Billing" {
            self.displayShippingAddress(dict: shipdict)
          }
        }
      }
      self.callService_ShippingChages()
    } else {
      
      imgBilling.image = #imageLiteral(resourceName: "rightSquare")
      viewMainBillingHeight.constant = 0
      viewMainBilling.isHidden = true
      self.strBillingFlag = "0"
      
      self.lblShippingName.text = ""
      self.lblShippingAddress.text = ""
      self.lblShippingCity.text = ""
      self.lblShippingState.text = ""
      self.lblShippingPhoneNo.text = ""
    }
    
    //BILLINGADDRESS
    if (UserDefaults.standard.object(forKey: BazarBit.BILLINGADDRESS) != nil) {
      
      let billdict:[String:AnyObject] = UserDefaultFunction.getDictionary(forKey: BazarBit.BILLINGADDRESS)! as [String : AnyObject]
      
      self.displayBillingAddress(dict: billdict)
      
      if APPDELEGATE.chkSelectedList == true {
        if self.strAddressType == "1" {
          if APPDELEGATE.strvalidate != "Shipping" {
            self.displayBillingAddress(dict: billdict)
          }
        }
      }
      self.callService_ShippingChages()
    } else {
      imgBilling.image = #imageLiteral(resourceName: "rightSquare")
      viewMainBillingHeight.constant = 0
      viewMainBilling.isHidden = true
      self.strBillingFlag = "0"
      
      self.lblBillingName.text = ""
      self.lblBillingAddress.text = ""
      self.lblBillingCity.text = ""
      self.lblBillingState.text = ""
      self.lblBillingPhoneNo.text = ""
    }
    
    //Click on Add button fom shipping (select Same as billing )
    if APPDELEGATE.strvalidate == "Shipping" {
      if APPDELEGATE.chkSameAsShipping == "SameAsShipping" {
        print("Shipping")
        APPDELEGATE.strvalidate = ""
        
        if UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil {
          let shipdict:[String:AnyObject] = UserDefaultFunction.getDictionary(forKey: BazarBit.SHIPPINGADDRESS)! as [String : AnyObject]
          self.displayShippingAddress(dict: shipdict)
        }
        
        //Set data To User Default Shipping Address
        var localShippingDict: [String: AnyObject] = [:]
        
        localShippingDict["first_name"] = strShippingFirstName as AnyObject
        localShippingDict["last_name"] = strShippingLastName as AnyObject
        localShippingDict["address"] = strShippingAddress as AnyObject
        localShippingDict["pincode"] = shippingPinCode as AnyObject
        localShippingDict["city"] = strShippingOnlyCity as AnyObject
        localShippingDict["state"] = strShippingState as AnyObject
        localShippingDict["phone_number"] = strShippingPhoneNo as AnyObject
        localShippingDict["landmark"] = strShippingLandMark as AnyObject
        localShippingDict["country"] = strShippingCountry as AnyObject
        
        UserDefaultFunction.setCustomDictionary(dict: localShippingDict, key: BazarBit.SHIPPINGADDRESS)
        APPDELEGATE.chkAddAddress = false
      }
    }
      
      //Click on Add button fom Billing (select Same as shipping)
    else if APPDELEGATE.strvalidate == "Billing" {
      if APPDELEGATE.chkSameAsShipping == "SameAsShipping" {
        
        print("Billing")
        APPDELEGATE.strvalidate = ""
        
        self.lblBillingName.text = "\(strFirstName) \(strLastName)"
        self.lblBillingAddress.text = strAddress
        self.lblBillingCity.text = strcity + " - " + strPincode
        self.lblBillingState.text = strState
        self.lblBillingPhoneNo.text = strPhone
        
        //Set data To User Default Shipping Address
        var localBillingDict: [String: AnyObject] = [:]
        
        localBillingDict["first_name"] = strFirstName as AnyObject
        localBillingDict["last_name"] = strLastName as AnyObject
        localBillingDict["address"] = strAddress as AnyObject
        localBillingDict["pincode"] = strPincode as AnyObject
        localBillingDict["city"] = strcity as AnyObject
        localBillingDict["state"] = strState as AnyObject
        localBillingDict["phone_number"] = strPhone as AnyObject
        
        UserDefaultFunction.setCustomDictionary(dict: localBillingDict, key: BazarBit.BILLINGADDRESS)
        APPDELEGATE.chkAddAddress = false
      }
    }
  }
  
  func displayShippingAddress(dict:[String:AnyObject]) {
    
    print(dict)
    
    self.strShippingFirstName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "first_name")
    self.strShippingLastName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "last_name")
    
    self.strShippingName = "\(self.strShippingFirstName) \(self.strShippingLastName)"
    self.lblShippingName.text = self.strShippingName
    
    self.strShippingAddress = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "address")
    self.lblShippingAddress.text = self.strShippingAddress
    
    self.strShippingCountry = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country")
    print(self.strShippingCountry)
    
    self.shippingPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
    
    if self.paramterPinCode == "" {
      self.paramterPinCode = self.shippingPinCode
    }
    
    self.strShippingLandMark = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "landmark")
    print(self.strShippingLandMark)
    
    self.strShippingOnlyCity = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "city")
    
    if self.strShippingOnlyCity == "" {
      self.strShippingCity =  self.shippingPinCode
    } else {
      self.strShippingCity = self.strShippingOnlyCity + " - " + self.shippingPinCode
    }
    
    self.lblShippingCity.text = self.strShippingCity
    
    self.strShippingState = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state")
    self.lblShippingState.text = self.strShippingState
    
    self.strShippingPhoneNo = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "phone_number")
    self.lblShippingPhoneNo.text = self.strShippingPhoneNo
  }
  
  func displayBillingAddress(dict:[String:AnyObject]) {
    
    self.strBillingFirstName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "first_name")
    self.strBillingLastName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "last_name")
    
    self.strBillingName = "\(self.strBillingFirstName) \(self.strBillingLastName)"
    self.lblBillingName.text = self.strBillingName
    
    
    self.strBillingAddress = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "address")
    self.lblBillingAddress.text = self.strBillingAddress
    
    self.strBillingPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
    self.strBillingOnlyCity = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "city")
    
    if self.strBillingOnlyCity == "" {
      self.strBillingCity =  self.strBillingPinCode
    } else {
      self.strBillingCity = self.strBillingOnlyCity + " - " + self.strBillingPinCode
    }
    
    self.lblBillingCity.text = self.strBillingCity
    
    self.strBillingLandMark = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "landmark")
    self.strBillingCountry = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country")
    
    self.strBillingState = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state")
    self.lblBillingState.text = self.strBillingState
    
    self.strBillingPhoneNo = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "phone_number")
    self.lblBillingPhoneNo.text = self.strBillingPhoneNo
    
    //Set data To User Default Shipping Address
    //        var localBillingDict: [String: AnyObject] = [:]
    //
    //        localBillingDict["first_name"] = strBillingFirstName as AnyObject
    //        localBillingDict["last_name"] = strBillingLastName as AnyObject
    //        localBillingDict["address"] = strBillingAddress as AnyObject
    //        localBillingDict["pincode"] = strBillingPinCode as AnyObject
    //        localBillingDict["city"] = strBillingCity as AnyObject
    //        localBillingDict["state"] = strBillingState as AnyObject
    //        localBillingDict["phone_number"] = strBillingPhoneNo as AnyObject
    //
    //        UserDefaultFunction.setCustomDictionary(dict: localBillingDict, key: BazarBit.BILLINGADDRESS)
  }
  
  func addPropertyPopupFunction()  {
    
    self.view.addSubview(ViewPromo)
    self.ViewPromo.alpha = 0.0
    
    var popupWidth: Int = 0
    var popupHeight: Int = 0
    
    if Constants.DeviceType.IS_IPAD {
      popupWidth = 400
      popupHeight = 150
      
    } else {
      popupWidth = 260
      popupHeight = 100
    }
    
    var x:Int = Int(UIScreen.main.bounds.width)
    var y:Int = Int(UIScreen.main.bounds.height)
    
    x =  (x - Int(popupWidth)) / 2
    y = (y - Int(popupHeight)) / 2
    
    ViewPromo.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: CGFloat(popupWidth), h: CGFloat(popupHeight))
    
    ViewPromo.layoutIfNeeded()
  }
  
  //MARK: addPropertyConfirmPurchasePopupFunction
  func addPropertyConfirmPurchasePopupFunction()  {
    
    self.view.addSubview(viewConfirmPurchase)
    self.viewConfirmPurchase.alpha = 0.0
    
    var popupWidth: Int = 0
    var popupHeight: Int = 0
    
    if Constants.DeviceType.IS_IPAD {
      popupWidth = 450
      popupHeight = 230
      
    } else {
      popupWidth = 260
      popupHeight = 150
    }
    
    var x:Int = Int(UIScreen.main.bounds.width)
    var y:Int = Int(UIScreen.main.bounds.height)
    
    x =  (x - Int(popupWidth)) / 2
    y = (y - Int(popupHeight)) / 2
    
    viewConfirmPurchase.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: CGFloat(popupWidth), h: CGFloat(popupHeight))
    
    viewConfirmPurchase.layoutIfNeeded()
  }
  
  //MARK: addPropertyConfirmPurchaseOtpPopupFunction
  func addPropertyConfirmPurchaseOtpPopupFunction()  {
    
    self.view.addSubview(viewConfirmPurchaseOtp)
    self.viewConfirmPurchaseOtp.alpha = 0.0
    
    var popupWidth: Int = 0
    var popupHeight: Int = 0
    
    if Constants.DeviceType.IS_IPAD {
      popupWidth = 450
      popupHeight = 280
    } else if Constants.DeviceType.IS_IPHONE_5 {
      popupWidth = 300
      popupHeight = 200
    }else {
      popupWidth = 350
      popupHeight = 220
    }
    
    var x:Int = Int(UIScreen.main.bounds.width)
    var y:Int = Int(UIScreen.main.bounds.height)
    
    x =  (x - Int(popupWidth)) / 2
    y = (y - Int(popupHeight)) / 2
    
    viewConfirmPurchaseOtp.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: CGFloat(popupWidth), h: CGFloat(popupHeight))
    
    viewConfirmPurchaseOtp.layoutIfNeeded()
    
  }
  
  func HideShow_PromoCode()  {
    
    if (UserDefaults.standard.object(forKey: BazarBit.PROMOCODE) != nil)
    {
      let getPromoCode: String = UserDefaults.standard.object(forKey: BazarBit.PROMOCODE) as! String
      print(getPromoCode)
      lblPromoCode.text = getPromoCode
      lblPromoCode.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
      
      viewPromoCode.isHidden = true
      viewPromoCodeHeight.constant = 0
      
      viewPromoCodeApplied.isHidden = false
      viewPromoCodeAppliedHeight.constant = 80
      
    } else {
      
      txtPromoCode.text = ""
      viewPromoCode.isHidden = false
      
      if Constants.DeviceType.IS_IPAD {
        viewPromoCodeHeight.constant = 44
      } else {
        viewPromoCodeHeight.constant = 30
      }
      
      viewPromoCodeApplied.isHidden = true
      viewPromoCodeAppliedHeight.constant = 0
    }
  }
  
  func GetImageUrl(strurl: String) -> String {
    
    var strUrl = strurl
    strUrl = strUrl.replacingOccurrences(of: "[", with: "")
    strUrl = strUrl.replacingOccurrences(of: "\"", with: "")
    strUrl = strUrl.replacingOccurrences(of: "]", with: "")
    strUrl = strUrl.replacingOccurrences(of: " ", with: "%20")
    
    return strUrl
  }
  
  func SetPropertiesOFView()  {
        viewShipping.applyShadowWithColor(.lightGray)
        viewBilling.applyShadowWithColor(.lightGray)
        viewPromoCode.applyShadowWithColor(.lightGray)
        viewPromoCodeApplied.applyShadowWithColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!)
        
        viewRoundPromoCode.setCornerRadius(radius: 15.0)
        viewRoundPromoCode.layer.borderColor = (UIColor(hexString: Constants.THEME_PRIMARY_COLOR))?.cgColor
        viewRoundPromoCode.layer.borderWidth = 2.0
    }
  
}

class PaymentGatewayCell: UITableViewCell {
  
  @IBOutlet var imgPayment: UIImageView!
  @IBOutlet var lblTitle: UILabel!
  @IBOutlet var viewBack : UIView!
  
  override func awakeFromNib() {
    self.contentView.applyShadowDefault()
    self.viewBack.applyShadowDefault()
    lblTitle.setTextLabelDynamicTextColor()
  }
}
