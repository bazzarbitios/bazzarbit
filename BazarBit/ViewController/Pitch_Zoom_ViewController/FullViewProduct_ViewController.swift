//
//  FullViewProduct_ViewController.swift
//  BazarBit
//
//  Created by Binoy on 18/04/19.
//  Copyright © 2019 Parghi Infotech. All rights reserved.
//

import UIKit


class FullViewProduct_ViewController: UIViewController, UIGestureRecognizerDelegate {

    
    @IBOutlet weak var imageScrollView: ImageScrollView!
    var images = [String]()
    var index = 0

    @IBAction func btn_Close(_ sender: UIButton) {
        
        _ = navigationController?.popViewController(animated: false)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        
        imageScrollView.setup()
        imageScrollView.imageScrollViewDelegate = self
        imageScrollView.imageContentMode = .aspectFit
        imageScrollView.initialOffset = .center
        imageScrollView.display(url: images[index])
        
        imageScrollView.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.white
        
        
        // Swipe Gesture Recognizers
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(FullViewProduct_ViewController.respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(FullViewProduct_ViewController.respondToSwipeGesture(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        
        swipeRight.delegate = self
        swipeLeft.delegate = self
        
        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeLeft)
        
    }
    // Debugging - Only Up & Down Swipes Are Detected
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                index = (index - 1 + images.count)%images.count
                imageScrollView.display(url: images[index])
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")

                index = (index + 1)%images.count
                imageScrollView.display(url: images[index])
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
}

extension FullViewProduct_ViewController: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}

