//
//  HomeVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 09/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import EZSwiftExtensions
import Alamofire
import SwiftyJSON
import Toast_Swift
import ImageSlideshow
import AlamofireImage
import Kingfisher

class HomeVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UIScrollViewDelegate,KIImagePagerDataSource,KIImagePagerDelegate {
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var viewSearch : UIView!
    @IBOutlet weak var cartView: UIView!
    
    @IBOutlet weak var tblHomeProductHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var cltnHomeBannerHeightConstraints: NSLayoutConstraint!
    
    // Height Constraints
    @IBOutlet weak var SlideShowHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var categoryTopheightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var lblCartTotalCount: UILabel!
    @IBOutlet weak var lblCartTotalWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var appImage: UIImageView!
    @IBOutlet weak var slideshow: KIImagePager!
    @IBOutlet weak var tblHomeProduct: UITableView!
    @IBOutlet weak var txtSearchProduct: UITextField!
    
    @IBOutlet weak var cltHomeCategory: UICollectionView!
    @IBOutlet weak var cltHomeBanner: UICollectionView!
    @IBOutlet var viewCategoryBack : UIView!
    
    var homeProductCell:HomeMainProductCell!
    var GLOBAL = ""
    var GLOBAL2 = ""
    var tabbar: TabbarVC!
    var arrSideImages:[String] = [String]()
    
    var strlimit: String = "10"
    
    var strBestSellerOffset: String = "0"
    var strMostWantedOffset: String = "0"
    var strNewArrivalOffset: String = "0"
    
    var isCompleteBestSellerRecord: Bool = false
    var isCompleteMostWantedRecord: Bool = false
    var isCompleteNewArrivalRecord: Bool = false
    
    // Slider Service Variables
    var dicResponceHomeSlider:[String:AnyObject] = [:]
    
    // Banner Service Variable
    var dicResponceHomeBanner:[String:AnyObject] = [:]
    
    // Best Seller Service Variables
    var dicResponceBestSeller:[String:AnyObject] = [:]
    var arrBestSeller:[AnyObject] = []
    var aryAllImagesBestSeller : [String] = [String]()
    var arrProductIdBestSeller : [String] = []
    
    // Most Wanted Product Service Variables
    var dicResponceMostWantedproducts:[String:AnyObject] = [:]
    var arrMostWanted:[AnyObject] = []
    var aryAllImagesMostWanted : [String] = [String]()
    
    // New Arrival Product Service Variables
    var dicResponceNewArrivalProducts:[String:AnyObject] = [:]
    var arrNewArrival:[AnyObject] = []
    var aryAllImagesNewArrival : [String] = [String]()
    
    // Home Banner Service Variables
    var arrHomeBannerImages:[AnyObject] = []
    var arrbannerImages:[String] = []
    
    // Home Category List Variables
    var dicResponceCategoryList:[String:AnyObject] = [:]
    var arrCategoryList:[AnyObject] = []
    var arrCategoryImages : [String] = []
    
    var dicResponceCartTotalList:[String:AnyObject] = [:]
    var CartTotal:String = ""
    
    var strRating:String = ""
    var isFromBtnHome:Bool = false
    
    // MARK: - UIVIEW METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblCartTotalCount.isHidden = true
        self.txtSearchProduct.resignFirstResponder()
        self.navigationController?.isNavigationBarHidden = true
        txtSearchProduct.delegate = self
        txtSearchProduct.backgroundColor = UIColor.white
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        viewSearch.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        // Main Logo Image set
        var LogoImage:String = APPDELEGATE.applicationImage
        LogoImage = LogoImage.replacingOccurrences(of: " ", with: "%20")
        let url = URL(string: LogoImage)
        appImage.kf.setImage(with: url)
        
        cltHomeBanner.applyShadowDefault()
        viewCategoryBack.applyShadowDefault()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if isFromBtnHome == false {
            self.txtSearchProduct.resignFirstResponder()
            let SearchVC : SearchViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchViewController
            self.navigationController?.pushViewController(SearchVC, animated: true)
        }
        else
        {
            self.txtSearchProduct.resignFirstResponder()
            isFromBtnHome = false
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        APPDELEGATE.chkBottomBar = false
        
        // Set Uitextfield Left Image
        let leftImageView = UIImageView()
        leftImageView.image = UIImage(named: "search")
        let leftView = UIView()
        leftView.addSubview(leftImageView)
        
        if Constants.DeviceType.IS_IPAD {
            leftView.frame = CGRect(x: 30, y: 0, width: 35, height: 25)
            leftImageView.frame = CGRect(x: 12, y: 0, width: 25, height: 25)
        }
        else
        {
            leftView.frame = CGRect(x: 15, y: 0, width: 25, height: 15)
            leftImageView.frame = CGRect(x: 6, y: 0, width: 15, height: 15)
        }
        txtSearchProduct.leftViewMode = .always
        txtSearchProduct.leftView = leftView
        txtSearchProduct.setTextFieldDynamicColor()
        txtSearchProduct.setCornerRadius(radius: 5)
        
        // Active / Nonactive
        // Slider Hide Show
        if APPDELEGATE.is_slider_active == "1" {
            if Constants.DeviceType.IS_IPAD {
                SlideShowHeightConstraints.constant = 300
                categoryTopheightConstraints.constant = 15
            } else {
                SlideShowHeightConstraints.constant = 130
                categoryTopheightConstraints.constant = 15
            }
        }
        else
        {
            if Constants.DeviceType.IS_IPAD {
                SlideShowHeightConstraints.constant = 0
                categoryTopheightConstraints.constant = 15
            }
            else
            {
                SlideShowHeightConstraints.constant = 0
                categoryTopheightConstraints.constant = 15
            }
        }
        tblHomeProductHeightConstraints.constant = 0
        
        if Constants.DeviceType.IS_IPAD {
            if APPDELEGATE.is_seller_active == "1" {
                tblHomeProductHeightConstraints.constant = tblHomeProductHeightConstraints.constant + 330
            }
            if APPDELEGATE.is_arrival_active == "1" {
                tblHomeProductHeightConstraints.constant = tblHomeProductHeightConstraints.constant + 330
            }
            if APPDELEGATE.is_mostwanted_active == "1" {
                tblHomeProductHeightConstraints.constant = tblHomeProductHeightConstraints.constant + 330
            }
        }
            
        else
        {
            if APPDELEGATE.is_seller_active == "1" {
                tblHomeProductHeightConstraints.constant = tblHomeProductHeightConstraints.constant + 240
                
            }
            if APPDELEGATE.is_arrival_active == "1" {
                tblHomeProductHeightConstraints.constant = tblHomeProductHeightConstraints.constant + 240
            }
            if APPDELEGATE.is_mostwanted_active == "1" {
                tblHomeProductHeightConstraints.constant = tblHomeProductHeightConstraints.constant + 240
            }
        }
        
        strBestSellerOffset = "0"
        strMostWantedOffset = "0"
        strNewArrivalOffset = "0"
        
        isCompleteBestSellerRecord = false
        isCompleteMostWantedRecord = false
        isCompleteNewArrivalRecord = false
        self.GetCartCountData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.slideshow.pageControl.isHidden = true
        self.slideshow.slideshowTimeInterval = UInt(2.0)
        self.slideshow.slideshowShouldCallScrollToDelegate = true
        self.slideshow.imageCounterDisabled = true
        self.slideshow.contentMode = .scaleAspectFit
    }
    
    // MARK:- KIImagePaper DataSource and Delegate Method
    func array(withImages pager: KIImagePager!) -> [Any]! {
        return self.arrSideImages
    }
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIViewContentMode {
        return UIViewContentMode.scaleAspectFit
    }
    
    func captionForImage(at index: Int, in pager: KIImagePager!) -> String! {
        return ""
    }
    
    func imagePager(_ imagePager: KIImagePager!, didScrollTo index: UInt) {
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        //   print("Index \(index)")
    }
    
    // MARK:- UITABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblHomeProduct {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblHomeProduct {
            if section == 0  {
                if APPDELEGATE.is_seller_active == "1" {
                    return 1
                }else {
                    return 0
                }
            }
            if section == 1  {
                if APPDELEGATE.is_mostwanted_active == "1" {
                    return 1
                }else {
                    return 0
                }
            }
            if section == 2  {
                if APPDELEGATE.is_arrival_active == "1" {
                    return 1
                }else {
                    return 0
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblHomeProduct {
            let identifier : String = "HomeProductCell"
            if indexPath.section == 0 {
                
                let BestSellerCell : HomeMainProductCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! HomeMainProductCell
                
                BestSellerCell.homevc = self
                BestSellerCell.strKey = "BestSeller"
                GLOBAL = BestSellerCell.strKey
                
                BestSellerCell.setUpBestSellerKey(strBestSeller: BestSellerCell.strKey)
                
                // For Best Seller Data
                BestSellerCell.setUpBestSellerData(arrModelDataBestSeller: arrBestSeller)
                
                // For Products Images
                BestSellerCell.setUpProductImagesData(arrModelDataProductImages: aryAllImagesBestSeller as [String])
                
                BestSellerCell.lblLableName.text = "Our Best Seller"
                BestSellerCell.lblLableName.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
                
                return BestSellerCell
            }
                
            else if indexPath.section == 1 {
                let MostWantedCell : HomeMainProductCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! HomeMainProductCell
                
                // GetMostWantedProduct Service use in HomeMianproduct Cell
                MostWantedCell.homevc = self
                
                MostWantedCell.strKey = "MostWanted"
                
                MostWantedCell.setUpBestSellerKey(strBestSeller: MostWantedCell.strKey)
                MostWantedCell.setUpMostWantedData(arrModelDataMostWanted: arrMostWanted)
                
                MostWantedCell.setUpProductImagesDataMostWanted(arrModelDataProductImagesMostWanted: aryAllImagesMostWanted as [String])
                
                MostWantedCell.lblLableName.text = "Most Wanted"
                MostWantedCell.lblLableName.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
                
                return MostWantedCell
                
            }
            else if indexPath.section == 2 {
                let NewArrivalCell : HomeMainProductCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! HomeMainProductCell
                
                NewArrivalCell.strKey = "NewArrival"
                NewArrivalCell.homevc = self
                
                NewArrivalCell.setUpBestSellerKey(strBestSeller: NewArrivalCell.strKey)
                
                NewArrivalCell.setUpNewArrivalData(arrModelDataNewArrival: arrNewArrival)
                
                NewArrivalCell.setUpProductImagesDataNewArrival(arrModelDataProductImagesNewArrival: aryAllImagesNewArrival as [String])
                
                NewArrivalCell.lblLableName.text = "New Arrival"
                NewArrivalCell.lblLableName.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
                
                return NewArrivalCell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblHomeProduct {
            if Constants.DeviceType.IS_IPAD {
                return 330
            }else {
                return 240
            }
        }
        return 0.0
    }
    
    // MARK:- UICOLLECTIONVIEW METHODS
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == cltHomeBanner {
            return arrHomeBannerImages.count
        }
            
        else if collectionView == cltHomeCategory {
            return arrCategoryList.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if collectionView == cltHomeCategory {
            
            let cellHomeCategoryCell: CltnHomeCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CltnHomeCategoryCell", for: indexPath) as! CltnHomeCategoryCell
            
            let dict:[String:AnyObject] = arrCategoryList[indexPath.row] as! [String:AnyObject]
            
            cellHomeCategoryCell.lblCategryName.text = dict["cat_name"] as? String
            
            if indexPath.row < arrCategoryImages.count {
                let strImage:String = arrCategoryImages[indexPath.row]
                let url = URL(string: strImage)!
                cellHomeCategoryCell.imgCategory.kf.setImage(with: url)
            }
            
            if indexPath.row == arrCategoryList.count {
                cellHomeCategoryCell.viewborder.isHidden = true
            }
            
            return cellHomeCategoryCell
        }
        
        if collectionView == cltHomeBanner {
            
            let cellHomeBannerCell: cltnHomeBannerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cltnHomeBannerCell", for: indexPath) as! cltnHomeBannerCell
            
            let dict:[String:AnyObject] = arrHomeBannerImages[indexPath.row] as! [String:AnyObject]
            
            cellHomeBannerCell.lblBannerName.text = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "title")
            
            if arrbannerImages.count > 0 {
                let strImage:String = arrbannerImages[indexPath.row]
                let url1 = URL(string: strImage)
                cellHomeBannerCell.imgBanner.kf.setImage(with: url1)
            }else {
                cltnHomeBannerHeightConstraints.constant = 0
            }
            
            return cellHomeBannerCell
        }
        return UICollectionViewCell()
    }
    
    //  Collection View Cell Height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == cltHomeCategory {
            if Constants.DeviceType.IS_IPAD {
                let width = collectionView.frame.size.width
                return CGSize(width: (width/4) - 10, height:170)
            } else {
                return CGSize(width: 78.0, height:72.0)
            }
        }
        
        if collectionView == cltHomeBanner {
            if Constants.DeviceType.IS_IPAD {
                let width = collectionView.frame.size.width
                return CGSize(width: (width/2) - 10, height:190)
            } else {
                let width = collectionView.frame.size.width
                return CGSize(width: (width/1) - 10, height:130.0)
            }
        }
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == cltHomeCategory {
            let dict:[String:AnyObject] = arrCategoryList[indexPath.row] as! [String:AnyObject]
            
            let objMegaMenuListVC : MegaMenuIteListViewController = self.storyboard?.instantiateViewController(withIdentifier: "MegaMenuIteListViewController") as! MegaMenuIteListViewController
            
            objMegaMenuListVC.strSlug = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "slug")
            objMegaMenuListVC.strTitle = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "cat_name")
            
            self.navigationController?.pushViewController(objMegaMenuListVC, animated: true)
        }
    }
    
    // MARK:- btnOpenDrawer
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    // MARK:- btnShoppingCart
    @IBAction func btnShoppingCart(_ sender: Any) {
        
        let shoppingCartViewController : ShoppingCartViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingCartViewController") as! ShoppingCartViewController
        
        self.navigationController?.pushViewController(shoppingCartViewController, animated: true)
    }
    
    func reloadHomeProductData() {
        self.tblHomeProduct.dataSource = self
        self.tblHomeProduct.delegate = self
        self.tblHomeProduct.reloadData()
    }
    
    // MARK: - SERVICE METHOD
    func GetHomeSlider() {
        
        let strUrl:String = BazarBit.HomeSliderService()
        let url:URL = URL(string: strUrl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            self.dicResponceHomeSlider = responseDict
            
            self.GetHomeBanner()
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceHomeSlider, key: "status")
            
            if status == "ok"
            {
                if (self.dicResponceHomeSlider["payload"] != nil)  {
                    
                    let payload:[String:Any] = self.dicResponceHomeSlider["payload"] as! [String:Any]
                    
                    //   print(payload)
                    
                    if (payload["slides"] as? [AnyObject] != nil) {
                        
                        let arrSlides:[AnyObject] = payload["slides"] as! [AnyObject]
                        
                        var dictSlides : [String : AnyObject] = [:]
                        
                        for dict in arrSlides
                        {
                            dictSlides = dict as! [String : AnyObject]
                            
                            let arrImages : [AnyObject] = dictSlides["image_url"] as! [AnyObject]
                            
                            if arrImages.count > 0
                            {
                                var imgUrl:String = arrImages[0] as! String
                                imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                                self.arrSideImages.append(imgUrl)
                                
                            }
                            //    print(self.arrSideImages)
                            
                        }
                        self.slideshow.dataSource = self
                        self.slideshow.delegate = self
                        self.slideshow.reloadData()
                    }else {
                        self.SlideShowHeightConstraints.constant = 0
                    }
                }
            }
        })
    }
    
    func GetBestSellerProducts() {
        
        let strurl:String = BazarBit.HomeBestSellerProsuctService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        parameter["offset"] = strBestSellerOffset
        parameter["limit"] = strlimit
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
            
        }else {
            parameter["user_id"] = ""
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceBestSeller = responseDict
            self.GetMostWantedProducts()
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceBestSeller, key: "status")
            
            if status == "ok"
            {
                if (self.dicResponceBestSeller["payload"] != nil)
                {
                    let payload:[String:AnyObject] = self.dicResponceBestSeller["payload"] as! [String:AnyObject]
                    
                    if payload["bestseller"] != nil
                    {
                        let BestSeller:[AnyObject] = payload["bestseller"] as! [AnyObject]
                        
                        if self.strBestSellerOffset == "0" {
                            self.aryAllImagesBestSeller = []
                            self.arrBestSeller = []
                        }
                        
                        if BestSeller.count != Int(self.strlimit.toDouble()!) {
                            self.isCompleteBestSellerRecord = true
                        }
                        
                        for dict in BestSeller
                        {
                            self.arrBestSeller.append(dict)
                            
                            if let key = dict["main_image"] {
                                if (key as? [String : AnyObject]) != nil
                                {
                                    let imgUrl:String = key!["main_image"] as! String
                                    self.aryAllImagesBestSeller.append(imgUrl)
                                }
                            }
                        }
                    }
                }
            }
            
            let limit: Int = Int(self.strlimit)!
            var bestSellerOffset: Int = Int(self.strBestSellerOffset)!
            bestSellerOffset = bestSellerOffset + limit
            self.strBestSellerOffset = String(bestSellerOffset)
            
        })
    }
    
    func GetMostWantedProducts() {
        
        let strurl:String = BazarBit.HomeMostWantedProductService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }
        else
        {
            parameter["user_id"] = ""
        }
        
        parameter["offset"] = strMostWantedOffset
        parameter["limit"] = strlimit
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceMostWantedproducts = responseDict
            self.GetNewArrivalProducts()
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceMostWantedproducts, key: "status")
            
            if status == "ok"
            {
                if (self.dicResponceMostWantedproducts["payload"] != nil) {
                    let payload:[String:AnyObject] = self.dicResponceMostWantedproducts["payload"] as! [String:AnyObject]
                    
                    if payload["mostWantedProducts"] != nil
                    {
                        let arrWantedProduct:[AnyObject] = payload["mostWantedProducts"] as! [AnyObject]
                        
                        if self.strMostWantedOffset == "0" {
                            self.aryAllImagesMostWanted = []
                            self.arrMostWanted = []
                        }
                        
                        if arrWantedProduct.count != Int(self.strlimit.toDouble()!) {
                            self.isCompleteMostWantedRecord = true
                        }
                        
                        for dict in arrWantedProduct
                        {
                            self.arrMostWanted.append(dict)
                            if let key = dict["main_image"] {
                                if (key as? [String : AnyObject]) != nil
                                {
                                    let imgUrl:String = key!["main_image"] as! String
                                    self.aryAllImagesMostWanted.append(imgUrl)
                                }
                            }
                        }
                        
                    }
                }
            }
            
            let limit: Int = Int(self.strlimit)!
            var MostWantedOffset: Int = Int(self.strMostWantedOffset)!
            MostWantedOffset = MostWantedOffset + limit
            self.strMostWantedOffset = String(MostWantedOffset)
        })
    }
    
    func GetNewArrivalProducts() {
        
        let strurl:String = BazarBit.HomeNewArrivalProductService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }
        else
        {
            parameter["user_id"] = ""
        }
        
        parameter["offset"] = strNewArrivalOffset
        parameter["limit"] = strlimit
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceNewArrivalProducts = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceNewArrivalProducts, key: "status")
            
            if status == "ok"
            {
                if (self.dicResponceNewArrivalProducts["payload"] != nil)
                {
                    let payload:[String:AnyObject] = self.dicResponceNewArrivalProducts["payload"] as! [String:AnyObject]
                    
                    if payload["newArrivals"] != nil
                    {
                        let arrNewarrivalProduct:[AnyObject] = payload["newArrivals"] as! [AnyObject]
                        
                        if self.strNewArrivalOffset == "0" {
                            self.aryAllImagesNewArrival = []
                            self.arrNewArrival = []
                        }
                        
                        if arrNewarrivalProduct.count != Int(self.strlimit.toDouble()!) {
                            self.isCompleteNewArrivalRecord = true
                        }
                        
                        for dict in arrNewarrivalProduct
                        {
                            self.arrNewArrival.append(dict)
                            
                            if let key = dict["main_image"] {
                                if (key as? [String : AnyObject]) != nil
                                {
                                    let imgUrl:String = key!["main_image"] as! String
                                    self.aryAllImagesNewArrival.append(imgUrl)
                                }
                            }
                        }
                        
                    }
                }
            }
            
            let limit: Int = Int(self.strlimit)!
            var NewArrivalOffset: Int = Int(self.strNewArrivalOffset)!
            NewArrivalOffset = NewArrivalOffset + limit
            self.strNewArrivalOffset = String(NewArrivalOffset)
            
            self.reloadHomeProductData()
        })
    }
    
    
    func GetHomeBanner() {
        
        let strurl:String = BazarBit.HomeBannerService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceHomeBanner = responseDict
            self.GetCategoryList()
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceHomeBanner, key: "status")
            
            if status == "ok"
            {
                
                if (self.dicResponceHomeBanner["payload"] != nil)
                {
                    var payload : [String:AnyObject] = self.dicResponceHomeBanner["payload"] as! [String:AnyObject]
                    
                    if payload.count > 0 {
                        if (payload["images"] as? [AnyObject]) != nil
                        {
                            self.arrHomeBannerImages = payload["images"] as! [AnyObject]
                            
                            self.arrbannerImages = []
                            
                            for i in 0...self.arrHomeBannerImages.count - 1 {
                                let dict:[String: AnyObject] = self.arrHomeBannerImages[i] as! [String : AnyObject]
                                
                                let imgUrl = dict["image_url"] as! [String:AnyObject]
                                
                                let MainImage = PIService.object_forKeyWithValidationForClass_String(dict: imgUrl, key: "main_image")
                                self.arrbannerImages.append(MainImage)
                            }
                            self.cltHomeBanner.dataSource = self
                            self.cltHomeBanner.delegate = self
                            self.cltHomeBanner.reloadData()
                        }
                    } else {
                        self.cltnHomeBannerHeightConstraints.constant = 0
                    }
                }
            }
        })
    }
    
    func GetCategoryList() {
        
        let strurl:String = BazarBit.GetCategoryListService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceCategoryList = responseDict
            self.GetBestSellerProducts()
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCategoryList, key: "status")
            
            if status == "ok"
            {
                if self.dicResponceCategoryList["payload"] != nil
                {
                    let payload:[AnyObject] = self.dicResponceCategoryList["payload"] as! [AnyObject]
                    self.arrCategoryList = []
                    self.arrCategoryImages = []
                    
                    for i in payload
                    {
                        self.arrCategoryList.append(i)
                        
                        self.arrCategoryImages = []
                        for p in 0...self.arrCategoryList.count-1
                        {
                            let dict:[String: AnyObject] = self.arrCategoryList[p] as! [String : AnyObject]
                            
                            if (dict["image_url"] as? [String:AnyObject]) != nil
                            {
                                let imgUrl:[String:AnyObject] = dict["image_url"] as! [String:AnyObject]
                                
                                if (imgUrl["main_image"] as? String) != nil
                                {
                                    var MainImage:String = PIService.object_forKeyWithValidationForClass_String(dict: imgUrl, key: "main_image")
                                    
                                    MainImage = BazarBit.GetImageUrl(strurl: MainImage)
                                    self.arrCategoryImages.append(MainImage)
                                }
                            }
                        }
                    }
                    self.cltHomeCategory.dataSource = self
                    self.cltHomeCategory.delegate = self
                    self.cltHomeCategory.reloadData()
                }
            }
        })
    }
    
    func GetCartCountData() {
        
        let strurl:String = BazarBit.CountCartTotalService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        if UserId != nil {
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }
        else
        {
            let strSessionId:String = BazarBit.getSessionId()
            parameter["session_id"] = strSessionId
            parameter["user_id"] = ""
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceCartTotalList = responseDict
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCartTotalList, key: "status")
            
            if status == "ok"
            {
                if self.dicResponceCartTotalList["payload"] != nil
                {
                    let payload:[String:AnyObject] = self.dicResponceCartTotalList["payload"] as! [String:AnyObject]
                    //     print(payload)
                    if let wishListCnt : String = payload["wishlist_cnt"] as? String {
                        //     print(wishListCnt)
                        UserDefaults.standard.set(wishListCnt, forKey: "WishListCount")
                        
                        let cntWishlist : String = UserDefaults.standard.value(forKey: "WishListCount") as! String
                        APPDELEGATE.setWishListCount(strCount: cntWishlist)
                    }
                    
                    let strCartTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "cart_items")
                    
                    UserDefaults.standard.set(strCartTotal, forKey: BazarBit.CARTTOTALCOUNT)
                    
                    if (UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) != nil) {
                        
                        let strCartCount: String = UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) as! String
                        
                        if strCartCount != "0" {
                            //                            print(strCartCount.characters.count)
                            if strCartCount.characters.count == 4 {
                                self.lblCartTotalWidthConstraints.constant = 30
                            }else if strCartCount.characters.count == 3  {
                                self.lblCartTotalWidthConstraints.constant = 25
                            }else {
                                self.lblCartTotalWidthConstraints.constant = 18
                            }
                            
                            self.lblCartTotalCount.isHidden = false
                            self.lblCartTotalCount.text = strCartCount
                            self.lblCartTotalCount.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
                            self.lblCartTotalCount.isHidden = false
                        } else {
                            self.lblCartTotalCount.isHidden = true
                        }
                        
                        if Constants.DeviceType.IS_IPAD {
                            self.lblCartTotalCount.layer.masksToBounds = true
                            self.lblCartTotalCount.layer.cornerRadius = 10
                        } else {
                            self.lblCartTotalCount.layer.masksToBounds = true
                            self.lblCartTotalCount.layer.cornerRadius = 10
                        }
                        
                        if APPDELEGATE.is_cart_active == "0" {
                            if Constants.DeviceType.IS_IPAD {
                                self.cartView.isHidden = true
                            }else {
                                self.cartView.isHidden = true
                            }
                        } else {
                            if Constants.DeviceType.IS_IPAD {
                                self.cartView.isHidden = false
                            }else {
                                self.cartView.isHidden = false
                            }
                        }
                    }
                }
            }
            
            self.GetHomeSlider()
            
        })
    }
}

