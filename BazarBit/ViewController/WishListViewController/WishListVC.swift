//
//  WishListVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 09/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import EZSwiftExtensions

class WishListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var identifier : String = "WishListCell"
    var tabbar: TabbarVC!
    @IBOutlet var lableTitle: PICustomTitleLable!
    @IBOutlet var tableViewWishList: UITableView!
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var viewEmptyCart : UIView!
    @IBOutlet var btnContinueShopping : UIButton!
    
    @IBOutlet var lableEmptyWishList : UILabel!
    
    var aryWishListItems:[AnyObject] = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableViewWishList.estimatedRowHeight = 180.0
        
        tableViewWishList.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        self.viewEmptyCart.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        btnContinueShopping.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: .normal)
        
        lableEmptyWishList.setTextLabelThemeColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.viewEmptyCart.isHidden = true
        self.ServiceCallForGetWishlistItem()
    }
    
    //MARK:- UITableView Method
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if Constants.DeviceType.IS_IPAD {
            return 230.0
        }else {
            return 180.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryWishListItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let wishListCell : WishListTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! WishListTableViewCell
        
        let dictData : [String : AnyObject] = aryWishListItems[indexPath.row] as! [String : AnyObject]
        
        wishListCell.setUpData(data: dictData["product_data"] as! [String : AnyObject])
        
        let ButtonTitle : String = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "ButtonTitle")
        wishListCell.buttonAddToCart.setTitle(ButtonTitle, for: .normal)
        
        wishListCell.buttonRemove.tag = indexPath.row
        wishListCell.buttonAddToCart.tag = indexPath.row
        
        return wishListCell
    }
    
    //MARK:- Action Method
    
    @IBAction func buttonBack_Clicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    @IBAction func buttonContinueShopping_Clicked(_ sender: Any) {
        
        let objHomeViewController : HomeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(objHomeViewController, animated: true)
    }
    
    @IBAction func buttonRemove_Clicked(_ sender: AnyObject) {
        
        let dictData : [String : AnyObject] = aryWishListItems[sender.tag] as! [String : AnyObject]
        let strOrderWidhListId : String = dictData["orderwishlist_id"] as! String
        self.ServiceCallForRemoveWishlistItem(strWishlistItemnId: strOrderWidhListId, index: sender.tag)
    }
    
    @IBAction func buttonAddToCart_Clicked(_ sender: UIButton) {
        
        let dictData : [String : AnyObject] = aryWishListItems[sender.tag] as! [String : AnyObject]
        
        let product_data : [String : AnyObject] = dictData["product_data"] as! [String : AnyObject]
        APPDELEGATE.ProductID = PIService.object_forKeyWithValidationForClass_String(dict: product_data, key: "product_id")
        
        APPDELEGATE.slugName = PIService.object_forKeyWithValidationForClass_String(dict: product_data, key: "slug")
        
        if sender.titleLabel?.text == "Notify Me" {
            self.ServiceCallForCheckItemInNotify(strProduct_ID: APPDELEGATE.ProductID)
        }else if sender.titleLabel?.text == "GO TO CART" {
            let shoppingCartViewController  = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingCartViewController") as! ShoppingCartViewController
            self.navigationController?.pushViewController(shoppingCartViewController, animated: true)
        }else {
            let productDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
            self.navigationController?.pushViewController(productDetailVC, animated: true)
        }
    }
    
    //MARK:-  Helper Method
    
    func displayWishlistRecord() {
        self.tableViewWishList.dataSource = self
        self.tableViewWishList.delegate = self
        self.tableViewWishList.reloadData()
        
        if self.aryWishListItems.count != 0 {
            self.viewEmptyCart.isHidden = true
        }else {
            self.viewEmptyCart.isHidden = false
        }
        
        BazarBitLoader.stopLoader()
    }
    
    func checkAndSetTitleForWishlistButton(indexValue: Int) {
        
        if self.aryWishListItems.count > indexValue {
            
            for i in indexValue...self.aryWishListItems.count - 1 {
                
                var dictWishlist: [String: AnyObject] = self.aryWishListItems[i] as! [String: AnyObject]
                
                let product_data : [String : AnyObject] = dictWishlist["product_data"] as! [String : AnyObject]
                
                var strStockStatus : String = ""
                if product_data["stock_status"] != nil {
                    strStockStatus = PIService.object_forKeyWithValidationForClass_String(dict: product_data, key: "stock_status")
                }
                
                let qty : String = PIService.object_forKeyWithValidationForClass_String(dict: product_data, key: "quantity")
                
                let product_id : String = PIService.object_forKeyWithValidationForClass_String(dict: product_data, key: "product_id")
                
                if strStockStatus != "" {
                    if (strStockStatus.toInt()! == 0 || qty.toInt()! <= 0) {
                        dictWishlist["ButtonTitle"] = "Notify Me" as AnyObject
                        self.aryWishListItems[i] = dictWishlist as AnyObject
                        if self.aryWishListItems.count - 1 == i {
                            self.displayWishlistRecord()
                        }
                        
                    } else {
                        self.ServiceCallForCheckTitleForExistInCart(strProduct_ID: product_id, index: i, dictWishlist: dictWishlist)
                        break
                    }
                    
                } else {
                    
                    dictWishlist["ButtonTitle"] = "Notify Me" as AnyObject
                    self.aryWishListItems[i] = dictWishlist as AnyObject
                    if self.aryWishListItems.count - 1 == i {
                        self.displayWishlistRecord()
                    }
                }
            }
        }
    }
    
    
    //MARK:-  Service Method
    func ServiceCallForGetWishlistItem() {
        
        let strurl:String = BazarBit.GetWishListItemService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            BazarBitLoader.stopLoader()
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            
            if status == "ok"
            {
                self.aryWishListItems = []
                if (response["payload"] != nil)
                {
                    let payload:[String:AnyObject] = response["payload"] as! [String:AnyObject]
                    
                    if payload["wishlist"] != nil
                    {
                        let wishListData:[String:AnyObject] = payload["wishlist"] as! [String:AnyObject]
                        
                        if wishListData["wishlist_items"] as? [AnyObject] != nil
                        {
                            self.aryWishListItems = wishListData["wishlist_items"] as! [AnyObject]
                            
                            if self.aryWishListItems.count != 0 {
                                self.viewEmptyCart.isHidden = true
                                self.checkAndSetTitleForWishlistButton(indexValue: 0)
                            }else {
                                self.viewEmptyCart.isHidden = false
                                BazarBitLoader.stopLoader()
                            }
                        }
                    }
                }
            }else {
                let message:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "message")
                PIAlertUtils.displayAlertWithMessage(message)
            }
            
            let count : Int = Int(self.aryWishListItems.count)
            APPDELEGATE.setWishListCount(strCount: String(count))
            
        })
    }
    
    func ServiceCallForRemoveWishlistItem(strWishlistItemnId : String, index: Int) {
        
        let strurl:String = BazarBit.RemoveWishListItemService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["wishlistitem_id"] = strWishlistItemnId
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            let response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok" {
                
                let cntWishlist : String = UserDefaults.standard.value(forKey: "WishListCount") as! String
                var count : Int = Int(cntWishlist)!
                if count != 0 {
                    count = count - 1
                    UserDefaults.standard.set(String(count), forKey: "WishListCount")
                }
                APPDELEGATE.setWishListCount(strCount: String(count))
                PIAlertUtils.displayAlertWithMessage(message)
            }
            
            self.aryWishListItems.remove(at: index)
            self.displayWishlistRecord()
        })
    }
    
    func ServiceCallForCheckItemInNotify(strProduct_ID : String) {
        
        let strurl:String = BazarBit.CheckItemInNotifyService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["product_id"] = strProduct_ID
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            BazarBitLoader.stopLoader()
            
            let response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                self.ServiceCallForAddProductInNotify(strProduct_ID: strProduct_ID)
            }else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func ServiceCallForAddProductInNotify(strProduct_ID : String) {
        
        let strurl:String = BazarBit.AddItemInNotifyService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["product_id"] = strProduct_ID
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            BazarBitLoader.stopLoader()
            
            let response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok" {
                PIAlertUtils.displayAlertWithMessage(message)
            }else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func ServiceCallForCheckTitleForExistInCart(strProduct_ID : String, index: Int, dictWishlist: [String: AnyObject]) {
        
        let strurl:String = BazarBit.CheckItemExistInCartService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        parameter["user_id"] = UserDefaults.standard.value(forKey: BazarBit.USERID)
        parameter["product_id"] = strProduct_ID
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: false, completion: {(swiftyJson, responseDict) in
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            
            if status == "ok"
            {
                if (response["payload"] != nil)
                {
                    let payload:[String:AnyObject] = response["payload"] as! [String:AnyObject]
                    
                    let itemExistCount : String = payload["itemexists_count"] as! String
                    
                    var LocaldictWishList: [String: AnyObject] = dictWishlist
                    
                    if itemExistCount.toInt()! > 0 {
                        if APPDELEGATE.is_cart_active == "1" {
                            LocaldictWishList["ButtonTitle"] = "GO TO CART" as AnyObject
                        }else {
                            LocaldictWishList["ButtonTitle"] = "VIEW DETAIL" as AnyObject
                        }
                    }else {
                        LocaldictWishList["ButtonTitle"] = "VIEW DETAIL" as AnyObject
                        
                    }
                    
                    self.aryWishListItems[index] = LocaldictWishList as AnyObject
                }
            }
            
            let checkIndex: Int = index + 1
            if  checkIndex <= self.aryWishListItems.count - 1 {
                self.checkAndSetTitleForWishlistButton(indexValue: checkIndex)
            } else {
                self.displayWishlistRecord()
            }
        })
    }

    func ServiceCallForAddToCart(strProduct_ID : String, strProductName : String, strPrice: String) {
        
        let strurl:String = BazarBit.AddToCartService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["user_id"] = UserDefaults.standard.value(forKey: BazarBit.USERID)
        parameter["product_id"] = strProduct_ID
        parameter["product_name"] = strProductName
        parameter["price"] = strPrice
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            BazarBitLoader.stopLoader()
            
            let response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                PIAlertUtils.displayAlertWithMessage(message)
                self.ServiceCallForGetWishlistItem()
            }
        })
    }
}

