//
//  MegaMenuIteListViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/25/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RSSelectionMenu


class MegaMenuIteListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UIScrollViewDelegate {
    
    @IBOutlet var viewNoData : UIView!
    @IBOutlet var lableNoDataFound : UILabel!
    
    @IBOutlet weak var lblCartTotal: UILabel!
    @IBOutlet weak var lblCartTotalWidthConstraints: NSLayoutConstraint!
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    
    @IBOutlet weak var viewcart: UIView!
    @IBOutlet var viewSearchBack : UIView!
    @IBOutlet var textFieldSearch : UITextField!
    
    @IBOutlet var buttonFilter : UIButton!
    
    @IBOutlet var collectionViewMegaMenuItem : UICollectionView!
    @IBOutlet var cltnFlowLayout: UICollectionViewFlowLayout!
    
    var strSlug : String = ""
    var strTitle : String = ""
    var is_Sort:Int = 0
    var aryProductData : [AnyObject] = [AnyObject]()
    var objArySearchResult : [AnyObject] = [AnyObject]()
    var objSelectedData : [String] = [String]()
    var wishList : Bool = false
    var isServiceCallComplete : Bool = false
    var isCompleteMegaMenuRecord : Bool = false
    var strlimit: String = "10"
    var strMegaMenuOffset: String = "0"
    
    var dicResponceCartTotalList:[String:AnyObject] = [:]
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        buttonFilter.layer.shadowColor = UIColor.black.cgColor
        buttonFilter.layer.shadowOffset = CGSize(width: 5, height: 5)
        buttonFilter.layer.shadowRadius = 5
        
        // Do any additional setup after loading the view.
        collectionViewMegaMenuItem.delegate = self
        collectionViewMegaMenuItem.dataSource = self
        
        collectionViewMegaMenuItem.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        if IS_IPAD_DEVICE() {
            cltnFlowLayout.sectionInset = UIEdgeInsetsMake(30, 30, 30, 30)
        }
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        viewSearchBack.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        lblCartTotal.setTextLabelDynamicTextColor()
        lableNoDataFound.setTextLabelThemeColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        lableTitle.text = strTitle
        
        if (UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) != nil) {
            
            let strCartCount: String = UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) as! String
            
            if strCartCount != "0" {
                if strCartCount.characters.count == 4 {
                    self.lblCartTotalWidthConstraints.constant = 30
                } else if strCartCount.characters.count == 3  {
                    self.lblCartTotalWidthConstraints.constant = 25
                }  else {
                    self.lblCartTotalWidthConstraints.constant = 18
                }
                
                self.lblCartTotal.text = strCartCount
                self.lblCartTotal.isHidden = false
            } else {
                self.lblCartTotal.isHidden = true
            }
            
            if Constants.DeviceType.IS_IPAD {
                lblCartTotal.layer.masksToBounds = true
                lblCartTotal.layer.cornerRadius = 10
            }else{
                lblCartTotal.layer.masksToBounds = true
                lblCartTotal.layer.cornerRadius = 10
            }
            if APPDELEGATE.is_cart_active == "0"
            {
                if Constants.DeviceType.IS_IPAD {
                    viewcart.isHidden = true
                }else {
                    viewcart.isHidden = true
                }
            }else{
                if Constants.DeviceType.IS_IPAD {
                    viewcart.isHidden = false
                }else{
                    viewcart.isHidden = false
                }
            }
        }
        
        GetCartCountData()
        
        self.navigationController?.navigationBar.isHidden = true
        
        // Set Uitextfield Left Image
        let leftImageView = UIImageView()
        leftImageView.image = UIImage(named: "search")
        let leftView = UIView()
        leftView.addSubview(leftImageView)
        
        if Constants.DeviceType.IS_IPAD {
            leftView.frame = CGRect(x: 30, y: 0, width: 35, height: 25)
            leftImageView.frame = CGRect(x: 12, y: 0, width: 25, height: 25)
        }
        else
        {
            leftView.frame = CGRect(x: 15, y: 0, width: 25, height: 15)
            leftImageView.frame = CGRect(x: 6, y: 0, width: 15, height: 15)
        }
        
        textFieldSearch.leftViewMode = .always
        textFieldSearch.leftView = leftView
        textFieldSearch.setTextFieldDynamicColor()
        textFieldSearch.setCornerRadius(radius: 5)
        
        print(objSelectedData)
        
        self.ServiceCallForGetCategoryWiseProduct(strSlug: strSlug, attrTermsId: objSelectedData)
    }
    
    //MARK: UITEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var strTemp : String = ""
        if (textField.text?.characters.count)! == 1 && string == "" {
            aryProductData = objArySearchResult
            self.collectionViewMegaMenuItem.reloadData()
            return true
        }
        
        if string == ""
        {
            strTemp = (textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!))!
        }
        else{
            strTemp = textField.text! + string
        }
        
        let preda = NSPredicate(format: "name CONTAINS[c] '\(strTemp)'")
        aryProductData = objArySearchResult.filter { preda.evaluate(with: $0) }
        print(aryProductData)
        self.collectionViewMegaMenuItem.reloadData()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = aryProductData[indexPath.row] as! [String:AnyObject]
        print(dict)
        
        APPDELEGATE.ProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
        //print(APPDELEGATE.ProductID)
        APPDELEGATE.slugName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "slug")
        //print(APPDELEGATE.slugName)
        
        
        let productDetailVC = storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        self.navigationController?.pushViewController(productDetailVC, animated: true)
        //if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            //myDelegate.OpenProductDetailPage()
        //}
    }
    
    //MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return aryProductData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let identifier : String = "megaMenuItemCell"
        let itemCell : MegaMenuItemCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! MegaMenuItemCollectionViewCell
        
        itemCell.contentView.layer.cornerRadius = 5.0
        itemCell.contentView.layer.masksToBounds = true
        
        let dictData : [String : AnyObject] = aryProductData[indexPath.row] as! [String : AnyObject]
        
        let existInWishList : String = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "exists_in_wishlist")
        //print(existInWishList)
        
        if existInWishList == "1" {
            itemCell.buttonWishList.setImage((UIImage(named: "like1.png"), for: UIControlState.normal))
            wishList = true
        } else {
            itemCell.buttonWishList.setImage((UIImage(named: "like2.png"), for: UIControlState.normal))
            wishList = false
        }
        
        itemCell.buttonWishList.tag = indexPath.row
        itemCell.buttonWishList.addTarget(self, action: #selector(self.buttonAddToWishListClicked), for: .touchUpInside)
        
        itemCell.setUpData(modelData: dictData)
        
        return itemCell
    }
    
    //MARK: UICollectionViewFlowLayOut
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if IS_IPAD_DEVICE() {
            return CGSize(width: (SCREEN_WIDTH / 2) - 50, height: 350)
        }
        else if IS_IPHONE_5_OR_5S()
        {
            return CGSize(width: (SCREEN_WIDTH / 2) - 21, height: 160)
        }
        return CGSize(width: (SCREEN_WIDTH / 2) - 21, height: 210)
    }
    
    //MARK: buttonBack_Clicked
    @IBAction func buttonBack_Clicked(_ sender: Any) {
        APPDELEGATE.arrSelect = []
        self.navigationController?.popViewController(animated: true)
        GetCartCountData()
    }
    
    //MARK: buttonCart_Clicked
    @IBAction func buttonCart_Clicked(_ sender: Any) {
        let shoppingCartViewController : ShoppingCartViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingCartViewController") as! ShoppingCartViewController
        
        self.navigationController?.pushViewController(shoppingCartViewController, animated: true)
    }
    
    //MARK: buttonFilter_Clicked
    @IBAction func buttonSorting_Clicked(_ sender: Any) {
    
        let simpleDataArray = ["Low to High", "High to Low"]
        var simpleSelectedArray = [String]()
        
        // Show menu with datasource array - Default SelectionStyle = single
        // Here you'll get cell configuration where you'll get array item for each index
        // Cell configuration following parameters.
        // 1. UITableViewCell   2. Item of type T   3. IndexPath
        
        let selectionMenu = RSSelectionMenu(dataSource: simpleDataArray) { (cell, item, indexPath) in
            cell.textLabel?.text = item
        }
        
        // set default selected items when menu present on screen.
        // here you'll get handler each time you select a row
        // 1. Selected Item  2. Index of Selected Item  3. Selected or Deselected  4. All Selected Items
        
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (item, index, isSelected, selectedItems) in
            
            if index == 0 {
                self?.is_Sort = 1
                
                self?.objSelectedData = []
                APPDELEGATE.arrSelect = []
                self?.ServiceCallForGetCategoryWiseProduct(strSlug: self!.strSlug, attrTermsId: self!.objSelectedData)
                
            }else if index == 1 {
                self?.is_Sort = 2
                
                self?.objSelectedData = []
                APPDELEGATE.arrSelect = []
                self?.ServiceCallForGetCategoryWiseProduct(strSlug: self!.strSlug, attrTermsId: self!.objSelectedData)
                
            }else {
                self?.is_Sort = 0
                
                self?.objSelectedData = []
                APPDELEGATE.arrSelect = []
                self?.ServiceCallForGetCategoryWiseProduct(strSlug: self!.strSlug, attrTermsId: self!.objSelectedData)
                
            }
            // update your existing array with updated selected items, so when menu show menu next time, updated items will be default selected.
           // simpleSelectedArray = selectedItems
            
            
        }
        
      
        // show as popover
        //selectionMenu.show(style: .popover(sourceView: sourceView, size: nil), from: self)
        
        // or specify popover size
        selectionMenu.show(style: .popover(sourceView: sender as! UIView, size: CGSize(width: 150, height: 60)), from: self)
        
        
        /// show as alert
        //selectionMenu.show(style: .alert(title: "Select", action: nil, height: nil), from: self)
        
        // or specify alert button title
        //selectionMenu.show(style: .alert(title: "Select", action: "Done", height: nil), from: self)

    }
    
    @IBAction func buttonFilter_Clicked(_ sender: Any) {
        let objFilterVC : FilterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        objFilterVC.objMegaMenuVC = self
        self.navigationController?.pushViewController(objFilterVC, animated: true)
    }
    
    //MARM : buttonReload_Clicked
    @IBAction func buttonReload(_ sender: Any) {
        objSelectedData = []
        APPDELEGATE.arrSelect = []
        self.ServiceCallForGetCategoryWiseProduct(strSlug: strSlug, attrTermsId: objSelectedData)
    }
    
    //MARM : Helper Method

    func getSearchIndex(strSearchText: String, strSearchKey: String, arrSearch: [AnyObject]) -> Int {
        
        var  searchIndex: Int = -1
        
        let preda = NSPredicate(format: "\(strSearchKey) CONTAINS[c] '\(strSearchText)'")
        let arrLocalSearch = arrSearch.filter { preda.evaluate(with: $0) }
        
        if arrLocalSearch.count > 0 {
            
            searchIndex = arrSearch.index {
                if let dic = $0 as? Dictionary<String,AnyObject> {
                    if let value = dic[strSearchKey]  as? String, value == strSearchText {
                        return true
                    }
                }
                return false
                }!
        }
        
        return searchIndex
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == collectionViewMegaMenuItem {
            
            if self.isServiceCallComplete == true {
                
                    if self.isCompleteMegaMenuRecord == false {
                        
                        if ((collectionViewMegaMenuItem.contentOffset.y + collectionViewMegaMenuItem.frame.size.height) >= collectionViewMegaMenuItem.contentSize.height)
                        {
                            self.isServiceCallComplete = false
                            self.ServiceCallForGetCategoryWiseProduct(strSlug: self.strSlug, attrTermsId: objSelectedData)
                        }
                    }
            }
        }
    }
    
    //MARK: buttonAddToWishListClicked
    func buttonAddToWishListClicked(sender:UIButton) {
        print(sender.tag)
        
        let dict = aryProductData[sender.tag] as! [String:AnyObject]
        print(dict)
        
        APPDELEGATE.selectedProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
        print(APPDELEGATE.selectedProductID)
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
            self.ServiceCallForAddToWishList(strProductId: APPDELEGATE.selectedProductID)
        }
        else
        {
            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                myDelegate.openLoginPage()
                //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
            }
        }
        sender.setImage(UIImage(named: "like1.png"), for: UIControlState.normal)
    }
    
    //MARK: ServiceCallForGetCategoryWiseProduct
    func ServiceCallForGetCategoryWiseProduct(strSlug : String, attrTermsId : [String]) {
        
        let strurl:String = BazarBit.GetCategoryWiseProductService()
        let url:URL = URL(string: strurl)!
        print(url)
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        print(headers)
        
        
        
        let userId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        var parameter:Parameters = [:]
        parameter["slug"] = strSlug
        parameter["attrtermsid"] = attrTermsId
        
        if is_Sort == 1 {
            parameter["sorting"] = "low_to_high"
        }else if is_Sort == 2 {
            parameter["sorting"] = "high_to_low"
        }else {
            parameter["sorting"] = ""
        }
        
        if userId != nil {
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }else {
            parameter["user_id"] = ""
        }
        
        parameter["offset"] = self.strMegaMenuOffset
        parameter["limit"] = self.strlimit
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            print(responseDict)
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if self.strMegaMenuOffset == "0" {
                    self.aryProductData = []
                    self.objArySearchResult = []
                }
                
                if (response["payload"] != nil)
                {
                    let payload:[String : AnyObject] = response["payload"] as! [String : AnyObject]
                    print(payload)
                    
                    if (payload["products"] as? [AnyObject]) != nil
                    {
                        let megaProduct: [AnyObject] = payload["products"] as! [AnyObject]
                        
                        if megaProduct.count != Int(self.strlimit.toDouble()!) {
                            self.isCompleteMegaMenuRecord = true
                        }
                        
                        self.aryProductData.append(contentsOf: megaProduct)
                        self.objArySearchResult.append(contentsOf: megaProduct)
                    }
                    
                    if self.aryProductData.count != 0 {
                        self.viewNoData.isHidden = true
                    }else {
                        self.viewNoData.isHidden = false
                    }
                }
            }else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
            
            let limit: Int = Int(self.strlimit)!
            var megaMenuOffset: Int = Int(self.strMegaMenuOffset)!
            megaMenuOffset = megaMenuOffset + limit
            self.strMegaMenuOffset = String(megaMenuOffset)
            self.isServiceCallComplete = true
            
            self.collectionViewMegaMenuItem.reloadData()
        })
    }
    
    //MARK: ServiceCallForAddToWishList
    func ServiceCallForAddToWishList(strProductId: String) {
        
        let strurl:String = BazarBit.AddItemToWishListService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        print(headers)
        
        var parameter:Parameters = [:]
        parameter["product_id"] = strProductId
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
            let messege = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok" {
                
                let megaMenuSearchIndex: Int = self.getSearchIndex(strSearchText: strProductId, strSearchKey: "product_id", arrSearch: self.aryProductData)
                
                if megaMenuSearchIndex >= 0 && megaMenuSearchIndex < self.aryProductData.count {
                    
                    var dictMegaMenu: [String: AnyObject] = self.aryProductData[megaMenuSearchIndex] as! [String:AnyObject]
                    dictMegaMenu["exists_in_wishlist"] = "1" as AnyObject
                    self.aryProductData[megaMenuSearchIndex] = dictMegaMenu as AnyObject
                }
                
                let megaAllSearchIndex: Int = self.getSearchIndex(strSearchText: strProductId, strSearchKey: "product_id", arrSearch: self.objArySearchResult)
                
                if megaAllSearchIndex >= 0 && megaAllSearchIndex < self.objArySearchResult.count {
                    
                    var dictMegaMenu: [String: AnyObject] = self.objArySearchResult[megaAllSearchIndex] as! [String:AnyObject]
                    dictMegaMenu["exists_in_wishlist"] = "1" as AnyObject
                    self.objArySearchResult[megaAllSearchIndex] = dictMegaMenu as AnyObject
                }
                
                self.collectionViewMegaMenuItem.reloadData()
                
            }else {
                PIAlertUtils.displayAlertWithMessage(messege)
            }
        })
    }
    
    // MARK:- Service call for GEt Cart Count
    
    
    func GetCartCountData() {
        
        let strurl:String = BazarBit.CountCartTotalService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        } else{
            let strSessionId:String = BazarBit.getSessionId()
            parameter["session_id"] = strSessionId
            parameter["user_id"] = ""
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceCartTotalList = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCartTotalList, key: "status")
            
            if status == "ok"
            {
                if self.dicResponceCartTotalList["payload"] != nil
                {
                    let payload:[String:AnyObject] = self.dicResponceCartTotalList["payload"] as! [String:AnyObject]
                    
                    let strCartTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "cart_items")
                    UserDefaults.standard.set(strCartTotal, forKey: BazarBit.CARTTOTALCOUNT)
                    
                    if let wishListCnt : String = payload["wishlist_cnt"] as? String {
                        print(wishListCnt)
                        UserDefaults.standard.set(wishListCnt, forKey: "WishListCount")
                        
                        let cntWishlist : String = UserDefaults.standard.value(forKey: "WishListCount") as! String
                        APPDELEGATE.setWishListCount(strCount: cntWishlist)
                    }
                }
            }
        })
    }
}
