//
//  AddFeedbackViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 11/1/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddFeedbackViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    
    @IBOutlet var tableViewFeedBack: UITableView!
    @IBOutlet var tableFeedBackQuestionHeight: NSLayoutConstraint!
    @IBOutlet var btnSubmit: UIButton!
    
    var arrFeedbackForm: [AnyObject] = []
    var strProductId: String = ""
    var strOrderItemId: String = ""
    
    var arrQuestions: [AnyObject] = []
    
    var offset1:Int=0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.ServiceCallForGetFeedbackForm()
        
        btnSubmit.setButtonDynamicColor()
        btnSubmit.Shadow()
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        tableViewFeedBack.contentInset = UIEdgeInsetsMake(-30, 0, 0, 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - UITableview Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrFeedbackForm.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let dict:[String:AnyObject] = self.arrFeedbackForm[section] as! [String:AnyObject]
        
        if (dict["ans_values"] as? [String:AnyObject]) != nil  {
            let dictAnsValue:[String: AnyObject] = dict["ans_values"] as! [String: AnyObject]
            return dictAnsValue.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableViewFeedBack {
            let dict:[String:AnyObject] = self.arrFeedbackForm[indexPath.section] as! [String:AnyObject]
            
            let strAnsType: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "ans_type")
            
            if strAnsType == "radio" {
                return 30
            } else {
                return 44
            }
        }
        return 0.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.size.width, height: 30))
        
        let dict:[String:AnyObject] = self.arrFeedbackForm[section] as! [String:AnyObject]
        
        let strQuestion: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "question")
        
        let label: UILabel = UILabel(frame: CGRect(x: 8, y: 0, width: tableView.size.width, height: 30))
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 14.0)
        
        label.text="Q\(section+1). \(strQuestion)"
        headerView.clipsToBounds = true
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewFeedBack {
            
            let dict:[String:AnyObject] = self.arrFeedbackForm[indexPath.section] as! [String:AnyObject]
            
            let strAnsType: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "ans_type")
            
            if strAnsType == "radio" {
                
                let feedBackRadioCell: FeedBackRadioCell = tableView.dequeueReusableCell(withIdentifier: "FeedBackRadioCell") as! FeedBackRadioCell
                
                if (dict["ans_values"] as? [String:AnyObject]) != nil  {
                    let dictAnsValue:[String: AnyObject] = dict["ans_values"] as! [String: AnyObject]
                    
                    if indexPath.row != 0 {
                        feedBackRadioCell.lblQuestion.isHidden = true
                    }
                    
                    let strOption = "option_\(indexPath.row+1)"
                    let strOptionAns = dictAnsValue[strOption] as! String
                    feedBackRadioCell.lblAnsText.text = strOptionAns
                }
                return feedBackRadioCell
                
            } else {
                
                let feedBackTextCell: FeedBackTextCell = tableView.dequeueReusableCell(withIdentifier: "FeedBackTextCell") as! FeedBackTextCell
                
                return feedBackTextCell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict123:[String:AnyObject] = self.arrFeedbackForm[indexPath.section] as! [String:AnyObject]
        
        let strAnsType: String = PIService.object_forKeyWithValidationForClass_String(dict: dict123, key: "ans_type")
        
        if strAnsType == "radio" {
            
            let dict: [String:AnyObject] = dict123["ans_values"] as! [String:AnyObject]
            
            for i in 0...dict.count - 1 {
                let indexChange: IndexPath = IndexPath(row: i, section: indexPath.section)
                
                let cell:FeedBackRadioCell = tableViewFeedBack.cellForRow(at: indexChange) as! FeedBackRadioCell
                
                if i != indexPath.row {
                    cell.imgRadio.image = #imageLiteral(resourceName: "radioLight")
                } else {
                    cell.imgRadio.image = #imageLiteral(resourceName: "radio")
                }
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let dict:[String:AnyObject] = self.arrFeedbackForm[indexPath.section] as! [String:AnyObject]
        
        let strQuestion: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "question")
        
        var dictQuestion:[String: String] = [:]
        dictQuestion["question"] = strQuestion
        
        let strAnsType: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "ans_type")
        
        if strAnsType == "radio" {
            
            let dictRadio: [String:AnyObject] = dict["ans_values"] as! [String:AnyObject]
            
            for j in 0...dictRadio.count - 1 {
                
                let indexChange: IndexPath = IndexPath(row: j, section: indexPath.section)
                
                if tableViewFeedBack.cellForRow(at: indexChange) != nil {
                    let cell:FeedBackRadioCell = tableViewFeedBack.cellForRow(at: indexChange) as! FeedBackRadioCell
                    
                    if cell.imgRadio.image == #imageLiteral(resourceName: "radio") {
                        let strKey: String = "option_\(j+1)"
                        let strAnswer: String = dictRadio[strKey] as! String
                        dictQuestion["answer"] = strAnswer
                    }
                }
            }
            
        } else {
            
            if tableViewFeedBack.cellForRow(at: indexPath) != nil {
                let cell: FeedBackTextCell = tableViewFeedBack.cellForRow(at: indexPath) as! FeedBackTextCell
                let strAnswer: String = cell.txtans.text!
                dictQuestion["answer"] = strAnswer
            }
        }
        
        self.arrQuestions[indexPath.section] = dictQuestion as AnyObject
        
    }
    
    // MARK: - Action Method
    @IBAction func btnSubmit(_ sender: AnyObject) {
        self.getAnswer()
        if checkValidation() {
            self.ServiceCallForSubmitFeedbackForm()
        }
    }
    
    //MARK: buttonBack_Clicked
    @IBAction func buttonBack_Clicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: ServiceCallForGetFeedbackForm
    func ServiceCallForGetFeedbackForm() {
        
        let strurl:String = BazarBit.GetFeedbackFormService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        print(headers)
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            print(responseDict)
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (response["payload"] != nil)
                {
                    if (response["payload"] as? [AnyObject]) != nil  {
                        
                        self.arrFeedbackForm  = response["payload"] as! [AnyObject]
                        var totalHeight: Int = 0
                        
                        self.arrQuestions = Array(repeatElement([], count: self.arrFeedbackForm.count)) as [AnyObject]
                        
                        for i in 0...self.arrFeedbackForm.count - 1 {
                            
                            let dict:[String:AnyObject] = self.arrFeedbackForm[i] as! [String:AnyObject]
                            
                            if (dict["ans_values"] as? [String:AnyObject]) != nil  {
                                let dictAnsValue:[String: AnyObject] = dict["ans_values"] as! [String: AnyObject]
                                totalHeight = totalHeight + (30 * dictAnsValue.count)
                            } else {
                                totalHeight = totalHeight + 44
                            }
                        }
                        
                        totalHeight = totalHeight + (30 * self.arrFeedbackForm.count) + 44
                        
                        self.tableViewFeedBack.dataSource = self
                        self.tableViewFeedBack.delegate = self
                        self.tableViewFeedBack.reloadData()
                    }
                    else {
                        self.arrFeedbackForm = []
                        self.tableViewFeedBack.dataSource = self
                        self.tableViewFeedBack.delegate = self
                        self.tableViewFeedBack.reloadData()
                    }
                }
                else
                {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            }
        })
    }
    
    //MARK: CHECK VALIDATION
    func checkValidation() -> Bool {
        
        var isValid : Bool = true
        
        for i in 0...arrQuestions.count - 1 {
            let dict : [String : AnyObject] = arrQuestions[i] as! [String : AnyObject]
            
            var strAnswer : String = ""
            if let answer = dict["answer"] {
                strAnswer = answer as! String
                if strAnswer == "" {
                    PIAlertUtils.displayAlertWithMessage("please fill up all answer !")
                    isValid = false
                    return isValid
                }
            }else {
                PIAlertUtils.displayAlertWithMessage("please fill up all answer !")
                isValid = false
                return isValid
            }
        }
        return isValid
    }
    
    //MARK: GET ANSWER
    func getAnswer() {
        
        for i in 0...arrFeedbackForm.count - 1 {
            let dict:[String:AnyObject] = self.arrFeedbackForm[i] as! [String:AnyObject]
            let strQuestion: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "question")
            
            var dictQuestion:[String: String] = [:]
            dictQuestion["question"] = strQuestion
            
            let strAnsType: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "ans_type")
            
            if strAnsType == "radio" {
                
                let dictRadio: [String:AnyObject] = dict["ans_values"] as! [String:AnyObject]
                
                for j in 0...dictRadio.count - 1 {
                    let indexChange: IndexPath = IndexPath(row: j, section: i)
                    
                    if tableViewFeedBack.cellForRow(at: indexChange) != nil {
                        let cell:FeedBackRadioCell = tableViewFeedBack.cellForRow(at: indexChange) as! FeedBackRadioCell
                        
                        if cell.imgRadio.image == #imageLiteral(resourceName: "radio") {
                            let strKey: String = "option_\(j+1)"
                            let strAnswer: String = dictRadio[strKey] as! String
                            dictQuestion["answer"] = strAnswer
                            self.arrQuestions[i] = dictQuestion as AnyObject
                        }
                    }
                }
                
            } else {
                let indexPath: IndexPath = IndexPath(row: 0, section: i)
                
                if tableViewFeedBack.cellForRow(at: indexPath) != nil {
                    let cell: FeedBackTextCell = tableViewFeedBack.cellForRow(at: indexPath) as! FeedBackTextCell
                    let strAnswer: String = cell.txtans.text!
                    dictQuestion["answer"] = strAnswer
                    self.arrQuestions[i] = dictQuestion as AnyObject
                }
            }
            
        }
    }
    
    //MARK: ServiceCallForSubmitFeedbackForm
    func ServiceCallForSubmitFeedbackForm() {
        
        let strurl:String = BazarBit.SubmitFeedbackFormService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        print(headers)
        
        var parameter: Parameters = [:]
        parameter["z_productid_fk"] = strProductId
        parameter["z_orderitemid_fk"] = strOrderItemId
        parameter["questions"] = arrQuestions
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            print(responseDict)
            let response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                PIAlertUtils.displayAlertWithMessage(message)
                APPDELEGATE.openHomePage()
            }
        })
    }
}

class FeedBackRadioCell:  UITableViewCell {
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblAnsText: UILabel!
    @IBOutlet var imgRadio: UIImageView!
    
    override func awakeFromNib() {
        lblQuestion.setTextLabelLightColor()
        lblAnsText.setTextLabelDynamicTextColor()
    }
}

class FeedBackTextCell:  UITableViewCell, UITextFieldDelegate {
    @IBOutlet var txtans:UITextField!
    @IBOutlet var lblAns: UILabel!
    
    override func awakeFromNib() {
        txtans.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
        lblAns.setTextLabelLightColor()
        txtans.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
