//
//  CommissionList_Controller.swift
//  BazarBit
//
//  Created by Binoy on 28/04/19.
//  Copyright © 2019 Parghi Infotech. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON
import EZSwiftExtensions


class CommissionList_Controller: UIViewController {

    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: Any)  {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
