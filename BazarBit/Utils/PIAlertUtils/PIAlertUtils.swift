//
//  PIAlertUtils.swift
//  TabInsta
//
//  Created by Sangani ajay on 8/16/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Toaster

class PIAlertUtils: NSObject {
    
    // MARK: - ALERT
    class func displayNoInternetMessage() {
        
       // ToastCenter.default.cancelAll()
        //Toast(text: "Internet connectivity not available. Please check your internet connection and try again.").show()
        
        displayAlertWithMessage("Internet connectivity not available. Please check your internet connection and try again.", cancelButtonTitle: "Dismiss", buttons:[], completion: nil)
    }
    
    class func displayUnderDevelopmentAlert() {
        displayAlertWithMessage("Feature unavailable in this build.\n Please contact the development team and / or review the release notes. \n\n Thank you!", cancelButtonTitle: "Dismiss", buttons:[], completion: nil)
    }
    
    class func displayAlertWithMessage(_ message:String) -> Void {
       
       // ToastCenter.default.cancelAll()
       // Toast(text: message).show()
        
        displayAlertWithMessage(message,cancelButtonTitle:  "Dismiss", buttons: [], completion: nil)
    }

    class func displayAlertWithMessage(_ message:String, cancelButtonTitle : String) -> Void {
        displayAlertWithMessage(message, cancelButtonTitle: cancelButtonTitle, buttons: [], completion: nil)
    }
    
    
    class func displayAlertWithMessage(_ message:String, cancelButtonTitle : String, buttons:[String], completion:((_ index:Int) -> Void)!) -> Void {
        
        let alertController = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        
        for index in 0..<buttons.count	{            
            
            let action = UIAlertAction(title: buttons[index], style: .default, handler: {
                (alert: UIAlertAction!) in
                if(completion != nil){
                    completion(index)
                }
            })
            
            //            if #available(iOS 9, *) {
            action.setValue(COLOR_THEME_BROWN, forKey: "titleTextColor")
            //            }
            alertController.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil)
        
        cancelAction.setValue(UIColor.darkGray, forKey: "titleTextColor")
        
        alertController.addAction(cancelAction)
        
        alertController.setValue(NSAttributedString(string: APP_NAME, attributes: [NSFontAttributeName : UIFont.appFont_Light_WithSize(13), NSForegroundColorAttributeName : COLOR_THEME_BROWN]), forKey: "attributedTitle")
        
        alertController.setValue(NSAttributedString(string: message, attributes: [NSFontAttributeName : UIFont.appFont_Light_WithSize(12), NSForegroundColorAttributeName : UIColor.darkGray]), forKey: "attributedMessage")
        
        APPDELEGATE.window?.rootViewController!.present(alertController, animated: true, completion:nil)
    }
    
    class func displayAlertWithTitleWithoutCancelButton(andMessage message:String, buttons:[String], completion:((_ index:Int) -> Void)!) -> Void {
        
        let alertController = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        alertController.setValue(NSAttributedString(string: APP_NAME, attributes: [NSFontAttributeName : UIFont.appFont_Light_WithSize(13), NSForegroundColorAttributeName : BLACK_COLOR]), forKey: "attributedTitle")
        
        alertController.setValue(NSAttributedString(string: message, attributes: [NSFontAttributeName : UIFont.appFont_Light_WithSize(12), NSForegroundColorAttributeName : COLOR_TEXT_FIELD_SEPARATOR]), forKey: "attributedMessage")
        
        for index in 0..<buttons.count	{
            let action = UIAlertAction(title: buttons[index], style: .default, handler: {
                (alert: UIAlertAction!) in
                if(completion != nil){
                    completion(index)
                }
            })
            
            //            if #available(iOS 9, *) {
            action.setValue(UIColor.darkGray, forKey: "titleTextColor")
            //            }
            alertController.addAction(action)
        }
        
        UIApplication.shared.delegate!.window!?.rootViewController!.present(alertController, animated: true, completion:nil)
    }
    
    class func displayAlertWithTitle( andMessage message:String, buttons:[String], completion:((_ index:Int) -> Void)!) -> Void {
        
        let alertController = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        alertController.setValue(NSAttributedString(string: APP_NAME, attributes: [NSFontAttributeName : UIFont.appFont_Light_WithSize(13), NSForegroundColorAttributeName : BLACK_COLOR]), forKey: "attributedTitle")
        
        alertController.setValue(NSAttributedString(string: message, attributes: [NSFontAttributeName : UIFont.appFont_Light_WithSize(12), NSForegroundColorAttributeName : COLOR_TEXT_FIELD_SEPARATOR]), forKey: "attributedMessage")
        
        for index in 0..<buttons.count	{
            let action = UIAlertAction(title: buttons[index], style: .default, handler: {
                (alert: UIAlertAction!) in
                if(completion != nil){
                    completion(index)
                }
            })
            
            //            if #available(iOS 9, *) {
            action.setValue(UIColor.darkGray, forKey: "titleTextColor")
            //            }
            alertController.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        //        if #available(iOS 9, *) {
        cancelAction.setValue(UIColor.darkGray, forKey: "titleTextColor")
        //        }
        
        alertController.addAction(cancelAction)
        
        UIApplication.shared.delegate!.window!?.rootViewController!.present(alertController, animated: true, completion:nil)
    }
    
    class func displayActionSheetWithTitle(_ title:String, andMessage message:String, buttons:[String], completion:((_ index:Int) -> Void)!) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        alertController.setValue(NSAttributedString(string: title, attributes: [NSFontAttributeName : UIFont.appFont_Medium_WithSize(13), NSForegroundColorAttributeName : COLOR_THEME_ORANGE]), forKey: "attributedTitle")
        
        alertController.setValue(NSAttributedString(string: message, attributes: [NSFontAttributeName : UIFont.appFont_Light_WithSize(12), NSForegroundColorAttributeName : COLOR_TEXT_FIELD_SEPARATOR]), forKey: "attributedMessage")
        
        for index in 0..<buttons.count	{
            
            
            
            let action = UIAlertAction(title: buttons[index], style: .default, handler: {
                (alert: UIAlertAction!) in
                if(completion != nil){
                    completion(index)
                }
            })
            
            action.setValue(UIColor.darkGray, forKey: "titleTextColor")
            alertController.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        cancelAction.setValue(UIColor.darkGray, forKey: "titleTextColor")
        alertController.addAction(cancelAction)
        
        UIApplication.shared.delegate!.window!?.rootViewController!.present(alertController, animated: true, completion:nil)
    }
}
