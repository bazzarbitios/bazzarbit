//
//  Constants.swift
//  Swift3Demo
//
//  Created by Agile Mac Mini 4 on 01/12/16.
//  Copyright © 2016 Agile Mac Mini 4. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class BazarBitLoader: NSObject {
    
    class public func startLoader() {
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    class public func stopLoader() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}

class Constants: NSObject {
    
    //DYNAMIC THEME COLOR
    static var THEME_PRIMARY_COLOR : String = ""
    static var THEME_PRIMARY_DARK_COLOR : String = ""
    static var TEXT_PRIMARY_COLOR : String = ""
    static var TEXT_LIGHT_COLOR : String = ""
    static var BUTTON_TEXT_COLOR : String = ""

    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6_7          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P_7P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    }
    
    class func getStoryBoard() -> UIStoryboard {
        let storyBoard: UIStoryboard!
        
        if DeviceType.IS_IPAD {
            storyBoard = UIStoryboard(name: "Ipad", bundle: nil)
        } else {
            storyBoard = UIStoryboard(name: "Main", bundle: nil)
        }
        
        return storyBoard
    }
    
    class func HeightforLabel(text: String, font: UIFont, maxSize: CGSize) -> CGFloat {
        let attrString = NSAttributedString.init(string: text, attributes: [NSFontAttributeName:font])
        let rect = attrString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        let size = CGSize(width: rect.width, height: rect.height)
        return size.height
    }
}


// First Portion

//MARK: - COLORS
let CLEAR_COLOR = UIColor.clear
let WHITE_COLOR = UIColor.white
let BLACK_COLOR = UIColor.black
let RED_COLOR = UIColor.red
let GRAY_COLOR = UIColor.darkGray
let GREEN_COLOR = UIColor.green
let SELECTED_COLOR = UIColor.cyan

// MARK: - PRIMARY COLORS
let APP_BLUE_COLOR = UIColor(red: 0, green: 49, blue: 112, alpha: 1)
let APP_ORANGE_COLOR = UIColor(red: 249, green: 160, blue: 48, alpha: 1)
let APP_YELLOW_COLOR = UIColor(red: 255, green: 211, blue: 86, alpha: 1)

//MARK: - SCREEN SIZE
let NAVIGATION_BAR_HEIGHT: CGFloat = 64
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_WIDTH = UIScreen.main.bounds.size.width

func GET_PROPORTIONAL_WIDTH (_ width:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * width)/750)
}
func GET_PROPORTIONAL_HEIGHT (_ height:CGFloat) -> CGFloat {
    return ((SCREEN_HEIGHT * height)/1334)
}

func GET_PROPORTIONAL_WIDTH_CELL (_ width:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * width)/750)
}

func GET_PROPORTIONAL_HEIGHT_CELL (_ height:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * height)/750)
}

// ALERT Box 
//MARK: - APP SPECIFIC
let APP_NAME = "BazarBit"
// MARK: COLOR CONSTANTS
// MARK:
let COLOR_TEXT_FIELD_SEPARATOR = UIColor.lightGray
let COLOR_THEME_GREEN = UIColor(red: 19, green: 121, blue: 52)
let COLOR_THEME_ORANGE = UIColor(red: 243, green: 111, blue: 36)
let COLOR_THEME_LIGHT_GRAY = UIColor(red: 250, green: 250, blue: 250)
let COLOR_THEME_BROWN = UIColor(red: 65, green: 41, blue: 27)
let COLOR_THEME_LIGHT_BROWN = UIColor(red: 193, green: 166, blue: 150)

// Set Price From Login Page To AnyPage Using
class BazarBitCurrency: NSObject {
    
    class func setPriceSign(strPrice : String) -> String {
        let dictCurrency : [String : Any] = UserDefaultFunction.getDictionary(forKey: "currencydata")! as [String : Any]
        
        var strLocalPrice : String = ""
        var sign : String = dictCurrency["sign"] as! String
        let sign_position : String = dictCurrency["sign_position"] as! String
        
        if sign_position == "1" {
            strLocalPrice = sign + "." + " " + strPrice
        }
        else if sign_position == "2"{
            strLocalPrice = strPrice + " " + sign
        }
        else
        {
            strLocalPrice = sign + "." + " " + strPrice
        }
  
        return strLocalPrice
    }
}

class BazarBitDate: NSObject {
    class func Convert_DateString_OneToAnotherFormat(strdate : String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateformatter.date(from: strdate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        let strCurrentDate: String = formatter.string(from: now!)
        return strCurrentDate
    }
}



// Extention //

extension UIButton {
    
    func Shadow() {
        
        self.layer.shadowRadius = 1.0
        self.layer.shadowColor = UIColor(red: CGFloat(64.0/255.0), green: CGFloat(41.0/255.0), blue: CGFloat(26.0/255.0), alpha: CGFloat(1.0)).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 0.20
        self.layer.masksToBounds = false
        self.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        if IS_IPAD_DEVICE() {
            self.layer.cornerRadius = 25
        }
        else
        {
            self.layer.cornerRadius = 15
        }
        //self.layer.borderWidth = 1.0
        
    }
    
    func RoundButton() {
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
//        self.layer.borderColor = UIColor(red:0.0/255.0, green:122.0/255.0, blue:255.0/255.0, alpha:1).cgColor
//        self.layer.borderWidth = 2.0
        self.clipsToBounds = true
        
    }
    
    //AJAY
    func setButtonDynamicColor() {
        self.setTitleColor(UIColor(hexString: Constants.BUTTON_TEXT_COLOR), for: .normal)
    }
    
    func setButtonTextThemeColor() {
        self.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
    }
    
    func setButtonTextLightColor() {
        self.setTitleColor(UIColor(hexString: Constants.TEXT_LIGHT_COLOR), for: .normal)
    }
    
    //------------------
    
    func DynamicBtnTitleColor()  {
        self.setTitleColor(UIColor(hexString: Constants.BUTTON_TEXT_COLOR), for: .normal)
    }
    
    func DynamicBtnTextColor() {
        self.setTitleColor(UIColor(hexString: APPDELEGATE.textColor1), for: .normal)
    }
    
    func DynamicBtnPlaceholderColor() {
        self.setTitleColor(UIColor(hexString: APPDELEGATE.placeholderColor), for: .normal)
    }
    
    func DynamicPlaceholdertextColor() {
        self.setTitleColor(UIColor(hexString: APPDELEGATE.placeholderColor), for: .normal)
    }
}

extension UIImageView {
    
    func RoundImage() {        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
        self.layer.cornerRadius = 0.5 * self.bounds.size.height
        self.clipsToBounds = true
    }
    
}

extension UIView {
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func DynamicViewBorder()  {
        //let color1 = hexStringToUIColor(hex: Constants.TEXT_LIGHT_COLOR)
        self.layer.cornerRadius = 20.0
        self.layer.borderWidth = 1.0
        //self.layer.borderColor = color1.cgColor
    }
}

extension UILabel {
    
    //AJAY
    func setTextLabelThemeColor() {
        self.textColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    }
    
    func setTextLabelDynamicTextColor() {
        self.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
    }
    
    func setTextLabelLightColor() {
        self.textColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
    }
    
    func setTextLabelButtonTextColor() {
        self.textColor = UIColor(hexString: Constants.BUTTON_TEXT_COLOR)
    }
    
    //---------
    
    func DynamicLblTextColor() {
        self.textColor = UIColor(hexString: APPDELEGATE.textColor1)
    }
    
    func DynamicLblTextColor2() {
        self.textColor = UIColor(hexString: APPDELEGATE.placeholderColor)
    }
    
    func DynamicPlaceholdertextColor() {
        self.textColor = UIColor(hexString: APPDELEGATE.placeholderColor)
    }
    
    func DynamicLblBackGrounColorSelected() {
        self.backgroundColor = UIColor(hexString: APPDELEGATE.placeholderColor)
    }
    
    func DynamicLblBackGrounColorDeselect() {
        self.backgroundColor = UIColor(hexString: APPDELEGATE.textColor1)
    }

}


extension UITextField {
    
    func addLeftImage(imageName: String) {
        
        let textLeftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 40 , height:30))
        // textLeftView.backgroundColor = UIColor.red
        self.leftViewMode = UITextFieldViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 10, y: 5, width: 20 , height: 20))
        let image = UIImage(named: imageName)
        imageView.image = image
        textLeftView.addSubview(imageView)
        self.leftView = textLeftView
    }
    
    func addRightImage(imageName: String) {
        
        let textRightview: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 40 , height:30))
        self.rightViewMode = UITextFieldViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 10, y: 3, width: 22 , height: 22))
        let image = UIImage(named: imageName)
        imageView.image = image
        textRightview.addSubview(imageView)
        self.rightView = textRightview
    }
    
    //AJAY
    
    func setTextFieldDynamicColorForFilter() {
        let color1 = hexStringToUIColor(hex: Constants.THEME_PRIMARY_COLOR)
        self.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
        if IS_IPAD_DEVICE() {
            self.layer.cornerRadius = 25.0
        }
        else
        {
            self.layer.cornerRadius = 15.0
        }
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color1.cgColor
    }
    
    func setTextFieldDynamicColor() {
        let color1 = hexStringToUIColor(hex: Constants.THEME_PRIMARY_COLOR)
        self.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
        self.layer.cornerRadius = 20.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color1.cgColor
    }
    
    func DynamictextBorder()  {
        let color1 = hexStringToUIColor(hex: APPDELEGATE.borderColor1)
        self.layer.cornerRadius = 20.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color1.cgColor
    }
    
    func DynamicPlaceholdertextColor() {
        let color2 = hexStringToUIColor(hex: APPDELEGATE.placeholderColor)
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName : color2])
    }

    override func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func DynamicTxtTextColor() {
        self.textColor = UIColor(hexString: APPDELEGATE.textColor1)
    }
}
