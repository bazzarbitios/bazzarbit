//
//  AIServiceConstants.swift
//  Swift3Demo
//
//  Created by Agile Mac Mini 4 on 02/01/17.
//  Copyright © 2017 Agile Mac Mini 4. All rights reserved.
//

import Foundation
import UIKit
import EZSwiftExtensions
import Alamofire
import SwiftyJSON
import SVProgressHUD
import NVActivityIndicatorView

struct BazarBit {
    
    // MARK: - Header Value
    
    static let CLIENT_ID = "81va8uw7fll2ey"
    static let CLIENT_SECRET = "x9QzbF8Zkidi5pLD"
    
    static let ContentType  = "Content-Type"
    
    static let authToken  = "auth-token"
    static let applicationJson  = "application/json"
    
    //Local
    static let appId  = "d6b2b4c0c749b307b658765d2bb89fc4"
    static let appSecret  = "d6b2b4c0c749b307b658765d2bb89fc3"
    
    //Amazing Decor
    //    static let appId  = "6159cd3d72b73199ef63bbd51aac3e0a"
    //    static let appSecret  = "bad8dd8af65cb7ce803a1f948d567bc6"
    
    // MARK: - Get Common Value
    
    static let appType  = "ios"
    static let appVersion  = ez.appVersion!
    static let deviceName  = UIDevice.deviceModelReadable()
    static let systemVersion  = UIDevice.systemVersion()
    
    // MARK: - userDefault Name
    
    static let PROMOCODE = "PROMOCODE"
    
    static let USERACCOUNT = "userAccount"
    static let USERID = "USERID"
    static let DEVICEID = "notification_key"
    static let AUTOPINCODE = "AUTOPINCODE"
    static let AUTHTOKEN  = "AUTHTOKEN"
    static let currency = "CURRENCY"
    static let USEREMAILID = "USEREMAILID"
    static let SESSIONID = "SESSIONID"
    static let ISLIVE = "ISLIVE"
    static let USERNAME = "USERNAME"
    static let USEREMAILADDRESS = "USEREMAILADDRESS"
    static let USERPROFILEIMAGE = "USERPROFILEIMAGE"
    static let CARTTOTALCOUNT = "CARTTOTALCOUNT"
    
    static let SHIPPINGADDRESS = "SHIPPINGADDRESS"
    static let BILLINGADDRESS = "BILLINGADDRESS"
    
    // MARK: - Main Path
    //Local
    //static let MainPath  = "http://192.168.0.110/bazzarbit_api/"
    static let MainPath  = "http://103.53.165.172/bazzarbit_api/"
    //static let MainPath = "http://bazarbit.com/bazzarbit/"
    
    // MARK: - Service Name
    
    static let appSetting  = "app/frontend/getappsetting?"
    static let applicationSetting  = "app/frontend/getapplicationsettings?"
    static let login = "app/frontend/login?"
    static let SocialDetail = "app/frontend/checksocialdetails"
    static let forgotPassword = "app/frontend/forgotpassword?"
    
    static let instagramPath  = "https://api.instagram.com/v1/users/self?access_token="
    static let countrycodes = "countrycodes?"
    static let Register = "app/frontend/register?"
    static let Logout = "app/frontend/logout?"
    
    static let HomeSlider = "app/frontend/getslider?"
    static let BestSellerProduct = "app/frontend/bestSeller?"
    static let MostWantedProduct = "app/frontend/mostwantedproducts?"
    static let NewArrivalProduct = "app/frontend/newarrivals?"
    static let HomeBanner = "app/frontend/getbanner?"
    static let GetSearchProductList = "app/frontend/searchproductlist?"
    
    static let GetWishList = "app/frontend/user/getwishlistitems?"
    static let RemoveWishListItem = "app/frontend/user/removeWishlistItem?"
    static let CheckItemInNotify = "app/frontend/user/checkiteminnotifyme?"
    static let AddItemInNotify = "app/frontend/user/notifyme?"
    static let CheckItemExistInCart = "app/frontend/checkitemincart?"
    static let AddToCartCart = "app/frontend/addtocart?"
    
    static let GetCountryList = "countries?"
    static let GetStateList = "states?"
    static let GetUserData = "app/frontend/user/myaccount/getbyid?"
    static let UpdateUserData = "app/frontend/user/myaccount/update?"
    
    static let AddItemToWishList = "app/frontend/user/addtowishlist?"
    static let GetHomeCategoryList = "app/frontend/categories?"
    
    static let GetproductdetailBySlug = "app/frontend/getproductdetailbyslug?"
    static let GetRelatedProductData = "app/frontend/relatedProducts?"
    static let GetUserReviewsData = "app/frontend/reviewdetail/gets?"
    static let CheckPincode = "app/frontend/checkpincodeexists?"
    static let DisableVariationOption = "app/frontend/disabledvariationoption?"
    
    static let changePassword = "app/frontend/user/myaccount/changepassword?"
    static let addressList = "app/frontend/user/myaccount/useraddresslist?"
    static let deleteAddress = "app/frontend/user/myaccount/deleteuseraddress?"
    static let editAddress = "app/frontend/user/myaccount/updateuseraddress?"
    static let addAddress = "app/frontend/user/myaccount/adduseraddress?"
    static let listReview = "app/frontend/user/myaccount/listreview?"
    static let orderList = "app/frontend/user/getorders?"
    static let getsingleorder = "app/frontend/user/getsingleorder?"
    static let getcartitems = "app/frontend/getcartitems?"
    
    static let updatecartitemqty = "app/frontend/updatecartitemqty?"
    static let removecartitem = "app/frontend/removecartitem?"
    static let getlastorderaddress = "app/frontend/user/getlastorderaddress?"
    static let getshippingcharges = "app/frontend/getshippingcharges?"
    static let getcarttotal = "app/frontend/getcarttotal?"
    
    static let getAddReview = "app/frontend/reviewdetail/create?"
    static let updateItemStatus = "app/frontend/user/updateitemstatus?"
    static let returnOrder = "app/frontend/user/returnorder?"
    static let returnNetOrder = "app/frontend/user/returnnetorder?"
    static let returnOrder_PayuMoney = "app/frontend/user/returnorder?"
    
    static let GetMegaMenu = "app/menu/getRows?"
    static let GetCategoryWiseProduct = "app/frontend/getcatwiseproducts?"
    static let GetAttributesSorting = "app/frontend/getattributessorting?"
    
    static let GetCMSPage = "app/frontend/cmslist?"
    static let LoadStaticPage = "app/frontend/cmsstaticpage?"
    static let SaveConstactForm = "app/frontend/createcontactform?"
    
    static let GetDeliveredOrder = "app/frontend/user/getdeliveredorders?"
    
    static let GetFeedbackForm = "app/frontend/feedbackform?"
    static let SubmitFeedbackForm = "app/frontend/user/submitfeedback?"
    static let countCartTotal = "app/frontend/cartcount?"
    static let packageService = "app/frontend/getfeaturessettings?"
    
    static let validatepromocode = "app/frontend/validatepromocode?"
    static let removepromocode = "app/frontend/removepromocode?"
    static let placeorder = "app/frontend/user/transaction/placeorder?"
    static let getpaymentgateway = "app/frontend/getpaymentgateway?"
    
    static let getProductVariation = "app/frontend/productvariation?"
    static let getDefaultVariation = "app/frontend/getdefaultvariationbyproductid?"
    
    static let signUpVerifyOtp = "app/frontend/verifyotp?"
    static let signUpResendOtp = "app/frontend/resendotp?"
    
    static let placeOrderVerifyOtp = "app/frontend/user/transaction/verifyotp?"
    static let placeOrderSendOtp = "app/frontend/user/transaction/sendotp?"
    
    // MARK: - Service function (with Parameter or not)
    
    static func getmessageFromService(dict:[String:AnyObject])-> String {
        var strmsg:String = ""
        if (dict["message"] as? String) != nil
        {
            strmsg = dict["message"] as! String
        }
        else if (dict["message"] as? [AnyObject]) != nil
        {
            let arrmessage = dict["message"] as! [AnyObject]
            strmsg = arrmessage[0] as! String
        }
        else
        {
            strmsg = ""
        }
        return strmsg
    }
    
    static func GetImageUrl(strurl: String) -> String {
        
        var strUrl = strurl
        
        strUrl = strUrl.replacingOccurrences(of: "[", with: "")
        strUrl = strUrl.replacingOccurrences(of: "\"", with: "")
        strUrl = strUrl.replacingOccurrences(of: "]", with: "")
        strUrl = strUrl.replacingOccurrences(of: " ", with: "%20")
        
        return strUrl
    }
    
    static func getValue(dict: [String: AnyObject], key: String) -> Int {
        
        var strValue: Int = 0
        if (dict[key] as? Int) != nil  {
            strValue = dict[key] as! Int
        } else {
            strValue = 0
        }
        return strValue
    }
    
    static func AppSettingService()-> String {
        
        var strAppSetting:String = "\(MainPath)\(appSetting)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strAppSetting = strAppSetting.replacingOccurrences(of: " ", with: "%20")
        return strAppSetting
    }
    
    static func ApplicationSettingService()-> String {
        
        var strAppSetting:String = "\(MainPath)\(applicationSetting)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strAppSetting = strAppSetting.replacingOccurrences(of: " ", with: "%20")
        return strAppSetting
    }
    
    static func loginService()-> String {
        
        var strLogin:String = "\(MainPath)\(login)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strLogin = strLogin.replacingOccurrences(of: " ", with: "%20")
        return strLogin
    }
    
    static func SocialMediaService()-> String {
        
        var strSocial:String = "\(MainPath)\(SocialDetail)"
        strSocial = strSocial.replacingOccurrences(of: " ", with: "%20")
        return strSocial
    }
    
    static func getInstagramUserService(strAccessToken: String) -> String {
        
        var strInstagramDetail = "\(instagramPath)\(strAccessToken)"
        strInstagramDetail = strInstagramDetail.replacingOccurrences(of: " ", with: "%20")
        
        return strInstagramDetail
    }
    
    static func forgotPasswordService()-> String {
        
        var strForgotPwd:String = "\(MainPath)\(forgotPassword)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strForgotPwd = strForgotPwd.replacingOccurrences(of: " ", with: "%20")
        return strForgotPwd
    }
    
    static func countryCodeService()-> String {
        
        var strCode:String = "\(MainPath)\(countrycodes)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strCode = strCode.replacingOccurrences(of: " ", with: "%20")
        return strCode
    }
    
    static func SignUpService()-> String {
        
        var strSignUp:String = "\(MainPath)\(Register)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strSignUp = strSignUp.replacingOccurrences(of: " ", with: "%20")
        return strSignUp
    }
    
    static func logoutService()-> String {
        
        var strLogout:String = "\(MainPath)\(Logout)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strLogout = strLogout.replacingOccurrences(of: " ", with: "%20")
        return strLogout
    }
    
    static func HomeSliderService()-> String {
        
        var strSlider:String = "\(MainPath)\(HomeSlider)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strSlider = strSlider.replacingOccurrences(of: " ", with: "%20")
        return strSlider
    }
    
    static func HomeBestSellerProsuctService()-> String {
        
        var strBestSeller:String = "\(MainPath)\(BestSellerProduct)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strBestSeller = strBestSeller.replacingOccurrences(of: " ", with: "%20")
        return strBestSeller
    }
    
    static func HomeMostWantedProductService()-> String {
        
        var strMostWanted:String = "\(MainPath)\(MostWantedProduct)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strMostWanted = strMostWanted.replacingOccurrences(of: " ", with: "%20")
        return strMostWanted
    }
    
    static func HomeNewArrivalProductService()-> String {
        
        var strNewArrival:String = "\(MainPath)\(NewArrivalProduct)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strNewArrival = strNewArrival.replacingOccurrences(of: " ", with: "%20")
        return strNewArrival
    }
    
    static func HomeBannerService()-> String {
        
        var strBanner:String = "\(MainPath)\(HomeBanner)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strBanner = strBanner.replacingOccurrences(of: " ", with: "%20")
        return strBanner
    }
    
    static func AddItemToWishListService()-> String {
        
        var strAddToWishList:String = "\(MainPath)\(AddItemToWishList)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strAddToWishList = strAddToWishList.replacingOccurrences(of: " ", with: "%20")
        return strAddToWishList
    }
    
    static func GetCategoryListService()-> String {
        
        var strCategoryList:String = "\(MainPath)\(GetHomeCategoryList)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strCategoryList = strCategoryList.replacingOccurrences(of: " ", with: "%20")
        return strCategoryList
    }
    
    static func GetSearchProductListService()-> String {
        
        var strCategoryList:String = "\(MainPath)\(GetSearchProductList)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strCategoryList = strCategoryList.replacingOccurrences(of: " ", with: "%20")
        return strCategoryList
    }
    
    static func GetWishListItemService()-> String {
        
        var strWishList:String = "\(MainPath)\(GetWishList)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func RemoveWishListItemService()-> String {
        
        var strWishList:String = "\(MainPath)\(RemoveWishListItem)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func CheckItemInNotifyService()-> String {
        
        var strWishList:String = "\(MainPath)\(CheckItemInNotify)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func AddItemInNotifyService()-> String {
        
        var strWishList:String = "\(MainPath)\(AddItemInNotify)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func CheckItemExistInCartService()-> String {
        
        var strWishList:String = "\(MainPath)\(CheckItemExistInCart)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func AddToCartService()-> String {
        
        var strWishList:String = "\(MainPath)\(AddToCartCart)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetCountryListService()-> String {
        
        var strWishList:String = "\(MainPath)\(GetCountryList)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetStateListService()-> String {
        
        var strWishList:String = "\(MainPath)\(GetStateList)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetUserDataService()-> String {
        
        var strWishList:String = "\(MainPath)\(GetUserData)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func UpdateUserDataService()-> String {
        
        var strWishList:String = "\(MainPath)\(UpdateUserData)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetProductDetailBySlugService()-> String {
        
        var strPeroductdetailList:String = "\(MainPath)\(GetproductdetailBySlug)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strPeroductdetailList = strPeroductdetailList.replacingOccurrences(of: " ", with: "%20")
        return strPeroductdetailList
    }
    
    static func GetRelatedProductDataService()-> String {
        
        var strRelatedProductList:String = "\(MainPath)\(GetRelatedProductData)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strRelatedProductList = strRelatedProductList.replacingOccurrences(of: " ", with: "%20")
        return strRelatedProductList
    }
    
    static func GetProductReviewsDataService()-> String {
        
        var strUserReviewList:String = "\(MainPath)\(GetUserReviewsData)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strUserReviewList = strUserReviewList.replacingOccurrences(of: " ", with: "%20")
        return strUserReviewList
    }
    
    static func CheckPincodeExistService()-> String {
        
        var strCheckPincode:String = "\(MainPath)\(CheckPincode)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strCheckPincode = strCheckPincode.replacingOccurrences(of: " ", with: "%20")
        return strCheckPincode
    }
    
    static func DisableVariationOptionService()-> String {
        
        var strCheckPincode:String = "\(MainPath)\(DisableVariationOption)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strCheckPincode = strCheckPincode.replacingOccurrences(of: " ", with: "%20")
        return strCheckPincode
    }
    
    static func changePasswordService()-> String {
        
        var strLogin:String = "\(MainPath)\(changePassword)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strLogin = strLogin.replacingOccurrences(of: " ", with: "%20")
        return strLogin
    }
    
    static func GetaddressListService()-> String {
        
        var straddressList:String = "\(MainPath)\(addressList)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        straddressList = straddressList.replacingOccurrences(of: " ", with: "%20")
        return straddressList
    }
    
    static func GetDeleteAddressService()-> String {
        
        var strDeleteAddress:String = "\(MainPath)\(deleteAddress)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strDeleteAddress = strDeleteAddress.replacingOccurrences(of: " ", with: "%20")
        return strDeleteAddress
    }
    
    static func GetEditAddressService()-> String {
        
        var strEditAddress:String = "\(MainPath)\(editAddress)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strEditAddress = strEditAddress.replacingOccurrences(of: " ", with: "%20")
        return strEditAddress
    }
    
    static func GetAddAddressService()-> String {
        
        var strAddAddress:String = "\(MainPath)\(addAddress)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strAddAddress = strAddAddress.replacingOccurrences(of: " ", with: "%20")
        return strAddAddress
    }
    
    static func GetListReviewService()-> String {
        
        var strlistReview:String = "\(MainPath)\(listReview)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strlistReview = strlistReview.replacingOccurrences(of: " ", with: "%20")
        return strlistReview
    }
    
    static func GetCartitemsService()-> String {
        
        var strGetCartItems:String = "\(MainPath)\(getcartitems)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetCartItems = strGetCartItems.replacingOccurrences(of: " ", with: "%20")
        return strGetCartItems
    }
    
    static func GetOrderListService()-> String {
        
        var strOrderList:String = "\(MainPath)\(orderList)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strOrderList = strOrderList.replacingOccurrences(of: " ", with: "%20")
        return strOrderList
    }
    
    static func GetSingleOrderService()-> String {
        
        var strGetSingleOrder:String = "\(MainPath)\(getsingleorder)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetSingleOrder = strGetSingleOrder.replacingOccurrences(of: " ", with: "%20")
        return strGetSingleOrder
    }
    
    static func GetUpdateCartItemQtyService()-> String {
        
        var strGetCartItems:String = "\(MainPath)\(updatecartitemqty)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetCartItems = strGetCartItems.replacingOccurrences(of: " ", with: "%20")
        return strGetCartItems
    }
    
    static func GetRemoveCartItemService()-> String {
        
        var strRemoveCartItem:String = "\(MainPath)\(removecartitem)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strRemoveCartItem = strRemoveCartItem.replacingOccurrences(of: " ", with: "%20")
        return strRemoveCartItem
    }
    
    static func GetLastOrderService()-> String {
        
        var strGetLastOrderAddress:String = "\(MainPath)\(getlastorderaddress)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetLastOrderAddress = strGetLastOrderAddress.replacingOccurrences(of: " ", with: "%20")
        return strGetLastOrderAddress
    }
    
    static func GetShippingChargesService()-> String {
        
        var strGetShippingCharges:String = "\(MainPath)\(getshippingcharges)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetShippingCharges = strGetShippingCharges.replacingOccurrences(of: " ", with: "%20")
        return strGetShippingCharges
    }
    
    static func GetCartTotalService()-> String {
        
        var strGetShippingCharges:String = "\(MainPath)\(getcarttotal)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetShippingCharges = strGetShippingCharges.replacingOccurrences(of: " ", with: "%20")
        return strGetShippingCharges
    }
    
    static func GetAddReviewService()-> String {
        
        var strGetAddReview:String = "\(MainPath)\(getAddReview)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetAddReview = strGetAddReview.replacingOccurrences(of: " ", with: "%20")
        return strGetAddReview
    }
    
    static func GetUpdateItemStatusService()-> String {
        
        var strUpdateItemStatus:String = "\(MainPath)\(updateItemStatus)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strUpdateItemStatus = strUpdateItemStatus.replacingOccurrences(of: " ", with: "%20")
        return strUpdateItemStatus
    }
    
    static func GetReturnOrderService()-> String {
        
        var strUpdateItemStatus:String = "\(MainPath)\(returnOrder)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strUpdateItemStatus = strUpdateItemStatus.replacingOccurrences(of: " ", with: "%20")
        return strUpdateItemStatus
    }
    
    static func GetReturnNetOrderService()-> String {
        
        var strReturnNetOrder:String = "\(MainPath)\(returnNetOrder)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strReturnNetOrder = strReturnNetOrder.replacingOccurrences(of: " ", with: "%20")
        return strReturnNetOrder
    }
    
    static func GetMegaMenuService()-> String {
        
        var strWishList:String = "\(MainPath)\(GetMegaMenu)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetCategoryWiseProductService()-> String {
        
        var strWishList:String = "\(MainPath)\(GetCategoryWiseProduct)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetAttributesSortingService()-> String {
        
        var strWishList:String = "\(MainPath)\(GetAttributesSorting)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetCMSPageService()-> String {
        
        var strWishList:String = "\(MainPath)\(GetCMSPage)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func LoadStaticPageService()-> String {
        
        var strWishList:String = "\(MainPath)\(LoadStaticPage)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func SaveConstactFormService()-> String {
        var strWishList:String = "\(MainPath)\(SaveConstactForm)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetDeliveredOrderService()-> String {
        var strWishList:String = "\(MainPath)\(GetDeliveredOrder)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func GetFeedbackFormService()-> String {
        var strWishList:String = "\(MainPath)\(GetFeedbackForm)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func SubmitFeedbackFormService()-> String {
        
        var strWishList:String = "\(MainPath)\(SubmitFeedbackForm)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strWishList = strWishList.replacingOccurrences(of: " ", with: "%20")
        return strWishList
    }
    
    static func CountCartTotalService()-> String {
        var strCartCount:String = "\(MainPath)\(countCartTotal)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strCartCount = strCartCount.replacingOccurrences(of: " ", with: "%20")
        return strCartCount
    }
    
    static func PackageService()-> String {
        var strCartCount:String = "\(MainPath)\(packageService)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strCartCount = strCartCount.replacingOccurrences(of: " ", with: "%20")
        return strCartCount
    }
    
    static func GetValidatePromoCodeService()-> String {
        
        var strGetShippingCharges:String = "\(MainPath)\(validatepromocode)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetShippingCharges = strGetShippingCharges.replacingOccurrences(of: " ", with: "%20")
        return strGetShippingCharges
    }
    
    static func GetRemovePromocodeService()-> String {
        
        var strGetShippingCharges:String = "\(MainPath)\(removepromocode)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetShippingCharges = strGetShippingCharges.replacingOccurrences(of: " ", with: "%20")
        return strGetShippingCharges
    }
    
    static func GetPlaceOrderService()-> String {
        
        var strGetPlaceOrder:String = "\(MainPath)\(placeorder)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetPlaceOrder = strGetPlaceOrder.replacingOccurrences(of: " ", with: "%20")
        return strGetPlaceOrder
    }
    
    static func GetPaymentGatewayService()-> String {
        
        var strGetPlaceOrder:String = "\(MainPath)\(getpaymentgateway)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetPlaceOrder = strGetPlaceOrder.replacingOccurrences(of: " ", with: "%20")
        return strGetPlaceOrder
    }
    
    static func GetProductVariationService()-> String {
        
        var strGetvariation:String = "\(MainPath)\(getProductVariation)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetvariation = strGetvariation.replacingOccurrences(of: " ", with: "%20")
        return strGetvariation
    }
    
    static func GetDefaultProductVariationService()-> String {
        
        var strGetDefaultvariation:String = "\(MainPath)\(getDefaultVariation)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetDefaultvariation = strGetDefaultvariation.replacingOccurrences(of: " ", with: "%20")
        return strGetDefaultvariation
    }
    
    static func SignUpVerifyOtp()-> String {
        var strGetvariation:String = "\(MainPath)\(signUpVerifyOtp)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetvariation = strGetvariation.replacingOccurrences(of: " ", with: "%20")
        return strGetvariation
    }
    
    static func SignUpResendOtp()-> String {
        var strGetvariation:String = "\(MainPath)\(signUpResendOtp)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetvariation = strGetvariation.replacingOccurrences(of: " ", with: "%20")
        return strGetvariation
    }
    
    static func PlaceOrderVerifyOtp()-> String {
        var strGetvariation:String = "\(MainPath)\(placeOrderVerifyOtp)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetvariation = strGetvariation.replacingOccurrences(of: " ", with: "%20")
        return strGetvariation
    }
    
    static func PlaceOrderSendOtp()-> String {
        var strGetvariation:String = "\(MainPath)\(placeOrderSendOtp)app_type=\(appType)&app_version=\(appVersion)&device_name=\(deviceName)&system_version=\(systemVersion)"
        strGetvariation = strGetvariation.replacingOccurrences(of: " ", with: "%20")
        return strGetvariation
    }
    
    
    static func getSessionId() -> String {
        
        var strSessionId: String = ""
        
        if UserDefaults.standard.object(forKey: BazarBit.SESSIONID) == nil {
            strSessionId = randomString(length: 32)
            UserDefaults.standard.set(strSessionId, forKey: BazarBit.SESSIONID)
        } else {
            strSessionId = UserDefaults.standard.object(forKey: BazarBit.SESSIONID) as! String
        }
        return strSessionId
    }
  
  static func getIsLive() -> String {
    
    var strIsLive: String = APPDELEGATE.strISLive
    return strIsLive
  }
    
    static func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }        
        return randomString
    }
    
}

class PIService: NSObject {
    
    // Check String is Null Or Not
    class func object_forKeyWithValidationForClass_String(dict: [String: AnyObject], key: String) -> String
    {
        var strValue: String = ""
        if (dict[key] as? String) != nil  {
            strValue = dict[key] as! String
        }
        else {
            strValue = ""
        }
        
        return strValue
    }
    
    static func getValue(dict: [String: AnyObject], key: String) -> Int {
        
        var strValue: Int = 0
        if (dict[key] as? Int) != nil  {
            strValue = dict[key] as! Int
        } else {
            strValue = 0
        }
        return strValue
    }
    
    class public func serviceCall(url: URL, method: HTTPMethod, parameter: Parameters, encoding: JSONEncoding, headers: HTTPHeaders,viewVC:UIViewController, isLoaderShow : Bool, completion: @escaping (_ json: JSON, _ responseDict: [String:AnyObject]) -> Void)    {
        
        print(url)
        print(parameter)
        print(headers)
        
        if IS_INTERNET_AVAILABLE() {
          
          let strISLive: String = BazarBit.getIsLive()
          
          if strISLive ==  "1" {
            
            if isLoaderShow == true {
              BazarBitLoader.startLoader()
            }
            
            Alamofire.request(url, method: method, parameters: parameter, encoding: encoding, headers: headers).responseJSON { (responseData) in
              
              print(url)
              print(parameter)
              print(headers)
              
              switch responseData.result{
              case .success(_):
                
                let itemUrl: String = BazarBit.CheckItemExistInCartService()
                
                if url.absoluteString != itemUrl {
                    BazarBitLoader.stopLoader()
                }
                
                let swiftyJsonData = JSON(responseData.result.value!)
                let responseDict : [String: AnyObject] = responseData.result.value as! [String : AnyObject]
                
                print(responseDict)
                
                if (responseDict["message"] as? String) != nil  {
                  let strmessage: String = responseDict["message"] as! String
                  
                  let status: Int = BazarBit.getValue(dict: responseDict, key: "code")
                  
                  print(status)
                  
                  if status == 401 {
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERACCOUNT)
                    UserDefaults.standard.removeObject(forKey: BazarBit.AUTHTOKEN)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERID)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILID)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERNAME)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILADDRESS)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERPROFILEIMAGE)
                    UserDefaults.standard.removeObject(forKey: "slug")
                    UserDefaults.standard.removeObject(forKey: "title")
                    
                    APPDELEGATE.GlobalVC = viewVC
                    print(APPDELEGATE.GlobalVC)
                    
                    let loginVC = viewVC.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    viewVC.navigationController?.pushViewController(loginVC, animated: true)
                    
                  }
                }
                
                completion(swiftyJsonData , responseDict)
                
              case .failure( _):
                BazarBitLoader.stopLoader()
                break
              }
            }
          } else {
            APPDELEGATE.checkAutoUpdateConfigure()
          //  checkAutoUpdateConfigure()
          }
          
        }
        else
        {
            BazarBitLoader.stopLoader()
            let alertController: UIAlertController = UIAlertController(title: "Alert!", message: "Your Internet connectivity not available. Please check your internet connection and try again!", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            APPDELEGATE.window?.currentViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    
    class public func serviceCallWithoutLoader(url: URL, method: HTTPMethod, parameter: Parameters, encoding: JSONEncoding, headers: HTTPHeaders, completion: @escaping (_ json: JSON, _ responseDict: [String:AnyObject]) -> Void)    {
        
        if IS_INTERNET_AVAILABLE() {
            
            //BazarBitLoader.startLoader()
            
            Alamofire.request(url, method: method, parameters: parameter, encoding: encoding, headers: headers).responseJSON { (responseData) in
                
                print(url)
                print(parameter)
                print(headers)
                
                switch responseData.result{
                case .success(_):
                    BazarBitLoader.stopLoader()
                    
                    let swiftyJsonData = JSON(responseData.result.value!)
                    let responseDict : [String: AnyObject] = responseData.result.value as! [String : AnyObject]
                    
                    print(responseDict)
                    
                    if (responseDict["message"] as? String) != nil  {
                        let strmessage: String = responseDict["message"] as! String
                        
                        let status: Int = BazarBit.getValue(dict: responseDict, key: "code")
                        
                        print(status)
                        
                        if status == 401 {
                            UserDefaults.standard.removeObject(forKey: BazarBit.USERACCOUNT)
                            UserDefaults.standard.removeObject(forKey: BazarBit.AUTHTOKEN)
                            UserDefaults.standard.removeObject(forKey: BazarBit.USERID)
                            UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILID)
                            UserDefaults.standard.removeObject(forKey: BazarBit.USERNAME)
                            UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILADDRESS)
                            UserDefaults.standard.removeObject(forKey: BazarBit.USERPROFILEIMAGE)
                            UserDefaults.standard.removeObject(forKey: "slug")
                            UserDefaults.standard.removeObject(forKey: "title")
                            
                            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                                myDelegate.openLoginPage()
                                return
                            }
                        }
                    }
                    
                    completion(swiftyJsonData , responseDict)
                    
                case .failure( _):
                    //BazarBitLoader.stopLoader()
                    break
                }
            }
        }
        else
        {
            BazarBitLoader.stopLoader()
            let alertController: UIAlertController = UIAlertController(title: "Alert!", message: "Your Internet connectivity not available. Please check your internet connection and try again!", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            APPDELEGATE.window?.currentViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    
}

func IS_INTERNET_AVAILABLE() -> Bool {
    return AIReachabilityManager.sharedManager.isInternetAvailableForAllNetworks()
}
