//
//  AILable.swift
//  Swift3Demo
//
//  Created by Agile Mac Mini 4 on 02/01/17.
//  Copyright © 2017 Agile Mac Mini 4. All rights reserved.
//

import UIKit

class PICustomLable: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor.black
        self.font = UIFont.appFont_Light_WithSize(20)
    }
}

class PICustomTitleLable: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //self.textColor = UIColor.white
        self.textColor = UIColor(hexString: Constants.BUTTON_TEXT_COLOR)
        self.font = UIFont.appFont_Bold_WithSize(16)
        //self.font = UIFont.appFont_Light_WithSize(16)
    }
}

class PICustomDescription: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
        //self.font = UIFont.appFont_Bold_WithSize(14)
        self.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PICustomMenuTitle: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
        //self.font = UIFont.appFont_Bold_WithSize(14)
        self.font = UIFont.appFont_Light_WithSize(16)
    }
}

class PICustomLableName: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
        //self.font = UIFont.appFont_Bold_WithSize(14)
        self.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PIProductTitleLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTextLabelDynamicTextColor()
        //self.font = UIFont.appFont_Bold_WithSize(14)
        self.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PIProductPriceLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTextLabelLightColor()
        //self.font = UIFont.appFont_Bold_WithSize(14)
        self.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PIProductSalePriceLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTextLabelDynamicTextColor()
        //self.font = UIFont.appFont_Bold_WithSize(14)
        self.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PIFilterTitleLightLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTextLabelLightColor()
        //self.font = UIFont.appFont_Bold_WithSize(14)
        self.font = UIFont.appFont_Light_WithSize(16)
    }
}

class PICustomTitleLableBlack : UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        self.font = UIFont.appFont_Light_WithSize(16)
    }
}

class PIFilterType: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTextLabelDynamicTextColor()
        self.font = UIFont.appFont_Light_WithSize(14)
        self.backgroundColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
        self.layer.cornerRadius = 5.0
    }
}

class PILabelDrawerTitle: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTextLabelDynamicTextColor()
        self.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PILabelFeedback: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTextLabelDynamicTextColor()
        self.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PIVarificationMsgLable: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
        self.font = UIFont.appFont_Bold_WithSize(14)
    }
}


