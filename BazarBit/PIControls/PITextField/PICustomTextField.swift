//
//  PICustomTextField.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/16/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import Foundation

class PITextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.font = UIFont.appFont_Light_WithSize(16)
        self.autocorrectionType = UITextAutocorrectionType.no
    }
    
    override func awakeFromNib() {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.lightGray])
        self.tintColor = UIColor.black
        self.setLeftPaddingPoints(10.0)
        self.setTextFieldDynamicColor()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //DISABLE PASTE
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
