 //
 //  HomeMainProductCell.swift
 //  BazarBit
 //
 //  Created by Parghi Infotech on 11/10/17.
 //  Copyright © 2017 Parghi Infotech. All rights reserved.
 //
 
 import UIKit
 import Alamofire
 import Cosmos
 import SwiftyJSON
 
 class HomeMainProductCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var CltnHomeProduct: UICollectionView!
    
    @IBOutlet weak var viewproductHeightConstraints: UIView!
    @IBOutlet weak var lblLableName: UILabel!
    
    var strKey : String = ""
    var homevc:HomeVC!
    
    var arrBestSellerData : [AnyObject] = []
    var arrMostWantedData : [AnyObject] = []
    var arrNewArrivalData : [AnyObject] = []
    var arrProductImagesDataBestSeller : [String] = []
    var arrProductImagesDataMostWanted : [String] = []
    var arrProductImagesDataNewArrival : [String] = []
    var isServiceCallComplete: Bool = true
    
    var strBestSellerKey1:String = ""
    
    var strRating:String = ""
    
    // WishList Variables
    
    // Check Item In WishList Service Variables
    var dictCheckItemWishlist:[String:AnyObject] = [:]
    var ItemExist:String = ""
    var sts:String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: SETUP DATA
    func setUpBestSellerData(arrModelDataBestSeller : [AnyObject])
    {
        arrBestSellerData = arrModelDataBestSeller
        CltnHomeProduct.delegate = self
        CltnHomeProduct.dataSource = self
        self.CltnHomeProduct.reloadData()
    }
    
    func setUpMostWantedData(arrModelDataMostWanted : [AnyObject])
    {
        arrMostWantedData = arrModelDataMostWanted
        CltnHomeProduct.delegate = self
        CltnHomeProduct.dataSource = self
        self.CltnHomeProduct.reloadData()
    }
    
    func setUpNewArrivalData(arrModelDataNewArrival : [AnyObject])
    {
        arrNewArrivalData = arrModelDataNewArrival
        CltnHomeProduct.delegate = self
        CltnHomeProduct.dataSource = self
        self.CltnHomeProduct.reloadData()
    }
    
    func setUpProductImagesData(arrModelDataProductImages : [String])
    {
        arrProductImagesDataBestSeller = arrModelDataProductImages
    }
    
    func setUpProductImagesDataMostWanted(arrModelDataProductImagesMostWanted : [String])
    {
        arrProductImagesDataMostWanted = arrModelDataProductImagesMostWanted
    }
    
    func setUpProductImagesDataNewArrival(arrModelDataProductImagesNewArrival : [String])
    {
        arrProductImagesDataNewArrival = arrModelDataProductImagesNewArrival
    }
    
    func setUpBestSellerKey(strBestSeller : String)
    {
        strKey = strBestSeller
        CltnHomeProduct.delegate = self
        CltnHomeProduct.dataSource = self
        self.CltnHomeProduct.reloadData()
    }
    
    //MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if strKey == "BestSeller" {
            return arrBestSellerData.count
        }
        
        if strKey == "MostWanted" {
            return arrMostWantedData.count
        }
        
        if strKey == "NewArrival" {
            return arrNewArrivalData.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView
        : UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellHomeProductCell: CltnHomeProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CltHomeProductCell", for: indexPath) as! CltnHomeProductCell
        
        cellHomeProductCell.applyShadowDefault()
        
        if strKey == "BestSeller"
        {
            cellHomeProductCell.setUpBestSellerData(arrModelDataBestSeller: arrBestSellerData)
            
            cellHomeProductCell.contentView.layer.cornerRadius = 3.0
            cellHomeProductCell.contentView.layer.masksToBounds = true
            
            let dict = arrBestSellerData[indexPath.row] as! [String:AnyObject]
            
            let productId:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
            
            let productName:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
            cellHomeProductCell.lblProductName.text = "  " + productName
            
            let basePrice:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "price")
            var strBasePrice = String(format:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               "%.2f", basePrice.toFloat()!)
            strBasePrice = BazarBitCurrency.setPriceSign(strPrice: strBasePrice)
            var attributedPrizeString = NSAttributedString(string: strBasePrice)
            attributedPrizeString = attributedPrizeString.strikethrough()
            
            let salePrice:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "sale_price")
            let strPrice = String(format: "%.2f", salePrice.toFloat()!)
            let strSalePrice = BazarBitCurrency.setPriceSign(strPrice: strPrice)
            
            if strPrice == "0" || strPrice == "0.00" {
                cellHomeProductCell.lblBaseprice.text = strBasePrice
                cellHomeProductCell.lblBaseprice.setTextLabelDynamicTextColor()
                cellHomeProductCell.lblSalePrice.isHidden = true
                cellHomeProductCell.lblSalePrice.text = ""

            }else {
                cellHomeProductCell.lblSalePrice.text = "  " + strSalePrice
                cellHomeProductCell.lblBaseprice.attributedText = attributedPrizeString
           //     cellHomeProductCell.lblSalePriceWidth.constant = 60.0
                cellHomeProductCell.lblSalePrice.isHidden = false
            }
            
            // Rating Value Set
            let rate = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "avg_rating")
            
            let value = Double(rate)
            if value != nil
            {
                cellHomeProductCell.ratingView.rating = value!
            }else {
                cellHomeProductCell.ratingView.rating = 0.0
            }
            
            // MARK:- Main Image Changes
            
            if (arrProductImagesDataBestSeller[indexPath.row] as? String) != nil
            {
                var strImage:String = arrProductImagesDataBestSeller[indexPath.row]
                strImage = strImage.replacingOccurrences(of: " ", with: "%20")
                let url1 = URL(string: strImage)
                cellHomeProductCell.imgproduct.kf.setImage(with: url1)
            }
            
            cellHomeProductCell.btnWishList.tag = indexPath.row
            cellHomeProductCell.btnWishList.addTarget(self, action: #selector(self.BtnWishListBestSeller), for: .touchUpInside)
            
            if APPDELEGATE.is_wishlist_active == "1"{
                cellHomeProductCell.btnWishList.isHidden = false
            }else{
                cellHomeProductCell.btnWishList.isHidden = true
            }
            
            let ExistInWishList:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "exists_in_wishlist")
            
            if ExistInWishList == "1" {
                cellHomeProductCell.btnWishList.setImage((UIImage(named: "like1.png"), for: UIControlState.normal))
                
            }  else  {
                // Fill
                cellHomeProductCell.btnWishList.setImage((UIImage(named: "like2.png"), for: UIControlState.normal))
            }
        }
        else if strKey == "MostWanted"
        {
            cellHomeProductCell.setUpMostWantedData(arrModelDataMostWanted: arrMostWantedData)
            let dict = arrMostWantedData[indexPath.row] as! [String:AnyObject]
            
            cellHomeProductCell.contentView.layer.cornerRadius = 3.0
            cellHomeProductCell.contentView.layer.masksToBounds = true
            
            let productName:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
            cellHomeProductCell.lblProductName.text = "  " + productName
            
            
            // var BasePrice:Int = 0
            let basePrice:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "price")
            
            var strBasePrice = String(format: "%.2f", basePrice.toFloat()!)
            strBasePrice = BazarBitCurrency.setPriceSign(strPrice: strBasePrice)
            var attributedPrizeString = NSAttributedString(string: strBasePrice)
            attributedPrizeString = attributedPrizeString.strikethrough()
            
            let salePrice:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "sale_price")
            let strPrice = String(format: "%.2f", salePrice.toFloat()!)
            let strSalePrice = BazarBitCurrency.setPriceSign(strPrice: strPrice)
            
            if strPrice == "0" || strPrice == "0.00" {
                cellHomeProductCell.lblBaseprice.text = strBasePrice
                cellHomeProductCell.lblBaseprice.setTextLabelDynamicTextColor()
                cellHomeProductCell.lblSalePrice.isHidden = true
                cellHomeProductCell.lblSalePrice.text = ""

            }else {
                cellHomeProductCell.lblSalePrice.text = "  " + strSalePrice
                cellHomeProductCell.lblBaseprice.attributedText = attributedPrizeString
             //   cellHomeProductCell.lblSalePriceWidth.constant = 60.0
                cellHomeProductCell.lblSalePrice.isHidden = false
            }
            
            // Rating Value Set
            let rate = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "avg_rating")
            
            let value = Double(rate)
            if value != nil
            {
                cellHomeProductCell.ratingView.rating = value!
            }else {
                cellHomeProductCell.ratingView.rating = 0.0
            }
            // MARK:- Main Image Changes
            
            if (arrProductImagesDataMostWanted[indexPath.row] as? String) != nil
            {
                var strImage:String = arrProductImagesDataMostWanted[indexPath.row]
                let url1 = URL(string: strImage)
                strImage = strImage.replacingOccurrences(of: " ", with: "%20")
                cellHomeProductCell.imgproduct.kf.setImage(with: url1)
            }
            
            cellHomeProductCell.btnWishList.tag = indexPath.row
            cellHomeProductCell.btnWishList.addTarget(self, action: #selector(self.BtnWishListBestSeller), for: .touchUpInside)
            
            if APPDELEGATE.is_wishlist_active == "1"{
                cellHomeProductCell.btnWishList.isHidden = false
            } else{
                cellHomeProductCell.btnWishList.isHidden = true
            }
            
            let ExistInWishList:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "exists_in_wishlist")
            
            if ExistInWishList == "1" {
                cellHomeProductCell.btnWishList.setImage((UIImage(named: "like1.png"), for: UIControlState.normal))
            }else  {
                // Fill
                cellHomeProductCell.btnWishList.setImage((UIImage(named: "like2.png"), for: UIControlState.normal))
            }
        }
        else if strKey == "NewArrival"
        {
            cellHomeProductCell.setUpNewArrivalData(arrModelDataNewArrival: arrNewArrivalData)
            let dict = arrNewArrivalData[indexPath.row] as! [String:AnyObject]
            
            cellHomeProductCell.contentView.layer.cornerRadius = 3.0
            cellHomeProductCell.contentView.layer.masksToBounds = true
            
            let productName:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
            cellHomeProductCell.lblProductName.text = "  " + productName
            
            let basePrice:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "price")
            
            var strBasePrice = String(format: "%.2f", basePrice.toFloat()!)
            strBasePrice = BazarBitCurrency.setPriceSign(strPrice: strBasePrice)
            var attributedPrizeString = NSAttributedString(string: strBasePrice)
            attributedPrizeString = attributedPrizeString.strikethrough()
            
            let salePrice:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "sale_price")
            let strPrice = String(format: "%.2f", salePrice.toFloat()!)
            let strSalePrice = BazarBitCurrency.setPriceSign(strPrice: strPrice)
            
            if strPrice == "0" || strPrice == "0.00" {
                cellHomeProductCell.lblBaseprice.text = strBasePrice
                cellHomeProductCell.lblBaseprice.setTextLabelDynamicTextColor()
                cellHomeProductCell.lblSalePrice.isHidden = true
                cellHomeProductCell.lblSalePrice.text = ""

            }else {
                cellHomeProductCell.lblSalePrice.text = "  " + strSalePrice
                cellHomeProductCell.lblBaseprice.attributedText = attributedPrizeString
              //  cellHomeProductCell.lblSalePriceWidth.constant = 60.0
                cellHomeProductCell.lblSalePrice.isHidden = false
            }
            
            // Rating Value Set
            let rate = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "avg_rating")
            
            let value = Double(rate)
            
            if value != nil {
                cellHomeProductCell.ratingView.rating = value!
            } else {
                cellHomeProductCell.ratingView.rating = 0.0
            }
            
            // MARK:- Main Image Changes
            if (arrProductImagesDataNewArrival[indexPath.row] as? String) != nil
            {
                var strImage:String = arrProductImagesDataNewArrival[indexPath.row]
                let url1 = URL(string: strImage)
                strImage = strImage.replacingOccurrences(of: " ", with: "%20")
                cellHomeProductCell.imgproduct.kf.setImage(with: url1)
            }
            
            cellHomeProductCell.btnWishList.tag = indexPath.row
            cellHomeProductCell.btnWishList.addTarget(self, action: #selector(self.BtnWishListBestSeller), for: .touchUpInside)
            
            if APPDELEGATE.is_wishlist_active == "1"{
                cellHomeProductCell.btnWishList.isHidden = false
            } else{
                cellHomeProductCell.btnWishList.isHidden = true
            }
            
            let ExistInWishList:String = dict["exists_in_wishlist"] as! String
            
            if ExistInWishList == "1" {
                cellHomeProductCell.btnWishList.setImage((UIImage(named: "like1.png"), for: UIControlState.normal))
                
            }  else  {
                // Fill
                cellHomeProductCell.btnWishList.setImage((UIImage(named: "like2.png"), for: UIControlState.normal))
            }
        }
        return cellHomeProductCell
    }
    
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if strKey == "BestSeller"
        {
            let dict = arrBestSellerData[indexPath.row] as! [String:AnyObject]
            
            APPDELEGATE.ProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
            APPDELEGATE.slugName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "slug")
            
            //            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            //                myDelegate.OpenProductDetailPage()
            //            }
            let productDetailVC = homevc.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
            homevc.navigationController?.pushViewController(productDetailVC, animated: true)
        }
        
        if strKey == "MostWanted"
        {
            let dict = arrMostWantedData[indexPath.row] as! [String:AnyObject]
            
            APPDELEGATE.ProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
            APPDELEGATE.slugName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "slug")
            
            //            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            //                myDelegate.OpenProductDetailPage()
            //            }
            let productDetailVC = homevc.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
            homevc.navigationController?.pushViewController(productDetailVC, animated: true)
        }
        
        if strKey == "NewArrival"
        {
            let dict = arrNewArrivalData[indexPath.row] as! [String:AnyObject]
            
            APPDELEGATE.ProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
            APPDELEGATE.slugName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "slug")
            
            //            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            //                myDelegate.OpenProductDetailPage()
            //            }
            let productDetailVC = homevc.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
            homevc.navigationController?.pushViewController(productDetailVC, animated: true)
        }
    }
    
    //MARK: UICollectionViewFlowLayOut
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if Constants.DeviceType.IS_IPAD {
            let width = collectionView.frame.size.width
            return CGSize(width: (width/3) - 10, height:260.0)
        }else if IS_IPHONE_5_OR_5S() {
            return CGSize(width: 160, height : 200.0)
        }else {
            return CGSize(width: 180, height : 200.0)
        }
    }
    
    // MARK:- UIScrollView METHODS
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == CltnHomeProduct {
            print(strKey)
            
            if self.isServiceCallComplete == true {
                
                if strKey == "BestSeller" {
                    
                    if self.homevc.isCompleteBestSellerRecord == false {
                        
                        if ((CltnHomeProduct.contentOffset.x + CltnHomeProduct.frame.size.width) >= CltnHomeProduct.contentSize.width)
                        {
                            self.isServiceCallComplete = false
                            self.GetBestSellerProducts()
                        }
                    }
                    
                } else if strKey == "MostWanted" {
                    
                    if self.homevc.isCompleteMostWantedRecord == false {
                        
                        if ((CltnHomeProduct.contentOffset.x + CltnHomeProduct.frame.size.width) >= CltnHomeProduct.contentSize.width)
                        {
                            self.isServiceCallComplete = false
                            self.GetMostWantedProducts()
                        }
                    }
                    
                } else if strKey == "NewArrival" {
                    
                    if self.homevc.isCompleteNewArrivalRecord == false {
                        
                        if ((CltnHomeProduct.contentOffset.x + CltnHomeProduct.frame.size.width) >= CltnHomeProduct.contentSize.width)
                        {
                            self.isServiceCallComplete = false
                            self.GetNewArrivalProducts()
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Service Method
    
    func getSearchIndex(strSearchText: String, strSearchKey: String, arrSearch: [AnyObject]) -> Int {
        
        var  searchIndex: Int = -1
        
        let preda = NSPredicate(format: "\(strSearchKey) CONTAINS[c] '\(strSearchText)'")
        let arrLocalSearch = arrSearch.filter { preda.evaluate(with: $0) }
        
        if arrLocalSearch.count > 0 {
            
            searchIndex = arrSearch.index {
                if let dic = $0 as? Dictionary<String,AnyObject> {
                    if let value = dic[strSearchKey]  as? String, value == strSearchText {
                        return true
                    }
                }
                return false
                }!
        }
        
        return searchIndex
    }
    
    func AddToWishList() {
        
        let strurl:String = BazarBit.AddItemToWishListService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        
        if strKey == "BestSeller" {
            parameter["product_id"] = APPDELEGATE.ProductIdBestSeller
        }
        else if strKey == "MostWanted"
        {
            parameter["product_id"] = APPDELEGATE.ProductIdMostWanted
        }
        else if strKey == "NewArrival"
        {
            parameter["product_id"] = APPDELEGATE.ProductidNewArrival
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: homevc, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dictCheckItemWishlist = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictCheckItemWishlist, key: "status")
            let messege:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictCheckItemWishlist, key: "message")
            
            if status == "ok"
            {
                let cntWishlist : String = UserDefaults.standard.value(forKey: "WishListCount") as! String
                var count : Int = Int(cntWishlist)!
                
                count = count + 1
                UserDefaults.standard.set(String(count), forKey: "WishListCount")
                APPDELEGATE.setWishListCount(strCount: String(count))
                
                var strProductID: String = ""
                
                if self.strKey == "BestSeller" {
                    strProductID = APPDELEGATE.ProductIdBestSeller
                }
                else if self.strKey == "MostWanted"
                {
                    strProductID = APPDELEGATE.ProductIdMostWanted
                }
                else if self.strKey == "NewArrival"
                {
                    strProductID = APPDELEGATE.ProductidNewArrival
                }
                
                let bestSellerIndex: Int = self.getSearchIndex(strSearchText: strProductID, strSearchKey: "product_id", arrSearch: self.homevc.arrBestSeller)
                
                if bestSellerIndex >= 0 && bestSellerIndex < self.homevc.arrBestSeller.count {
                    var dictBestSeller: [String: AnyObject] = self.homevc.arrBestSeller[bestSellerIndex] as! [String:AnyObject]
                    dictBestSeller["exists_in_wishlist"] = "1" as AnyObject
                    self.homevc.arrBestSeller[bestSellerIndex] = dictBestSeller as AnyObject

                }
                
                let MostWantedIndex: Int = self.getSearchIndex(strSearchText: strProductID, strSearchKey: "product_id", arrSearch: self.homevc.arrMostWanted)
                
                if MostWantedIndex >= 0 && MostWantedIndex < self.homevc.arrMostWanted.count {
                    var dictMostWanted: [String:AnyObject] = self.homevc.arrMostWanted[MostWantedIndex] as! [String:AnyObject]
                    dictMostWanted["exists_in_wishlist"] = "1" as AnyObject

                    self.homevc.arrMostWanted[MostWantedIndex] = dictMostWanted as AnyObject
                    
                }
                
                let newArrivalIndex: Int = self.getSearchIndex(strSearchText: strProductID, strSearchKey: "product_id", arrSearch: self.homevc.arrNewArrival)
                
                if newArrivalIndex >= 0 && newArrivalIndex < self.homevc.arrNewArrival.count {
                    var dictNewArrival: [String:AnyObject] = self.homevc.arrNewArrival[newArrivalIndex] as! [String:AnyObject]
                    dictNewArrival["exists_in_wishlist"] = "1" as AnyObject
                    self.homevc.arrNewArrival[newArrivalIndex] = dictNewArrival as AnyObject
                    
                }
                
                self.homevc.reloadHomeProductData()
                
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage(messege)
            }
        })
    }
    
    func GetBestSellerProducts() {
        
        let strurl:String = BazarBit.HomeBestSellerProsuctService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        parameter["offset"] = self.homevc.strBestSellerOffset
        parameter["limit"] = self.homevc.strlimit
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
            
        }else {
            parameter["user_id"] = ""
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self.homevc, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.homevc.dicResponceBestSeller = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.homevc.dicResponceBestSeller, key: "status")
            
            if status == "ok"
            {
                if (self.homevc.dicResponceBestSeller["payload"] != nil)
                {
                    let payload:[String:AnyObject] = self.homevc.dicResponceBestSeller["payload"] as! [String:AnyObject]
                    
                    if payload["bestseller"] != nil
                    {
                        let BestSeller:[AnyObject] = payload["bestseller"] as! [AnyObject]
                        
                        if self.homevc.strBestSellerOffset == "0" {
                            self.homevc.aryAllImagesBestSeller = []
                            self.homevc.arrBestSeller = []
                        }
                        
                        if BestSeller.count != Int(self.homevc.strlimit.toDouble()!) {
                            self.homevc.isCompleteBestSellerRecord = true
                        }
                        
                        for dict in BestSeller
                        {
                            self.homevc.arrBestSeller.append(dict)
                            
                            if let key = dict["main_image"] {
                                if (key as? [String : AnyObject]) != nil
                                {
                                    let mainImage: [String: AnyObject] = key as! [String: AnyObject]
                                    
                                    let imgUrl:String = mainImage["main_image"] as! String
                                    self.homevc.aryAllImagesBestSeller.append(imgUrl)
                                }
                            }
                        }
                    }
                }
            }
            
            let limit: Int = Int(self.homevc.strlimit)!
            var bestSellerOffset: Int = Int(self.homevc.strBestSellerOffset)!
            bestSellerOffset = bestSellerOffset + limit
            self.homevc.strBestSellerOffset = String(bestSellerOffset)
            
            self.setUpBestSellerData(arrModelDataBestSeller: self.homevc.arrBestSeller)
            
            self.setUpProductImagesData(arrModelDataProductImages: self.homevc.aryAllImagesBestSeller as [String])
            
            self.isServiceCallComplete = true
            
        })
    }
    
    func GetMostWantedProducts() {
        
        let strurl:String = BazarBit.HomeMostWantedProductService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }
        else
        {
            parameter["user_id"] = ""
        }
        
        parameter["offset"] = self.homevc.strMostWantedOffset
        parameter["limit"] = self.homevc.strlimit
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self.homevc, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.homevc.dicResponceMostWantedproducts = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.homevc.dicResponceMostWantedproducts, key: "status")
            
            if status == "ok"
            {
                if (self.homevc.dicResponceMostWantedproducts["payload"] != nil) {
                    let payload:[String:AnyObject] = self.homevc.dicResponceMostWantedproducts["payload"] as! [String:AnyObject]
                    
                    if payload["mostWantedProducts"] != nil
                    {
                        let arrWantedProduct:[AnyObject] = payload["mostWantedProducts"] as! [AnyObject]
                        
                        if self.homevc.strMostWantedOffset == "0" {
                            self.homevc.aryAllImagesMostWanted = []
                            self.homevc.arrMostWanted = []
                        }
                        
                        if arrWantedProduct.count != Int(self.homevc.strlimit.toDouble()!) {
                            self.homevc.isCompleteMostWantedRecord = true
                        }
                        
                        for dict in arrWantedProduct
                        {
                            self.homevc.arrMostWanted.append(dict)
                            if let key = dict["main_image"] {
                                if (key as? [String : AnyObject]) != nil
                                {
                                    let mainImage: [String: AnyObject] = key as! [String: AnyObject]
                                    
                                    let imgUrl:String = mainImage["main_image"] as! String
                                    self.homevc.aryAllImagesMostWanted.append(imgUrl)
                                }
                            }
                        }
                        
                    }
                }
            }
            
            let limit: Int = Int(self.homevc.strlimit)!
            var mostWantedOffset: Int = Int(self.homevc.strMostWantedOffset)!
            mostWantedOffset = mostWantedOffset + limit
            self.homevc.strMostWantedOffset = String(mostWantedOffset)
            
            self.setUpMostWantedData(arrModelDataMostWanted: self.homevc.arrMostWanted)
         self.setUpProductImagesDataMostWanted(arrModelDataProductImagesMostWanted: self.homevc.aryAllImagesMostWanted as [String])
            
            self.isServiceCallComplete = true
        })
    }
    
    func GetNewArrivalProducts() {
        
        let strurl:String = BazarBit.HomeNewArrivalProductService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }
        else
        {
            parameter["user_id"] = ""
        }
        
        parameter["offset"] = self.homevc.strNewArrivalOffset
        parameter["limit"] = self.homevc.strlimit
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self.homevc, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.homevc.dicResponceNewArrivalProducts = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.homevc.dicResponceNewArrivalProducts, key: "status")
            
            if status == "ok"
            {
                if (self.homevc.dicResponceNewArrivalProducts["payload"] != nil)
                {
                    let payload:[String:AnyObject] = self.homevc.dicResponceNewArrivalProducts["payload"] as! [String:AnyObject]
                    
                    if payload["newArrivals"] != nil
                    {
                        let arrNewarrivalProduct:[AnyObject] = payload["newArrivals"] as! [AnyObject]
                        
                        if self.homevc.strNewArrivalOffset == "0" {
                            self.homevc.aryAllImagesNewArrival = []
                            self.homevc.arrNewArrival = []
                        }
                        
                        if arrNewarrivalProduct.count != Int(self.homevc.strlimit.toDouble()!) {
                            self.homevc.isCompleteNewArrivalRecord = true
                        }
                        
                        for dict in arrNewarrivalProduct
                        {
                            self.homevc.arrNewArrival.append(dict)
                            
                            if let key = dict["main_image"] {
                                if (key as? [String : AnyObject]) != nil
                                {
                                    let mainImage: [String: AnyObject] = key as! [String: AnyObject]
                                    
                                    let imgUrl:String = mainImage["main_image"] as! String
                                    self.homevc.aryAllImagesNewArrival.append(imgUrl)
                                }
                            }
                        }
                    }
                }
            }
            
            let limit: Int = Int(self.homevc.strlimit)!
            var newArrivalOffset: Int = Int(self.homevc.strNewArrivalOffset)!
            newArrivalOffset = newArrivalOffset + limit
            self.homevc.strNewArrivalOffset = String(newArrivalOffset)
            
            self.setUpNewArrivalData(arrModelDataNewArrival: self.homevc.arrNewArrival)
            
            self.setUpProductImagesDataNewArrival(arrModelDataProductImagesNewArrival: self.homevc.aryAllImagesNewArrival as [String])
            
            self.isServiceCallComplete = true
            
        })
    }
    
    // MARK: - Helper Method
    func BtnWishListBestSeller(sender:UIButton) {
        
        if strKey == "BestSeller" {
            
            let dict = arrBestSellerData[sender.tag] as! [String:AnyObject]
            
            APPDELEGATE.ProductIdBestSeller = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
            
            if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
                AddToWishList()
            }
            else
            {
                if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                    myDelegate.openLoginPage()
                    //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                }
            }
        }
        
        if  strKey == "MostWanted" {
            let dict = arrMostWantedData[sender.tag] as! [String:AnyObject]
            
            APPDELEGATE.ProductIdMostWanted = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
            
            if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
                AddToWishList()
            }
            else
            {
                if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                    myDelegate.openLoginPage()
                    //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                }
            }
        }
        
        if strKey == "NewArrival" {
            let dict = arrNewArrivalData[sender.tag] as! [String:AnyObject]
            
            APPDELEGATE.ProductidNewArrival = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
            
            if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
                AddToWishList()
            }
            else
            {
                if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                    myDelegate.openLoginPage()
                    //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                }
            }
        }
    }
    
    // MARK: Rating Helper Method
    // Delegate
    
    //COSMOS
    func didTouchCosmos(_ rating: Double) {
        
    }
    
    func didFinishTouchingCosmos(_ rating: Double) {
        strRating = rating.toString
    }
    
 }
 
 extension Float
 {
    var cleanValue: String
    {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
 }
