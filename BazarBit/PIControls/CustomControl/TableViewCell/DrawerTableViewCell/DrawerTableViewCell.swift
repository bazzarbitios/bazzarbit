//
//  DrawerTableViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/31/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class DrawerTableViewCell: UITableViewCell {
    
    @IBOutlet var labelDot: UILabel!
    @IBOutlet var labelTitle: PILabelDrawerTitle!
    @IBOutlet weak var labelSideView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        self.labelSideView.isHidden = true
        self.labelDot.layer.masksToBounds = false
        if IS_IPAD_DEVICE() {
            self.labelDot.layer.cornerRadius = 10.0
        }
        else
        {
            self.labelDot.layer.cornerRadius = 7.0
        }
        self.labelDot.clipsToBounds = true
        self.labelDot.backgroundColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
