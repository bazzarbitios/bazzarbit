//
//  cltnHomeBannerCell.swift
//  BazarBit
//
//  Created by Parghi Infotech on 12/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class cltnHomeBannerCell: UICollectionViewCell {
    @IBOutlet weak var imgBanner: UIImageView!    
    @IBOutlet weak var lblBannerName: UILabel!
    
    override func awakeFromNib() {

    }
}
