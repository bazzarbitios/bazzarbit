//
//  MegaMenuItemCollectionViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/26/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Kingfisher
import EZSwiftExtensions
import Cosmos

class MegaMenuItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageViewItem: UIImageView!
    @IBOutlet var buttonWishList: UIButton!
    @IBOutlet var labelProductName: PIProductTitleLabel!
    @IBOutlet var labelSalePrice: PIProductSalePriceLabel!
    @IBOutlet var labelPrice: PIProductPriceLabel!
    @IBOutlet var lblSalePriceWidth: NSLayoutConstraint!
    @IBOutlet var lblBasePriceWidth: NSLayoutConstraint!
    @IBOutlet weak var reratingView: CosmosView!
    
    override func awakeFromNib() {
        
        buttonWishList.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.cornerRadius = 5.0
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 2.0
        self.layer.masksToBounds = false
    }
    
    func setUpData(modelData : [String : AnyObject]) {
        
        self.labelProductName.text = PIService.object_forKeyWithValidationForClass_String(dict: modelData, key: "name")
        
        let sale_Price : String = PIService.object_forKeyWithValidationForClass_String(dict: modelData, key: "sale_price")
        let strSalePrice = String(format: "%.2f", sale_Price.toFloat()!)
        
        if sale_Price == "0.00" || sale_Price == "0" {
            
            let attributedPrizeString = NSAttributedString(string: "")
            
            self.labelPrice.attributedText = attributedPrizeString
            self.labelPrice.text = ""
            self.labelSalePrice.text = ""
            self.labelSalePrice.attributedText = attributedPrizeString

            let price : String = PIService.object_forKeyWithValidationForClass_String(dict: modelData, key: "price")
            let strPrice = String(format: "%.2f", price.toFloat()!)
            
            self.labelPrice.text = BazarBitCurrency.setPriceSign(strPrice: strPrice)
            self.labelPrice.setTextLabelDynamicTextColor()
            self.labelSalePrice.text = ""
            
        } else {
          //  self.labelPrice.set
            
            let attributedString = NSAttributedString(string: "")
            
            self.labelPrice.attributedText = attributedString
            self.labelPrice.text = ""
            self.labelSalePrice.text = ""
            self.labelSalePrice.attributedText = attributedString
            
            let price : String = PIService.object_forKeyWithValidationForClass_String(dict: modelData, key: "price")
            let strPrice = String(format: "%.2f", price.toFloat()!)
            
            var attributedPrizeString = NSAttributedString(string: BazarBitCurrency.setPriceSign(strPrice: strPrice))
            attributedPrizeString = attributedPrizeString.strikethrough()
            
            self.labelSalePrice.setTextLabelDynamicTextColor()
            self.labelPrice.setTextLabelLightColor()
            self.labelPrice.attributedText = attributedPrizeString
            self.labelSalePrice.text = BazarBitCurrency.setPriceSign(strPrice: strSalePrice)
        }
        
        if let key = modelData["main_image"] {
            if (key as? [String : AnyObject]) != nil
            {
                print(key)
                var imgUrl:String = key["main_image"] as! String
                imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                let url1 = URL(string: imgUrl)
                self.imageViewItem.kf.setImage(with: url1)
            }
        }
        
        let Rating:String = PIService.object_forKeyWithValidationForClass_String(dict: modelData, key: "avg_rating")
        let value = Double(Rating)
        if value != nil {
            self.reratingView.rating = value!
        }else{
            self.reratingView.rating = 0.0
        }
    }
}
