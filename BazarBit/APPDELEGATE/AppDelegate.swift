//
//  AppDelegate.swift
//  BazarBit
//
//  Created by Parghi Infotech on 9/30/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import FBSDKLoginKit
import DrawerController
import UserNotifications
import Fabric
import Crashlytics
import SVProgressHUD
import NVActivityIndicatorView
import PinterestSDK
import TwitterKit
import TwitterCore

var APPDELEGATE : AppDelegate{
  get{
    return UIApplication.shared.delegate as! AppDelegate
  }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SirenDelegate {
  
  var arrSelect : [String] = [String]()
  var window: UIWindow?
  var drawerController: DrawerController!
  var slugName:String = ""
  var ProductID:String = ""
  var slugProdID:String = ""
  var strSelectedCountry : String = ""
  var selectedProductID : String = ""
  var variationDisable:Bool = false
  var GlobalVC:UIViewController!
  var chkBottomBar : Bool = false
  var checkDisplayBillingRecord: Bool = false

  var arySocialLoginButton : [String] = [String]()
  
  // App Setting variable
  var dicResponceSetting:[String:AnyObject] = [:]
  var AppSettingPayload:[String:AnyObject] = [:]
  var logoImage:String = ""
  var googleImage:String = ""
  var facebookImage:String = ""
  var instagramImage:String = ""
  var linkedInImage:String = ""
  var pintrestImage:String = ""
    var strISLive: String = "1"

  var applicationImage:String = ""
  
  // Dynamic Color
  var backgroundColor:String = ""
  var borderColor1:String = ""
  var placeholderColor:String = ""
  var textColor1:String = ""
  var buttontextColor:String = ""
  
  // Active / Nonactive
  var is_slider_active:String = ""
  var is_arrival_active:String = ""
  var is_seller_active:String = ""
  var is_mostwanted_active:String = ""
  
  var is_cart_active:String = ""
  var is_order_active:String = ""
  var allow_otp_verification: String = ""
  var check_pincode_availibility: String = ""
  var is_amazon_active:String = ""
  var is_wishlist_active:String = ""
  var chkLablecheck : String = ""
  var chkProfile : Bool = false
  var strChkProfile : Bool = false
  
  var ProductIdBestSeller = ""
  var ProductIdMostWanted = ""
  var ProductidNewArrival = ""
  
  var RelatedProductID = ""
  var TermsProductId = ""
  var TermsAtributedID = ""
  
  var defaultvariationProductID = ""
  var defaultvariationAttributedID = ""
  
  var SelectedName = ""
  var firstID = ""
  
  var siren:Siren!
  
  var loginVC : LoginVC!
  var tabbarVC : TabbarVC!
  var strHomeAlpha : String = "Hide"
  var chkPayUMoneyTitle : Bool = false
  var chkNewUser : Bool = false
  var chkAddAddress : Bool = false
  var strvalidate : String = ""
  var chklastOrder : Bool = false
  var chkSelectedList : Bool = false
  var chkSameAsShipping : String = ""
  var chkFirstAddress : Bool = false
  var isBilling: Bool = true
  var shoppingCartVC : ShoppingCartViewController!
  var chkListingBack:Bool = false
  
  // Cart Count Variables
  var dicResponceCartTotalList:[String:AnyObject] = [:]
  var arrCartList:[AnyObject] = []
  var CartTotal:String = ""
  
  var navigationWishListVC: UINavigationController!
  var wishListVC: WishListVC!
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    CheckInternet()
    
    NVActivityIndicatorView.DEFAULT_TYPE = .lineSpinFadeLoader
    
    IQKeyboardManager.sharedManager().enable = true
    IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = false
    
    self.openStartUpPage()
    self.GetAppSetting()
    ServiceCallForPackageData()
    GetCartCountData()
    
    Fabric.with([Crashlytics.self])
    self.notificationSetting(application: application)
    UIApplication.shared.applicationIconBadgeNumber = 0
    
    // Facebook Integration Code
    FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    TWTRTwitter.sharedInstance().start(withConsumerKey:"xeXCsk62TLcIHNTxDLHWL5gs1", consumerSecret:"TCEE3gDKASw5vyEtFnKfmZEYJ6hWHN92XGDdSJChmz1VuAbCvF")
    // GMAIL - Initialize sign-in
    var configureError: NSError?
    GGLContext.sharedInstance().configureWithError(&configureError)
    assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
    
    //PINTREST
    PDKClient.configureSharedInstance(withAppId: "4942597597022925280")
    
    return true
  }
  
  // MARK: - Notification Delegate Method
  
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    // Convert token to string
    let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
    
    UserDefaults.standard.set(deviceTokenString, forKey: BazarBit.DEVICEID)
    print("APNs device token: \(deviceTokenString)")
  //  self.displayAlertForDeviceToken(strDeviceToken: deviceTokenString)
    
  }
  
  // MARK: - Notification Setting
  func notificationSetting(application: UIApplication)  {
    
    if #available(iOS 10, *) {
      UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
      application.registerForRemoteNotifications()
    }
      // iOS 9 support
    else if #available(iOS 9, *) {
      
      UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
      UIApplication.shared.registerForRemoteNotifications()
    }
      // iOS 8 support
    else if #available(iOS 8, *) {
      UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
      UIApplication.shared.registerForRemoteNotifications()
    }
      // iOS 7 support
    else {
      application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
    }
  }
  
  func displayAlertControl(strMessage: String)  {
    
    let alertController: UIAlertController = UIAlertController(title: "BazarBit", message: strMessage, preferredStyle: .alert)
    
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
    }
    
    alertController.addAction(okAction)
    self.window?.currentViewController()?.present(alertController, animated: true, completion: nil)
    
  }
  
  // MARK: - Notification
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    
    print(userInfo)
    
    let arrnotificationDetail: [String:AnyObject] = userInfo as! [String:AnyObject]
    print(arrnotificationDetail)
    
    
    let DictApsDetail: [String: AnyObject] = arrnotificationDetail["aps"] as! [String: AnyObject]
    
    let alertString: String = DictApsDetail["alert"] as! String
    
  }
  
  // MARK:- Social
 
    
  func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
    
    // Facebook Implementation Code - Open URL
    let facebookHandler = FBSDKApplicationDelegate.sharedInstance().application (
      app,
      open: url,
      sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
      annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    
    
    // Gmail Implementation Code - Open URL
    let googleHandler = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    
    //Linked In
    /*if LISDKCallbackHandler.shouldHandle(url) {
     }*/
    
    let linkedInHandler = LISDKCallbackHandler.application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    
    let TwitterInHandler = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    
    return facebookHandler || googleHandler || linkedInHandler || TwitterInHandler || PDKClient.sharedInstance().handleCallbackURL(url)
  }
  
  func application(_ application: UIApplication,
                   open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    let googleDidHandle = GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: sourceApplication, annotation: annotation)
    
    return googleDidHandle
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    CheckInternet()
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
  
  // MARK: - Helper Method
    
    func displayAlertForDeviceToken(strDeviceToken: String) {
    
        let alertController: UIAlertController = UIAlertController(title: "Alert!", message: strDeviceToken, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        }
        
        alertController.addAction(okAction)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.window?.currentViewController()!.present(alertController, animated: true, completion: nil)
        }
        
    }
    
  func openLoginPage()  {
    
    let storyBoard: UIStoryboard!
    storyBoard = UIStoryboard(name: "Main", bundle: nil)
    loginVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    loginVC.isFromLogin = true
    let navigationVC = UINavigationController(rootViewController: loginVC)
    self.window?.rootViewController = navigationVC
  }
  
  func openSignUpPage()  {
    
    let storyBoard: UIStoryboard!
    storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let signUpVC = storyBoard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
    let navigationVC = UINavigationController(rootViewController: signUpVC)
    self.window?.rootViewController = navigationVC
  }
  
  func openHomePage()  {
    let mainStoryboard: UIStoryboard!
    mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    tabbarVC = TabbarVC()
    
    let drawerVC = mainStoryboard.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
    
    drawerVC.tabbar = tabbarVC
    
    drawerController = DrawerController(centerViewController: tabbarVC, leftDrawerViewController: drawerVC)
    
    if Constants.DeviceType.IS_IPAD {
      drawerController.setMaximumLeftDrawerWidth(400, animated: true, completion: nil)
      
    } else {
      drawerController.setMaximumLeftDrawerWidth(250, animated: true, completion: nil)
    }
    
    drawerController.openDrawerGestureModeMask = .all
    drawerController.closeDrawerGestureModeMask = .all
    
    self.window?.rootViewController = drawerController
    
  }
  
  func OpenProductDetailPage() {
    let storyBoard: UIStoryboard!
    storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let productDetailVC = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
    let navigationVC = UINavigationController(rootViewController: productDetailVC)
    self.window?.rootViewController = navigationVC
  }
  
  func OpenPGoToCartPage() {
    let storyBoard: UIStoryboard!
    storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let productDetailVC = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
    let navigationVC = UINavigationController(rootViewController: productDetailVC)
    self.window?.rootViewController = navigationVC
  }
  
  func OpenAddressListPage() {
    let storyBoard: UIStoryboard!
    storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let addressListVC = storyBoard.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
    let navigationVC = UINavigationController(rootViewController: addressListVC)
    self.window?.rootViewController = navigationVC
  }
  
  func OpenCheckOutPage() {
    let storyBoard: UIStoryboard!
    storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let checkoutVC = storyBoard.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
    let navigationVC = UINavigationController(rootViewController: checkoutVC)
    self.window?.rootViewController = navigationVC
  }
  
  func OpenContactUsPage() {
    let storyBoard: UIStoryboard!
    storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let contactUsVC = storyBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
    let navigationVC = UINavigationController(rootViewController: contactUsVC)
    self.window?.rootViewController = navigationVC
  }
  
  func checkAutoUpdateConfigure() {
    self.siren = Siren.shared
    self.siren.alertType = .force
    self.siren.debugEnabled = true
    self.siren.majorUpdateAlertType = .force
    self.siren.minorUpdateAlertType = .force
    self.siren.patchUpdateAlertType = .force
    self.siren.revisionUpdateAlertType = .force
    self.siren = Siren.shared
    self.siren.delegate = self
  }
  
  // MARK: - Service Method
  
    func GetAppSetting() {
        
        if IS_INTERNET_AVAILABLE() {
            let strurl:String = BazarBit.AppSettingService()
            let url:URL = URL(string: strurl)!
            
            var headers:HTTPHeaders = [:]
            headers[BazarBit.ContentType] = BazarBit.applicationJson
            headers["app-id"] = BazarBit.appId
            headers["app-secret"] = BazarBit.appSecret
            
            let parameter:Parameters = [:]
            
            let a = UIViewController()
            
            PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: a, isLoaderShow: false, completion: {(swiftyJson, responseDict) in
                
                let dict:[String: AnyObject] = responseDict
                
                print(dict)
                
                let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCartTotalList, key: "status")
                
                if status == "ok"
                {
                    
                    if (dict["payload"] as? [String: AnyObject]) != nil
                    {
                        let payload: [String: AnyObject] = dict["payload"] as! [String: AnyObject]
                        
                        if (payload["app_setting"] as? [String: AnyObject]) != nil
                        {
                            let dictAppSetting: [String: AnyObject] = payload["app_setting"] as! [String: AnyObject]
                            
                            self.strISLive = PIService.object_forKeyWithValidationForClass_String(dict: dictAppSetting, key: "is_live")
                        }
                    }
                }
                
                self.getApplicationSetting()
            })
        }
        else
        {
            let alertController: UIAlertController = UIAlertController(title: "Alert!", message: "Your Internet connectivity not available. Please check your internet connection and try again!", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                exit(0)
            }
            
            alertController.addAction(okAction)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.window?.currentViewController()!.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
  func getApplicationSetting() {
    
    if IS_INTERNET_AVAILABLE() {
      
      let strurl:String = BazarBit.ApplicationSettingService()
      let url : URL = URL(string: strurl)!
      
      var headers : HTTPHeaders = [:]
      headers[BazarBit.ContentType] = BazarBit.applicationJson
      headers["app-id"] = BazarBit.appId
      headers["app-secret"] = BazarBit.appSecret
      
      let parameter:Parameters = [:]
      let a = UIViewController()
      
      PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: a, isLoaderShow: false, completion: { (swiftyJson, responseDict) in
        
        self.dicResponceSetting = responseDict
        print(self.dicResponceSetting)
        
        let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceSetting, key: "status")
        
        if status == "ok"
        {
          if self.dicResponceSetting["payload"] != nil
          {
            self.AppSettingPayload = self.dicResponceSetting["payload"] as! [String:AnyObject]
            
            print(self.AppSettingPayload)
            
            //DYNAMIC THEME COLOR
            Constants.THEME_PRIMARY_COLOR = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "themeprimarycolor")
            
            Constants.THEME_PRIMARY_DARK_COLOR = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "themeprimarydarkcolor")
            
            Constants.TEXT_PRIMARY_COLOR = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "textprimarycolor")
            
            Constants.TEXT_LIGHT_COLOR = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "textlightcolor")
            
            Constants.BUTTON_TEXT_COLOR = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "buttontextcolor")
            
            // Activa / NonActive
            self.is_slider_active = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "is_slider_active")
            
            self.is_arrival_active = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "is_arrival_active")
            
            self.is_seller_active = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "is_seller_active")
            
            self.is_mostwanted_active = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "is_mostwanted_active")
            
            self.is_cart_active = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "is_cart_active")
            
            self.is_order_active = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "is_order_active")
            
            self.is_amazon_active = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "is_amazon_active")
            
            self.is_wishlist_active = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "is_wishlist_active")
            
            self.allow_otp_verification = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "allow_otp_verification")
            
            
            self.check_pincode_availibility = PIService.object_forKeyWithValidationForClass_String(dict: self.AppSettingPayload, key: "check_pincode_availibility")
            
            
            // Logo Image Array
            if (self.AppSettingPayload["logoimage"] as? [Any]) != nil
            {
              var logo:[Any] = self.AppSettingPayload["logoimage"] as! [Any]
              self.logoImage = logo[0] as! String
              print(self.logoImage)
              
            }
            
            // Set Currency Symbol And get From Constant File NSOBJECT CLASS
            // Currency Dictionary
            if (self.AppSettingPayload["currencydata"]) as? [String:Any] != nil
            {
              let currency:[String:Any] = self.AppSettingPayload["currencydata"] as! [String:Any]
              
              print(currency)
              UserDefaultFunction.setCustomDictionary(dict: currency , key: "currencydata")
            }
            
            // Google Image
            if (self.AppSettingPayload["googleimage"]) as? [Any] != nil
            {
              var google:[Any] = self.AppSettingPayload["googleimage"] as! [Any]
              self.googleImage = google[0] as! String
              print(self.googleImage)
            }
            
            // Facebook Image
            if (self.AppSettingPayload["facebookimage"]) as? [Any] != nil
            {
              var facebook:[Any] = self.AppSettingPayload["facebookimage"] as! [Any]
              self.facebookImage = facebook[0] as! String
              print(self.facebookImage)
            }
            
            // Instagram Image
            if(self.AppSettingPayload["instagramimage"]) as? [Any] != nil
            {
              var instagram:[Any] = self.AppSettingPayload["instagramimage"] as! [Any]
              self.instagramImage = instagram[0] as! String
              print(self.instagramImage)
            }
            
            // LinkedIn Image
            if(self.AppSettingPayload["linkedinimage"]) as? [Any] != nil
            {
              var linkedin:[Any] = self.AppSettingPayload["linkedinimage"] as! [Any]
              self.linkedInImage = linkedin[0] as! String
              print(self.linkedInImage)
            }
            
            // Pintrest Image
            if(self.AppSettingPayload["pinterestimage"]) as? [Any] != nil
            {
              var pintrest:[Any] = self.AppSettingPayload["pinterestimage"] as! [Any]
              self.pintrestImage = pintrest[0] as! String
              print(self.pintrestImage)
            }
            
            
            // Dashboard Application Image
            if(self.AppSettingPayload["applicationimage"]) as? [Any] != nil
            {
              var appImage:[Any] = self.AppSettingPayload["applicationimage"] as! [Any]
              self.applicationImage = appImage[0] as! String
              print(self.applicationImage)
            }
            
            self.openHomePage()
            
          }
          NVActivityIndicatorView.DEFAULT_COLOR = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!
        }
      })
    }
    else
    {
      //PIAlertUtils.displayNoInternetMessage()
      let alertController: UIAlertController = UIAlertController(title: "Alert!", message: "Your Internet connectivity not available. Please check your internet connection and try again!", preferredStyle: .alert)
      
      let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        exit(0)
      }
      
      alertController.addAction(okAction)
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
        self.window?.currentViewController()!.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
  func GetCartCountData() {
    if IS_INTERNET_AVAILABLE() {
      let strurl:String = BazarBit.CountCartTotalService()
      let url:URL = URL(string: strurl)!
      
      var headers:HTTPHeaders = [:]
      headers[BazarBit.ContentType] = BazarBit.applicationJson
      headers["app-id"] = BazarBit.appId
      headers["app-secret"] = BazarBit.appSecret
      
      var parameter:Parameters = [:]
      let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
      
      if UserId != nil {
        parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
      }
      else
      {
        let strSessionId:String = BazarBit.getSessionId()
        parameter["session_id"] = strSessionId
        parameter["user_id"] = ""
      }
      let a = UIViewController()
      
      PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: a, isLoaderShow: false, completion: {(swiftyJson, responseDict) in
        
        self.dicResponceCartTotalList = responseDict
        
        let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCartTotalList, key: "status")
        
        if status == "ok"
        {
          if self.dicResponceCartTotalList["payload"] != nil
          {
            let payload:[String:AnyObject] = self.dicResponceCartTotalList["payload"] as! [String:AnyObject]
            print(payload)
            
            let strCartTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "cart_items")
            UserDefaults.standard.set(strCartTotal, forKey: BazarBit.CARTTOTALCOUNT)
          }
        }
      })
    }
    else
    {
      let alertController: UIAlertController = UIAlertController(title: "Alert!", message: "Your Internet connectivity not available. Please check your internet connection and try again!", preferredStyle: .alert)
      
      let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        exit(0)
      }
      
      alertController.addAction(okAction)
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
        self.window?.currentViewController()!.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
  func ServiceCallForPackageData() {
    if IS_INTERNET_AVAILABLE() {
      let strurl:String = BazarBit.PackageService()
      let url:URL = URL(string: strurl)!
      
      var headers:HTTPHeaders = [:]
      headers[BazarBit.ContentType] = BazarBit.applicationJson
      headers["app-id"] = BazarBit.appId
      headers["app-secret"] = BazarBit.appSecret
      
      let parameter:Parameters = [:]
      let a = UIViewController()
      PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: a, isLoaderShow: false, completion: {(swiftyJson, responseDict) in
        
        print(responseDict)
        
        let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
        
        if status == "ok"
        {
          if responseDict["payload"] != nil
          {
            var payload:[AnyObject] = responseDict["payload"] as! [AnyObject]
            print(payload)
            
            if payload.count > 0 {
              let preda = NSPredicate(format: "slug CONTAINS[c] 'social-media-logins'")
              payload = payload.filter { preda.evaluate(with: $0) }
              
              let dictSocialLogins : [String : AnyObject] = payload[0] as! [String : AnyObject]
              
              print(dictSocialLogins)
              
              var strLoginType = PIService.object_forKeyWithValidationForClass_String(dict: dictSocialLogins, key: "feature_textval")
              
              strLoginType = strLoginType.replacingOccurrences(of: " ", with: "")
              
              self.arySocialLoginButton = strLoginType.components(separatedBy: ",")
              print(self.arySocialLoginButton)
            }
          }
        }
      })
    }
    else
    {
      
      //PIAlertUtils.displayNoInternetMessage()
      let alertController: UIAlertController = UIAlertController(title: "Alert!", message: "Your Internet connectivity not available. Please check your internet connection and try again!", preferredStyle: .alert)
      
      let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        exit(0)
      }
      
      alertController.addAction(okAction)
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
        self.window?.currentViewController()!.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
  func CheckInternet() {
    if IS_INTERNET_AVAILABLE() {
      
    }
    else{
      
      let alertController: UIAlertController = UIAlertController(title: "Alert!", message: "Your Internet connectivity not available. Please check your internet connection and try again!", preferredStyle: .alert)
      
      let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        exit(0)
      }
      
      alertController.addAction(okAction)
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
        self.window?.currentViewController()!.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
  func openStartUpPage()  {
    let storyBoard = UIStoryboard(name: "StartUp", bundle: nil)
    let viewStartUp: UIViewController = storyBoard.instantiateInitialViewController()!
    
    self.window?.rootViewController = viewStartUp
    self.window?.makeKeyAndVisible()
    
  }
  
  func setWishListCount(strCount : String) {
    let wishlistBarItem = UITabBarItem(title: "Wishlist", image: UIImage(named: "wishlist"), selectedImage: UIImage(named: "wishlist"))
    
    if strCount != "0" {
        wishlistBarItem.badgeValue = strCount
        if #available(iOS 10.0, *) {
            wishlistBarItem.badgeColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        } else {
            // Fallback on earlier versions
        }
    }
    
    if self.is_wishlist_active != "0" {
      self.wishListVC.tabBarItem = wishlistBarItem
    }
  }
}
